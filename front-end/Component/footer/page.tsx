import React from 'react';

const Footer = () => {
    return (
        <footer className="footer footer-center text-base-content mt-4">
            <aside>
                <p>Copyright © 2023 - All right reserved by Mhoomind - Mek - DDay</p>
            </aside>
        </footer>
    );
};

export default Footer;
