'use client';
import Link from 'next/link';
import Image from 'next/image';
import logo from '../../assets/Logo.png';
import { Menu } from '@headlessui/react'
import styles from '../../styles/Navbar.module.css';
import jwt_decode from 'jwt-decode';
import 'tailwindcss/tailwind.css';
import Head from 'next/head';
import { useDebugValue, useEffect, useState } from 'react';
import axios from 'axios';
import { profile } from 'console';

import { ChevronDownIcon } from "@heroicons/react/24/solid"; // Assuming you have ChevronDownIcon imported for dropdown indicator
import router from 'next/router';

type Props = {};

type Admin = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    age: number;
    gender: string;
    role: string;

};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
    profile_image_url: string;
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
    sub: string;
    exp: number;
    a: Role[];
};


// Navbar component
const CustomNavbar = () => {
    const [userProfile, setUserProfile] = useState("");
    const [customer, setCustomer] = useState<Customer | null>(null);
    const [admin, setAdmin] = useState<Admin | null>(null);
    const [nannies, setNanny] = useState<Nanny | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [isMenuOpen, setIsMenuOpen] = useState(true);
    const [decodedToken, setDecodedToken] = useState<DecodedToken | null>(null); // Initialize decodedToken state
    const [notificationCount, setNotificationCount] = useState(0);
    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    useEffect(() => {
        const fetchNotifications = async () => {
            const token = localStorage.getItem('jwt');
            if (token) {
                try {
                    const decodedToken: DecodedToken = jwt_decode(token);
                    setDecodedToken(decodedToken); // Set decodedToken state
                    const userId = decodedToken.sub;

                    let apiUrl = '';
                    if (decodedToken.a.includes('USER')) {

                        // apiUrl = `http://localhost:9000/api/bookingqueue/getbookingstestpaidCustomer/${userId}`;
                        //     apiUrl = `http://35.213.139.253:9000/api/bookingqueue/getbookingstestpaidCustomer/${userId}`;
                        // } else if (decodedToken.a.includes('NANNY')) {
                        //     // apiUrl = `http://localhost:9000/api/nannies/booking-queues/pending/${userId}`;
                        //     apiUrl = `http://35.213.139.253:9000/api/nannies/booking-queues/pending/${userId}`;

                        apiUrl = `http://localhost:9000/api/bookingqueue/getfindBookingsPaidAndCancleCustomer/${userId}`;
                        // apiUrl = `http://localhost:9000/api/bookingqueue/getbookingsbycustomer/${userId}`;
                    } else if (decodedToken.a.includes('NANNY')) {
                        apiUrl = `http://localhost:9000/api/nannies/booking-queues/pending/${userId}`;
                        // apiUrl = `http://localhost:9000/api/bookingqueue/getbookingsbycustomer/${userId}`;
                    }

                    const response = await axios.get(apiUrl);
                    if (response.data) {
                        setNotificationCount(response.data.length);
                    }
                    setLoading(false);
                } catch (error) {
                    setError("Error fetching notifications.");
                    setLoading(false);
                }
            }
        };

        fetchNotifications();
    }, []);



    useEffect(() => {
        const fetchUserProfile = async () => {
            const token = localStorage.getItem('jwt');
            console.log("Console from navbar")
            if (token) {
                try {
                    const decodedToken: any = jwt_decode(token);

                    let apiEndpoint;
                    let setUserFunction;

                    if (decodedToken.a.includes('USER')) {
                        apiEndpoint = `http://localhost:9000/api/customers/${decodedToken.sub}`;
                        // apiEndpoint = `http://35.213.139.253:9000/api/customers/${decodedToken.sub}`;
                        setUserFunction = setCustomer;

                    } else if (decodedToken.a.includes('NANNY')) {
                        apiEndpoint = `http://localhost:9000/api/nannies/getby/${decodedToken.sub}`;
                        // apiEndpoint = `http://35.213.139.253:9000/api/nannies/getby/${decodedToken.sub}`;
                        setUserFunction = setNanny;
                    } else if (decodedToken.a.includes('ADMIN')) {
                        apiEndpoint = `http://localhost:9000/api/admins/${decodedToken.sub}`;
                        // apiEndpoint = `http://35.213.139.253:9000/api/admins/${decodedToken.sub}`;
                        setUserFunction = setAdmin;
                    } else {
                        setError("Invalid user role");
                        setLoading(false);
                        return;
                    }

                    console.log("User ID:", decodedToken.sub);
                    const response = await axios.get(apiEndpoint);

                    if (response.data) {
                        setUserFunction(response.data);
                        console.log("User DATA:", response.data);
                        if (decodedToken.a.includes('USER')) {
                            setUserProfile(response.data.profile_image_url);
                        } else if (decodedToken.a.includes('NANNY')) {
                            setUserProfile(response.data.profile_image_url);
                        }
                    } else {
                        setError("Invalid user profile data");
                    }

                    setLoading(false);
                } catch (error) {
                    console.error('Error decoding token or fetching user profile:', error);
                    setLoading(false);
                }
            }
        };

        fetchUserProfile();
    }, []);

    return (
        <div className={styles.rootContainer}>
            <nav className={styles.navbar}>
                <div className={styles.logo}>
                    <Link href="/">
                        <a>
                            <Image
                                src={logo}
                                alt="logo-webapp"
                                className={`${styles.pics}`}
                                width={250}
                                height={180}
                            />
                        </a>
                    </Link>
                </div>
                <div className={styles.services}>
                    <Link href="/home">
                        <a className={styles.service}>Service</a>
                    </Link>
                    <Link href="/customerservice">
                        <a className={styles.service}>Help</a>
                    </Link>
                    {decodedToken && (
                        <Link href={decodedToken.a.includes('USER') ? "/user-notifications" : "/nanny-notifications"}>
                            <a className={styles.service}>
                                Notification {notificationCount > 0 && <span className="bg-red-500 text-white rounded-full px-2 py-1">{notificationCount}</span>}
                            </a>
                        </Link>
                    )}
                    {userProfile && (
                        <div className="relative">
                            <Menu as="div" className={`${styles.menu}`} onMouseEnter={() => setIsMenuOpen(true)} onMouseLeave={() => setIsMenuOpen(false)}>
                                <Menu.Button className="flex items-center">
                                    <Image
                                        src={"data: image/png;base64," + userProfile}
                                        alt='user-profile'
                                        width={40}
                                        height={40}
                                        className='rounded-xl cursor-pointer'
                                    />
                                    <ChevronDownIcon className="w-5 h-5 ml-1" />
                                </Menu.Button>
                                {isMenuOpen && (
                                    <Menu.Items
                                        className="absolute z-10 right-0 mt-2 w-48 bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                                        style={{ top: '3rem', right: '3rem' }}
                                        onMouseEnter={() => setIsMenuOpen(true)}
                                    >
                                {
                               (decodedToken?.a.includes('USER') || decodedToken?.a.includes('NANNY')) && (
                               <Menu.Item>
                              <Link href={decodedToken?.a.includes('USER') ? "/user-profile_information" : "/nanny-profile_information"}>
                               <a className='block px-4 py-2 text-gray-800 hover:bg-gray-200'>Profile</a>
                               </Link>
                              </Menu.Item>
                                )
                                }

                                        {
                                         decodedToken?.a.includes('USER') && (
                                         <Menu.Item>
                                         <Link href="/favoritenanny">
                                         <a className='block px-4 py-2 text-gray-800 hover:bg-gray-200'>Favorite Nanny</a>
                                         </Link>
                                         </Menu.Item>
                                            )
                                         }
                                        <Menu.Item>
                                            <Link href={decodedToken?.a.includes('USER') ? "/login-user" : decodedToken?.a.includes('NANNY') ? "/login-nanny" : decodedToken?.a.includes('ADMIN') ? "/login-admin" : "/login"}>
                                                <a className='block px-4 py-2 text-gray-800 hover:bg-gray-200'>Logout</a>
                                            </Link>
                                        </Menu.Item>
                                    </Menu.Items>
                                )}
                            </Menu>
                        </div>
                    )}
                </div>
            </nav>
        </div>
    );
};

export default CustomNavbar;
function async() {
    throw new Error('Function not implemented.');
}