import axios from 'axios';

// Create an Axios instance
const api = axios.create({
  // baseURL: 'http://localhost:9000',
  baseURL: 'http://localhost:9000',
  headers: {
    'Content-Type': 'application/json'
  }
});


type Customer = {
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
};

type ActivityProgram = {
  ProgramID: number;
  Normal_Period1: string;
  Normal_Period2: string;
  Normal_Period3: string;
  Normal_Period4: string;
  Normal_Period5: string;
  Overnight_Period1: string;
  Overnight_Period2: string;
  Overnight_Period3: string;
  Overnight_Period4: string;
  Overnight_Period5: string;
  customer_id: number;
}

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  profile_image_url: string;
  weightedScore: number;
};
/**
 * Set the Authorization token for the Axios instance.
 * @param token - JWT token
 */
export const setAuthToken = (token: string | null) => {
  if (token) {
    api.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  } else {
    delete api.defaults.headers.common['Authorization'];
  }
};

/**
 * Authenticate a user using their email and password.
 * @param email - User's email
 * @param password - User's password
 * @returns AxiosResponse
 */
export const login = async (email: string, password: string) => {
  return api.post('/auth/user-login', { email, password });
};

export const login_admin = async (email: string, password: string) => {
  return api.post('/auth/admin-login', { email, password });
};

export const login_nanny = async (email: string, password: string) => {
  return api.post('/auth/nanny-login', { email, password });
};

export const userResponse = async (userId: number) => {
  // Use template literals to embed the userId in the URL
  return api.get<Customer>(`/api/customers/${userId}`);
};

export const fetchActivityPrograms = async (userId: number) => {
  // Use template literals to embed the userId in the URL
  return api.get<ActivityProgram[]>(`/api/activityprogram/getbyusername/${userId}`);
};

export const updateActivityProgram = async (customerId: number, activityProgram: ActivityProgram) => {
  // Use template literals to embed the customerId in the URL
  return api.put(`/api/activityprogram/${customerId}`, activityProgram);
};



export const registerCustomer = async (formData:any) => {
  return api.post('/api/customers/register', formData);
};

export const registerNanny = async (formData:any) => {
  return api.post('/api/nannies/register', formData);
};

export const registerAdmin = async (formData:any) => {
  return api.post('/api/admins/register', formData);
};

export const fetchNannyByUsername = async (username: string) => {
  // Use template literals to embed the username in the URL
  return api.get<Nanny>(`/api/nannies/getbyusername/${username}`);
}

export const fetchNannyRankings = async (): Promise<Nanny[]> => {
  try {
    const response = await axios.get<{ nanny: Nanny; weightedScore: number }[]>(
      "/api/nannies/ranking"
    );
    return response.data.map((item) => item.nanny);
  } catch (error) {

    console.error('Failed to fetch nanny rankings:', error);
    throw error; 
  }
};

// You can add other API related functions here as needed.
