describe('Login Test', () => {
  it('successfully logs in users', () => {
    // Ignore uncaught exceptions
    Cypress.on('uncaught:exception', (err, runnable) => {
      // Returning false here prevents Cypress from
      // failing the test due to the uncaught exception
      return false;
    });

    // Visit the login page
    cy.visit('http://localhost:3000/login-user');

    // Fill in the login form
    cy.get('input[name="email"]').type('User01@hotmail.com',{ delay: 100 });
    cy.wait(2000);
    cy.get('input[name="password"]').type('123456789');
    cy.wait(2000);

    // Submit the form
    cy.get('form').submit();

    // Verify we're redirected to the home page
    cy.url().should('include', '/home');
    cy.wait(5000);
  });

  it('successfully logs in nanies', () => {
    // Ignore uncaught exceptions
    Cypress.on('uncaught:exception', (err, runnable) => {
      // Returning false here prevents Cypress from
      // failing the test due to the uncaught exception
      return false;
    });

    // Visit the login page
    cy.visit('http://localhost:3000/login-nanny');

    // Fill in the login form
    cy.get('input[name="email"]').type('Nanny01@hotmail.com',{ delay: 100 });
    cy.wait(2000);
    cy.get('input[name="password"]').type('123456789');
    cy.wait(2000);

    // Submit the form
    cy.get('form').submit();

    // Verify we're redirected to the home page
    cy.url().should('include', '/home');
    cy.wait(5000);
  });

  it('successfully logs in admins', () => {
    // Ignore uncaught exceptions
    Cypress.on('uncaught:exception', (err, runnable) => {
      // Returning false here prevents Cypress from
      // failing the test due to the uncaught exception
      return false;
    });

    // Visit the login page
    cy.visit('http://localhost:3000/login-admin');

    // Fill in the login form
    cy.get('input[name="email"]').type('Admin01@hotmail.com',{ delay: 100 });
    cy.wait(2000);
    cy.get('input[name="password"]').type('123456789');
    cy.wait(2000);

    // Submit the form
    cy.get('form').submit();

    // Verify we're redirected to the home page
    cy.url().should('include', '/home');
    cy.wait(5000);
  });
});
