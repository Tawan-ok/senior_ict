

describe('Login Test', () => {
  beforeEach(() => {

    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });

    cy.visit('http://localhost:3000/login-nanny', {
      onBeforeLoad(win) {
        cy.stub(win, 'alert').as('alertStub');
      },
    });
  });

  
  it('fails to log in with incorrect email and password', () => {
    cy.wait(2000);
    cy.get('input[name="email"]').type('wrong.email@example.com',{ delay: 100 });
    cy.get('input[name="password"]').type('wrongpassword');
    cy.get('form').submit();

    cy.get('@alertStub').should('have.been.calledWith', "Your account or password is incorrect");


    cy.url().should('include', '/login-nanny');
  });

  it('fails to log in with incorrect password', () => {
    cy.wait(2000);
    cy.get('input[name="email"]').type('Nanny01@hotmail.com',{ delay: 100 });
    cy.get('input[name="password"]').type('wrongpassword');
    cy.get('form').submit();

    cy.get('@alertStub').should('have.been.calledWith', "Your account or password is incorrect");


    cy.url().should('include', '/login-nanny');
  });

  it('alerts "You\'re not a nanny" for check role nannies', () => {
 
    cy.wait(2000);
    cy.get('input[name="email"]').type('User01@hotmail.com',{ delay: 100 });
    cy.get('input[name="password"]').type('123456789');
    cy.get('form').submit();


    cy.get('@alertStub').should('have.been.calledWith', "You're not a nanny");

    cy.url().should('include', '/login-nanny');
  });

  it('alerts "You\'re not a nanny" for check role nannies', () => {
    // Simulate user entering credentials for a non-existent account
    cy.get('input[name="email"]').type('Admin01@hotmail.com',{ delay: 100 });
    cy.get('input[name="password"]').type('123456789');
    cy.get('form').submit();
    cy.wait(2000);
 
    cy.get('@alertStub').should('have.been.calledWith', "You're not a nanny");


    cy.url().should('include', '/login-nanny');
  })
});
