'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
import { Console } from 'console';
type Props = {};



type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
};

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
  profile_image_url: string;
};

type BookingQueue = {
  id: number,
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  hours: number
};

type BookingHistory = {
  id: number,
  booking_id: number,
  status: string,
  time_session: number
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};

// ของเก่า 
export default function CustomersPage({ }: Props) {

  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [bookinghistory, setbookinghistory] = useState<BookingHistory[]>([]);
  const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
  const [nanny, setnanny] = useState<Nanny | null>(null);
  const [customer, setCustomer] = useState<Customer | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const router = useRouter(); // Initialize the router
  const handleExit = () => {
    localStorage.removeItem('jwt'); // Remove the JWT token
    router.push('/login-user'); // Redirect to /login
  };



  useEffect(() => {
    const token = localStorage.getItem('jwt');
    // Decode the JWT to extract user ID
    if (token) {
      const decodedToken: any = jwt_decode(token);

      // Extract user ID from the "sub" key in JWT
      if (!decodedToken.a.includes('USER')) {
        setError('Access denied. You do not have the required permissions.');
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }

      // Extract user ID from the "sub" key in JWT
      const userId = decodedToken.sub;

      if (!userId) {
        setError("User ID not found in token.");
        setLoading(false);
        return;
      }

      console.log("User ID:", userId);
      const fetchData = async () => {
        try {
          // const response = await axios.get(`http://localhost:9000/api/admins/${userId}`);
          const response1 = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
          // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/customers/${userId}`);
          // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/customers/${userId}`);
          // const response2 = await axios.get<BookingHistory>(`http://localhost:9000/api/nannies/booking-dataBH/${userId}`);
          // const response3 = await axios.get<BookingQueue>(`http://localhost:9000/api/nannies/booking-dataBQ/${userId}`);
          // const response2 = await axios.get(`http://localhost:9000/api/customers/bookingcs-dataBH/${userId}`);
          const response2 = await axios.get(`http://localhost:9000/api/customers/bookingcs-dataBH/${userId}`);
          // const response3 = await axios.get(`http://localhost:9000/api/customers/bookingcs-dataBQ/${userId}`);
          const response3 = await axios.get(`http://localhost:9000/api/customers/bookingcs-dataBQ/${userId}`);
          // setAdmin(response.data);
          setCustomer(response1.data);
          setbookinghistory(response2.data);
          setbookingqueue(response3.data);

          // const response = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${nanny?.id}`);
          // setnanny(response.data);
          const uniqueNannyIds = Array.from(new Set(response3.data.map((item: { nanny_id: any; }) => item.nanny_id)));

          // Fetch nanny information for each nanny ID
          const nannyPromises = uniqueNannyIds.map(nannyId =>
            axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${nannyId}`)
          );

          // Resolve all promises
          const nanniesData = await Promise.all(nannyPromises);

          // Set nanny data
          setNannies(nanniesData.map(nanny => nanny.data));
          
          
          console.log(response1.data);
          console.log(response2.data);
          console.log(response3.data);

        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
        }
        setLoading(false);
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-user');
    }
  }, []);

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  
  if (!bookinghistory) return <div>Bookinghistory Not found {error}</div>;
  if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;


  return (
    <div style={{ fontFamily: 'Montserrat' }} className="text-center">
      <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white">
        User History
      </div>
      {bookinghistory.length > 0 && bookingqueue.length > 0 ? (
        bookinghistory.map((history) => {
          const queueItem = bookingqueue.find(queue => queue.id === history.booking_id);
          const nannyInfo = nannies.find(nanny => nanny.id === queueItem?.nanny_id);
          return (
            <div key={history.id} className="border-2 border-pink-500 bg-white rounded-lg p-4 mb-4 mt-4 m-3">
              <div className="flex justify-between">
                <p className="text-gray-800 font-semibold mb-2">Booking ID: {history.booking_id}</p>
                <p className="text-gray-700 font-bold text-xl bg-pink-200 p-2 rounded">Status: {history.status}</p>
              </div>
              <p className="text-gray-700">Time Session: {history.time_session}</p>
              {queueItem && nannyInfo ? (
                <div>
                  <p className="text-gray-700">Nanny Name: {nannyInfo.first_name} {nannyInfo.last_name}</p>
                  <p className="text-gray-700">Nanny Contact Number: {nannyInfo.contact_number}</p>
                  <p className="text-gray-700">Total Amount: {queueItem.total_amount.toLocaleString()} Baht</p>
                  <p className="text-gray-700">Hours: {queueItem.hours}</p>
                  <div className="flex justify-between font-bold bg-pink-200 text-black p-3">
                    <p>Start Date: {new Date(queueItem.start_date).toLocaleDateString()}</p>
                    <p>End Date: {new Date(queueItem.end_date).toLocaleDateString()}</p>
                  </div>
                </div>
              ) : (
                <p className="text-red-500">Booking Queue data not found for this ID</p>
              )}
            </div>
          );
        })
      ) : (
        <p>No Booking History data found</p>
      )}
    </div>
  );

}


