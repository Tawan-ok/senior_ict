'use client';

import axios from 'axios';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import jwt_decode from 'jwt-decode';
import nanny1 from '../../../../assets/hanni.png'
import Image from 'next/image';
type Props = {};

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
  descriptions: string;
};

type BookingQueue = {
  id: number,
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number
  hours_cost: number,
  days_cost: number,
  months_cost: number,
  child_age: number,
  basic_totalrates: number,
  type_hiring: string,
  bk_score: number;
};


type BookingQueuetest = {
  id: number;
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number
};

export default function Page({ params }: { params: { username: string } }) {
  const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [customer, setCustomer] = useState<Customer | null>(null);
  const [booking, setBooking] = useState<BookingQueue | null>(null); // Initialize as null
  const [showModal, setShowModal] = useState<boolean>(false); // State for modal
  const [newTotalAmount, setNewTotalAmount] = useState<number | null>(null); // State for updated amount
  const [minAmount, setMinAmount] = useState<number | null>(null);
  const [oldtotal, setOldvalue] = useState<number | null>(null);
  const [maxAmount, setMaxAmount] = useState<number | null>(null);
  const router = useRouter();
  const handleExit = () => {
    localStorage.removeItem('jwt');
    router.push('/login-admin');
  };


  const fetchData = async () => {
    try {
      const token = localStorage.getItem('jwt');
      if (token) {
        const decodedToken: any = jwt_decode(token);
        const userId: number = decodedToken.sub;

        if (!decodedToken.a.includes('USER')) {
          setError('Access denied. You do not have the required permissions.');
          alert('Access denied. You do not have the required permissions.');
          setLoading(false);
          router.push('/home');
          return;
        }

        console.log("User ID:", userId);
        const response = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
        // const response = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
        const response1 = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);

        // const response1 = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);

        const response3 = await axios.get<BookingQueue>(`http://localhost:9000/api/bookingqueue/getbookingstatuspending/${userId}/${response.data.id}`);

        setBooking(response3.data);

        setNanny(response.data);
        setCustomer(response1.data);
        setLoading(false);

        setOldvalue(response3.data.basic_totalrates);

        setMinAmount(response3.data.basic_totalrates * 0.7);
        setMaxAmount(response3.data.basic_totalrates * 1.3);



      } else {
        alert('You need to be logged in first.');
        router.push('/login-user');
      }
    } catch (err) {
      if (err instanceof Error) {
        setError(err.message);
      } else {
        setError('An error occurred.');
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, [params.username]);

  const startDate = booking?.start_date && new Date(booking.start_date);
  startDate && startDate.setHours(startDate.getHours() + 0);
  const formattedStartDate = startDate instanceof Date ? startDate.toLocaleDateString('en-GB', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) + ' ' + startDate.toLocaleTimeString('en-GB') : '';

  // Convert booking end date string to Date object, subtract 7 hours, and convert back to string
  const endDate = booking?.end_date && new Date(booking.end_date);
  endDate && endDate.setHours(endDate.getHours() + 0);
  const formattedEndDate = endDate instanceof Date ? endDate.toLocaleDateString('en-GB', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) + ' ' + endDate.toLocaleTimeString('en-GB') : '';

  const handleUpdateTotalAmount = () => {
    setShowModal(true);
  };

  // const handleConfirmNewAmount = async () => {
  //   if (newTotalAmount !== null && booking) {
  //     try {

  //       const response = await axios.put(`http://localhost:9000/api/bookingqueue/updateTotalAmountbk/${booking.id}`, newTotalAmount, {
  //         // const response = await axios.put(`http://35.213.139.253:9000/api/nannies/updateScore/${nanny.id}`, newScore, {
  //         params: {
  //           newTotalAmount: newTotalAmount,
  //         },
  //       });
  //       console.log('Nanny score updated successfully:', response.data);
  //       setShowModal(false);
  //       window.location.reload();
  //     } catch (error) {
  //       console.error('Error updating nanny score:', error);
  //     }
  //   }
  // };
  const handleConfirmNewAmount = async () => {
    // Ensure the input is not null and is a number
    if (newTotalAmount === null || isNaN(newTotalAmount)) {
      alert('Please enter a valid number for the total amount.');
      return;
    }

    // Calculate the acceptable range
    const currentTotalAmount = booking?.basic_totalrates || 0;
    const minAmount2 = currentTotalAmount * 0.7;
    const maxAmount2 = currentTotalAmount * 1.3;

    // Check if the input is within the acceptable range
    if (newTotalAmount < minAmount2 || newTotalAmount > maxAmount2) {
      alert(`The new total amount must be within the range of ${minAmount2.toFixed(2)} to ${maxAmount2.toFixed(2)}.`);
      return;
    }

    // If the input is valid and within range, proceed with the update
    if (booking && newTotalAmount !== null) {
      try {
        const response = await axios.put(
          `http://localhost:9000/api/bookingqueue/updateTotalAmountbk/${booking.id}`,
          newTotalAmount,
          {
            params: {
              newTotalAmount: newTotalAmount,
            },
          }
        );
        console.log('Total amount updated successfully:', response.data);
        setShowModal(false);
        window.location.reload();
      } catch (error) {
        console.error('Error updating total amount:', error);
        alert('Failed to update total amount. Please try again.');
      }
    }
  };



  // Handle modal cancel
  const handleCancelNewAmount = () => {
    setShowModal(false);
  };

  const handleHiringCheck = async () => {
    // Check if booking is available
    if (!booking) {
      alert("You choose the Basic rate not Offer");
      return;
    }

    // Check if total amount is zero
    if (booking.total_amount === 0.00) {
      // Alert the user

      try {
        const response = await axios.put(
          `http://localhost:9000/api/bookingqueue/updateTotalAmountbk/${booking.id}`,
          newTotalAmount,
          {
            params: {
              newTotalAmount: booking.basic_totalrates,
            },
          }
        );
        console.log('Total amount updated successfully:', response.data);


      } catch (error) {
        console.error('Error updating total amount:', error);
        alert('Failed to update total amount. Please try again.');
      }

      console.log(booking.total_amount)

    }

    
  };

  const renderModal = () => {
    if (!showModal) return null;

    return (
      <div style={{ fontFamily: 'Montserrat', }} className="fixed inset-0 bg-gray-900 bg-opacity-75 flex items-center justify-center z-50">
        <div className="bg-white p-8 rounded-lg shadow-lg">
          <h3 className="text-2xl font-bold mb-4">Update Total Amount</h3>
          <p>The Old total amount of this work from rate nanny</p>
          {/* <p>{nanny?.hours_cost}</p>
          <p>{nanny?.days_cost}</p>
          <p>{nanny?.months_cost}</p>
          <p>{booking?.total_amount}</p>
          <p>{minAmount}</p> */}
          <p>{booking?.basic_totalrates} Basic Rate</p>
          {/* <p>{maxAmount}</p> */}
          <p className="mb-2">Acceptable range: {minAmount?.toFixed(2)} to {maxAmount?.toFixed(2)} baht</p>
          <input
            type="number"
            placeholder="Enter new total amount"
            className="w-full mb-4 p-2 border border-gray-300 rounded-lg"
            value={newTotalAmount || ''}
            onChange={(e) => setNewTotalAmount(Number(e.target.value))}
          />
          <div className="flex justify-between">
            <button
              className="bg-pink-500 text-white px-4 py-2 rounded-lg"
              onClick={handleConfirmNewAmount}
            >
              Confirm
            </button>
            <button
              className="bg-gray-300 text-black px-4 py-2 rounded-lg"
              onClick={handleCancelNewAmount}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    );
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!customer) return <div>Customer not found.</div>;
  if (!nanny) return <div>Nanny not found.</div>;

  return (
    <div>
      {renderModal()}
      <div className="block justify-center">
        <div className="text-center text-5xl md:text-5xl lg:text-5xl xl:text-5xl 2xl:text-5xl font-bold">
          <span style={{ fontFamily: 'Montserrat', }} className="text-white">Hiring Nanny</span>
        </div>
        <div className="mt-6 bg-white p-5 rounded-2xl shadow-lg w-full md:max-w-3xl xl:max-w-2xl mx-auto border-3 border-solid border-pink-300">

          <div className="flex flex-col md:flex-row items-center md:items-start">
            <div className="mb-4 sm:mb-0 sm:mr-8 flex items-center justify-center rounded-full overflow-hidden h-36 w-36 border-4 border-pink-300">
              <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
            </div>

            <div className="mb-4 md:mb-0">
              <div style={{ fontFamily: 'Montserrat' }}>
                <p className="bg-pink-500 inline-block mb-2 p-2 text-white font-medium">
                  {nanny.type_work === 'F' ? 'Full-Time' : nanny.type_work === 'P' ? 'Part-Time' : nanny.type_work === 'A' ? 'Full-Time & Part-Time' : nanny.type_work}
                </p>
                <p className='mt-2'><strong>Name:</strong> {nanny.first_name} {nanny.last_name}</p>
                <p><strong>Email:</strong> {nanny.email}</p>
                <p className='mb-4'><strong>Contact Number:</strong> {nanny.contact_number}</p>
                <p className='mb-4'><strong>Bio:</strong> {nanny.descriptions}</p>

                <div className="bg-gray-100 rounded-lg p-4 border-2 border-solid border-pink-300">
                  <p className="mb-2"><strong>Detail of Hiring</strong></p>
                  <p><strong>Location:</strong> {booking?.locationhiring}</p>
                  <p><strong>Start Date:</strong> {formattedStartDate}</p>
                  <p><strong>End Date:</strong> {formattedEndDate}</p>
                  <p><strong>Total Hours:</strong> {booking?.hours}</p>
                </div>
              </div>
            </div>

            <div style={{ fontFamily: 'Montserrat' }}>
              <p><strong>Role Level:</strong> {nanny.role_level}</p>
              <p><strong>Total: </strong>
                {booking?.total_amount === 0 ? booking?.basic_totalrates : booking?.total_amount} baht
              </p>
              <button
                className="bg-pink-500 border-2 border-pink-200 text-white px-4 py-2 rounded-lg mt-2"
                onClick={handleUpdateTotalAmount}
              >
                Offered Rate
              </button>
            </div>
          </div>

          {/* <div className="flex justify-center mt-8" key={nanny.username}>
            <Link href={`/payment/${nanny.username}`}>
              <button className="bg-pink-400 shadow-lg text-white px-4 py-2 rounded-lg hover:bg-blue-700 mt-5">
                <div style={{ fontFamily: 'Montserrat', }}>
                  Hiring
                </div>
              </button>
            </Link>
          </div> */}
          <div className="flex justify-center mt-8" key={nanny.username}>
          <Link href={`/payment/${nanny.username}`}>
            <button
              className="bg-pink-400 shadow-lg text-white px-4 py-2 rounded-lg hover:bg-blue-700 mt-5"
              onClick={handleHiringCheck}
            >
              <div style={{ fontFamily: 'Montserrat', }}>
                Hiring
              </div>
            </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}


function fetchData() {
  throw new Error('Function not implemented.');
}

