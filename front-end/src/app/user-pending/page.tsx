'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
type Props = {};



type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    hours: number
};

type BookingHistory = {
    id: number,
    booking_id: number,
    status: string,
    time_session: number
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
    sub: string;
    exp: number;
    a: Role[];
};

// ของเก่า 
export default function CustomersPage({ }: Props) {
    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [bookinghistory, setbookinghistory] = useState<BookingHistory[]>([]);
    const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
    const [nanny, setnanny] = useState<Nanny | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const router = useRouter(); // Initialize the router

    const handleExit = () => {
        localStorage.removeItem('jwt'); // Remove the JWT token
        router.push('/login-user'); // Redirect to /login
    };



    useEffect(() => {
        const token = localStorage.getItem('jwt');
        // Decode the JWT to extract user ID
        if (token) {
            const decodedToken: any = jwt_decode(token);
            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }
            // Extract user ID from the "sub" key in JWT
            const userId: number = decodedToken.sub;

            if (!userId) {
                setError("User ID not found in token.");
                setLoading(false);
                return;
            }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    const response3 = await axios.get(`http://localhost:9000/api/bookingqueue/getbookingstatuspendingcustomer/${userId}`);
                    // const response3 = await axios.get(`http://35.213.139.253:9000/api/customers/bookings/byCustomerId/${userId}`);
                    setbookingqueue(response3.data);
                    console.log(response3.data);

                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
    }, []);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;
    if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;
    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                <span className="text-white">User Pending</span>
            </div>
            <div className="bg-white border border-gray-200 p-4 rounded-lg shadow-md m-4">
                <h3 className="text-2xl font-semibold mb-4">Booking Queue Pending</h3>
                {bookingqueue && bookingqueue.length > 0 ? (
                    bookingqueue.map((queue) => (
                        <div key={queue.id} className="border border-gray-300 p-3 mb-4 rounded-md">
                            <p className="font-semibold">ID:</p>
                            <p>{queue.id}</p>
                            <p className="font-semibold">Customer ID:</p>
                            <p>{queue.customer_id}</p>
                            <p className="font-semibold">Nanny ID:</p>
                            <p>{queue.nanny_id}</p>
                            <p className="font-semibold">Start Date:</p>
                            <p>{queue.start_date.toString()}</p>
                            <p className="font-semibold">End Date:</p>
                            <p>{queue.end_date.toString()}</p>
                            <p className="font-semibold">Total Amount:</p>
                            <p>{queue.total_amount}</p>
                            <p className="font-semibold">Status Payment:</p>
                            <p>{queue.status_payment}</p>
                            <p className="font-semibold">Hours:</p>
                            <p>{queue.hours}</p>
                        </div>
                    ))
                ) : (
                    <p className="text-gray-500">Booking Queue data not found</p>
                )}
            </div>

        </div>
    );

}

