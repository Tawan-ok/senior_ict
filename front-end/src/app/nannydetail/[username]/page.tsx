'use client';

import axios from 'axios';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import jwt_decode from 'jwt-decode';
import nanny1 from '../../../../assets/hanni.png'
import Image from 'next/image';
import woman from '../../../../assets/woman.png'
type Props = {};

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
  descriptions: string;
};

type BookingQueue = {
  id: number;
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number
  hours_cost: number;
  days_cost: number;
  months_cost: number;
  child_age: number;
  bk_score: number;
  type_hiring: string;
};

type BookingHistory = {
  booking_id: number,
  status: string,
  time_session: number,
  nanny_id: number
};

type Complains = {
  booking_id: number;
  customer_id: number;
  nanny_id: number;
  complain_field_1: string,
  complain_field_2: string,
  complain_field_3: string,
  complain_field_4: string,
  complain_field_5: string,
  complain_text: string,
  complain_count: number;
};

export default function Page({ params }: { params: { username: string } }) {
  const [nanny, setNanny] = useState<Nanny | null>(null);
  const [complain, setComplain] = useState<Complains[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [customer, setCustomer] = useState<Customer | null>(null);
  // const [newScore, setNewScore] = useState<number | null>(null);
  const [bq, setBq] = useState<BookingQueue[]>([]);
  const [bh, setBh] = useState<BookingHistory[]>([]);
  const router = useRouter();

  const handleExit = () => {
    localStorage.removeItem('jwt');
    router.push('/login-admin');
  };

  const handleInterview = () => {
    router.push('/interview');
  }

  const fetchData = async () => {
    try {
      const token: any = localStorage.getItem('jwt');
      if (token) {
        const decodedToken: any = jwt_decode(token);
        const userId: number = decodedToken.sub;

        if (!decodedToken.a.includes("USER")) {
          setError("Access denied. You do not have the required permissions.");
          alert('Access denied. You do not have the required permissions.');
          setLoading(false);
          router.push('/home');
          return;
        }

        if (!userId) {
          setError("User ID not found in token");
          return;
        }

        console.log("User ID:", userId);
        const response = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
        // const response = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
        const response1 = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
        // const response1 = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
        const response3 = await axios.get(`http://localhost:9000/api/nannies/booking-dataBHComplete/${response.data.id}`);
        // const response3 = await axios.get(`http://35.213.139.253:9000/api/nannies/booking-dataBHComplete/${response.data.id}`);
        const response4 = await axios.get(`http://localhost:9000/api/nannies/booking-dataBQSuccess/${response.data.id}`);
        // const response4 = await axios.get(`http://35.213.139.253:9000/api/nannies/booking-dataBQSuccess/${response.data.id}`);
        const response5 = await axios.get(`http://localhost:9000/api/complains/getcomplainsbyNannyId/${response.data.id}`);
        setNanny(response.data);
        setCustomer(response1.data);
        setLoading(false);
        setBq(response4.data);
        setBh(response3.data);
        setComplain(response5.data);
      } else {
        alert('You need to be logged in first.');
        router.push('/login-user');
      }
    } catch (err) {
      if (err instanceof Error) {
        setError(err.message);
      } else {
        setError('An error occurred.');
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, [params.username]);

  // const handleScoreChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const newScoreValue = parseFloat(e.target.value);
  //   setNewScore(newScoreValue);
  // };

  // const handleUpdateScore = async () => {
  //   if (newScore === null) {
  //     console.error('New score is missing or invalid.');
  //     return;
  //   }
  //   if (!nanny) return <div>Nanny not found.</div>;
  //   try {

  //     const response = await axios.put(`http://localhost:9000/api/nannies/updateScore/${nanny.id}`, newScore, {
  //       // const response = await axios.put(`http://35.213.139.253:9000/api/nannies/updateScore/${nanny.id}`, newScore, {
  //       params: {
  //         newScore: newScore,
  //       },
  //     });
  //     console.log('Nanny score updated successfully:', response.data);

  //   } catch (error) {
  //     console.error('Error updating nanny score:', error);
  //   }
  // };

  const handleUpdateStatus = async (username: string) => {
    try {
      // Send a PUT request to the API endpoint to update the status
      await axios.put(`http://localhost:9000/api/nannies/updateStatus/${username}`, {
        // await axios.put(`http://35.213.139.253:9000/api/nannies/updateStatus/${username}`, {
        status: 'Inactive',
      });

      // After a successful update, you can reload the data or perform other actions
      // For example, refetch the nannies list
      fetchData();
    } catch (err) {
      if (err instanceof Error) {
        console.error(err.message);
      } else {
        console.error('An error occurred while updating the status.');
      }
    }
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!customer) return <div>Customer not found.</div>;
  if (!nanny) return <div>Nanny not found.</div>;

  const bookingHistoryCount = bh.length;
  const bookingQueueCount = bq.length;

  const getTypeOfWork = (typeWork: any) => {
    switch (typeWork) {
      case 'F':
        return 'Full Time';
      case 'A':
        return 'All Time';
      case 'P':
        return 'Part Time';
      default:
        return 'Not Specified'; // Default case if none of the above matches
    }
  };


  return (
    <div className="mt-5 mb-5 block justify-center">
      <div className="text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
        <span style={{ fontFamily: 'Montserrat', }} className="text-white">Hiring Nanny</span>
      </div>

      <div style={{ fontFamily: 'Montserrat' }} className="mt-8 bg-white p-8 rounded-2xl shadow-lg mx-4 flex flex-col sm:flex-row items-center border-2 border-solid border-pink-200">
        <div className="flex flex-col sm:flex-row items-center flex-grow p-4">
          <div className="mb-4 sm:mb-0 sm:mr-4 flex items-center justify-center rounded-full overflow-hidden h-48 w-48 border-4 border-pink-300">
            <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
          </div>

          <div className="flex-grow p-2">
            <p className="bg-pink-500 inline-block mb-2 p-2 text-white font-medium">
              {nanny.type_work === 'F' ? 'Full-Time' : nanny.type_work === 'P' ? 'Part-Time' : nanny.type_work === 'A' ? 'Full-Time & Part-Time' : nanny.type_work}
            </p>
            <p><strong>Name:</strong> {`${nanny.first_name} ${nanny.last_name}`}</p>
            <p><strong>Age:</strong> {nanny.age}</p>
            <p><strong>Email:</strong> {nanny.email}</p>
            <p><strong>Contact Number:</strong> {nanny.contact_number}</p>
            <p className="mb-4"><strong>Role:</strong> {nanny.role_level}</p>
            <p className="mb-4">
              <strong>Skill: </strong>
              {`${nanny.skill_1}${nanny.skill_2 ? ', ' + nanny.skill_2 : ''}${nanny.skill_3 ? ', ' + nanny.skill_3 : ''}`}
            </p>
            <div className="bg-pink-200 inline-block p-4 rounded-lg border-2 border-solid border-pink-400">
              <p className="font-semibold">Bio:</p>
              <p>{nanny.descriptions}</p>
            </div>

          </div>
        </div>

        <div className="flex flex-col justify-start">
          <p><strong>Hours Cost:</strong> {nanny.hours_cost.toLocaleString()}</p>
          <p><strong>Day Cost:</strong> {nanny.days_cost.toLocaleString()}</p>
          <p><strong>Month Cost:</strong> {nanny.months_cost.toLocaleString()}</p>
          <Link href={`/appointment/${nanny.username}`}>
            <button className="mt-2 text-black px-8 py-2 rounded-lg bg-white hover:bg-blue-700 hover:text-white shadow-md mb-2 border-2 border-solid border-pink-200" style={{ width: '150px', fontFamily: 'Montserrat' }} onClick={handleInterview}>
              Interview
            </button>
          </Link>
          <Link rel="preload" href={`/nannydetaildate/${nanny.username}`}>
            <button className="mt-2 bg-pink-400 text-white px-8 py-2 rounded-lg hover:bg-blue-700 shadow-md border-2 border-solid border-pink-200" style={{ width: '150px', fontFamily: 'Montserrat' }}>
              Hiring
            </button>
          </Link>
        </div>
      </div>

      <div style={{ fontFamily: 'Montserrat' }} className="mt-4 bg-white rounded-lg shadow-md p-6 mx-4 border-2 border-solid border-pink-200">
        <h2 className="text-2xl font-bold mb-4">Review Nanny</h2>
        {bq.length > 0 ? (
          <div className="overflow-x-auto">
            <table className="min-w-full">
              <thead>
                <tr className="bg-gray-200">
                  <th className="px-4 py-2 text-left">Type of Hiring</th>
                  <th className="px-4 py-2 text-left">Child Age</th>
                  <th className="px-4 py-2 text-left">Nanny Score</th>
                  <th className="px-4 py-2 text-left">Has the nanny been arriving late to the service?</th>
                  <th className="px-4 py-2 text-left">Does the nanny exhibit harmful behavior?</th>
                  <th className="px-4 py-2 text-left">Has the nanny used rude language when speaking to your children?</th>
                  <th className="px-4 py-2 text-left">Are there any concerns about the nanny professionalism or boundaries?</th>
                  <th className="px-4 py-2 text-left">Does the nanny appear unkempt or dirty?</th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200">
                {bq.slice(-5).reverse().map((booking, index) => {
                  const correspondingComplain = complain.find(
                    (complainItem) => complainItem.booking_id === booking.id
                  );
                  return (
                    <tr key={booking.id}>
                      <td>
                        {booking.type_hiring === 'day' ? 'Days' : booking.type_hiring === 'hour' ? 'Hours' : booking.type_hiring === 'month' ? 'Months' : booking.type_hiring}
                      </td>
                      <td className="px-5 py-3 whitespace-nowrap">{`${Math.floor(booking.child_age)} y ${Math.round((booking.child_age % 1) * 12)} m`}</td>
                      <td>{booking.bk_score ? booking.bk_score : "-"}</td>
                      {complain[index] && Array.from({ length: 5 }, (_, fieldIndex) => (
                        <td key={fieldIndex} className="px-4 py-2">
                          {(complain[index] as any)[`complain_field_${fieldIndex + 1}`] === 'T' ? "Yes" : "No"}
                        </td>
                      ))}
                      {!complain[index] && Array.from({ length: 5 }, (_, fieldIndex) => (
                        <td key={fieldIndex} className="px-4 py-2">
                          -
                        </td>
                      ))}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        ) : (
          <div className="flex flex-col justify-center items-center">
            <p className="text-gray-500 mt-2">Currently, no data available</p>
          </div>
        )}
      </div>

      <div style={{ fontFamily: 'Montserrat', }} className="mt-4 bg-white rounded-lg shadow-md p-6 mx-4 border-2 border-solid border-pink-200">
        <h2 className="text-2xl font-bold mb-4">Complain Comment</h2>
        {bq.length > 0 ? (
          <div className="overflow-x-auto">
            <table className="min-w-full">
              <thead>
                <tr className="bg-gray-200">
                  <th className="px-4 py-2 text-left">Comment</th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200">
                {complain.map((c, complainIndex) => (
                  <tr key={complainIndex}>
                    <td className="px-4 py-2">{c.complain_text.trim() !== "" ? c.complain_text : "-"}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          <div className='flex flex-col justify-center items-center'>
            <p className="text-gray-500 mt-2">Currently, no complains available</p>
          </div>
        )}
      </div>



    </div>
  );
}

