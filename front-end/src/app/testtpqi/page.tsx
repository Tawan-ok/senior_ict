'use client'
import jwt_decode from 'jwt-decode';
import React, { ChangeEvent, useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/UploadImg.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';

type Props = {};

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
  profile_image_url: string;
};

type Admin = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  age: number;
  gender: string;
  role: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
};
type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};
// ของเก่า 
export default function CustomersPage({ }: Props) {
  const [admin, setAdmin] = useState<Admin | null>(null);
  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [selectedNanny, setSelectedNanny] = useState<Nanny | null>(null);
  const router = useRouter(); // Initialize the router

  const handleExit = () => {
      localStorage.removeItem('jwt'); // Remove the JWT token
      router.push('/login-admin'); // Redirect to /login
  };

  useEffect(() => {
    const token = localStorage.getItem('jwt');
    // Decode the JWT to extract user ID
    if (token) {
        const decodedToken: any = jwt_decode(token);
        if (!decodedToken.a.includes('ADMIN')) {
            setError('Access denied. You do not have the required permissions.');
            alert('Access denied. You do not have the required permissions.');
            setLoading(false);
            router.push('/home');
            return;
          }
        // Extract user ID from the "sub" key in JWT
        const userId: number = decodedToken.sub;

        if (!userId) {
            setError("User ID not found in token.");
            setLoading(false);
            return;
        }

        console.log("User ID:", userId);
        const fetchData = async () => {
            try {
                const response = await axios.get(`http://localhost:9000/api/admins/${userId}`);
                const response1 = await axios.get('http://localhost:9000/api/nannies');
                setAdmin(response.data);
                setNannies(response1.data);
            } catch (err) {
                if (err instanceof Error) {
                    setError(err.message);
                } else {
                    setError('An error occurred.');
                }
            }
            setLoading(false);
        };

        fetchData();
    } else {
        alert('You need to be logged in first.');
        router.push('/login-admin');
    }
}, []);


  const BacktoProfile = () => {
    router.push('/user-profile_information');
  };

  const runWebAutomation = async () => {
    try {
      await axios.get("http://localhost:9000/api/nannies/perform-web-automation", {
        params: {
          searchValue: "1200456987987", // Replace with a dynamic value if needed
        },
      });

      console.log("Web automation completed successfully");
    } catch (error) {
      console.error("Error during web automation:", error);
    }
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!nannies) return <div>Nannies Not found {error}</div>;

  return (
    <div>
      <div className='flex items-center justify-center'>
        <div className={`flex items-center justify-center ${styles.cardcontainer}`}>
          <div className={styles.card}>
            <div className='flex items-center justify-center'>
              <button className="mt-2 bg-blue-500 text-white px-4 py-2 rounded-md focus:outline-none focus:ring focus:border-blue-300"
                onClick={runWebAutomation}>Run Web Automation</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}



