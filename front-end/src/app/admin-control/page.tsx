'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
type Props = {};

type Admin = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    age: number;
    gender: string;
    role: string;

};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
};
type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
    sub: string;
    exp: number;
    a: Role[];
};
// ของเก่า 
export default function CustomersPage({ }: Props) {
    const [admin, setAdmin] = useState<Admin | null>(null);
    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [selectedNanny, setSelectedNanny] = useState<Nanny | null>(null);
    const router = useRouter(); // Initialize the router
    const [searchValue, setSearchValue] = useState<string | null>(null); // Step 1

    const handleExit = () => {
        localStorage.removeItem('jwt'); // Remove the JWT token
        router.push('/login-admin'); // Redirect to /login
    };

    const handleEditNanny = (nanny: Nanny) => {
        setSelectedNanny(nanny);
        setSearchValue(nanny.citizen_id);
    };

    const handleUpdateNanny = async (updatedNanny: Nanny) => {
        try {
            // Make an API request to update the nanny data
            await axios.put(`http://localhost:9000/api/nannies/updatebyAdmin/${updatedNanny.id}`, updatedNanny);
            // await axios.put(`http://35.213.139.253:9000/api/nannies/updatebyAdmin/${updatedNanny.id}`, updatedNanny);
            alert(updatedNanny.id);
            // Reload the nanny data
            const response = await axios.get('http://localhost:9000/api/nannies');
            // const response = await axios.get('http://35.213.139.253:9000/api/nannies');
            setNannies(response.data);
            // Clear the selected nanny
            setSelectedNanny(null);
        } catch (err) {
            if (err instanceof Error) {
                setError(err.message);
            } else {
                setError('An error occurred.');
            }
        }
    };

    useEffect(() => {
        const token = localStorage.getItem('jwt');
        // Decode the JWT to extract user ID
        if (token) {
            const decodedToken: any = jwt_decode(token);
            if (!decodedToken.a.includes('ADMIN')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }

            // Extract user ID from the "sub" key in JWT
            const userId: number = decodedToken.sub;

            if (!userId) {
                setError("User ID not found in token.");
                setLoading(false);
                return;
            }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    const response = await axios.get(`http://localhost:9000/api/admins/${userId}`);
                    // const response = await axios.get(`http://35.213.139.253:9000/api/admins/${userId}`);
                    const response1 = await axios.get('http://localhost:9000/api/nannies');
                    // const response1 = await axios.get('http://35.213.139.253:9000/api/nannies');
                    setAdmin(response.data);
                    setNannies(response1.data);
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-admin');
        }
    }, []);

    const runWebAutomation = async () => {
        try {
            if (!searchValue) {
                console.error("Search value is missing.");
                return;
            }

            await axios.get("http://localhost:9000/api/nannies/perform-web-automation", {
                params: {
                    searchValue: searchValue,
                },
            });

            console.log("Web automation completed successfully");
        } catch (error) {
            console.error("Error during web automation:", error);
        }
    };

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;
    if (!nannies) return <div>Nannies Not found {error}</div>;
    return (
        <div>
            <div className={styles.logoweb}>
                {/* Display admin information */}
            </div>
            <div className="mt-3 text-center text-3xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                <span style={{ fontFamily: 'Montserrat', }} className="text-white">Admin Set Up Nanny</span>
            </div>
            <div style={{ fontFamily: 'Montserrat', }} className="mt-4 grid grid-cols-1 md:grid-cols-3 gap-4">
                {nannies.map((nanny) => (
                    // nanny.status !== 'Inactive' && (
                    <div className="bg-white rounded-lg shadow-md overflow-hidden" key={nanny.username}>
                        <div className="flex justify-center items-center h-48">
                            <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                        </div>
                        <div className="bg-gray-100 p-4 rounded-lg">
                            {/* Display nanny information */}
                            {selectedNanny?.id === nanny.id ? (
                                // Show edit form
                                <div>
                                    {/* Edit form for nanny data */}
                                    <form onSubmit={() => handleUpdateNanny(selectedNanny)}>

                                        {/*  /* No password  กับ id */}
                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="username" className="block">Username: </label>
                                            <input type="text" value={selectedNanny?.username ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, username: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>
                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="email">Email: </label>
                                            <input type="text" value={selectedNanny?.email ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, email: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="district">District: </label>
                                            <input type="text" value={selectedNanny?.district ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, district: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="sub_district">Sub_District: </label>
                                            <input type="text" value={selectedNanny?.sub_district ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, sub_district: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>



                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="age" className="text-sm">Age:</label>
                                            <input
                                                type="number"
                                                id="age"
                                                value={(selectedNanny?.age ?? 0).toString()}
                                                onChange={(e) => setSelectedNanny({ ...selectedNanny, age: parseInt(e.target.value, 10) || 0 })}
                                                className="border border-gray-300 focus:ring-blue-500 focus:border-blue-500 block w-full p-2 rounded-md shadow-sm"
                                                placeholder="Enter age"
                                            />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="role_level" className="text-sm">Role Level:</label>
                                            <div className="relative">
                                                <select
                                                    id="role_level"
                                                    className="block appearance-none w-full bg-white border border-gray-300 text-gray-700 py-2 px-3 pr-8 rounded-md leading-tight focus:outline-none focus:border-blue-500 focus:ring"
                                                    value={(selectedNanny?.role_level?.toString() ?? '1')}
                                                    onChange={(e) => {
                                                        setSelectedNanny({ ...selectedNanny, role_level: e.target.value });
                                                    }}
                                                >
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                                <div className="pointer-evendts-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                        <path d="M10 12L4 6h12z" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>



                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="cost">Cost: </label>
                                            <input type="text" value={(selectedNanny?.cost ?? 0).toString()} onChange={(e) => setSelectedNanny({ ...selectedNanny, cost: parseFloat(e.target.value) || 0 })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="hours_cost">Hours_cost: </label>
                                            <input type="text" value={(selectedNanny?.hours_cost ?? 0).toString()} onChange={(e) => setSelectedNanny({ ...selectedNanny, hours_cost: parseFloat(e.target.value) || 0 })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="days_cost">Days_cost: </label>
                                            <input type="text" value={(selectedNanny?.days_cost ?? 0).toString()} onChange={(e) => setSelectedNanny({ ...selectedNanny, days_cost: parseFloat(e.target.value) || 0 })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="months_cost">Months_cost: </label>
                                            <input type="text" value={(selectedNanny?.months_cost ?? 0).toString()} onChange={(e) => setSelectedNanny({ ...selectedNanny, months_cost: parseFloat(e.target.value) || 0 })} className="border p-2 rounded-md w-full" />
                                        </div>
                                        {/* <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="skill_1">Skill_1: </label>
                                            <input type="text" value={selectedNanny?.skill_1 ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, skill_1: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>
                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="skill_2">Skill_2: </label>
                                            <input type="text" value={selectedNanny?.skill_2 ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, skill_2: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>
                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="skill_3">Skill_3: </label>
                                            <input type="text" value={selectedNanny?.skill_3 ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, skill_3: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div> */}

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="skill_1">Cooking Skill: </label>
                                            <select
                                                id="skill_1"
                                                value={selectedNanny?.skill_1 ?? ""}
                                                onChange={(e) => setSelectedNanny({ ...selectedNanny, skill_1: e.target.value })}
                                                className="border p-2 rounded-md w-full"
                                            >
                                                <option value="">Select Cooking Skill</option>
                                                <option value="Thai cuisine cooking">Thai cuisine cooking</option>
                                                <option value="Western cuisine cooking">Western cuisine cooking</option>
                                                <option value="Japanese cuisine cooking">Japanese cuisine cooking</option>
                                                <option value="Baby/toddler food preparation">Baby/toddler food preparation</option>
                                            </select>
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="skill_2">Speaking Skill: </label>
                                            <select
                                                id="skill_2"
                                                value={selectedNanny?.skill_2 ?? ""}
                                                onChange={(e) => setSelectedNanny({ ...selectedNanny, skill_2: e.target.value })}
                                                className="border p-2 rounded-md w-full"
                                            >
                                                <option value="">Select Speaking Skill</option>
                                                <option value="Native Thai speaker">Native Thai speaker</option>
                                                <option value="Fluent English speaker">Fluent English speaker</option>
                                                <option value="Mandarin/Cantonese Chinese speaker">Mandarin/Cantonese Chinese speaker</option>
                                                <option value="Fluent Japan speaker">Fluent Japan speaker</option>
                                                <option value="Fluent French Speaker">Fluent French Speaker</option>
                                            </select>
                                        </div>
                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="skill_3">General Skill: </label>
                                            <select
                                                id="skill_3"
                                                value={selectedNanny?.skill_3 ?? ""}
                                                onChange={(e) => setSelectedNanny({ ...selectedNanny, skill_3: e.target.value })}
                                                className="border p-2 rounded-md w-full"
                                            >
                                                <option value="">Select General Skill</option>
                                                <option value="Drawing Skill">Drawing Skill</option>
                                                <option value="Singing Skill">Singing Skill</option>
                                                <option value="Entertain Skill">Entertain Skill</option>
                                            </select>
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="role_level" className="text-sm">Role Level:</label>
                                            <div className="relative">
                                                <select
                                                    id="role_level"
                                                    className="block appearance-none w-full bg-white border border-gray-300 text-gray-700 py-2 px-3 pr-8 rounded-md leading-tight focus:outline-none focus:border-blue-500 focus:ring"
                                                    value={(selectedNanny?.role_level?.toString() ?? '1')}
                                                    onChange={(e) => {
                                                        setSelectedNanny({ ...selectedNanny, role_level: e.target.value });
                                                    }}
                                                >
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                        <path d="M10 12L4 6h12z" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="contact_number">Contact_Number: </label>
                                            <input type="text" value={selectedNanny?.contact_number ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, contact_number: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="first_name">First_Name: </label>
                                            <input type="text" value={selectedNanny?.first_name ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, first_name: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="last_name">Last_Name: </label>
                                            <input type="text" value={selectedNanny?.last_name ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, last_name: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="zip_code">Zip_Code: </label>
                                            <input type="text" value={selectedNanny?.zip_code ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, zip_code: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="province">Province: </label>
                                            <input type="text" value={selectedNanny?.province ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, province: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="street_number">Street_Number: </label>
                                            <input type="text" value={selectedNanny?.street_number ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, street_number: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="citizen_id">Citizen_Id: </label>
                                            <input type="text" value={selectedNanny?.citizen_id ?? ""} onChange={(e) => setSelectedNanny({ ...selectedNanny, citizen_id: e.target.value })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="score">Score: </label>
                                            <input type="text" value={(selectedNanny?.score ?? 0).toString()} onChange={(e) => setSelectedNanny({ ...selectedNanny, score: parseFloat(e.target.value) || 0 })} className="border p-2 rounded-md w-full" />
                                        </div>

                                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-4 gap-y-2">
                                            <label htmlFor="status" className="text-sm">Status: </label>
                                            <div className="relative">
                                                <select
                                                    id="status"
                                                    className="block appearance-none w-full bg-white border border-gray-300 text-gray-700 py-2 px-3 pr-8 rounded-md leading-tight focus:outline-none focus:border-blue-500 focus:ring"
                                                    value={selectedNanny?.status}
                                                    onChange={(e) => setSelectedNanny({ ...selectedNanny, status: e.target.value })}
                                                >
                                                    <option value="Active">Active</option>
                                                    <option value="Inactive">Inactive</option>
                                                </select>
                                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                        <path d="M10 12L4 6h12z" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="mt-4 flex flex-col items-center">
                                            <div className="mb-4">
                                                <button
                                                    type="submit"
                                                    className="bg-green-500 hover:bg-gray-600 text-white  py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                                >
                                                    Save
                                                </button>
                                            </div>
                                            <div>
                                                <button
                                                    type="button"
                                                    onClick={() => setSelectedNanny(null)}
                                                    className="bg-gray-500 hover:bg-gray-600 text-white  py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                                >
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>


                                        <div className="mt-6 flex flex-col items-center">
                                            <Link href={`/testuploadnanny/${selectedNanny.id}`}>
                                                <a className="bg-blue-500 hover:bg-gray-600 text-white px-4 py-2 rounded-md focus:outline-none focus:ring focus:border-blue-300">
                                                    Edit Image
                                                </a>
                                            </Link>
                                        </div>

                                    </form>
                                    <div className='flex items-center justify-center'>
                                        <button
                                            className="mt-2 bg-indigo-500 text-white px-4 py-2 rounded-md focus:outline-none focus:ring focus:border-blue-300"
                                            onClick={runWebAutomation}
                                        >
                                            Run Web Automation
                                        </button>
                                    </div>
                                </div>
                            ) : (

                                <div>
                                    <h3>{nanny.username}</h3>
                                    <button onClick={() => handleEditNanny(nanny)}>Edit</button>
                                </div>
                            )}
                        </div>
                    </div>
                    // )
                ))}
            </div>
            <button onClick={handleExit}>Exit</button>

        </div>
    );
}