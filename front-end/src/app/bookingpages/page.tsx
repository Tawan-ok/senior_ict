'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"
import { useRouter } from 'next/navigation'
import jwt_decode from 'jwt-decode';
import nanny1 from '../../../../assets/hanni.png'
import Image from 'next/image';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
  };

  type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type BookingHistory = {
    booking_id: number,
    status: string,
    time_session: number
};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}



export default function Page({ params }: { params: { username: string } }) {
    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    // const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [bookingqueuehistory, setBookingqueuehistory] = useState<BookingHistory | null>(null); // Initialize as null
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);

    const [bookingqueue2, setBookingqueue2] = useState<BookingQueue | null>(null); // Initialize as null

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [value, onChange] = useState<Value>(new Date());
    const [response1, setResponse1] = useState<Nanny | null>(null);
  
    const router = useRouter();

    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
          const decodedToken: any = jwt_decode(token);
          const userId: number = decodedToken.sub;
    
          if (!decodedToken.a.includes('USER')) {
            setError("User ID not found in token.");
            alert('Access denied. You do not have the required permissions.');
            setLoading(false);
            router.push('/home');
            return;
          }
    
    
          const fetchData = async () => {
            try {
              const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
              setCustomer(response.data);
    
              const bookingResponse = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getbookingsbycustomerBookings/${response.data.id}`);
              setBookingqueue(bookingResponse.data);
    
              // Fetch nanny data for each booking queue item
              const nannyPromises = bookingResponse.data.map(async (booking) => {
                const nannyResponse = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${booking.nanny_id}`);
                return nannyResponse.data;
              });
    
              const fetchedNannies = await Promise.all(nannyPromises);
              setNannies(fetchedNannies);
            } catch (err) {
              if (err instanceof Error) {
                setError(err.message);
              } else {
                setError('An error occurred.');
              }
            } finally {
              setLoading(false);
            }
          };
    
          fetchData();
        } else {
          alert('You need to be logged in first.');
          router.push('/login-user');
        }
      }, [params.username]);
    
      const handleHireNanny = (nannyUsername: string) => {
        router.push(`/successfullyhiring/${nannyUsername}`);
      };
    
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error: {error}</div>;
    
      if (!customer) return <div>Customer not found.</div>;
      if (!bookingqueue.length) return <div>No booking queue found.</div>;
    
      return (
        <>
          <div className="flex justify-center">
            <div style={{ fontFamily: 'Montserrat' }} className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white font-montserrat">
              Bookings Hiring
            </div>
          </div>
          <div style={{ fontFamily: 'Montserrat' }} className="flex justify-center">
            <div className="mx-auto bg-white bg-opacity-40 p-5 rounded-2xl shadow-lg md:w-2/3 lg:w-1/2 xl:w-2/5 mt-5">
              <h2>Booking Queue:</h2>
              <ul>
                {bookingqueue.map((booking, index) => (
                  <li key={index}>
                    Booking ID: {booking.id}, Nanny ID: {booking.nanny_id}
                    <button onClick={() => handleHireNanny(nannies[index].username)}>Hire Nanny</button>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </>
      );
    
}