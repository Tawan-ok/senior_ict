'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
type Props = {};



type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    hours: number
};

type BookingHistory = {
    id: number,
    booking_id: number,
    status: string,
    time_session: number
};

type Role = 'USER' | 'ADMIN' |'NANNY' ;

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};

// ของเก่า 
export default function CustomersPage({ }: Props) {
    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [bookinghistory, setbookinghistory] = useState<BookingHistory[]>([]);
    const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
    const [nanny, setnanny] = useState<Nanny | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const router = useRouter(); // Initialize the router

    const handleExit = () => {
        localStorage.removeItem('jwt'); // Remove the JWT token
        router.push('/login-user'); // Redirect to /login
    };



    useEffect(() => {
        const token = localStorage.getItem('jwt');
        // Decode the JWT to extract user ID
        if (token) {
            const decodedToken: any = jwt_decode(token);
            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
              }
            // Extract user ID from the "sub" key in JWT
            const userId: number = decodedToken.sub;

            if (!userId) {
                setError("User ID not found in token.");
                setLoading(false);
                return;
            }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    // const response3 = await axios.get(`http://localhost:9000/api/customers/bookings/byCustomerId/${userId}`);
                    const response3 = await axios.get(`http://localhost:9000/api/customers/bookings/byCustomerId/${userId}`);
                    setbookingqueue(response3.data);
                    console.log(response3.data);

                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
    }, []);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;
    if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;
    // return (
    //     <div>
    //         <div className={styles.logoweb}>
    //             {/* Display admin information */}
    //         </div>
    //         <div className={styles.Banner}>
    //             <h2>Booking Customer</h2>
    //         </div>

    //         <button onClick={handleExit}>Exit</button>
    //         <div>
    //             <h3>Booking Queue</h3>
    //             {bookingqueue ? (
    //                 bookingqueue.map((queue) => (
    //                     <div key={queue.id}>
    //                         <p>ID: {queue.id}</p>
    //                         <p>Customer ID: {queue.customer_id}</p>
    //                         <p>Nanny ID: {queue.nanny_id}</p>
    //                         <p>Start Date: {queue.start_date.toString()}</p>
    //                         <p>End Date: {queue.end_date.toString()}</p>
    //                         <p>Total Amount: {queue.total_amount}</p>
    //                         <p>Status Payment: {queue.status_payment}</p>
    //                         <p>Hours: {queue.hours}</p>
    //                     </div>
    //                 ))
    //             ) : (
    //                 <p>Booking Queue data not found</p>
    //             )}
    //         </div>
    //     </div>
    // );

    return (
        <div style={{ fontFamily: 'Montserrat' }} className="text-center">
          <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white">
          Booking Customer
          </div>
          {bookingqueue.length > 0 && bookingqueue.length > 0 ? (
            bookingqueue.map((history) => {
              const queueItem = bookingqueue.find(queue => queue.id === history.id );
              const nannyInfo = nannies.find(nanny => nanny.id === queueItem?.nanny_id);
              return (
                <div key={history.id} className="border-2 border-pink-500 bg-white rounded-lg p-4 mb-4 mt-4 m-3">
                  <div className="flex justify-between">
                    <p className="text-gray-800 font-semibold mb-2">Booking ID: {history.id}</p>
                    <p className="text-gray-700 font-bold text-xl bg-pink-200 p-2 rounded">Status: {history.status_payment}</p>
                  </div>
                 
                  {queueItem && nannyInfo ? (
                    <div>
                      <p className="text-gray-700">Nanny Name: {nannyInfo.first_name} {nannyInfo.last_name}</p>
                      <p className="text-gray-700">Nanny Contact Number: {nannyInfo.contact_number}</p>
                      <p className="text-gray-700">Total Amount: {queueItem.total_amount.toLocaleString()} Baht</p>
                      <p className="text-gray-700">Hours: {queueItem.hours}</p>
                      <div className="flex justify-between font-bold bg-pink-200 text-black p-3">
                        <p>Start Date: {new Date(queueItem.start_date).toLocaleDateString()}</p>
                        <p>End Date: {new Date(queueItem.end_date).toLocaleDateString()}</p>
                      </div>
                    </div>
                  ) : (
                    <p className="text-red-500">Booking Queue data not found for this ID</p>
                  )}
                </div>
              );
            })
          ) : (
            <p>No Booking History data found</p>
          )}
        </div>
      );

}

