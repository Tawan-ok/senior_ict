'use client';

import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import { useRouter } from 'next/navigation';
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import woman from '../../../assets/woman.png'
import Link from 'next/link';
import jwt_decode from 'jwt-decode';
import RootLayout from "../layout";
import info from '../../../assets/info.png';
import cancel from '../../../assets/cancel.png'
import um from '../../../assets/communication.png';

type Props = {};

type SelectedNanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};

type Customer = {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  district: string;
  subDistrict: string;
  province: string;
  zipCode: string;
  streetNumber: string;
  contactNumber: string;
  age: number;
  reviewScoreWeight: number;
  successfulJobsWeight: number;
  gender: string;
  role: string;
  locationall: string;
  profileImageUrl: string | null;
};

type favoriteNanny = {

  customer_id: number;
  nanny_id: number;
}

type favoriteNannytest = {
  id: number;
  customer_id: number;
  nanny_id: number;
}

type availability_slots = {
  availability_slots_id: number;
  end_time: Date;
  is_available: Boolean;
  start_time: Date;
  nanny_id: number;
}


type availability_slotsTest = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  availability_slots_id: number;
  end_time: Date;
  is_available: Boolean;
  start_time: Date;
  nanny_id: number;
}

type AvailabilityData = {
  available: boolean;
  id: number;
  nanny_id: number;
  start_time: string;
  end_time: string;
  is_available: boolean;
};


interface GroupedSlots {
  [key: number]: AvailabilityData[]; // This is the index signature
}



export default function HiringPage({ }: Props) {
  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [hire, setHire] = useState<availability_slotsTest[]>([]);
  const [favNannyIds, setFavNannyIds] = useState<number[]>([]);
  const [selectedNanny, setSelectedNanny] = useState<SelectedNanny | null>(null);
  const [wishlistVisible, setWishlistVisible] = useState(false);
  const [customer, setCustomer] = useState<Customer | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const compareSectionRef = useRef<HTMLDivElement>(null);
  const [selectedSkills, setSelectedSkills] = useState<string[]>(['', '', '']);
  const [hiretest, sethiretest] = useState<availability_slotsTest[]>([]);
  const [startDate, setStartDate] = useState<string>('');
  const [endDate, setEndDate] = useState<string>('');
  const router = useRouter();
  const [selectedHiringType, setSelectedHiringType] = useState<string>('hour');
  const [availabilityCheckPerformed, setAvailabilityCheckPerformed] = useState(false);
  const [nanniesBasedOnAvailability, setNanniesBasedOnAvailability] = useState<Nanny[]>([]);
  const handleExit = () => {
    localStorage.removeItem('jwt');
    router.push('/login-user');
  };

  useEffect(() => {
    const token = localStorage.getItem('jwt');
    if (token) {
      const decodedToken: any = jwt_decode(token);

      const userId: number = decodedToken.sub;


      if (!decodedToken.a.includes('USER')) {
        setError("User ID not found in token.");
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }

      console.log("User ID:", userId);

      const fetchData = async () => {
        try {
          const response = await axios.get('http://localhost:9000/api/nannies');
          // const response = await axios.get('http://35.213.139.253:9000/api/nannies');
          const response2 = await axios.get('http://localhost:9000/api/nannies/with-availability-slots');
          // const response2 = await axios.get('http://35.213.139.253:9000/api/nannies/with-availability-slots');
          const response3 = await axios.get('http://localhost:9000/api/availability');
          // const response3 = await axios.get('http://35.213.139.253:9000/api/availability');
          const response1 = await axios.get<Customer>(
            `http://localhost:9000/api/customers/${userId}`
            // `http://35.213.139.253:9000/api/customers/${userId}`
          );
          sethiretest(response3.data);
          setNannies(response.data);
          setCustomer(response1.data);
          setLoading(false);
          setHire(response2.data);
          console.log("Nanny" + response.data);
          console.log(response2.data);
          console.log("availability" + response3.data);
        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
        }
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-user');
    }
  }, []);

  // Function to format date to "YYYY-MM-DD HH:MM:SS.SSSSSS" in ICT timezone
  function formatDateToICT(date: Date) {
    const offset = 7; // ICT timezone offset from UTC
    const ictDate = new Date(date.getTime() + offset * 3600 * 1000);

    let year = ictDate.getFullYear();
    let month = ('0' + (ictDate.getMonth() + 1)).slice(-2);
    let day = ('0' + ictDate.getDate()).slice(-2);
    let hours = ('0' + ictDate.getHours()).slice(-2);
    let minutes = ('0' + ictDate.getMinutes()).slice(-2);
    let seconds = ('0' + ictDate.getSeconds()).slice(-2);
    let milliseconds = ('000000' + ictDate.getMilliseconds()).slice(-6);

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
  }

  // Your existing fetchAvailability function modified to use formatDateToICT

  const fetchAvailability = async () => {
    if (!startDate || !endDate) {
      // Alert the user that both start date and end date are required
      alert("Please input both start date and end date.");
      console.error("Start date and end date are required.");
      return; // Exit the function if either date is missing
    }

    if (new Date(startDate) > new Date(endDate)) {
      alert("Start date cannot be after the end date.");
      console.error("Start date cannot be after the end date.");
      return;
    }

    try {
      const formattedStartDate = formatDateToICT(new Date(startDate));
      const formattedEndDate = formatDateToICT(new Date(endDate));;



      // Fetch availability slots within the specified date range
      const { data: availabilitySlots } = await axios.get<AvailabilityData[]>(
        `http://localhost:9000/api/availability/search?start_time=${formattedStartDate}&end_time=${formattedEndDate}`
      );

      // Group availability slots by nanny_id
      const groupedByNannyId: GroupedSlots = availabilitySlots.reduce((acc: GroupedSlots, slot: AvailabilityData) => {
        (acc[slot.nanny_id] = acc[slot.nanny_id] || []).push(slot);
        return acc;
      }, {});
      // Find the slot closest to the input start date for each nanny
      const closestSlots = Object.values(groupedByNannyId).map(slots => slots.reduce((closest: { start_time: string | number | Date; }, current: { start_time: string | number | Date; }) => {
        const closestDiff = Math.abs(new Date(closest.start_time).getTime() - new Date(startDate).getTime());
        const currentDiff = Math.abs(new Date(current.start_time).getTime() - new Date(startDate).getTime());
        return currentDiff < closestDiff ? current : closest;
      }));

      // Fetch nanny details for each closest slot
      const nanniesDetailsPromises = closestSlots.map(slot =>
        axios.get(`http://localhost:9000/api/nannies/getby/${slot.nanny_id}`)
      );
      const nanniesDetailsResponses = await Promise.all(nanniesDetailsPromises);
      console.log("nanniesDetailsResponses", nanniesDetailsResponses);

      const fetchedNannies = nanniesDetailsResponses.map(response => response.data);

      setNanniesBasedOnAvailability(fetchedNannies);
      setAvailabilityCheckPerformed(true);

    } catch (error) {
      console.error("Failed to fetch availability:", error);
    }
  };
  const addfavoriteNanny = async (nannyId: number) => {
    try {
      if (!customer) return <div>Customer not found.</div>;

      // Check if the nanny is already in the list of favorite nannies
      const response2 = await axios.get(`http://localhost:9000/api/favouriteNanny/getbyId/${customer.id}`);
      // const response2 = await axios.get(`http://35.213.139.253:9000/api/favouriteNanny/getbyId/${customer.id}`);
      console.log("Check FavNanny response", response2.data);

      const isNannyAlreadyFavorite = response2.data.some(
        (favoriteNanny: { nanny_id: number; }) => favoriteNanny.nanny_id === nannyId
      );

      if (isNannyAlreadyFavorite) {
        console.log('Nanny is already a favorite.');
        // You can show a message or take some action here
      } else {
        const fav: favoriteNanny = {
          customer_id: customer.id,
          nanny_id: nannyId,
        };

        const response = await axios.post<favoriteNanny>('http://localhost:9000/api/favouriteNanny', fav);
        // const response = await axios.post<favoriteNanny>('http://35.213.139.253:9000/api/favouriteNanny', fav);
        console.log('Favorite Nanny added successfully:', response.data);

        const selected = nannies.find(nanny => nanny.id === nannyId);
        if (selected) setSelectedNanny(selected);

        setWishlistVisible(true);

        // Update the state with the new favorited nanny ID
        setFavNannyIds((prevFavNannyIds) => [...prevFavNannyIds, nannyId]);
      }
    } catch (error) {
      console.error('Error adding favorite nanny:', error);
    }
  };

  const [selectedNannies, setSelectedNannies] = useState<SelectedNanny[]>([]);

  const addCompare = async (nannyId: number) => {
    try {
      const selected = nannies.find(nanny => nanny.id === nannyId);
      const isAlreadySelected = selectedNannies.some(nanny => nanny.id === nannyId);

      if (selected && !isAlreadySelected) {
        setSelectedNannies(prevSelectedNannies => [...prevSelectedNannies, selected]);
        setWishlistVisible(true);

        if (compareSectionRef.current) {
          compareSectionRef.current.scrollIntoView({ behavior: 'smooth' });
        }

        console.log('Selected Nannies:', selectedNannies);
      } else {
        console.log('Nanny is already selected or not found.');
      }
    } catch (error) {
      console.error('Error adding compare nanny:', error);
    }
  };

  const isNannyAvailable = (availabilitySlots: availability_slotsTest[]) => {
    const currentTime = new Date();
    return availabilitySlots.some(
      (slot) => currentTime >= new Date(slot.start_time) && currentTime <= new Date(slot.end_time) && slot.is_available
    );
  };

  //search part
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [selectedRoleLevel, setSelectedRoleLevel] = useState<string>('');
  const [selectedTypeOfWork, setSelectedTypeOfWork] = useState<string>('');
  const [selectedGender, setSelectedGender] = useState<string>('');
  const [searchByRoleLevel, setSearchByRoleLevel] = useState<boolean>(false);
  const [searchByTypeOfWork, setSearchByTypeOfWork] = useState<boolean>(false);
  const [searchByGender, setSearchByGender] = useState<boolean>(false);
  const [searchResults, setSearchResults] = useState<Nanny[]>([]);
  const [selectedProvince, setSelectedProvince] = useState<string>('');
  const [provinceValid, setProvinceValid] = useState(true);
  const [selectedDistrict, setSelectedDistrict] = useState<string>('');
  const [showModal, setShowModal] = useState(false);
  const [districtSuggestions, setDistrictSuggestions] = useState<string[]>([]);
  const [selectedRoleLevel2, setSelectedRoleLevel2] = useState<string[]>([]);
  const [searchPerformed, setSearchPerformed] = useState(false);

  const handleProvinceChange = (e: { target: { value: any; }; }) => {
    const provinceValue = e.target.value;
    setSelectedProvince(provinceValue);
    setProvinceValid(provinceValue !== '');
  };

  const provinceInputClassName = `border ${provinceValid ? 'border-gray-300' : 'border-red-500'} rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500`;

  const [selectedSkill1, setSelectedSkill1] = useState<string>('');
  const [selectedSkill2, setSelectedSkill2] = useState<string>('');
  const [selectedSkill3, setSelectedSkill3] = useState<string>('');
  const [searchBySkill, setSearchBySkill] = useState<boolean>(false);
  const handleRoleLevelChange = (value: string) => {
    setSelectedRoleLevel2((prevSelectedRoleLevel) => {
      if (prevSelectedRoleLevel.includes(value)) {
        return prevSelectedRoleLevel.filter((level) => level !== value);
      } else {
        return [...prevSelectedRoleLevel, value];
      }
    });
  };
  const toggleSkill = (skill: string) => {
    const updatedSkills = selectedSkills.includes(skill)
      ? selectedSkills.filter(s => s !== skill)
      : [...selectedSkills, skill];
    setSelectedSkills(updatedSkills);
  };


  // const handleSearch = async () => {
  //   try {
  //     let criteria: any = {
  //       keyword: searchTerm,
  //     };

  //     if (searchByRoleLevel && selectedRoleLevel2.length > 0) {
  //       criteria.role_level = selectedRoleLevel2.join(',');
  //     }

  //     if (searchByTypeOfWork && selectedTypeOfWork !== '') {
  //       criteria.type_work = selectedTypeOfWork;
  //     }

  //     if (searchByGender && selectedGender !== '') {
  //       criteria.gender = selectedGender;
  //     }

  //     if (selectedDistrict !== '') {
  //       criteria.district = selectedDistrict;
  //     }

  //     if (searchBySkill) {
  //       let allSkills = selectedSkills.filter(skill => skill.trim() !== '').join(',');
  //       criteria.allskill = allSkills;
  //     }


  //     const response = await axios.get('http://localhost:9000/api/nannies/searchtestall', {
  //       // const response = await axios.get('http://35.213.139.253:9000/api/nannies/searchtestall', {
  //       params: criteria,
  //     });
  //     setSearchResults(response.data);
  //   } catch (err) {
  //     if (err instanceof Error) {
  //       setError(err.message);
  //     } else {
  //       setError('An error occurred while searching.');
  //     }
  //   }
  // };

  const handleSearch = async () => {
    setSearchPerformed(true);  // Set search as performed regardless of the result
    try {
      let criteria: any = {
        keyword: searchTerm,
        role_level: searchByRoleLevel ? selectedRoleLevel2.join(',') : undefined,
        type_work: searchByTypeOfWork ? selectedTypeOfWork : undefined,
        gender: searchByGender ? selectedGender : undefined,
        district: selectedDistrict !== '' ? selectedDistrict : undefined,
        allskill: searchBySkill ? selectedSkills.filter(skill => skill.trim() !== '').join(',') : undefined,
      };

      const response = await axios.get('http://localhost:9000/api/nannies/searchtestall', {
        params: criteria,
      });

      setSearchResults(response.data.length > 0 ? response.data : []);
      if (response.data.length === 0) {
        setNannies([]);
      }
    } catch (err) {
      if (err instanceof Error) {
        setError(err.message);
      } else {
        setError('An error occurred while searching.');
      }
    }
  }

  const handleDistrictSearch = (inputValue: string) => {
    const suggestions = nannies
      .filter((nanny) => nanny.district.toLowerCase().includes(inputValue.toLowerCase()))
      .map((nanny) => nanny.district);

    setDistrictSuggestions(suggestions);
  };

  const selectDistrictSuggestion = (district: string) => {
    setSelectedDistrict(district);
    setDistrictSuggestions([]);
  };
  {/* ///////////////////////////////////////////////////////////////////////////////////// */ }
  {/*   ranking part  */ }
  interface RankedNanny {
    [x: string]: any;
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
    weightedScore: number;
  }

  const [isRanked, setIsRanked] = useState(false);
  const [rankedNannies, setRankedNannies] = useState<RankedNanny[]>([]);

  const [nanniesNotFound, setNanniesNotFound] = useState(false);

  const RankedNannies = async () => {
    if (!customer) {
      console.error("Customer not found.");
      return;
    }
    try {
      // const response = await axios.get<RankedNanny[]>(`http://localhost:9000/api/nannies/ranking/${customer.id}`);
      const response = await axios.get<RankedNanny[]>(`http://localhost:9000/api/nannies/ranking/${customer.id}?hiringType=${selectedHiringType}`);
      setRankedNannies(response.data.map(rankedNanny => ({
        ...rankedNanny.nanny, // Assuming the API response wraps the nanny object
        weightedScore: rankedNanny.weightedScore
      })));
      setIsRanked(true);
    } catch (error) {
      console.error('Failed to fetch ranked nannies:', error);
    }
  };


  // const nanniesToShow = isRanked ? rankedNannies : (searchResults.length > 0 ? searchResults : nannies);
  let nanniesToShow = availabilityCheckPerformed ? nanniesBasedOnAvailability : isRanked ? rankedNannies : searchResults.length > 0 ? searchPerformed ? (searchResults.length > 0 ? searchResults : []) : nannies : nannies;

  // Now applying province filter if a province is selected

  {/* ///////////////////////////////////////////////////////////////////////////////////// */ }

  {/* ///////////////////////////////////////////////////////////////////////////////////// */ }

  {/*  update weights ranking part  */ }
  const [reviewScoreWeight, setReviewScoreWeight] = useState<number>(0.5);
  const [successfulJobsWeight, setSuccessfulJobsWeight] = useState<number>(0.5);

  useEffect(() => {
    // Function to fetch customer preferences
    const token = localStorage.getItem('jwt');
    if (token) {
      const decodedToken: any = jwt_decode(token);

      const userId = decodedToken.sub;

      if (!userId) {
        setError("User ID not found in token.");
        setLoading(false);
        return;
      }

      console.log("User ID:", userId);

      const fetchCustomerPreferences = async () => {
        try {
          const response = await axios.get(`http://localhost:9000/api/customers/${userId}`);
          console.log("Response", response.data);
          const { review_score_weight, successful_jobs_weight } = response.data;
          setReviewScoreWeight(review_score_weight);
          setSuccessfulJobsWeight(successful_jobs_weight);
        } catch (error) {
          console.error('Error fetching customer preferences:', error);
        }
      };

      fetchCustomerPreferences();
    }
  }, []);

  const roundToDecimalPlace = (number: number, decimals = 1) => {
    const factor = Math.pow(10, decimals);
    return Math.round(number * factor) / factor;
  };

  const handleReviewScoreWeightChange = (e: { target: { value: string; }; }) => {
    const newValue = parseFloat(e.target.value) / 100;
    setReviewScoreWeight(newValue);
    setSuccessfulJobsWeight(roundToDecimalPlace(1 - newValue));
  };

  const handleSuccessfulJobsWeightChange = (e: { target: { value: string; }; }) => {
    const newValue = parseFloat(e.target.value) / 100;
    setSuccessfulJobsWeight(newValue);
    setReviewScoreWeight(roundToDecimalPlace(1 - newValue));
  };
  const updatePreferences = async () => {
    if (!customer || !customer.id) {
      console.error('Customer ID is not available.');
      return;
    }

    const url = `http://localhost:9000/api/customers/${customer.id}/preferences`;
    try {
      const payload = {
        reviewScoreWeight: roundToDecimalPlace(reviewScoreWeight),
        successfulJobsWeight: roundToDecimalPlace(successfulJobsWeight),
      };

      await axios.put(url, payload, {
        headers: {
          'Content-Type': 'application/json',
        },
      });

      console.log('Preferences updated successfully.');
    } catch (error) {
      console.error('Failed to update preferences:', error);
    }
  };

  const handleClear = () => {
    setSelectedSkills([]); // Clear all selected skills
  };

  // Auto-adjust the weights to ensure their total does not exceed 1
  useEffect(() => {
    if (reviewScoreWeight + successfulJobsWeight > 1) {
      setSuccessfulJobsWeight(1 - reviewScoreWeight);
    }
  }, [reviewScoreWeight]);

  useEffect(() => {
    if (reviewScoreWeight + successfulJobsWeight > 1) {
      setReviewScoreWeight(1 - successfulJobsWeight);
    }
  }, [successfulJobsWeight]);

  const handleOpenModal = (content: any) => {
    setModalContent(content);
    setShowModal(true);
  };

  // Function to handle closing the modal
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const today = new Date().toISOString().split('T')[0];
  {/* ///////////////////////////////////////////////////////////////////////////////////// */ }
  // const nanniesToShow = searchResults.length > 0 ? searchResults : nannies;   // if search results are available, use them, otherwise use all nannies 
  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  return (
    <>
      <div style={{ fontFamily: 'Montserrat' }} className="container mx-auto px-4 py-8">
        <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white font-montserrat relative flex justify-center items-center">
          <span className="inline-block">Hiring Nanny</span>
          <button onClick={() => setShowModal(true)} className="absolute right-0 top-1/2 transform -translate-y-1/2 mr-4 sm:ml-3">
            <Image src={info} width={20} height={20} alt='..' layout='fixed' />
          </button>
        </div>

        <div className="flex flex-col items-center sm:flex-row sm:justify-center mt-8 bg-white rounded-lg shadow-lg p-4 m-4 border border-pink-300">
          <label htmlFor="start-date" className="text-black mt-4 sm:mt-0 mr-4">Start Date:</label>
          <input
            type="date"
            id="start-date"
            value={startDate}
            min={today}
            onChange={(e) => setStartDate(e.target.value)} // Update startDate on change
          />
          <label htmlFor="end-date" className="text-black mt-4 sm:mt-0 ml-4 sm:ml-0 sm:mr-4">End Date:</label>
          <input
            type="date"
            id="end-date"
            value={endDate}
            onChange={(e) => setEndDate(e.target.value)}
            min={today} // Update endDate on change
            className="ml-2 mr-2" // Adding left margin to the End Date input
          />
          <div className="flex justify-center mt-4 sm:mt-0">
            <button onClick={fetchAvailability} className="bg-blue-500 text-white px-4 py-2 rounded">Check Availability</button> {/* Button to trigger availability check */}
          </div>
        </div>

        {showModal && (
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-auto bg-gray-900 bg-opacity-50">
            <div className="relative bg-white rounded-lg mx-4 md:max-w-md p-6 w-full">
              <p>Before hiring the nanny, you need to input the information of the activity program. If not, you will send a blank to the nanny, and nanny will design the program for your children.</p>
              <div className='mt-4 flex justify-center'>
                <Image src={um} width={80} height={80} alt='..' layout='fixed' />
              </div>
              <button onClick={() => setShowModal(false)} className="absolute top-0 right-0 m-2">
                <Image src={cancel} width={20} height={20} alt='..' layout='fixed' />
              </button>
            </div>
          </div>
        )}

        <div className="mt-4 mx-auto">
          <div className="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4">
            <h2 className="text-center text-2xl font-semibold mb-4">Search Nannies</h2>

            <div className="flex justify-center">
              <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
                <input
                  type="text"
                  placeholder="Search by full name"
                  value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                  className="border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
                />

                <input
                  type="text"
                  placeholder="Search by district"
                  value={selectedDistrict}
                  onChange={(e) => {
                    setSelectedDistrict(e.target.value);
                    handleDistrictSearch(e.target.value);
                  }}
                  className="border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
                />

                <select
                  value={selectedProvince}
                  onChange={handleProvinceChange}
                  className={provinceInputClassName}
                >
                  <option value="">Select Province</option>
                  <option value="Bangkok">Bangkok</option>
                  <option value="Nakhon Pathom">Nakhon Pathom</option>
                </select>
              </div>
            </div>

            <div className="mt-4 flex flex-wrap justify-center">
              <label className="inline-flex items-center mb-4 sm:mb-0 mr-4">
                <input
                  type="checkbox"
                  checked={searchByRoleLevel}
                  onChange={() => setSearchByRoleLevel(!searchByRoleLevel)}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">Search by Role Level</span>
              </label>

              <label className="inline-flex items-center mb-4 sm:mb-0 mr-4">
                <input
                  type="checkbox"
                  checked={searchByTypeOfWork}
                  onChange={() => setSearchByTypeOfWork(!searchByTypeOfWork)}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">Search by Type of Work</span>
              </label>

              <label className="inline-flex items-center mb-4 sm:mb-0 mr-4">
                <input
                  type="checkbox"
                  checked={searchByGender}
                  onChange={() => setSearchByGender(!searchByGender)}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">Search by Gender</span>
              </label>

              <label className="inline-flex items-center mb-4 sm:mb-0">
                <input
                  type="checkbox"
                  checked={searchBySkill}
                  onChange={() => setSearchBySkill(!searchBySkill)}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">Search by Skill</span>
              </label>
            </div>


            {searchByRoleLevel && (
              <div>
                <label>
                  <input
                    type="checkbox"
                    value={1}
                    checked={selectedRoleLevel2.includes("1")}
                    onChange={(e) => handleRoleLevelChange("1")}
                  />
                  1
                </label>
                <label>
                  <input
                    type="checkbox"
                    value={2}
                    checked={selectedRoleLevel2.includes("2")}
                    onChange={(e) => handleRoleLevelChange("2")}
                  />
                  2
                </label>
                <label>
                  <input
                    type="checkbox"
                    value={3}
                    checked={selectedRoleLevel2.includes("3")}
                    onChange={(e) => handleRoleLevelChange("3")}
                  />
                  3
                </label>
                <label>
                  <input
                    type="checkbox"
                    value={4}
                    checked={selectedRoleLevel2.includes("4")}
                    onChange={(e) => handleRoleLevelChange("4")}
                  />
                  4
                </label>
              </div>
            )}

            {searchByTypeOfWork && (
              <select
                value={selectedTypeOfWork}
                onChange={(e) => setSelectedTypeOfWork(e.target.value)}
                className="mt-2 border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
              >
                <option value="">Select Type of Work</option>
                <option value="F">Full Time</option>
                <option value="P">Part Time</option>
                <option value="A">ALL</option>
              </select>
            )}

            {searchByGender && (
              <select
                value={selectedGender}
                onChange={(e) => setSelectedGender(e.target.value)}
                className="mt-2 border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
              >
                <option value="">Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            )}

            {searchBySkill && (
              <div>
                <p className="text-lg font-semibold">Select Skills:</p>

                {/* Cooking Skill */}
                <div className="mt-4">
                  <p className="font-semibold">Cooking Skill:</p>
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="thaiCuisineCooking"
                      checked={selectedSkills.includes('Thai cuisine cooking')}
                      onChange={() => toggleSkill('Thai cuisine cooking')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="thaiCuisineCooking" className="ml-2">Thai cuisine cooking</label>
                  </div>
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="westernCuisineCooking"
                      checked={selectedSkills.includes('Western cuisine cooking')}
                      onChange={() => toggleSkill('Western cuisine cooking')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="westernCuisineCooking" className="ml-2">Western cuisine cooking</label>
                  </div>
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="japaneseCuisineCooking"
                      checked={selectedSkills.includes('Japanese cuisine cooking')}
                      onChange={() => toggleSkill('Japanese cuisine cooking')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="japaneseCuisineCooking" className="ml-2">Japanese cuisine cooking</label>
                  </div>
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="babyFoodPreparation"
                      checked={selectedSkills.includes('Baby/toddler food preparation')}
                      onChange={() => toggleSkill('Baby/toddler food preparation')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="babyFoodPreparation" className="ml-2">Baby/toddler food preparation</label>
                  </div>
                </div>

                {/* Speaking Skill */}
                <div className="mt-4">
                  <p className="font-semibold">Speaking Skill:</p>
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="nativeThaiSpeaker"
                      checked={selectedSkills.includes('Native Thai speaker')}
                      onChange={() => toggleSkill('Native Thai speaker')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="nativeThaiSpeaker" className="ml-2">Native Thai speaker</label>
                  </div>
                  
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="nativeEnglishSpeaker"
                      checked={selectedSkills.includes('Fluent English speaker')}
                      onChange={() => toggleSkill('Fluent English speaker')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="nativeEnglishSpeaker" className="ml-2">Fluent English speaker</label>
                  </div>

                  <div className="mt-2">
                    <input
                      type="radio"
                      id="nativeChineseSpeaker"
                      checked={selectedSkills.includes('Mandarin/Cantonese Chinese speaker')}
                      onChange={() => toggleSkill('Mandarin/Cantonese Chinese speaker')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="nativeChineseSpeaker" className="ml-2">Mandarin/Cantonese Chinese speaker</label>
                  </div>

                  <div className="mt-2">
                    <input
                      type="radio"
                      id="nativeJapanSpeaker"
                      checked={selectedSkills.includes('Fluent Japan speaker')}
                      onChange={() => toggleSkill('Fluent Japan speaker')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="nativeJapanSpeaker" className="ml-2">Fluent Japan speaker</label>
                  </div>

                  <div className="mt-2">
                    <input
                      type="radio"
                      id="nativeFrenchSpeaker"
                      checked={selectedSkills.includes('Fluent French Speaker')}
                      onChange={() => toggleSkill('Fluent French Speaker')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="nativeFrenchSpeaker" className="ml-2">Fluent French Speaker</label>
                  </div>
                </div>

                {/* General Skill */}
                <div className="mt-4">
                  <p className="font-semibold">General Skill:</p>
                  <div className="mt-2">
                    <input
                      type="radio"
                      id="drawingSkill"
                      checked={selectedSkills.includes('Drawing Skill')}
                      onChange={() => toggleSkill('Drawing Skill')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="drawingSkill" className="ml-2">Drawing Skill</label>
                  </div>

                  <div className="mt-2">
                    <input
                      type="radio"
                      id="singSkill"
                      checked={selectedSkills.includes('Singing Skill')}
                      onChange={() => toggleSkill('Singing Skill')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="singSkill" className="ml-2">Singing Skill</label>
                  </div>

                  <div className="mt-2">
                    <input
                      type="radio"
                      id="entertainSkill"
                      checked={selectedSkills.includes('Entertain Skill')}
                      onChange={() => toggleSkill('Entertain Skill')}
                      className="form-radio h-5 w-5 text-blue-500"
                    />
                    <label htmlFor="entertainSkill" className="ml-2">Entertain Skill</label>
                  </div>
                  {/* Add other general skills similarly */}
                </div>

                <div className="mt-6 flex justify-center">
                  <button
                    onClick={handleClear}
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
                  >
                    Clear
                  </button>
                </div>

              </div>
            )}


            {/* {searchBySkill && (
              <div>
                <p className="text-lg font-semibold">Select Skills:</p>
                <div className="flex items-center mt-2">
                  <input
                    type="checkbox"
                    checked={selectedSkills.includes('Educate')}
                    onChange={() => toggleSkill('Educate')}
                    className="form-checkbox h-5 w-5 text-blue-500"
                  />
                  <label className="ml-2">Educate</label>
                </div>
                <div className="flex items-center mt-2">
                  <input
                    type="checkbox"
                    checked={selectedSkills.includes('Coding')}
                    onChange={() => toggleSkill('Coding')}
                    className="form-checkbox h-5 w-5 text-blue-500"
                  />
                  <label className="ml-2">Coding</label>
                </div>
                <div className="flex items-center mt-2">
                  <input
                    type="checkbox"
                    checked={selectedSkills.includes('English')}
                    onChange={() => toggleSkill('English')}
                    className="form-checkbox h-5 w-5 text-blue-500"
                  />
                  <label className="ml-2">English</label>
                </div>
                
              </div>
            )} */}

            <div className="mt-6 flex justify-center">
              <button
                onClick={handleSearch}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4"
              >
                Search
              </button>
            </div>


            {/* ///////////////////////////////////////////////////////////////////////////////////// */}
            <div className='mt-8 p-4 bg-pink-100 border border-pink-300 rounded-lg'>
              <h2 className="text-center text-2xl font-semibold mb-4">Sort Nannies by Preferences</h2>
              <div className="flex justify-center mb-4">
                <div className="flex items-center mr-4">
                  <input
                    type="radio"
                    id="hour"
                    name="hiringType"
                    value="hour"
                    checked={selectedHiringType === 'hour'}
                    onChange={(e) => setSelectedHiringType(e.target.value)}
                    className="form-radio h-5 w-5 text-pink-500"
                  />
                  <label htmlFor="hour" className="ml-2">Hour</label>
                </div>
                <div className="flex items-center mr-4">
                  <input
                    type="radio"
                    id="day"
                    name="hiringType"
                    value="day"
                    checked={selectedHiringType === 'day'}
                    onChange={(e) => setSelectedHiringType(e.target.value)}
                    className="form-radio h-5 w-5 text-pink-500"
                  />
                  <label htmlFor="day" className="ml-2">Day</label>
                </div>
                <div className="flex items-center">
                  <input
                    type="radio"
                    id="month"
                    name="hiringType"
                    value="month"
                    checked={selectedHiringType === 'month'}
                    onChange={(e) => setSelectedHiringType(e.target.value)}
                    className="form-radio h-5 w-5 text-pink-500"
                  />
                  <label htmlFor="month" className="ml-2">Month</label>
                </div>
              </div>

              <div className="mt-6 flex flex-col items-center">
                <label htmlFor="reviewScoreWeight" className="text-lg font-medium mb-2">Review Score Weight:</label>
                <div className="relative w-full sm:w-64"> {/* Adjusted width for small screens */}
                  <input
                    type="range"
                    min="0"
                    max="100"
                    step="10"
                    value={reviewScoreWeight * 100} // Convert to percentage for the slider UI
                    onChange={handleReviewScoreWeightChange}
                    id="reviewScoreWeight"
                    className="appearance-none w-full h-2 bg-pink-200 rounded-full outline-none transition-colors"
                  />
                  <div className="absolute top-0 left-0 right-0 bottom-0 bg-gradient-to-r from-pink-300 to-pink-700 rounded-full h-2" />
                  <span className="absolute top-4 right-0 text-sm text-gray-600">{Math.round(reviewScoreWeight * 100)}%</span> {/* Display in percentage */}
                </div>
              </div>

              <div className="mt-6 flex flex-col items-center">
                <label htmlFor="successfulJobsWeight" className="text-lg font-medium mb-2">Successful Jobs Weight:</label>
                <div className="relative w-full sm:w-64"> {/* Adjusted width for small screens */}
                  <input
                    type="range"
                    min="0"
                    max="100"
                    step="10"
                    value={successfulJobsWeight * 100} // Convert to percentage for the slider UI
                    onChange={handleSuccessfulJobsWeightChange}
                    id="successfulJobsWeight"
                    className="appearance-none w-full h-2 bg-pink-200 rounded-full outline-none transition-colors"
                  />
                  <div className="absolute top-0 left-0 right-0 bottom-0 bg-gradient-to-r from-pink-500 to-pink-700 rounded-full h-2" />
                  <span className="absolute top-4 right-0 text-sm text-gray-600">{Math.round(successfulJobsWeight * 100)}%</span> {/* Display in percentage */}
                </div>
              </div>

              <div className="mt-10 flex flex-col sm:flex-row justify-center"> {/* Adjusted flex layout for small screens */}
                <button onClick={updatePreferences} className="bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-6 rounded-lg transition-colors mb-2 sm:mb-0 mr-0 sm:mr-2"> {/* Adjusted margin for small screens */}
                  Retrieve Your Setting
                </button>

                <button onClick={RankedNannies} className="bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-6 rounded-lg transition-colors">
                  Sort
                </button>
              </div>
            </div>


          </div>
        </div>
      </div >
      {/* ///////////////////////////////////////////////////////////////////////////////////// */}
      < div className={styles.rootContainer} >
        {/* <RootLayout showNavbar={true}> */}
        < div className="container mx-auto mt-10" >
          {nanniesToShow.length > 0 ? (
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-3 justify-items-center">
              {nanniesToShow.filter(nanny => nanny.status === 'Active' && (!selectedProvince || nanny.province === selectedProvince)).map((nanny, index) => (
                nanny.status === 'Active' && (
                  <div className="col p-3 text-center bg-white rounded-xl" key={nanny.username}>
                    <div
                      style={{
                        backgroundImage: `url(${nanny1})`,
                        backgroundSize: 'contain',
                        backgroundPosition: 'center',
                        minHeight: '200px',
                        minWidth: '200px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      className='mb-5'
                    >
                      <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                    </div>
                    <div style={{ fontFamily: 'Montserrat' }} className='text-start p-2 font-medium border border-blue-200 rounded-md bg-pink-100'>
                      <p className="text-pink-600 text-2xl font-bold">Review Score: {nanny.score}</p> {/* Enhanced score appearance */}
                    </div>
                    <Link href={`/nannydetail/${nanny.username}`}>
                      <div style={{ fontFamily: 'Montserrat' }} className='bg-pink-400 text-white p-2 mt-6 text-xl md:text-xl lg:text-2xl xl:text-2xl 2xl:text-2xl font-medium'>
                        <span className='font-bold'>{nanny.first_name} {nanny.last_name}</span><br />
                        <span className='text-l'>Age: {nanny.age}</span><br />
                        <span className='text-l'>Skill: {`${nanny.skill_1}${nanny.skill_2 ? ', ' + nanny.skill_2 : ''}${nanny.skill_3 ? ', ' + nanny.skill_3 : ''}`}</span>
                      </div>
                    </Link>

                    <div className='mt-2'>
                      <button onClick={() => addfavoriteNanny(nanny.id)}>
                        <span className='mt-4 text-m md:text-m lg:text-xl xl:text-xl 2xl:text-xl font-bold'>
                          <div style={{ fontFamily: 'Montserrat' }}>Add Favourite</div>
                        </span>
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                        >
                          {/* <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" /> */}
                        </div>
                      </button>
                      <div>
                        <button onClick={() => addCompare(nanny.id)}>
                          <span className='mt-2 text-xl md:text-xl lg:text-2xl xl:text-2xl 2xl:text-2xl font-medium'>
                            <div style={{ fontFamily: 'Montserrat' }}>Compare</div>
                          </span>
                          <div
                            style={{
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                          </div>
                        </button>
                      </div>
                    </div>
                  </div>
                )
              ))}
            </div>
          ) : searchPerformed ? (
            <div style={{ fontFamily: 'Montserrat' }} className='flex flex-col items-center justify-center bg-white border-2 border-pink-300 rounded-lg p-4 mb-4'>
              <Image src={woman} width={80} height={80} alt='alert' layout='fixed' />
              <p className="text-center text-xl font-medium mt-4 text-black">Apologies, no nannies found matching your search.</p>
            </div>
          ) : (
            <div style={{ fontFamily: 'Montserrat' }} className='flex flex-col items-center justify-center bg-white border-2 border-pink-300 rounded-lg p-4 mb-4'>
              <Image src={woman} width={80} height={80} alt='alert' layout='fixed' />
              <p className="text-center text-xl font-medium mt-4 text-black">Apologies, no nannies found matching your search.</p>
            </div>
          )}
          {wishlistVisible && selectedNannies.length > 0 && (
            <div style={{ fontFamily: 'Montserrat' }} ref={compareSectionRef} className="mt-8 p-4 bg-gray-100 border border-gray-300 rounded-lg">
              <h2 className="text-center text-2xl font-semibold mb-4 text-gray-800">Compare Nannies</h2>

              <div className="container mx-auto mt-6 grid gap-6 md:grid-cols-2 lg:grid-cols-3">
                {selectedNannies.map((nanny, index) => (
                  <div key={index} className="flex flex-col items-center justify-center bg-white rounded-lg shadow-md p-6">
                    <div
                      style={{
                        backgroundImage: `url(${nanny1})`,
                        backgroundSize: 'contain',
                        background: 'white',
                        backgroundPosition: 'center',
                        minHeight: '200px',
                        minWidth: '200px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      className='my-3 mx-auto'
                    >
                      <div className="my-3 mx-auto h-48 w-48 flex items-center justify-center bg-cover bg-center rounded-full overflow-hidden">
                        <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                      </div>
                    </div>
                    <div className="text-left mt-4">
                      <p className="text-lg font-semibold">{nanny.first_name} {nanny.last_name}</p>
                      <p><strong>Age:</strong> {nanny.age}</p>
                      <p><strong>Role:</strong> {nanny.role_level}</p>
                      <p><strong>Skill:</strong> {`${nanny.skill_1}${nanny.skill_2 ? ', ' + nanny.skill_2 : ''}${nanny.skill_3 ? ', ' + nanny.skill_3 : ''}`}</p>
                      <p><strong>Hours Cost:</strong> {nanny.hours_cost}</p>
                      <p><strong>Day Cost:</strong> {nanny.days_cost}</p>
                      <p><strong>Month Cost:</strong> {nanny.months_cost}</p>
                    </div>
                    <div className="text-center mt-4">
                      <Link href={`/nannydetail/${nanny.username}`} passHref>
                        <button className="text-white font-medium bg-pink-400 px-4 py-2 rounded-lg hover:bg-pink-500 transition duration-300 ease-in-out">Hire Nanny</button>
                      </Link>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )
          }

        </div >
        {/* </RootLayout> */}
      </div ></>
  );
}



function setModalContent(content: any) {
  throw new Error('Function not implemented.');
}

