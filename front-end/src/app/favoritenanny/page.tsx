'use client';


import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Image from 'next/image';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import styles from '../../../styles/favoriteNanny.module.css';
import jwt_decode from 'jwt-decode';
import { useRouter } from 'next/navigation';
import Link from 'next/link';
// type Props = {};

// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import Image from 'next/image';
// import logo from '../../../assets/Logo.png';
// import styles from '../../../styles/favoriteNanny.module.css';

type Props = {};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
};

type favoriteNanny = {
  customer_id: number;
  nanny_id: number;
};

export default function FavoriteNanny({ }: Props) {
  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [customer, setCustomer] = useState<Customer | null>(null);
  const [error, setError] = useState<string | null>();
  const [loading, setLoading] = useState(true); // Initialize as loading
  const router = useRouter();

  const handleExit = () => {
    localStorage.removeItem('jwt');
    router.push('/login-admin');
  };

  useEffect(() => {
    const token = localStorage.getItem('jwt');
    if (token) {
      const decodedToken: any = jwt_decode(token);

      const userId: number = decodedToken.sub;

      if (!decodedToken.a.includes('USER')) {
        setError("User ID not found in token.");
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }


      console.log("User ID:", userId);
      const fetchData = async () => {
        try {
          // const response = await axios.get('http://35.213.139.253:9000/api/nannies');
          const response = await axios.get('http://localhost:9000/api/nannies');
          const response1 = await axios.get<Customer>(
            // `http://35.213.139.253:9000/api/customers/${userId}`
            `http://localhost:9000/api/customers/${userId}`
          );
          // const response2 = await axios.get(`http://35.213.139.253:9000/api/favouriteNanny/getbyId/${response1.data.id}`);
          const response2 = await axios.get(`http://localhost:9000/api/favouriteNanny/getbyId/${response1.data.id}`);

          setCustomer(response1.data);
          // Map the list of favoriteNannies to an array of Promises that fetch nanny data
          const nannyPromises = response2.data.map(async (favoriteNanny: favoriteNanny) => {
            // const response3 = await axios.get(`http://35.213.139.253:9000/api/nannies/getby/${favoriteNanny.nanny_id}`);
            const response3 = await axios.get(`http://localhost:9000/api/nannies/getby/${favoriteNanny.nanny_id}`);
            return response3.data;
          });

          // Wait for all the Promises to resolve
          const nanniesData = await Promise.all(nannyPromises);
          setNannies(nanniesData);

          setLoading(false); // Data fetching is complete
        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
          setLoading(false); // Set loading to false even in case of an error
        }
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-user');
    }
  }, []);

  return (
    <div style={{ fontFamily: 'Montserrat' }}>
      <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
        <span style={{ fontFamily: 'Montserrat', }} className="text-white">Favorite Nanny</span>
      </div>

      <div className="container mx-auto mt-10">
        {loading ? (
          <div>Loading...</div>
        ) : nannies.length > 0 ? (
          nannies.map((nanny) => (
            <div className="col p-3 mt-2 text-center bg-white rounded-xl" key={nanny.id}>
              <div
                style={{
                  backgroundImage: `url(${nanny1})`,
                  backgroundSize: 'contain',
                  background: 'white',
                  backgroundPosition: 'center',
                  minHeight: '200px',
                  minWidth: '200px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                className='my-3 mx-auto'
              >
                <div className="my-3 mx-auto h-48 w-48 flex items-center justify-center bg-cover bg-center rounded-full overflow-hidden">
                  <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                </div>
              </div>
              <p className="mt-3 text-lg font-semibold">{nanny.first_name} {nanny.last_name}</p>
              <p><strong>Age:</strong> {nanny.age}</p>
              <p><strong>Role:</strong> {nanny.role_level}</p>
              <p><strong>Skills:</strong> {`${nanny.skill_1}${nanny.skill_2 ? ', ' + nanny.skill_2 : ''}${nanny.skill_3 ? ', ' + nanny.skill_3 : ''}`}</p>
              <div className="grid grid-cols-3 gap-2 mt-3 text-sm bg-pink-100 p-2">
                <div>
                  <p><strong>Hours Cost:</strong> {nanny.hours_cost}</p>
                </div>
                <div>
                  <p><strong>Day Cost:</strong> {nanny.days_cost}</p>
                </div>
                <div>
                  <p><strong>Month Cost:</strong> {nanny.months_cost}</p>
                </div>
              </div>
              <div className="mt-4">
                <Link href={`/nannydetail/${nanny.username}`} passHref>
                  <button className="text-white font-medium bg-pink-400 px-4 py-2 rounded-lg hover:bg-pink-500 transition duration-300 ease-in-out">Hire Nanny</button>
                </Link>
              </div>
              {/* Display other details about the nanny from response3 */}
            </div>


          ))
        ) : (
          <div>No nannies found.</div>
        )}
      </div>
    </div>
  );
}



