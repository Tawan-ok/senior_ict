'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"
import { useRouter } from 'next/navigation'
import jwt_decode from 'jwt-decode';
import nanny1 from '../../../../assets/hanni.png'
import Image from 'next/image';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type BookingHistory = {
    booking_id: number,
    status: string,
    time_session: number
};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}



export default function Page({ params }: { params: { username: string } }) {
    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null

    const [bookingqueuehistory, setBookingqueuehistory] = useState<BookingHistory | null>(null); // Initialize as null
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);

    const [bookingqueue2, setBookingqueue2] = useState<BookingQueue | null>(null); // Initialize as null

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [value, onChange] = useState<Value>(new Date());
    const [response1, setResponse1] = useState<Nanny | null>(null);
    const [startdate, setstartDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [enddate, setEndtDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })


    const getstartTimes = () => {
        if (!startdate.justDate) return

        const { justDate } = startdate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const starttimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            starttimes.push(i)
        }
        return starttimes
    }

    const getendTimes = () => {
        if (!enddate.justDate) return

        const { justDate } = enddate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const endtimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            endtimes.push(i)
        }
        return endtimes
    }

    const starttimes = getstartTimes()
    const endtimes = getendTimes()
    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-admin');
    };

    const formatDateToISO = (dateString: string | number | Date, includeTime: boolean = false) => {
        const date = new Date(dateString);
        const day = date.getDate();
        const month = date.toLocaleString('default', { month: 'long' });
        const year = date.getFullYear();
        const hour = date.getHours();
        const minute = date.getMinutes();

        const paddedHour = hour.toString().padStart(2, '0');
        const paddedMinute = minute.toString().padStart(2, '0');

        let formattedDate = `${day} ${month} ${year}`;
        if (includeTime) {
            formattedDate += ` ${paddedHour}:${paddedMinute}`;
        }

        return formattedDate;
    };

    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);;
                router.push('/home');
                return;
              }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    if (!customer || !nanny) {
                        const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                        const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                        setCustomer(response.data);
                        setNanny(response1.data);

                        if (response.data && response1.data) {
                            const response2 = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            setBookingqueue(response2.data);
                            console.log("All DATA Booking ", response2.data);
                            // Loop through bookingqueue and create BookingHistory for each booking

                            const response4 = await axios.get<BookingQueue>(`http://localhost:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            setBookingqueue2(response4.data);
                            console.log("All DATA Booking TEST STATUS ", response4.data);
                            console.log("All DATA Booking TEST STATUS ", response4.data.id);
                            // for (const booking of response2.data) {

                            const response3 = await axios.get<BookingHistory>(`http://localhost:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            setBookingqueuehistory(response3.data);
                            //   console.log("Booking ID:", response3.data.booking_id);
                            console.log("All DATA Booking History", response3.data);
                            // }
                        }

                    }
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes
    }, [params.username, response1]);

    // Confirm Hiring เป็นหน้ากดปุ่ม Confirm เพื่อ post ข้อมูล ลง Booking History เพื่อไปเรียกใช้ ในหน้า success fully hiring
    const handleUpdateStatus = async () => {
        try {

            if (!bookingqueue2) return <div>Bookingqueuehistory not found.</div>;
            console.log(bookingqueue2?.id)
        } catch (err) {
            if (err instanceof Error) {
                console.error(err.message);
            } else {
                console.error('An error occurred while updating the status.');
            }
        }
    };

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;

    if (!bookingqueuehistory) return <div>Bookingqueuehistory not found.</div>;

    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="flex justify-center">
                <div className="text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white font-montserrat mt-3">
                    Successfully Hiring
                </div>
            </div>
            <div className="flex justify-center mt-6">
                <div className="bg-white bg-opacity-70 rounded-xl shadow-lg p-6 w-full md:max-w-4xl">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                        <div className="rounded-lg border border-pink-400 shadow-md flex flex-col items-center justify-center p-8">
                            <div className="w-36 h-36 overflow-hidden rounded-full shadow-md mb-4">
                                <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                            </div>
                            <div>
                                <h2 className="text-lg font-bold mb-2 underline">Nanny Detail</h2>
                                <p className="mb-1"><span className="font-semibold">Name:</span> {nanny.first_name} {nanny.last_name}</p>
                                <p className="mb-1"><span className="font-semibold">Email:</span> {nanny.email}</p>
                                <p className="mb-1"><span className="font-semibold">Contact Number:</span> {nanny.contact_number}</p>
                                <p className="mb-1"><span className="font-semibold">Type of Work:</span> {nanny.type_work === 'F' ? 'Full-Time' : nanny.type_work === 'P' ? 'Part-Time' : nanny.type_work === 'A' ? 'Full-Time & Part-Time' : nanny.type_work} </p>
                                <p className="mb-1"><span className="font-semibold">Role Level:</span> {nanny.role_level}</p>
                            </div>
                        </div>
                        <div className="border border-pink-400 bg-pink-100 p-6 rounded-lg shadow-md">
                            {bookingqueue.map((bookingqueues) => (
                                bookingqueues.status_payment === 'Bookings' && (
                                    <div key={bookingqueues.id} className="mb-6">
                                        <h2 className="text-lg font-bold mb-2 underline">Booking Details</h2>
                                        <p className="mb-1"><span className="font-semibold">ID Bookings:</span> {bookingqueues.id}</p>
                                        <p className="mb-1">Customer Name: {customer?.first_name}  {customer?.last_name} </p>
                                        <p className="mb-1">Nanny Name: {nanny?.first_name}  {nanny?.last_name} </p>
                                        <p className="mb-1">Location Hiring: {bookingqueues.locationhiring}</p>
                                        <p className="mb-1">Hours: {bookingqueues.hours}</p>
                                        <p className="mb-1">Total Amount: {bookingqueues.total_amount.toLocaleString()}</p>
                                        <p className="mb-1"><span className="font-semibold">Start Date:</span> {formatDateToISO(new Date(bookingqueues.start_date), true)}</p>
                                        <p className="mb-1"><span className="font-semibold">End Date:</span> {formatDateToISO(new Date(bookingqueues.end_date), true)}</p>
                                    </div>
                                )
                            ))}
                        </div>
                    </div>
                    <div className="bg-gray-100 border border-pink-400 p-6 rounded-xl mt-6">
                        <h2 className="text-lg font-bold mb-2 underline">Booking History</h2>
                        <p><span className="font-semibold">Booking ID:</span> {bookingqueuehistory.booking_id}</p>
                        <p><span className="font-semibold">Status:</span> {bookingqueuehistory.status}</p>
                        <p><span className="font-semibold">Time Session:</span> {bookingqueuehistory.time_session} Hour</p>
                    </div>
                    <div className="flex justify-center mt-6">
                        <Link href={`/assessment/${nanny.username}`}>
                            <button className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded-lg">Finished Hiring</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>

    );

}