'use client';
import React, { useState, useEffect } from 'react';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';

const MapComponent: React.FC = () => {
  const [currentLocation, setCurrentLocation] = useState<{ lat: number; lng: number } | null>(null);

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyCswn_uJTZYtbXDTOWXXqES3p43ONV1Co0', // Replace with your API key
  });

  useEffect(() => {
    // Fetch the user's current location using the browser's Geolocation API
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setCurrentLocation({ lat: latitude, lng: longitude });
      },
      (error) => {
        console.error('Error getting user location:', error);
      }
    );
  }, []); // Run once on component mount

  if (loadError) return <div>Error loading map</div>;
  if (!isLoaded) return <div>Loading map</div>;

  return (
    <GoogleMap
      mapContainerStyle={{ width: '100%', height: '100vh' }}
      center={currentLocation || { lat: 0, lng: 0 }}
      zoom={15}
    >
      {currentLocation && <Marker position={currentLocation} />}
    </GoogleMap>
  );
};

export default MapComponent;

