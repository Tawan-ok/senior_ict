"use client";
import React, { useEffect, useState } from "react";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import axios from "axios";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import "react-big-calendar/lib/css/react-big-calendar.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { create } from "domain";
import { endOfDay, formatISO } from "date-fns";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/navigation";
import styles from "../../../styles/nannyschedule.module.css";
import { startOfDay } from "date-fns";
import { isToday, setHours, setMinutes } from "date-fns";
const locales = {
  "en-US": require("date-fns/locale/en-US"),
};

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

interface Customer {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
}

interface Nanny {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
}

interface Slot {
  is_available: boolean;
  start_time: string;
  end_time: string;
}

interface Event {
  title: string;
  start: Date;
  end: Date;
  isAvailable: boolean;
  type?: string;
}

interface AppointmentResponse {
  title: string;
  customer_id: number;
  nanny_id: number;
  start_time: string; // Assuming the API returns dates in ISO format
  end_time: string;
  status: string;
  interviewDetails?: string;
}

export default function PageNannySchedule() {
  const [nanny, setNanny] = useState<Nanny | null>(null);
  const [allEvents, setAllEvents] = useState<Event[]>([]);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [newEvent, setNewEvent] = useState<Event>({
    title: "Availability",
    start: new Date(),
    end: new Date(),
    isAvailable: true,
  });

  const router = useRouter();

  useEffect(() => {
    const fetchNannyData = async () => {
      const token = localStorage.getItem("jwt");

      if (token) {
        const decodedToken: any = jwt_decode(token);
        const userId: number = decodedToken.sub;

        if (!decodedToken.a.includes("NANNY")) {
          setError("Access denied. You do not have the required permissions.");
          alert("Access denied. You do not have the required permissions.");
          setLoading(false);
          router.push("/home");
          return;
        }
        try {
          const response = await axios.get<Nanny>(
            `http://localhost:9000/api/nannies/getby/${userId}`
          );
          // const response = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getby/${userId}`);
          setNanny(response.data);
        } catch (error) {
          console.error("Failed to fetch nanny data:", error);
        }
      } else {
        router.push("/login");
      }
    };

    fetchNannyData();
  }, [router]);

  useEffect(() => {
    const fetchAvailabilitySlots = async () => {
      if (nanny?.id) {
        try {
          const response = await axios.get<Slot[]>(
            `http://localhost:9000/api/availability/nanny/${nanny.id}`
          );
          // const response = await axios.get<Slot[]>(`http://35.213.139.253:9000/api/availability/nanny/${nanny.id}`);
          const currentDateTime = new Date(); // make it show only current date
          const startOfCurrentDay = startOfDay(currentDateTime);
          const slots = response.data
            // .filter(slot => new Date(slot.start_time) >= startOfCurrentDay) // Filter slots starting from current time onwards
            .map((slot) => ({
              title: slot.is_available
                ? "Nanny available time"
                : "Nanny is busy",
              start: new Date(slot.start_time),
              end: new Date(slot.end_time),
              isAvailable: slot.is_available,
              type: "availabilitySlot",
            }));

          const appointmentsResponse = await axios.get<AppointmentResponse[]>(
            `http://localhost:9000/api/appointments?&nanny_id=${nanny.id}&status=confirmed`
            // `http://35.213.139.253:9000/api/appointments?customer_id=${customer.id}&nanny_id=${nanny.id}&status=confirmed`
          );
          const appointmentsConfirmed = appointmentsResponse.data
            // .filter(currrent => new Date(currrent.start_time) >= startOfCurrentDay)
            .map((appointment) => ({
              title: appointment.title,
              start: new Date(appointment.start_time),
              end: new Date(appointment.end_time),
              customer_id: appointment.customer_id,
              nanny_id: appointment.nanny_id,
              status: appointment.status,
              interviewDetails: appointment.interviewDetails,
              isAvailable: true, // Assuming all appointments are marked as available
            }));
          const combinedEvents: Event[] = [...appointmentsConfirmed, ...slots];

          setAllEvents(combinedEvents);
        } catch (error) {
          console.error("Failed to fetch availability slots:", error);
        }
      }
    };

    fetchAvailabilitySlots();
  }, [nanny?.id]);

  interface MyEvent {
    title: string;
    start: Date;
    end: Date;
    interviewDetails?: string; // Include other properties as needed
  }
  interface CustomEventProps {
    event: MyEvent; // Use the interface you defined
  }

  const CustomEvent: React.FC<CustomEventProps> = ({ event }) => {
    // Function to detect URLs within a string and return an array of parts (text and URLs)
    const parseTextForURLs = (text: string) => {
      const urlRegex = /(https?:\/\/[^\s]+)/g;
      const parts = text.split(urlRegex); // Split the text by URLs

      return parts.map((part, index) => {
        if (part.match(urlRegex)) {
          // If the part is a URL, return it as an anchor element
          return (
            <a
              key={index}
              href={part}
              target="_blank"
              rel="noopener noreferrer"
              style={{ marginLeft: "5px" }}
            >
              {part}
            </a>
          );
        } else {
          // If the part is not a URL, return it as a span element
          return <span key={index}>{part}</span>;
        }
      });
    };

    return (
      <div>
        <strong>{event.title}</strong>
        {event.interviewDetails && (
          <p>Details: {parseTextForURLs(event.interviewDetails)}</p>
        )}
      </div>
    );
  };

  const doesEventOverlap = (
    newEvent: Event,
    existingEvents: Event[]
  ): boolean => {
    return existingEvents.some((event) => {
      const existingStart = new Date(event.start).getTime();
      const existingEnd = new Date(event.end).getTime();
      const newStart = new Date(newEvent.start).getTime();
      const newEnd = new Date(newEvent.end).getTime();

      // Check for overlap
      return newStart < existingEnd && newEnd > existingStart;
    });
  };

  const eventStyleGetter = (event: Event) => {
    let backgroundColor = "#007bff"; // Default blue color for normal events

    // Apply color coding only for availability slots
    if (event.type === "availabilitySlot") {
      backgroundColor = event.isAvailable ? "lightgreen" : "lightcoral";
    }

    return { style: { backgroundColor } };
  };

  const handleAddEvent = async () => {
    if (newEvent.end <= newEvent.start) {
      alert("The end date must be after the start date.");
      return; // Prevents proceeding if end is before start
    }

    // Checking for event overlap
    if (doesEventOverlap(newEvent, allEvents)) {
      alert(
        "This event overlaps with an existing event. Please choose another time."
      );
      return; // Stops the addition if there's an overlap
    }

    // Adding the availability slot
    if (nanny?.id) {
      try {
        // Adjusting the newEvent object to match your API requirements
        const adjustedEvent = {
          ...newEvent,
          start_time: formatISO(startOfDay(newEvent.start)), // Ensure the start time covers the whole day
          end_time: formatISO(endOfDay(newEvent.end)), // Ensure the end time covers the whole day
          is_available: newEvent.isAvailable,
          nanny_id: nanny.id,
        };

        const response = await axios.post(
          "http://localhost:9000/api/availability",
          adjustedEvent
        );
        if (response.data) {
          // Assuming your API returns the newly created event object
          const addedEvent = {
            ...newEvent,
            start: startOfDay(new Date(response.data.start_time)), // Converting back to Date object
            end: endOfDay(new Date(response.data.end_time)), // Converting back to Date object
          };

          // Updating the UI immediately
          setAllEvents((prevEvents) => [...prevEvents, addedEvent]);
          alert("Availability slot added successfully!");
        }
      } catch (error) {
        console.error("Failed to create the availability slot:", error);
        alert("Failed to add the availability slot. Please try again.");
      }
    }
  };

  return (
    <div style={{ fontFamily: "Montserrat" }} className="p-4">
      <div className="mt-3 text-center text-3xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
        <span className="text-white">Nanny Schedule</span>
      </div>
      <div className="bg-white rounded-lg shadow-md p-6 mb-8 mt-3">
        <h2 className="text-xl font-bold mb-4">Add New Availability Slot</h2>

        <div className="mb-4">
          <label className="block text-sm font-medium text-gray-700">
            Start Date:
          </label>
          <DatePicker
            selected={newEvent.start}
            onChange={(date: Date | null) =>
              setNewEvent({
                ...newEvent,
                start: startOfDay(date ?? new Date()),
              })
            }
            dateFormat="MM/dd/yyyy"
            minDate={new Date()}
            // // showTimeSelect
            // dateFormat="Pp"
            // minDate={newEvent.start || new Date()}
            // minTime={isToday(newEvent.start) ? setHours(setMinutes(new Date(), new Date().getMinutes()), new Date().getHours()) : setHours(setMinutes(new Date(), 0), 0)} // Set minimum time to current time if today, else start of day
            // maxTime={setHours(setMinutes(new Date(), 59), 23)}

            className="ml-3 mt-1 p-2 border border-gray-300 rounded-md w-full"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-gray-700">
            End Date:
          </label>
          <DatePicker
            selected={newEvent.end}
            onChange={(date: Date | null) =>
              setNewEvent({ ...newEvent, end: endOfDay(date ?? new Date()) })
            }
            dateFormat="MM/dd/yyyy"
            minDate={new Date()}
            // showTimeSelect
            // dateFormat="Pp"
            // minDate={newEvent.start || new Date()}
            // minTime={isToday(newEvent.start) ? setHours(setMinutes(new Date(), new Date().getMinutes()), new Date().getHours()) : setHours(setMinutes(new Date(), 0), 0)} // Set minimum time to current time if today, else start of day
            // maxTime={setHours(setMinutes(new Date(), 59), 23)}
            className="ml-3 mt-1 p-2 border border-gray-300 rounded-md w-full"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-gray-700">
            Status:
          </label>
          <select
            value={newEvent.isAvailable ? "Available" : "Not Available"}
            onChange={(e) =>
              setNewEvent({
                ...newEvent,
                isAvailable: e.target.value === "Available",
              })
            }
            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
          >
            <option value="Available">Available</option>
            <option value="Not Available">Not Available</option>
          </select>
        </div>

        <button
          className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded-md"
          onClick={handleAddEvent}
        >
          Add Slot
        </button>
      </div>
      <Calendar
        localizer={localizer}
        events={allEvents}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 500 }}
        eventPropGetter={eventStyleGetter}
        components={{
          event: CustomEvent as any,
        }}
        key={allEvents.length} // Use the length of allEvents as a key
        className="bg-white rounded-lg shadow-md p-6"
      />
    </div>
  );
}
