"use client";
import React, { useEffect, useState } from "react";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import axios from "axios";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import "react-big-calendar/lib/css/react-big-calendar.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { create } from "domain";
import { formatISO } from "date-fns";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/navigation";
import Image from 'next/image';
import cancel from '../../../../assets/cancel.png'
import um from '../../../../assets/communication.png'

const locales = {
  "en-US": require("date-fns/locale/en-US"),
};

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

const events = [
  {
    title: "Big Meeting",
    customer_id: 1, // Example ID, adjust as necessary
    nanny_id: 1, // Example ID, adjust as necessary
    start_time: new Date(2024, 1, 6),
    end_time: new Date(2024, 1, 8),
    status: "Scheduled",
  },
  {
    title: "Vacation",
    allDay: true,
    customer_id: 1, // Example ID, adjust as necessary
    nanny_id: 1, // Example ID, adjust as necessary
    start: new Date(2024, 2, 9),
    end: new Date(2024, 2, 10),
    status: "Scheduled",
  },
  {
    title: "3-Hour Meeting",
    customer_id: 1, // Example ID, adjust as necessary
    nanny_id: 1, // Example ID, adjust as necessary
    start: new Date(2024, 1, 6, 10, 0), // March 6, 2024, 10:00 AM
    end: new Date(2024, 1, 6, 13, 0), // March 6, 2024, 1:00 PM
    status: "Scheduled",
  },
];

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
};

interface AppointmentResponse {
  title: string;
  customer_id: number;
  nanny_id: number;
  start_time: string; // Assuming the API returns dates in ISO format
  end_time: string;
  status: string;
}
export default function PageAppointmentInterview({
  params,
}: {
  params: { username: string };
}) {
  const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
  const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [response1, setResponse1] = useState<Nanny | null>(null);
  const [allEvent, setAllEvent] = useState<Event[]>([]);
  const [newEvent, setNewEvent] = useState<Event | null>(null); // Initialize as null
  const [showModal, setShowModal] = useState(false);
  type Event = {
    // title: string;
    // start: Date | null; // Can be either Date or string
    // end: Date | null; // Can be either Date or string
    title: string;
    customer_id: number | "";
    nanny_id: number | "";
    start: Date; // Updated to accept string for initial fetch from backend
    end: Date;
    status: string;
  };

  const router = useRouter();

  const fetchData = async () => {
    try {
      const token = localStorage.getItem("jwt");
      if (token) {
        const decodedToken: any = jwt_decode(token);
        const userId: number = decodedToken.sub;

        if (!userId) {
          setError("User ID not found in token");
          return;
        }

        console.log("User ID:", userId);

        const response = await axios.get<Nanny>(
          `http://localhost:9000/api/nannies/getbyusername/${params.username}`
        );
        const response1 = await axios.get<Customer>(
          `http://localhost:9000/api/customers/${userId}`
        );
        setNanny(response.data);
        setCustomer(response1.data);
        setLoading(false);
      } else {
        alert("You need to be logged in first.");
        router.push("/login-user");
      }
    } catch (err) {
      if (err instanceof Error) {
        setError(err.message);
      } else {
        setError("An error occurred.");
      }
    }
  };

  useEffect(() => {
    const fetchAppointments = async () => {
      try {
        const response = await axios.get<AppointmentResponse[]>( `http://localhost:9000/api/appointments?customer_id=2&nanny_id=1`);
        const appointments = response.data.map(appointment => ({
          ...appointment,
          start: new Date(appointment.start_time),
          end: new Date(appointment.end_time),
        }));
        setAllEvent(appointments);
      } catch (error) {
        console.error('Failed to fetch appointments:', error);
      }
    };

    fetchAppointments();
  }, []);


  useEffect(() => {
    fetchData();
  }, [params.username]);

  useEffect(() => {
    // Ensure customer and nanny are not null before setting newEvent
    if (customer && nanny) {
      setNewEvent({
        title: "",
        customer_id: customer.id, // Now safe to access id
        nanny_id: nanny.id, // Now safe to access id
        start: new Date(),
        end: new Date(),
        status: "Scheduled",
      });
    }
  }, [customer, nanny]);
  

  const convertedEvents = allEvent.map((event) => ({
    ...event,
    start: event.start ? new Date(event.start) : new Date(), // Default to current date if undefined or null
    end: event.end ? new Date(event.end) : new Date(), // Default to current date if undefined or null
  }));
    console.log("convertedEvents" +convertedEvents);

     const testEvents = [
    {
      title: 'Test Event',
      start: new Date(),
      end: new Date(new Date().setDate(new Date().getDate() + 1)),
    },
  ];
  const createAppointment = async (appointment: Event) => {
    if (!appointment.start || !appointment.end) {
      console.error("Start and end times must be provided.");
      return; // Exit the function if start or end times are null
    }
    try {
      const formattedAppointment = {
        ...appointment,
        start_time: formatISO(appointment.start),
        end_time: formatISO(appointment.end),
      };

      const response = await axios.post(
        "http://localhost:9000/api/appointments",
        formattedAppointment
      );
      console.log("Appointment created:", response.data);
      return response.data;
    } catch (error) {
      console.error("Failed to create appointment:", error);
      throw error;
    }
  };

  function handleAddEvent() {
    if (newEvent !== null) {
      if (!newEvent.title || newEvent.title.trim() === "") {
        alert('Please enter a title.');
        return; // Exit the function if the title is empty or only whitespace
      }
      createAppointment(newEvent)
        .then((createdEvent) => {
          // Update the allEvent state with the newly created event
          // Make sure to convert the start and end times from the created event to Date objects
          const updatedEvent = {
            ...createdEvent,
            start: new Date(createdEvent.start_time),
            end: new Date(createdEvent.end_time),
          };
          setAllEvent((prevEvents) => [...prevEvents, updatedEvent]);
        })
        .catch((error) => {
          console.error("Failed to create handleAddEvent:", error);
        });
    } else {
      console.error("New event is null, cannot create appointment.");
    }
  }

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!customer) return <div>Customer not found.</div>;
  if (!nanny) return <div>Nanny not found.</div>;
  if (!newEvent) return <div>Loading event form...</div>; // Render loading state until newEvent is set
  return (
    <div>
      <h1>Calendar</h1>
      {/* {showModal && (
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-auto bg-gray-900 bg-opacity-50">
            <div className="relative bg-white rounded-lg mx-4 md:max-w-md p-6 w-full">
              <p>Add an appointment on dates when the nanny indicates availability. For dates without specific availability or unavailability (empty slots), you're welcome to schedule events. However, please be aware there's no guarantee the nanny will be available.</p>
              <div className='mt-4 flex justify-center'>
                <Image src={um} width={80} height={80} alt='..' layout='fixed' />
              </div>
              <button onClick={() => setShowModal(false)} className="absolute top-0 right-0 m-2">
                <Image src={cancel} width={20} height={20} alt='..' layout='fixed' />
              </button>
            </div>
          </div>
        )} */}
      <h2>Add New Event</h2>
   
      <div>
        <input
          type="text"
          placeholder="Add Title"
          style={{ width: "20%", marginRight: "10px" }}
          value={newEvent.title}
          onChange={(e) => setNewEvent({ ...newEvent, title: e.target.value })}
        />
        <div style={{ marginRight: "10px" }}>
          <DatePicker
            placeholderText="Start Date"
            selected={newEvent.start}
            onChange={(date: Date) =>
              setNewEvent({ ...newEvent, start: date || new Date() })
            } 
            minDate={new Date()}
            showTimeSelect
            dateFormat="MMMM d, yyyy h:mm aa"
          />
        </div>
        <div>
          <DatePicker
            placeholderText="End Date"
            selected={newEvent.end}
            onChange={(date: Date) =>
              setNewEvent({ ...newEvent, end: date || new Date() })
            }
               minDate={new Date()}
            showTimeSelect
            dateFormat="MMMM d, yyyy h:mm aa"
          />
        </div>
        <button style={{ marginTop: "10px" }} onClick={handleAddEvent}>
          Add Event
        </button>
      </div>
      <Calendar
        localizer={localizer}
        events={allEvent}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 500 }}
      />
    </div>
  );
}
