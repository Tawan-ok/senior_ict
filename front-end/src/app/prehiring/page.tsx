'use client';

import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import { useRouter } from 'next/navigation';
import jwt_decode from 'jwt-decode';

type Props = {};

type SelectedNanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
    descriptions: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
    availability_slots: availability_slots[];
};


type Customer = {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    district: string;
    subDistrict: string;
    province: string;
    zipCode: string;
    streetNumber: string;
    contactNumber: string;
    age: number;
    reviewScoreWeight: number;
    successfulJobsWeight: number;
    gender: string;
    role: string;
    locationall: string;
    profileImageUrl: string | null;
};

type favoriteNanny = {

    customer_id: number;
    nanny_id: number;
}

type favoriteNannytest = {
    id: number;
    customer_id: number;
    nanny_id: number;
}

type availability_slots = {
    availability_slots_id: number;
    end_time: Date;
    is_available: Boolean;
    start_time: Date;
    nanny_id: number;
}


type availability_slotsTest = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    availability_slots_id: number;
    end_time: Date;
    is_available: Boolean;
    start_time: Date;
    nanny_id: number;
}

interface Slot {
    is_available: boolean;
    start_time: string;
    end_time: string;
}

interface Event {
    title: string;
    start: Date;
    end: Date;
    isAvailable: boolean;
    type?: string;
}

interface AppointmentResponse {
    title: string;
    customer_id: number;
    nanny_id: number;
    start_time: string;
    end_time: string;
    status: string;
    interviewDetails?: string;
}


export default function HiringPage({ }: Props) {
    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [hire, setHire] = useState<availability_slotsTest[]>([]);
    const [startDate, setStartDate] = useState<string>('');
    const [endDate, setEndDate] = useState<string>('');
    const [customer, setCustomer] = useState<Customer | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const compareSectionRef = useRef<HTMLDivElement>(null);
    const [hiretest, sethiretest] = useState<availability_slotsTest[]>([]);
    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-user');
    };

    const handleStartDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setStartDate(event.target.value); 
    };

    const handleEndDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEndDate(event.target.value); 
    };

    const handleSubmit = () => {
        if (!startDate || !endDate) {
            alert('Please input both start and end dates.');
            return;
        }

        const query = `?startDate=${encodeURIComponent(startDate)}&endDate=${encodeURIComponent(endDate)}`;
        router.push(`/hiring${query}`);
    };


    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!userId) {
                setError("User ID not found in token.");
                setLoading(false);
                return;
            }

            console.log("User ID:", userId);

            const fetchData = async () => {
                try {
                    const response = await axios.get('http://localhost:9000/api/nannies');
                    // const response = await axios.get('http://35.213.139.253:9000/api/nannies');
                    const response2 = await axios.get('http://localhost:9000/api/nannies/with-availability-slots');
                    // const response2 = await axios.get('http://35.213.139.253:9000/api/nannies/with-availability-slots');
                    const response3 = await axios.get('http://localhost:9000/api/availability');
                    // const response3 = await axios.get('http://35.213.139.253:9000/api/availability');
                    const response1 = await axios.get<Customer>(
                        `http://localhost:9000/api/customers/${userId}`
                        // `http://35.213.139.253:9000/api/customers/${userId}`
                    );
                    sethiretest(response3.data);
                    setNannies(response.data);
                    setCustomer(response1.data);
                    setLoading(false);
                    setHire(response2.data);
                    console.log("Nanny" + response.data);
                    console.log(response2.data);
                    console.log("availability" + response3.data);
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
    }, []);

    return (
        <div style={{ fontFamily: 'Montserrat' }}>
            <div className="mt-3 text-center text-3xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white font-montserrat">
                When to hire?
            </div>
            <div className="flex flex-col items-center sm:flex-row sm:justify-center mt-8 bg-white rounded-lg shadow-lg p-4 m-4 border border-pink-300">
                <label htmlFor="start-date" className="text-black mt-4 sm:mt-0 mr-4">Start Date:</label>
                <input type="date" id="start-date" onChange={handleStartDateChange} className="mt-2 p-2 border border-pink-500 rounded-lg focus:outline-none focus:border-pink-700 mr-4" />
                <label htmlFor="end-date" className="text-black mt-4 sm:mt-0 ml-4 sm:ml-0 sm:mr-4">End Date:</label>
                <input type="date" id="end-date" onChange={handleEndDateChange} className="mt-2 p-2 border border-pink-500 rounded-lg focus:outline-none focus:border-pink-700" />
                <div className="flex flex-col mr-2 mt-4 sm:flex-row sm:mt-0 sm:ml-4 sm:mr-4">
                    <button onClick={handleSubmit} className="bg-pink-300 border-2 border-solid border-pink-400 rounded-lg text-white py-2 px-4 hover:bg-pink-700 focus:outline-none focus:bg-pink-700">Search</button>
                </div>
            </div>
        </div>
    );
}