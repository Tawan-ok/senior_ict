'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/UserProfile.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
import Customerpage from '@/app/testupload/page';
import 'bootstrap/dist/css/bootstrap.min.css';
type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
    profile_image_url: string;
};

type Admin = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    age: number;
    gender: string;
    role: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    profile_image_url: string;
};
type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
    sub: string;
    exp: number;
    a: Role[];
};
// ของเก่า 
export default function CustomersPage({ }: Props) {
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    // const [customer, setcustomer] = useState<Customer | null>(null);
    const [customer, setCustomer] = useState<Customer | null>(null);

    const [editing, setEditing] = useState<boolean>(false);
    const [editedCustomer, setEditedCustomer] = useState<Customer | null>(null);
    const editableFields: (keyof Customer)[] = ['first_name', 'last_name', 'email', 'contact_number', 'district'];
    const router = useRouter(); // Initialize the router

    const handleExit = () => {
        localStorage.removeItem('jwt'); // Remove the JWT token
        router.push('/login-user'); // Redirect to /login
    };

    const handleEdit = () => {
        if (customer) {
            setEditing(true);
            setEditedCustomer({ ...customer });
        }
    };

    const EditImage = () => {
        router.push('/testupload');
    };

    const Home = () => {
        router.push('/home');
    };

    const handleSave = async () => {
        try {
            const response = await axios.put<Customer>(
                // `http://localhost:9000/api/customers/${customer?.id}`,
                `http://localhost:9000/api/customers/${customer?.id}`,
                editedCustomer
            );
            setCustomer(response.data);
            setEditing(false);
        } catch (err) {
            if (err instanceof Error) {
                setError(err.message);
            } else {
                setError('An error occurred.');
            }
        }
    };

    function refreshPage() {
        window.location.reload();
    }

    const handleInputChange = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        setEditedCustomer({
            ...editedCustomer!,
            [e.target.name]: e.target.value,
        });


    };

    useEffect(() => {
        const token = localStorage.getItem('jwt');

        if (token) {
            const decodedToken: any = jwt_decode(token);
            if (!decodedToken.a.includes('USER')) {
                setError(
                    'Access denied. You do not have the required permissions.'
                );
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }

            const userId: number = decodedToken.sub;

            if (!userId) {
                setError('User ID not found in token.');
                setLoading(false);
                return;
            }

            const fetchData = async () => {
                try {
                    const response = await axios.get<Customer>(
                        `http://localhost:9000/api/customers/${userId}`
                        // `http://35.213.139.253:9000/api/customers/${userId}`
                    );
                    setCustomer(response.data);
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
    }, []);


    if (loading) return <div>Loading...</div>;
    if (error) return <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white">Error: {error}</div>;
    if (!customer) return <div>Customer Null ...</div>;
    
    return (
        <div className={styles.rootContainer}>
            <div className="mt-2 mb-8 text-center text-3xl md:text-4xl lg:text-4xl xl:text-4xl font-bold">
                <span style={{ fontFamily: 'Montserrat' }} className="text-white">Edit Profile</span>
            </div>
            <div className="flex justify-center">
                <div className="flex flex-col items-center">
                    <div className="profileImageContainer">
                        <Image className={styles.profileImage} src={"data:image/png;base64," + customer.profile_image_url}
                            alt="" width={150} height={150} layout="fixed" />
                    </div>
                    <div className='mt-2'>
                        <button
                            onClick={EditImage}
                            style={{ fontFamily: 'Montserrat' }}
                            className="bg-blue-500 text-white px-4 py-2 rounded-md focus:outline-none focus:ring focus:border-blue-300 mb-2"
                        >
                            Edit Image
                        </button>
                    </div>
                </div>
            </div>

            <div className="mt-4 mb-8 flex flex-col items-center justify-center">
                <div className={styles.logoweb}></div>
                <div className={styles.cardcontainer}>
                    <div className={styles.card} key={customer?.username}>
                        <div className="block items-center justify-center">
                            <div className={styles.groupbanner}>
                                {editing ? (
                                    <>
                                        <div style={{ fontFamily: 'Montserrat' }} className="flex flex-col lg:grid lg:grid-cols-2 gap-2 text-start items-center">
                                            {editableFields.map((field) => (
                                                <div key={field} className="mb-4 flex flex-col w-full">
                                                    <label className="text-black mb-1 font-medium">{field.replace(/_/g, ' ')}</label>
                                                    <input
                                                        type="text"
                                                        name={field}
                                                        value={editedCustomer ? editedCustomer[field] : ''}
                                                        onChange={handleInputChange}
                                                        className="bg-white border rounded-3xl px-3 py-2 w-full"
                                                    />
                                                </div>
                                            ))}
                                        </div>
                                    </>
                                ) : (
                                    <div>
                                        {editableFields.map((field) => (
                                            <p key={field} style={{ fontFamily: 'Montserrat' }} className="flex flex-col lg:grid lg:grid-cols-2 gap-2 text-start items-center">
                                                <span className="text-start text-black mb-1 text-2xl font-medium" >{field.replace(/_/g, ' ')}</span>
                                                <span className="p-2 bg-white text-black mb-1 font-normal rounded-3xl">{customer ? customer[field] : ''}</span>
                                            </p>
                                        ))}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="mt-2 flex items-center justify-center">
                            {!editing && (
                                <div className="flex items-center">
                                    <button
                                        onClick={handleEdit}
                                        style={{ fontFamily: 'Montserrat' }}
                                        className="bg-blue-500 text-white px-4 py-2 rounded-md focus:outline-none
                                        focus:ring
                                        focus:border-blue-300"
                                    >Edit</button>
                                </div>
                            )}
                            {editing &&
                                <button
                                    onClick={() => {
                                        handleSave();
                                        refreshPage();
                                    }}
                                    className="mt-2 bg-green-500 text-white px-4 py-2 rounded-md focus:outline-none focus:ring focus:border-green-300"
                                >Save</button>}
                        </div>
                        <button onClick={() => {
                                        Home();
                                        
                                    }}  className="bg-blue-500 text-white px-4 py-2 rounded-md focus:outline-none
                                        focus:ring
                                        focus:border-blue-300">Confirm</button>
                    </div>
                </div>
            </div>
        </div>

    );
}

