'use client'
// CustomerService.tsx
import React, { useState } from 'react';
import RootLayout from "../layout";
import styles from "../../../styles/CustomerService.module.css";
import message from "../../../assets/Dialog.png"
import phonesignal from "../../../assets/phonesignal.png"
import Image from "next/image";

export default function CustomerService() {
    const [comment, setComment] = useState("");

    const handleCommentChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setComment(event.target.value);
    };

    const handleSubmitComment = () => {
        console.log('Comment submitted:', comment);
        setComment('');
    };

    return (
        <RootLayout showNavbar={true} showFooter={false}>
            <div className={styles.rootContainer}>
                <div className="mt-10 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-white">Customer Service</span>
                </div>
                <div className={styles.headerBanner}>
                    Choose one method to contact us
                </div>
                <div className={styles.container}>
                    <div className={styles.left}>
                        <Image
                            src={message}
                            alt={""}
                            className={styles.centeredImage}
                        />
                        <p className={styles.chatMessage}>Chat with us via email nanny.rightnow@hotmail.com</p>
                    </div>
                    <div className={styles.right}>
                        <Image
                            src={phonesignal}
                            alt={""}
                            className={styles.centeredImage}
                        />
                        <p className={styles.chatMessage}>Call with our call-center +66-xxxxxxxxxxxx</p>
                    </div>
                </div>
                {/* <div style={{ fontFamily: 'Montserrat' }} className="mt-10 flex justify-center items-center flex-col">
                    <div className="mb-4">
                        <p className="text-center text-gray-800 font-semibold">Quick Session for emergency help</p>
                    </div>
                    <textarea
                        className="border border-gray-400 rounded-lg p-4 w-96 h-40 text-gray-700 focus:outline-none focus:border-blue-500"
                        placeholder="Write your comment here..."
                        value={comment}
                        onChange={handleCommentChange}
                    />
                    <button
                        className="mt-4 bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 transition-colors duration-300"
                        onClick={handleSubmitComment}
                    >
                        Submit
                    </button>
                </div> */}

            </div>
        </RootLayout>
    );
}
