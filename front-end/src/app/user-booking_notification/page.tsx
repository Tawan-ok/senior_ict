'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
import { addHours } from 'date-fns';
import bell from '../../../assets/bell.png';
type Props = {};



type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    hours: number
};

type BookingHistory = {
    id: number,
    booking_id: number,
    status: string,
    time_session: number
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
    sub: string;
    exp: number;
    a: Role[];
};
type Appointment = {
    id: number;
    title: string;
    start_time: string;
    end_time: string;
    interview_details: string;
};

type Notification = {
    id: number;
    message: string;
    appointment_id: number;
    status: string;
};
// ของเก่า 
export default function CustomersPage({ }: Props) {

    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
    const [nanny, setnanny] = useState<Nanny | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const router = useRouter(); // Initialize the router
    const [notifications, setNotifications] = useState<Notification[]>([]);
    const [appointmentDetails, setAppointmentDetails] = useState<Map<number, Appointment>>(new Map());
    const handleExit = () => {
        localStorage.removeItem('jwt'); // Remove the JWT token
        router.push('/login-nanny'); // Redirect to /login
    };
    const handleinterview = () => {
        router.push('/user-interview_notification');
    };
    const handlebooking = () => {
        router.push('/user-booking_notification');
    };
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        // Decode the JWT to extract user ID
        if (token) {
            const decodedToken: any = jwt_decode(token);
            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }
            // Extract user ID from the "sub" key in JWT
            const userId: number = decodedToken.sub;

            if (!userId) {
                setError("User ID not found in token.");
                setLoading(false);
                return;
            }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    const response = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getfindBookingsPaidAndCancleCustomer/${userId}`);
                    // const response = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/bookingqueue/getbookingsbycustomer/${userId}`);
                    setbookingqueue(response.data);
                    console.log("118" + response.data);
                    const notificationsResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}`);
                    const notificationsConfirmedResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}?status=confirmed`);
                    const notificationsDeniedResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}?status=denied`);
                    const notificationsPaddingResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}?status=pending`);
                    // const notificationsResponse = await axios.get<Notification[]>(`http://35.213.139.253:9000/api/notifications/for-user/${userId}`);
                    const mergedNotifications = [...notificationsConfirmedResponse.data, ...notificationsDeniedResponse.data, ...notificationsPaddingResponse.data];
                    setNotifications(mergedNotifications);
                    const appointmentIds = notificationsResponse.data.map((notification) => notification.appointment_id);
                    const appointments = await Promise.all(
                        appointmentIds.map((id) => axios.get<Appointment>(`http://localhost:9000/api/appointments/${id}`))
                        // appointmentIds.map((id) => axios.get<Appointment>(`http://35.213.139.253:9000/api/appointments/${id}`))
                    );
                    const newAppointmentDetails = new Map();
                    appointments.forEach((response) => {
                        const appointment = response.data;
                        newAppointmentDetails.set(appointment.id, appointment);
                    });
                    setAppointmentDetails(newAppointmentDetails);


                    setLoading(false);
                    // const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${userId}`);
                    // setnanny(response1.data);
                    const nannyIds = response.data.map((queue) => queue.nanny_id);
                    const nanniesResponse = await Promise.all(
                        nannyIds.map((nannyId) =>
                            axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${nannyId}`)
                            // axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getby/${nannyId}`)
                        )
                    );
                    const nanniesData = nanniesResponse.map((res) => res.data);
                    setNannies(nanniesData);
                    console.log(nanniesData);
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-nanny');
        }
    }, []);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!nannies) return <div>Nanny Not found {error}</div>;
    if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;


    const handleCancelBooking = async (queueId: number) => {
        try {
            // Send a request to the API to cancel the booking using the queue ID
            await axios.delete(`http://localhost:9000/api/bookingqueue/delete/${queueId}`);
            // await axios.delete(`http://35.213.139.253:9000/api/bookingqueue/delete/${queueId}`);

            // Reload the page after cancellation
            
            
            router.push('/hiring');
        } catch (err) {
            if (err instanceof Error) {
                console.error(err.message);
            } else {
                console.error('An error occurred while canceling the booking.');
            }
        }
    };



    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="mt-8 px-4">
                <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-pink-600">Booking Notification</span>
                </div>


                <div className="flex justify-center mt-8">
                    <div className="mr-4">
                        <button onClick={() => handleinterview()} className="bg-pink-400 border border-pink-200 hover:bg-pink-600 text-white font-semibold px-4 py-2 rounded-md transition duration-300 ease-in-out">
                            Interview
                        </button>
                    </div>
                    <div>
                        <button onClick={() => handlebooking()} className="bg-white border border-pink-500 hover:bg-pink-600 text-black  font-semibold px-4 py-2 rounded-md transition duration-300 ease-in-out">
                            Booking
                        </button>
                    </div>
                </div>


                {bookingqueue.length > 0 ? (
                    bookingqueue.map((queue) => (
                        <div style={{ fontFamily: 'Montserrat', }} className="mt-3 mb-3 text-center text-black text-xl sm:text-xl md:text-2xl lg:text-2xl xl:text-2xl 2xl:text-3xl font-semibold" key={queue.id}>
                            <div className="bg-pink-100 rounded-lg shadow-md p-6 mx-5">
                                <p className="text-xl font-semibold mb-4">Booking Details</p>
                                <div className="grid grid-cols-2 gap-2">
                                    <div className="mb-4">
                                        <p className="text-gray-700 font-medium">Nanny Name:</p>
                                        <p className="text-gray-600">{nannies.find(nanny => nanny.id === queue.nanny_id)?.first_name} {nannies.find(nanny => nanny.id === queue.nanny_id)?.last_name}</p>
                                    </div>
                                    <div className="mb-4">
                                        <p className="text-gray-700 font-medium">Start Date:</p>
                                        <p className="text-gray-600">{new Date(queue.start_date).toLocaleString('en-US', { weekday: 'short', year: 'numeric', month: 'short', day: '2-digit', hour12: true, hour: '2-digit', minute: '2-digit', second: '2-digit' })}</p>
                                    </div>
                                    <div className="mb-4">
                                        <p className="text-gray-700 font-medium">End Date:</p>
                                        <p className="text-gray-600">{new Date(queue.end_date).toLocaleString('en-US', { weekday: 'short', year: 'numeric', month: 'short', day: '2-digit', hour12: true, hour: '2-digit', minute: '2-digit', second: '2-digit' })}</p>
                                    </div>

                                    <div className="mb-4">
                                        <p className="text-gray-700 font-medium">Total Amount:</p>
                                        <p className="text-gray-600">{queue.total_amount}</p>
                                    </div>
                                    <div className="mb-4">
                                        <p className="text-gray-700 font-medium">Hours:</p>
                                        <p className="text-gray-600">{queue.hours}</p>
                                    </div>
                                </div>
                            </div>

                            {queue.status_payment === 'Paid' && (
                                <div className='bg-pink-200 border border-pink-400 border-l-6 shadow-lg rounded-lg p-5 mt-2 text-lg'>
                                    <p className='text-black'>Continue Booking</p>
                                    {nannies && nannies.length > 0 && (
                                        <div className="mt-2 mb-2">
                                            <Link href={`/confirmhiring/${nannies.find(nanny => nanny.id === queue.nanny_id)?.username}`}>
                                                <a className='mt-4 p-2 bg-pink-600 text-white rounded-xl'>OK</a>
                                            </Link>
                                        </div>
                                    )}
                                </div>
                            )}

                            {queue.status_payment === 'Cancle' && (
                                <div className='mt-2 inline-block text-lg'>
                                    <div className='bg-pink-200 border border-pink-400 text-white p-4 rounded-lg inline-block'>
                                        <p className='text-black'>Additional Cancellation Information</p>
                                        <button onClick={() => handleCancelBooking(queue.id)} className="mt-4 bg-red-500 hover:bg-red-600 text-white font-semibold px-4 py-2 rounded-md transition duration-300 ease-in-out">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            )}
                        </div>
                    ))
                ) : (
                    <div className='mt-4 p-6 bg-white m-8 flex flex-col items-center justify-center rounded-md border border-pink-300'>
                        <Image src={bell} width={100} height={100} alt='..' layout='fixed' />
                        <p className='mt-4'>No notifications</p>
                    </div>
                )}
            </div>
        </div>


    );
}

