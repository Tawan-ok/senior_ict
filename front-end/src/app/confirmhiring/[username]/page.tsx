'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"
import { useRouter } from 'next/navigation'
import jwt_decode from 'jwt-decode';
import nanny1 from '../../../../assets/hanni.png'
import Image from 'next/image';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
};


type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type BookingHistory = {
    booking_id: number,
    status: string,
    time_session: number,
    nanny_id: number
};

type ValuePiece = Date | null;

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}



export default function Page({ params }: { params: { username: string } }) {

    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
    const [clicked, setClicked] = useState(false);
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [response1, setResponse1] = useState<Nanny | null>(null);
    const [startdate, setstartDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [enddate, setEndtDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })


    const getstartTimes = () => {
        if (!startdate.justDate) return

        const { justDate } = startdate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const starttimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            starttimes.push(i)
        }
        return starttimes
    }

    const getendTimes = () => {
        if (!enddate.justDate) return

        const { justDate } = enddate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const endtimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            endtimes.push(i)
        }
        return endtimes
    }

    const starttimes = getstartTimes()
    const endtimes = getendTimes()
    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-admin');
    };

    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError("User ID not found in token.");
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
              }
        
            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    if (!customer || !nanny) {
                        const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                        // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
                        const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                        // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
                        setCustomer(response.data);
                        setNanny(response1.data);

                        if (response.data && response1.data) {
                            const response2 = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getbookingstatus/${response.data.id}/${response1.data.id}`);
                            // const response2 = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/bookingqueue/getbookingstatus/${response.data.id}/${response1.data.id}`);
                            setBookingqueue(response2.data);
                            console.log("All DATA Booking ", response2.data);
                            // Loop through bookingqueue and create BookingHistory for each booking

                        }
                    }
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes
    }, [params.username, response1]);

    // Confirm Hiring เป็นหน้ากดปุ่ม Confirm เพื่อ post ข้อมูล ลง Booking History เพื่อไปเรียกใช้ ในหน้า success fully hiring

    const createBookingHistory = async () => {
        if (clicked) {
            alert("Already Confirm");
        }
        try {

            if (!nanny) {
                console.error('Nanny data is missing or null.');
                return;
            }
            // ต้องทำระบบ ปรับเสตตัสปุ่ม เป็น Bookings
            const bookingIds = bookingqueue.map((bookingqueues) => bookingqueues.id);

            for (const bookingId of bookingIds) {
                const bookingqueueItem = bookingqueue.find((item) => item.id === bookingId);
                if (!bookingqueueItem) {
                    console.error(`Booking queue with ID ${bookingId} not found.`);
                    continue;
                }
                const bookingHistory = {
                    booking_id: bookingId,
                    status: "Process",
                    // time_session: 10,
                    time_session: bookingqueueItem.hours,
                    nanny_id: nanny.id,
                };

                const response = await axios.post<BookingHistory>('http://localhost:9000/api/bookinghistory', bookingHistory);
                // const response = await axios.post<BookingHistory>('http://35.213.139.253:9000/api/bookinghistory', bookingHistory);
                console.log('Booking history created successfully for booking ID:', bookingId);

                await axios.put(`http://localhost:9000/api/bookingqueue/updateStatusBookings/${bookingId}`, {
                    // await axios.put(`http://35.213.139.253:9000/api/bookingqueue/updateStatusBookings/${bookingId}`, {
                    status: 'Bookings',
                });
            }
            // alert("Booking created successfully");
            setClicked(true);
        } catch (error) {
            alert("Booking created unsuccessfully");
            console.error('Error creating booking history:', error);
        }
        router.push(`/successfullyhiring/${nanny?.username}`);
    };

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;

    const formatDateToISO = (dateString: string | number | Date, includeTime: boolean = false) => {
        const date = new Date(dateString);
        const day = date.getDate();
        const month = date.toLocaleString('default', { month: 'long' });
        const year = date.getFullYear();
        const hour = date.getHours();
        const minute = date.getMinutes();

        const paddedHour = hour.toString().padStart(2, '0');
        const paddedMinute = minute.toString().padStart(2, '0');

        let formattedDate = `${day} ${month} ${year}`;
        if (includeTime) {
            formattedDate += ` ${paddedHour}:${paddedMinute}`;
        }

        return formattedDate;
    };

    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="block justify-center">
                <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-white">Hiring Process</span>
                </div>
                <div className="mt-5 bg-white p-8 border border-pink-300 rounded-lg shadow-md md:w-2/3 lg:w-1/2 xl:w-2/5 mx-auto">
                    <div className="rounded-lg border border-pink-400 shadow-md flex flex-col items-center justify-center md:flex-row md:items-center p-8"> 
                        <div className="mb-4 md:mr-4">
                            <div className="w-36 h-36 overflow-hidden rounded-full shadow-md">
                                <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                            </div>
                        </div>
                        <div className="ml-5">
                            <div className="mb-6">
                                <h2 className="text-lg font-bold mb-2 underline">Nanny Detail</h2>
                                <p className="mb-1"><span className="font-semibold">Name:</span> {nanny.first_name} {nanny.last_name}</p>
                                <p className="mb-1"><span className="font-semibold">Email:</span> {nanny.email}</p>
                                <p className="mb-1"><span className="font-semibold">Contact Number:</span> {nanny.contact_number}</p>
                                <p className="mb-1"><span className="font-semibold">Type of Work:</span> {nanny.type_work === 'F' ? 'Full-Time' : nanny.type_work === 'P' ? 'Part-Time' : nanny.type_work === 'A' ? 'Full-Time & Part-Time' : nanny.type_work} </p>
                                <p className="mb-1"><span className="font-semibold">Role Level:</span> {nanny.role_level}</p>
                            </div>
                        </div>
                    </div>


                    <div className="mt-2 bg-pink-100 p-8 rounded-lg shadow-md"> 
                        {bookingqueue.map((bookingqueues) => (
                            bookingqueues.status_payment === 'Paid' && (
                                <div key={bookingqueues.id} className="mb-4">
                                    <h2 className="text-lg font-bold mb-2 underline">Booking Details</h2>
                                    <p className="mb-1">Transaction Number: {bookingqueues.id}</p>
                                    <p className="mb-1">Customer Name: {customer?.first_name}  {customer?.last_name} </p>
                                    <p className="mb-1">Nanny Name: {nanny?.first_name}  {nanny?.last_name} </p>
                                    <p className="mb-1">Location Hiring: {bookingqueues.locationhiring}</p>
                                    <p className="mb-1">Hours: {bookingqueues.hours}</p>
                                    <p className="mb-1">Total Amount: {bookingqueues.total_amount}</p>
                                    <p className="mb-1"><span className="font-semibold">Start Date:</span> {formatDateToISO(new Date(bookingqueues.start_date), true)}</p>
                                    <p className="mb-1"><span className="font-semibold">End Date:</span> {formatDateToISO(new Date(bookingqueues.end_date), true)}</p>
                                </div>
                            )
                        ))}
                    </div>
                </div>


                <div className="mt-4 mb-6 flex justify-center">
                    <button className="px-8 py-3 bg-black text-white rounded-lg mr-3 hover:bg-gray-800" onClick={() => createBookingHistory()} disabled={clicked}>Confirm</button>
                </div>
            </div>

        </div>
    );

}