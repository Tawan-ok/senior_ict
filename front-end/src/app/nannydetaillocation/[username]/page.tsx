'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import jwt_decode from 'jwt-decode';
import { Autocomplete } from '@react-google-maps/api';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';
import Image from 'next/image';
import cancel from '../../../../assets/cancel.png';

type Props = {};

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};

type BookingQueue = {
  id: number,
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number

};


type BookingQueuetest = {
  id: number;
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number
};

// type MapboxComponentProps = {
//   center: LngLatLike; // Pass center as a prop
// };
interface MapComponentProps {
  onPositionChange: (location: { lat: number; lng: number } | null) => void;
}

interface MapConverterProps {
  lng?: number;
  lat?: number;
}

export default function Page({ params }: { params: { username: string } }) {
  const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
  const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
  const [booking, setBooking] = useState<BookingQueue | null>(null); // Initialize as null
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();
  const [loadingLocation, setLoadingLocation] = useState<boolean>(true);
  const [showMap, setShowMap] = useState<boolean>(false); // New state for map visibility
  const [map1, setMap1] = useState<google.maps.Map | null>(null);
  const [currentLocation, setCurrentLocation] = useState<{ lat: number; lng: number } | null>(null);
  const [markerPosition, setMarkerPosition] = useState<{ lat: number; lng: number } | null>(null);
  const [testselect, settestselect] = useState<string | null>(null);
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyCswn_uJTZYtbXDTOWXXqES3p43ONV1Co0', // Replace with your Google Maps API key
    libraries: ['places'],
  });

  const [markerMoved, setMarkerMoved] = useState<boolean>(false);
  const [searchBox, setSearchBox] = useState<google.maps.places.Autocomplete | null>(null);

  const handleExit = () => {
    localStorage.removeItem('jwt');
    router.push('/login-admin');
  };

  const handleMarkerDragEnd = (e: google.maps.MouseEvent) => {
    const newPosition = e.latLng ? { lat: e.latLng.lat(), lng: e.latLng.lng() } : null;
    setMarkerPosition(newPosition);
    handleLocationChange(newPosition);
    setMarkerMoved(true);
    // Center the map on the new marker position
    setCurrentLocation(newPosition);
  };

  const handleLocationChange: MapComponentProps['onPositionChange'] = (location) => {
    // Do something with the selected location
    console.log('Selected Location:', location);
  };

  const handleConfirmLocation = () => {
    // When the user confirms the location, show an alert with the converted address
    if (markerMoved && markerPosition) {
      const { lat, lng } = markerPosition;
      const geocoder = new window.google.maps.Geocoder();

      const latlng = new window.google.maps.LatLng(lat, lng);

      geocoder.geocode({ location: latlng }, (results, status) => {
        if (status === 'OK') {
          if (results && results[0]) {
            const formattedAddress = results[0].formatted_address;
            alert(`Selected Address: ${formattedAddress}`);
          } else {
            alert('No results found');
          }
        } else {
          alert(`Geocoder failed due to: ${status}`);
        }
      });
    } else {
      alert('Please move the marker before confirming the location.');
    }
  };

  const MapConverter: React.FC<MapConverterProps> = ({ lng, lat }) => {

    useEffect(() => {

      navigator.geolocation.getCurrentPosition(
        (position) => {
          console.log('Geolocation success:', position);
          // ... rest of the code
        },
        (error) => {
          console.error('Geolocation error:', error);
          setLoadingLocation(false);  // Set loadingLocation to false even if there's an error
        }
      );
      // Ensure the Google Maps API is loaded
      if (window.google && lng !== undefined && lat !== undefined) {
        const geocoder = new window.google.maps.Geocoder();

        const latlng = new window.google.maps.LatLng(lat, lng);

        geocoder.geocode({ location: latlng }, (results, status) => {
          if (status === 'OK') {
            if (results && results[0]) {
              const formattedAddress = results[0].formatted_address;
              settestselect(formattedAddress);
              console.log('Formatted Address:', formattedAddress);
              // Do something with the formatted address
            } else {
              console.error('No results found');
            }
          } else {
            console.error(`Geocoder failed due to: ${status}`);
          }
        });
      }
    }, [lng, lat]);

    return null; // or you can return a component if needed
  };

  useEffect(() => {
    const token = localStorage.getItem('jwt');
    if (token) {
      const decodedToken: any = jwt_decode(token);

      const userId: number = decodedToken.sub;

      if (!decodedToken.a.includes('USER')) {
        setError('Access denied. You do not have the required permissions.');
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }
      console.log("User ID:", userId);
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          setCurrentLocation({ lat: latitude, lng: longitude });
          setMarkerPosition({ lat: latitude, lng: longitude });
          handleLocationChange({ lat: latitude, lng: longitude });
          setLoadingLocation(false);  // Set loadingLocation to false once the location is fetched
        },
        (error) => {
          console.error('Error getting user location:', error);
          setLoadingLocation(false);  // Set loadingLocation to false even if there's an error
        }
      );
      const fetchData = async () => {
        try {
          // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
          const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
          // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
          const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
          const response3 = await axios.get<BookingQueue>(`http://localhost:9000/api/bookingqueue/getbookingstatuspending/${userId}/${response1.data.id}`);
          // const response3 = await axios.get<BookingQueue>(`http://35.213.139.253:9000/api/bookingqueue/getbookingstatuspending/${userId}/${response1.data.id}`);
          setCustomer(response.data);
          setNanny(response1.data);
          setBooking(response3.data);
          console.log(response.data);
          console.log(response1.data);
          console.log(response3.data);
        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
        } finally {
          setLoading(false);
        }
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-user');
    }
  }, [params.username]);

  const onLoad = (autocomplete: google.maps.places.Autocomplete) => {
    setSearchBox(autocomplete);
  };
  const onPlaceChanged = () => {
    if (searchBox) {
      const place = searchBox.getPlace();
      if (place.geometry && place.geometry.location) {
        const newPosition = {
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
        };
        setMarkerPosition(newPosition);
        setCurrentLocation(newPosition); // Set the current location to the searched position
        handleLocationChange(newPosition);
        setMarkerMoved(true);
      } else {
        alert('Cannot find the selected location. Please try again.');
      }
    }
  };

  const handleUpdateLocation = async () => {
    try {
      // Make the API call to update location for hiring

      // await axios.put(`http://35.213.139.253:9000/api/bookingqueue/updateLocationForHiring/${booking.id}/${testselect}`);
      // await axios.put(`http://localhost:9000/api/bookingqueue/updateLocationForHiring/${booking.id}/${testselect}`);

      // await axios.put(`http://localhost:9000/api/bookingqueue/updateLocationForHiring/${booking?.id}/${testselect}`);
      await axios.put(`http://localhost:9000/api/bookingqueue/updateLocationForHiring/${booking?.id}/${testselect}`);

      alert('Location for hiring updated successfully');
    } catch (error) {
      console.error('Error updating location for hiring:', error);
      alert('Error updating location for hiring');
    }
    // router.push(`/nannydetail2/${nanny?.username}`);
  };

  const handleUpdateLocationall = async () => {
    try {
      // Make the API call to update location for hiring

      // await axios.put(`http://35.213.139.253:9000/api/bookingqueue/updateLocationForHiring/${booking.id}/${customer.locationall}`);
      // await axios.put(`http://localhost:9000/api/bookingqueue/updateLocationForHiring/${booking.id}/${customer.locationall}`);

      // await axios.put(`http://localhost:9000/api/bookingqueue/updateLocationForHiring/${booking?.id}/${customer?.locationall}`);
      await axios.put(`http://localhost:9000/api/bookingqueue/updateLocationForHiring/${booking?.id}/${customer?.locationall}`);

      alert('Location for hiring updated successfully');
    } catch (error) {
      console.error('Error updating location for hiring:', error);
      alert('Error updating location for hiring');
    }
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;


  if (!nanny) return <div>Nanny not found.</div>;

  return (
    <div>
      <div className="block justify-center">
        <div className="text-center text-5xl md:text-5xl lg:text-5xl xl:text-5xl 2xl:text-5xl font-bold">
          <span style={{ fontFamily: 'Montserrat', }} className="text-white">Choose Location</span>
        </div>
        <div className="mt-5 bg-white p-5 rounded-2xl shadow-lg md:w-2/3 lg:w-1/2 xl:w-2/5 mx-auto">
          <div className="mt-3 text-center text-2xl md:text-2xl lg:text-2xl xl:text-2xl 2xl:text-2xl font-bold">
            <span style={{ fontFamily: 'Montserrat', }} className="text-black">Where do you want to get a nanny ?</span>
          </div>

          <div className="flex flex-col  sm:flex-row">
            {/* Shared location details */}
            <div className="mt-5  bg-pink-200 border-2 border-solid border-pink-300 p-5 shadow-lg mb-5 sm:mb-0 sm:mr-5 w-full sm:w-2/3 lg:w-1/2 xl:w-3/5 mx-auto">
              <div style={{ fontFamily: 'Montserrat', }} className="mt-3 text-center text-2xl font-bold text-white font-montserrat">Home</div>

              <Link href={`/nannydetail2/${nanny.username}`}>
                <button style={{ fontFamily: 'Montserrat', }} className="mt-3 text-sm rounded-lg text-white bg-pink-300 hover:bg-pink-600" onClick={handleUpdateLocationall}>Location: {customer?.locationall}</button>
              </Link>

            </div>

            {/* Enter New Location */}
            <div className="mt-5  bg-pink-200 border-2 border-solid border-pink-300  p-5 shadow-lg w-full sm:w-2/3 lg:w-1/2 xl:w-3/5 ml-5 mx-auto">
              <div style={{ fontFamily: 'Montserrat', }} className="mt-3 text-center text-2xl font-bold text-white font-montserrat">New Location</div>

              <div key={nanny.username} className="flex flex-col">
              </div>
              <div className='flex justify-between mt-4'>
                <button style={{ fontFamily: 'Montserrat', }} className="bg-pink-300 text-white py-2 px-4 rounded-lg hover:bg-pink-600" onClick={() => setShowMap(true)}>Enter New Location</button>
              </div>
            </div>

          </div>

        </div>
      </div>
      {showMap && (
        <div className='p-5 mt-4 bg-slate-200'>
          <div className=' flex justify-center border-3 border-solid border-indigo-400'>
            <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged}>
              <input style={{ fontFamily: 'Montserrat', }} type="text" placeholder="Search for a location" className='p-2 mb-2' />
            </Autocomplete>
          </div>
          <div className="text-end text-xl md:text-xl lg:text-xl xl:text-2xl 2xl:text-2xl font-bold">
            <button style={{ fontFamily: 'Montserrat', }} onClick={() => setShowMap(false)}>
              <Image src={cancel} alt='..' width={40} height={40} layout="fixed" />
            </button>
          </div>
          <div className="text-center text-xl md:text-xl lg:text-xl xl:text-2xl 2xl:text-2xl font-bold mb-2">
            <span style={{ fontFamily: 'Montserrat', }} className="text-black bg-white m-3">Map with marker</span>
          </div>
          {loadingLocation ? (
            <p>Loading location...</p>
          ) : currentLocation ? (
            <GoogleMap
              mapContainerStyle={{ width: '100%', height: '400px' }}
              center={currentLocation}  // Set the center to the current marker position
              zoom={15}
            >
              <Marker position={markerPosition || currentLocation} draggable={true} onDragEnd={handleMarkerDragEnd} />
            </GoogleMap>
          ) : (
            <p>Failed to fetch location</p>
          )}

          <div className='text-center'>
            <p>{`Latitude: ${markerPosition?.lat}, Longitude: ${markerPosition?.lng}`}</p>
          </div>

          <div style={{ fontFamily: 'Montserrat', }} className='p-2'>
            <MapConverter lng={markerPosition?.lng} lat={markerPosition?.lat} />
            <p>${testselect}</p>
            <div>
              <button className='mt-2 p-3 bg-white border-2 border-solid border-pink-300 shadow-xl text-black' onClick={handleUpdateLocation}>Update Location for Hiring</button>
            </div>
            <div>
              <Link href={`/nannydetail2/${nanny.username}`}>
                <button className='mt-2 p-3 bg-pink-300 border-2 border-solid border-pink-400 shadow-xl text-white' onClick={handleConfirmLocation}>Next</button>
              </Link>
            </div>
          </div>
        </div>
      )}

    </div>
  );


}

