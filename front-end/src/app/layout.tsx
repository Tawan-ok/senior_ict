import './globals.css'
import { Inter } from 'next/font/google'
import Footer from '../../Component/footer/page';
import Navbar from '../../Component/navigation/page';
const inter = Inter({ subsets: ['latin'] })

export default function RootLayout({
  children,
  showNavbar = true,
  showFooter = true,
}: {
  children: React.ReactNode
  showNavbar?: boolean,
  showFooter?: boolean,
}) {
  return (
    <>
      {/* <div className='mt-16 my-5'>
        <button className='text-white text-center'  >Sign In</button>
      </div> */}
      <body className={inter.className}>
        {showNavbar && <Navbar />}
        {children}
        {/* {showFooter && <Footer />} */}
      </body>
    </>
  )
}