'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import RootLayout from "../layout";
import Image from 'next/image';
import interview from '../../../assets/question.png'
import booking from '../../../assets/appointment.png'
import cancel from '../../../assets/cancel.png'
import Link from 'next/link';
import { addHours, addMinutes, isBefore } from 'date-fns';
import Modal from '../../../Component/userinformation/page';
import profile from '../../../assets/profile.png';
import no from '../../../assets/no-alarm.png'

type Props = {};

type Customers = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
  profile_image_url: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};

type BookingQueue = {
  id: number,
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number

};

type BookingHistory = {
  id: number,
  booking_id: number,
  status: string,
  time_session: number
};
type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};

interface Notification {
  id: number;
  message: string;
  title?: string; // Appointment title
  interviewDetails?: string;
  appointment_id?: number;
  // Add other properties of notification as needed
}
interface Customer {
  id: number;
  username: string;
  // Add other customer properties as needed
}

interface AppointmentDetails {
  title: string;
  customer_id: number;
  nanny_id: number;
  start_time: string; // Assuming the API returns dates in ISO format
  end_time: string;
  status: string;
  interviewDetails?: string;
}

type ActivityProgram = {
  ProgramID: number;
  Normal_Period1: string;
  Normal_Period2: string;
  Normal_Period3: string;
  Normal_Period4: string;
  Normal_Period5: string;
  Overnight_Period1: string;
  Overnight_Period2: string;
  Overnight_Period3: string;
  Overnight_Period4: string;
  Overnight_Period5: string;
  customer_id: number;
}
// ของเก่า 
export default function CustomersPage({ }: Props) {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
  const [selectedCustomer, setSelectedCustomer] = useState<Customers | null>(null);
  const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
  const [customers, setcustomers] = useState<Customers | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [notifications, setNotifications] = useState<Notification[]>([]);
  const [appointmentDetailsMap, setAppointmentDetailsMap] = useState(new Map());
  const [customerDetailsMap, setCustomerDetailsMap] = useState(new Map());
  const [activity, setActivity] = useState<ActivityProgram[]>([]);
  const router = useRouter(); // Initialize the router

  const handleExit = () => {
    localStorage.removeItem('jwt'); // Remove the JWT token
    router.push('/login-nanny'); // Redirect to /login
  };

  const handleinterview = () => {
    router.push('/nanny-interview_notification');
  };
  const handlebooking = () => {
    router.push('/nanny-booking_notification');
  };

  // const handleUpdateStatusCancel = async (bookingId: number) => {
  //   try {
  //     await axios.put(`http://localhost:9000/api/bookingqueue/updateStatusCancle/${bookingId}`, {
  //       status: 'Bookings',
  //     });
  //     window.location.reload();
  //   } catch (err) {
  //     if (err instanceof Error) {
  //       console.error(err.message);
  //     } else {
  //       console.error('An error occurred while updating the status.');
  //     }
  //   }
  // };

  const handleUpdateStatusCancel = async (bookingId: number) => {
    try {
      // await axios.put(`http://localhost:9000/api/bookingqueue/updateStatusCancle/${bookingId}`,
      await axios.put(`http://localhost:9000/api/bookingqueue/updateStatusCancle/${bookingId}`, {
        status: 'Bookings',
      });
      window.location.reload();
    } catch (err) {
      if (err instanceof Error) {
        console.error(err.message);
      } else {
        console.error('An error occurred while updating the status.');
      }
    }
  };





  useEffect(() => {
    const token = localStorage.getItem('jwt');
    // Decode the JWT to extract user ID
    if (token) {
      const decodedToken: any = jwt_decode(token);
      if (!decodedToken.a.includes('NANNY')) {
        setError('Access denied. You do not have the required permissions.');
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }
      // Extract user ID from the "sub" key in JWT
      const userId: number = decodedToken.sub;

      if (!userId) {
        setError("User ID not found in token.");
        setLoading(false);
        return;
      }

      console.log("User ID:", userId);

      // const fetchData = async () => {
      //   try {
      //     const response = await axios.get<BookingQueue[]>(`http://localhost:9000/api/nannies/booking-queues/pending/${userId}`);
      //     // const response = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/nannies/booking-queues/pending/${userId}`);
      //     setbookingqueue(response.data);

      //     const notificationResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-nanny/${userId}?status=pending`);
      //     // const notificationResponse = await axios.get<Notification[]>(`http://35.213.139.253:9000/api/notifications/for-nanny/${userId}?status=pending`);
      //     setNotifications(notificationResponse.data);

      // const fetchData = async () => {
      //       try {
      //         const now = new Date();
      //         const response = await axios.get<BookingQueue[]>(`http://localhost:9000/api/nannies/booking-queues/pending/${userId}`);
      //         setbookingqueue(response.data);
      //         for (const queue of bookingqueue) {
      //           const startDate = new Date(queue.start_date);
      //           if (isBefore(startDate, addMinutes(now, 30))) {
      //             await handleUpdateStatusCancel(queue.id);
      //           }
      //         }
      //         // const response1 = await axios.get<Customers>(`http://localhost:9000/api/customers/${userId}`);
      //         // setcustomers(response1.data);
      //         const response2 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${userId}`);
      //         setNanny(response2.data);
      //         for (const queue of response.data) {
      //           const customerResponse = await axios.get<Customers>(`http://localhost:9000/api/customers/${queue.customer_id}`);
      //           // Use customer.id instead of userId
      //           const customer = customerResponse.data;
      //           console.log("Customer ID:", customer.id); // Check if customer ID is correct
      //           setcustomers(customer);
      //         }
      //         console.log("CUSTOMER TEST" + customers);
      //         const notificationResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-nanny/${userId}?status=pending`);
      //         setNotifications(notificationResponse.data);

      //         const appointmentDetailsMap = new Map<number, AppointmentDetails>();
      //         const customerDetailsMap = new Map<number, Customer>();


      //         // Fetch appointment details for each notification
      //         for (const notification of notificationResponse.data) {
      //           if (notification.appointment_id) {
      //             const appointmentResponse = await axios.get<AppointmentDetails>(`http://localhost:9000/api/appointments/${notification.appointment_id}`);
      //             const appointmentDetails = appointmentResponse.data;
      //             appointmentDetailsMap.set(notification.appointment_id, appointmentDetails);


      // Fetch appointment details for each notification
      // for (const notification of notificationResponse.data) {
      //   if (notification.appointment_id) {
      //     const appointmentResponse = await axios.get<AppointmentDetails>(`http://localhost:9000/api/appointments/${notification.appointment_id}`);
      //     // const appointmentResponse = await axios.get<AppointmentDetails>(`http://35.213.139.253:9000/api/appointments/${notification.appointment_id}`);
      //     const appointmentDetails = appointmentResponse.data;
      //     appointmentDetailsMap.set(notification.appointment_id, appointmentDetails);

      //     // Now, use the customer_id from the appointment to fetch customer details
      //     if (appointmentDetails.customer_id) {
      //       const customerResponse = await axios.get<Customer>(`http://localhost:9000/api/customers/${appointmentDetails.customer_id}`);
      //       // const customerResponse = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${appointmentDetails.customer_id}`);
      //       const customerDetails = customerResponse.data;
      //       customerDetailsMap.set(appointmentDetails.customer_id, customerDetails);
      //     }
      //   }
      // }

      //             // Now, use the customer_id from the appointment to fetch customer details
      //             if (appointmentDetails.customer_id) {
      //               const customerResponse = await axios.get<Customer>(`http://localhost:9000/api/customers/${appointmentDetails.customer_id}`);
      //               const customerDetails = customerResponse.data;
      //               customerDetailsMap.set(appointmentDetails.customer_id, customerDetails);
      //             }
      //           }
      //         }

      //         // Update your state with the new maps
      //         setAppointmentDetailsMap(appointmentDetailsMap);
      //         setCustomerDetailsMap(customerDetailsMap);


      //         setLoading(false);

      //       } catch (err) {
      //         if (err instanceof Error) {
      //           setError(err.message);
      //         } else {
      //           setError('An error occurred.');
      //         }
      //       }
      //       setLoading(false);
      //     };

      //     fetchData();
      //   } else {
      //     alert('You need to be logged in first.');
      //     router.push('/login-nanny');
      //   }
      // }, []);
      const fetchData = async () => {
        try {
          const notificationResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-nanny/${userId}?status=pending`);
          // const notificationResponse = await axios.get<Notification[]>(`http://35.213.139.253:9000/api/notifications/for-nanny/${userId}?status=pending`);
          setNotifications(notificationResponse.data);
          for (const notification of notificationResponse.data) {
            if (notification.appointment_id) {
              const appointmentResponse = await axios.get<AppointmentDetails>(`http://localhost:9000/api/appointments/${notification.appointment_id}`);
              // const appointmentResponse = await axios.get<AppointmentDetails>(`http://35.213.139.253:9000/api/appointments/${notification.appointment_id}`);
              const appointmentDetails = appointmentResponse.data;
              appointmentDetailsMap.set(notification.appointment_id, appointmentDetails);

              // Now, use the customer_id from the appointment to fetch customer details
              if (appointmentDetails.customer_id) {
                // const customerResponse = await axios.get<Customer>(`http://localhost:9000/api/customers/${appointmentDetails.customer_id}`);
                const customerResponse = await axios.get<Customer>(`http://localhost:9000/api/customers/${appointmentDetails.customer_id}`);
                const customerDetails = customerResponse.data;
                customerDetailsMap.set(appointmentDetails.customer_id, customerDetails);
              }
            }
          }
          const now = new Date();
          // const response = await axios.get<BookingQueue[]>(`http://localhost:9000/api/nannies/booking-queues/pending/${userId}`);
          const response = await axios.get<BookingQueue[]>(`http://localhost:9000/api/nannies/booking-queues/pending/${userId}`);
          const updatedBookingQueue = await Promise.all(response.data.map(async (queue) => {
            const startDateMinus30Minutes = addMinutes(new Date(queue.start_date), -30); // Subtract 30 minutes from start date
            if (isBefore(now, startDateMinus30Minutes)) {
              // If the current time is more than 30 minutes before start time, do not update status to 'Cancel'
              return queue;
            } else {
              // If the current time is less than or equal to 30 minutes before start time, update status to 'Cancel'
              await handleUpdateStatusCancel(queue.id);
              // Update the status in the queue object itself
              return { ...queue, status_payment: 'Cancel' };
            }
          }));
          setbookingqueue(updatedBookingQueue);
        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
        } finally {
          setLoading(false);
        }
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-nanny');
    }
  }, []);

  const handleNotificationAction = async (notificationId: number, action: 'confirm' | 'deny') => {
    try {
      await axios.put(`http://localhost:9000/api/notifications/${notificationId}/status`, { action });
      // await axios.put(`http://35.213.139.253:9000/api/notifications/${notificationId}/status`, { action });
      setNotifications(notifications.filter((notif) => notif.id !== notificationId));
    } catch (err: any) {
      console.error('Failed to update notification status:', err.message);
    }
  };

  const handleUpdateStatus = async (bookingId: number) => {
    try {
      await axios.put(`http://localhost:9000/api/bookingqueue/updateStatusPaid/${bookingId}`, {
        // await axios.put(`http://35.213.139.253:9000/api/bookingqueue/updateStatusPaid/${bookingId}`, {
        status: 'Bookings',
      });
      window.location.reload();
    } catch (err) {
      if (err instanceof Error) {
        console.error(err.message);
      } else {
        console.error('An error occurred while updating the status.');
      }
    }
  };



  interface options {
    year: string;
    month: string;
    day: string;
    hour: string;
    minute: string;
    second: string;
    hour12: boolean;
  }
  const formatDate = (dateString: string): string => {
    const date = new Date(dateString);
    const options: Intl.DateTimeFormatOptions = {
      year: 'numeric', month: '2-digit', day: '2-digit',
      hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false,
    };
    return new Intl.DateTimeFormat('default', options).format(date).replace(',', '');
  };

  const fetchCustomerDetails = async (customerId: number) => {
    try {
      // const response = await axios.get(`http://localhost:9000/api/customers/${customerId}`);
      const response = await axios.get(`http://localhost:9000/api/customers/${customerId}`);
      setSelectedCustomer(response.data); // Update the state with customer details
      // const response2 = await axios.get(`http://localhost:9000/api/activityprogram/getbyusername/${customerId}`);
      const response2 = await axios.get(`http://localhost:9000/api/activityprogram/getbyusername/${customerId}`);
      setActivity(response2.data); // Update the state with customer details
      console.log("TESTA", response2.data);
    } catch (error) {
      console.error('Error fetching customer details:', error);
    }
  };

  const handleShowModal = async (customerId: number) => {
    await fetchCustomerDetails(customerId);
    setShowModal(true); // This sets showModal to true when a notification is clicked
  };

  const formatDateToISO = (dateString: string | number | Date) => {
    const date = new Date(dateString);
    const day = date.getDate();
    const month = date.toLocaleString('default', { month: 'long' });
    const year = date.getFullYear();
    const hour = date.getHours();
    const minute = date.getMinutes();

    const paddedHour = hour.toString().padStart(2, '0');
    const paddedMinute = minute.toString().padStart(2, '0');
    return `${day} ${month} ${year} ${paddedHour}:${paddedMinute}`;
  };



  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;


  return (
    // <RootLayout showFooter={false}>
      <div style={{ fontFamily: 'Montserrat', }}>
        <div>
          <div className="mt-3 text-center text-3xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
            <span style={{ fontFamily: 'Montserrat', }} className="text-white">Notification</span>
          </div>

          <div className="bg-pink-200 p-6 mt-10 m-4 shadow-lg rounded-lg flex flex-col justify-between items-center md:flex-row md:justify-center md:items-center">
            <div className="bg-white border border-pink-300 p-6 rounded-md flex flex-col items-center mr-0 md:mr-8">
              <Image src={interview} alt="Interview" width={120} height={120} />
              <button onClick={handleinterview} className="mt-2 px-4 py-2 text-black font-semibold">
                Interview
              </button>
            </div>
            <div className="bg-white border border-pink-300 rounded-md p-6 flex flex-col items-center mt-4 md:mt-0">
              <Image src={booking} alt="Booking" width={120} height={120} />
              <button onClick={handlebooking} className="mt-2 px-4 py-2 text-black font-semibold">
                Booking
              </button>
            </div>
          </div>



          {/* {notifications.length > 0 && (
          <div className="notifications-container">
            {notifications.map((notification) => (
              <div key={notification.id} className="notification-item">

                <div className='mt-5 p-5 mx-5 text-center text-xl sm:text-l md:text-xl lg:text-xl xl:text-2xl 2xl:text-3xl font-semibold bg-slate-50 rounded-2xl' style={{ fontFamily: 'Montserrat' }}>
                  {notification.appointment_id && appointmentDetailsMap.has(notification.appointment_id) && customerDetailsMap.has(appointmentDetailsMap.get(notification.appointment_id)!.customer_id) && (
                    <>
                      <div>
                        <p>You have a new appointment request from {customerDetailsMap.get(appointmentDetailsMap.get(notification.appointment_id)!.customer_id)!.username}</p>
                        <p>Appointment Title: {appointmentDetailsMap.get(notification.appointment_id)!.title}</p>
                        <p>Start Time: {formatDate(appointmentDetailsMap.get(notification.appointment_id)!.start_time)}</p>
                        <p>End Time: {formatDate(appointmentDetailsMap.get(notification.appointment_id)!.end_time)}</p>
                        <p>Details: {appointmentDetailsMap.get(notification.appointment_id)!.interview_details}</p>

                      </div>
                    </>
                  )}
                  <div className='mt-4 flex justify-center'>
                    <button className='p-4 bg-green-400 text-white rounded-xl' onClick={() => handleNotificationAction(notification.id, 'confirm')}>Confirm</button>
                  </div>
                  <div className='mt-4 flex justify-center'>
                    <button className='p-5 bg-red-500 text-white rounded-xl' onClick={() => handleNotificationAction(notification.id, 'deny')}>Cancel</button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        )} */}

          {/* {bookingqueue.length > 0 ? (
          bookingqueue.map((queue) => {
           
            const startDate = addHours(new Date(queue.start_date), 0).toString();
            const endDate = addHours(new Date(queue.end_date), 0).toString();

            return (

              <div className='mt-5 p-5 mx-5 text-center text-xl sm:text-l md:text-xl lg:text-xl xl:text-2xl 2xl:text-3xl font-semibold bg-slate-50 rounded-2xl' style={{ fontFamily: 'Montserrat' }} key={queue.id}>
               
                <div className='flex justify-between'>
                  <div className='inline-block rounded-lg bg-yellow-300 border border-yellow-400 shadow-md text-white py-1 px-2'>
                    <p>Status: {queue.status_payment}</p>
                  </div>
                  <div className='text-sm font-bold flex rounded-lg items-center justify-center p-2 shadow-md bg-white text-black border border-pink-300'>
                    <button onClick={() => handleShowModal(queue.customer_id)}>Show Activity Program</button>
                  </div>
                </div>

               
                <div className='mt-4 bg-white border border-pink-300 text-start text-lg p-4 shadow-inner'>
               
                  <p>Location Hiring: {queue.locationhiring}</p>
                  <p>Start Date: {formatDateToISO(startDate)}</p>
                  <p>End Date: {formatDateToISO(endDate)}</p>
                  <p>Hours: {queue.hours}</p>
                  <p>Total Amount: {queue.total_amount}</p>
                </div>


                <div className='mt-4 flex justify-center'>
                  <button className='p-4 bg-green-400 text-white rounded-xl mr-4' onClick={() => handleUpdateStatus(queue.id)}>Confirm</button>
                  <button className='p-4 bg-red-500 text-white rounded-xl' onClick={() => handleUpdateStatusCancel(queue.id)}>Cancel</button>
                </div>

              </div>


            );

          })

        ) : (
          <div className='flex flex-col items-center justify-center p-5 bg-slate-50 m-3'>
            <Image
              src={no}
              alt="No notifications"
              width={150}
              height={150}
              onClick={() => setShowModal(false)}
              className="cursor-pointer"
            />
            <p className='mt-5 p-5 mx-5 text-center text-xl sm:text-l md:text-xl lg:text-xl xl:text-2xl 2xl:text-3xl font-semibold rounded-2xl' style={{ fontFamily: 'Montserrat' }}>
              No Notifications
            </p>
          </div>
        )}
      </div>
      {showModal && (
        <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
          <div className="flex flex-col">
            <div className='mt-5 p-5 mx-5 text-center text-xl sm:text-l md:text-xl lg:text-xl xl:text-2xl 2xl:text-3xl font-semibold bg-slate-50 rounded-2xl' style={{ fontFamily: 'Montserrat' }}>
              <div className="flex justify-end">
                <Image
                  src={cancel}
                  alt="Close"
                  width={40}
                  height={40}
                  onClick={() => setShowModal(false)}
                  className="cursor-pointer"
                />
              </div>
              <p>Customer Name: {selectedCustomer?.first_name}</p>
              <div className="profileImageContainer flex justify-center">
                <Image
                  className={styles.profileImage}
                  src={selectedCustomer?.profile_image_url ? `data:image/png;base64,${selectedCustomer?.profile_image_url}` : profile}
                  alt=""
                  width={80}
                  height={80}
                  layout="fixed"
                />
              </div>

              {activity.length > 0 && (
                <div className="mt-2 bg-gray-100 rounded-lg p-4 mb-4">
                  <h2 className="text-lg font-semibold mb-2">Activity Program</h2>
                  <ul>
                    {activity.map((program) => (
                      <li key={program.ProgramID}>
                        <div>
                          <p className="text-gray-600">Normal Period 1: {program.Normal_Period1}</p>
                          <p className="text-gray-600">Normal Period 2: {program.Normal_Period2}</p>
                          <p className="text-gray-600">Normal Period 3: {program.Normal_Period3}</p>
                          <p className="text-gray-600">Normal Period 4: {program.Normal_Period4}</p>
                          <p className="text-gray-600">Normal Period 5: {program.Normal_Period5}</p>
                        </div>
                        <div>
                          <p className="text-gray-600">Overnight Period 1: {program.Overnight_Period1}</p>
                          <p className="text-gray-600">Overnight Period 2: {program.Overnight_Period2}</p>
                          <p className="text-gray-600">Overnight Period 3: {program.Overnight_Period3}</p>
                          <p className="text-gray-600">Overnight Period 4: {program.Overnight_Period4}</p>
                          <p className="text-gray-600">Overnight Period 5: {program.Overnight_Period5}</p>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              )}
            </div>
          </div>
        </Modal>
      )} */}
        </div>
      </div>
    // </RootLayout>
  );
}


