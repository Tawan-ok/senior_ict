'use client';

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Image from 'next/image';
import Link from 'next/link';
import qs from 'querystring';
type Props = {};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};

export default function Page({ }: Props) {
  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [selectedRoleLevel, setSelectedRoleLevel] = useState<string>('');
  const [selectedTypeOfWork, setSelectedTypeOfWork] = useState<string>('');
  const [selectedGender, setSelectedGender] = useState<string>('');
  const [searchByRoleLevel, setSearchByRoleLevel] = useState<boolean>(false);
  const [searchByTypeOfWork, setSearchByTypeOfWork] = useState<boolean>(false);
  const [searchByGender, setSearchByGender] = useState<boolean>(false);
  const [searchResults, setSearchResults] = useState<Nanny[]>([]);

  const [selectedDistrict, setSelectedDistrict] = useState<string>('');
  const [districtSuggestions, setDistrictSuggestions] = useState<string[]>([]);

  const [selectedNanny, setSelectedNanny] = useState<Nanny | null>(null);
  const [selectedSkill1, setSelectedSkill1] = useState<string>('');
  const [selectedSkill2, setSelectedSkill2] = useState<string>('');
  const [selectedSkill3, setSelectedSkill3] = useState<string>('');
  const [searchBySkill, setSearchBySkill] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('http://localhost:9000/api/nannies');
        // const response = await axios.get('http://35.213.139.253:9000/api/nannies');
        setNannies(response.data);
      } catch (err) {
        if (err instanceof Error) {
          setError(err.message);
        } else {
          setError('An error occurred.');
        }
      }
      setLoading(false);
    };

    fetchData();
  }, []);

  const handleSkillSelection = (e: React.ChangeEvent<HTMLInputElement>, skill: string) => {
    const { checked } = e.target;
    switch (skill) {
      case 'English':
        setSelectedSkill1(checked ? 'English' : '');
        break;
      case 'Coding':
        setSelectedSkill2(checked ? 'Coding' : '');
        break;
      case 'Write':
        setSelectedSkill3(checked ? 'Write' : '');
        break;
      default:
        break;
    }
  };

  const handleSearch = async () => {
    try {
      let criteria: any = {
        keyword: searchTerm,
      };

      if (searchByRoleLevel && selectedRoleLevel !== '') {
        criteria.role_level = selectedRoleLevel;
      }

      if (searchByTypeOfWork && selectedTypeOfWork !== '') {
        criteria.type_work = selectedTypeOfWork;
      }

      if (searchByGender && selectedGender !== '') {
        criteria.gender = selectedGender;
      }

      if (selectedDistrict !== '') {
        criteria.district = selectedDistrict;
      }

      if (searchBySkill) {
        const selectedSkills = [selectedSkill1, selectedSkill2, selectedSkill3].filter(skill => skill.trim() !== '');
        criteria.allskill = selectedSkills; // Use allskill as the key for skills
      }

      const response = await axios.get('http://localhost:9000/api/nannies/searchtestall', {
        params: criteria,
        paramsSerializer: params => { // Custom params serializer
          let queryString = '';
          Object.keys(params).forEach((key, index) => {
            const value = params[key];
            if (Array.isArray(value)) {
              value.forEach(val => {
                queryString += `${index === 0 ? '' : '&'}${key}=${val}`;
              });
            } else {
              queryString += `${index === 0 ? '' : '&'}${key}=${value}`;
            }
          });
          return queryString;
        }
      });
      setSearchResults(response.data);

    } catch (err) {
      if (err instanceof Error) {
        setError(err.message);
      } else {
        setError('An error occurred while searching.');
      }
    }
  };

  const handleDistrictSearch = (inputValue: string) => {
    const suggestions = nannies
      .filter((nanny) => nanny.district.toLowerCase().includes(inputValue.toLowerCase()))
      .map((nanny) => nanny.district);

    setDistrictSuggestions(suggestions);
  };

  const selectDistrictSuggestion = (district: string) => {
    setSelectedDistrict(district);
    setDistrictSuggestions([]);
  };



  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;

  return (
    <div style={{ fontFamily: 'Montserrat' }} className="container mx-auto px-4 py-8">
      <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white font-montserrat">
        List of Nannies
      </div>
      <div className="mt-4 max-w-lg mx-auto">
        <div className="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4">
          <h2 className="text-center text-2xl font-semibold mb-4">Search Nannies</h2>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <input
              type="text"
              placeholder="Search by full name"
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
              className="border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
            />

            <input
              type="text"
              placeholder="Search by district"
              value={selectedDistrict}
              onChange={(e) => {
                setSelectedDistrict(e.target.value);
                handleDistrictSearch(e.target.value);
              }}
              className="border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
            />
          </div>
          {districtSuggestions.length > 0 && (
            <ul className="mt-2">
              {districtSuggestions.map((district) => (
                <li
                  key={district}
                  onClick={() => selectDistrictSuggestion(district)}
                  className="cursor-pointer py-1 px-2 hover:bg-gray-100 rounded-md"
                >
                  {district}
                </li>
              ))}
            </ul>
          )}
          <div className="mt-4">
            <label className="inline-flex items-center">
              <input
                type="checkbox"
                checked={searchByRoleLevel}
                onChange={() => setSearchByRoleLevel(!searchByRoleLevel)}
                className="form-checkbox h-5 w-5 text-blue-500"
              />
              <span className="ml-2">Search by Role Level</span>
            </label>

            <label className="inline-flex items-center ml-6">
              <input
                type="checkbox"
                checked={searchByTypeOfWork}
                onChange={() => setSearchByTypeOfWork(!searchByTypeOfWork)}
                className="form-checkbox h-5 w-5 text-blue-500"
              />
              <span className="ml-2">Search by Type of Work</span>
            </label>

            <label className="inline-flex items-center ml-6">
              <input
                type="checkbox"
                checked={searchByGender}
                onChange={() => setSearchByGender(!searchByGender)}
                className="form-checkbox h-5 w-5 text-blue-500"
              />
              <span className="ml-2">Search by Gender</span>
            </label>

            <label className="inline-flex items-center ml-6">
              <input
                type="checkbox"
                checked={searchBySkill}
                onChange={() => setSearchBySkill(!searchBySkill)}
                className="form-checkbox h-5 w-5 text-blue-500"
              />
              <span className="ml-2">Search by Skill</span>
            </label>
          </div>

          {searchByRoleLevel && (
            <select
              value={selectedRoleLevel}
              onChange={(e) => setSelectedRoleLevel(e.target.value)}
              className="mt-2 border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
            >
              <option value="">Select Role Level</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
          )}

          {searchByTypeOfWork && (
            <select
              value={selectedTypeOfWork}
              onChange={(e) => setSelectedTypeOfWork(e.target.value)}
              className="mt-2 border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
            >
              <option value="">Select Type of Work</option>
              <option value="F">Full Time</option>
              <option value="P">Part Time</option>
              <option value="A">ALL</option>
            </select>
          )}

          {searchByGender && (
            <select
              value={selectedGender}
              onChange={(e) => setSelectedGender(e.target.value)}
              className="mt-2 border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
            >
              <option value="">Select Gender</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          )}

          {/* {searchBySkill && (
            <select
              value={selectedSkill1}
              onChange={(e) => setSelectedSkill1(e.target.value)}
              className="mt-2 border border-gray-300 rounded-lg py-2 px-4 focus:outline-none focus:border-blue-500"
            >
              <option value="">Select Skill</option>
              <option value="Educate">Educate</option>
              <option value="Sleep">Sleep</option>
              <option value="Activity">Activity</option>
            </select>
          )} */}
          {searchBySkill && (
            <div className="mt-2">
              <p>Select Skill:</p>
              <label className="inline-flex items-center">
                <input
                  type="checkbox"
                  checked={selectedSkill1 !== ''}
                  onChange={(e) => handleSkillSelection(e, 'English')}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">English</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input
                  type="checkbox"
                  checked={selectedSkill2 !== ''}
                  onChange={(e) => handleSkillSelection(e, 'Coding')}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">Coding</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input
                  type="checkbox"
                  checked={selectedSkill3 !== ''}
                  onChange={(e) => handleSkillSelection(e, 'Cooking')}
                  className="form-checkbox h-5 w-5 text-blue-500"
                />
                <span className="ml-2">Cooking</span>
              </label>
            </div>
          )}

          <div className="mt-4 flex justify-center">
            <button
              onClick={handleSearch}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            >
              Search
            </button>
          </div>
        </div>
      </div>


      <div className="mt-8">
        {searchResults.length > 0 ? (
          searchResults.map((nanny) => (
            <div key={nanny.username} className="bg-pink-100 border-pink-400 border-l-4 p-4 mb-4">
              <h3 className="text-xl font-semibold mb-2">Nanny Details</h3>
              <p><span className="font-semibold">Username:</span> {nanny.username}</p>
              <p><span className="font-semibold">First Name:</span> {nanny.first_name}</p>
              <p><span className="font-semibold">Last Name:</span> {nanny.last_name}</p>
              <p><span className="font-semibold">Type Work:</span> {nanny.type_work}</p>
              <p><span className="font-semibold">Gender:</span> {nanny.gender}</p>
              <p><span className="font-semibold">District:</span> {nanny.district}</p>
              <p><span className="font-semibold">Skill 1:</span> {nanny.skill_1}</p>
              <p><span className="font-semibold">Skill 2:</span> {nanny.skill_2}</p>
              <p><span className="font-semibold">Skill 3:</span> {nanny.skill_3}</p>
            </div>
          ))
        ) : (
          nannies.map((nanny) => (
            <div key={nanny.username} className="bg-pink-100 border-pink-400 border-l-4 p-4 mb-4">
              <h3 className="text-xl font-semibold mb-2">Nanny Details</h3>
              <p><span className="font-semibold">Username:</span> {nanny.username}</p>
              <p><span className="font-semibold">First Name:</span> {nanny.first_name}</p>
              <p><span className="font-semibold">Last Name:</span> {nanny.last_name}</p>
              <p><span className="font-semibold">Type Work:</span> {nanny.type_work}</p>
              <p><span className="font-semibold">Gender:</span> {nanny.gender}</p>
              <p><span className="font-semibold">District:</span> {nanny.district}</p>
              <p><span className="font-semibold">Skill 1:</span> {nanny.skill_1}</p>
              <p><span className="font-semibold">Skill 2:</span> {nanny.skill_2}</p>
              <p><span className="font-semibold">Skill 3:</span> {nanny.skill_3}</p>
            </div>
          ))
        )}
      </div>


    </div>
  );
}