'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"

import * as fs from 'fs';
import jsPDF from 'jspdf';
import { useRouter } from 'next/navigation'
import jwt_decode from 'jwt-decode';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
};

type Customertest = {

    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
};

type Nannytest = {

    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;

    type_work: string;
    status: string;

    gender: string;

};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type BookingHistory = {
    booking_id: number,
    status: string,
    time_session: number
};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}

type Complains = {
    booking_id: number;
    customer_id: number;
    nanny_id: number;
    complain_field_1: string,
    complain_field_2: string,
    complain_field_3: string,
    complain_field_4: string,
    complain_field_5: string,
    complain_text: string,
    complain_count: number;
};



export default function Page({ params }: { params: { username: string } }) {

    const [customertest, setCustomertest] = useState<Customertest | null>(null);
    const [nannytest, setNannytest] = useState<Nannytest | null>(null);

    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null

    const [bookingqueuehistory, setBookingqueuehistory] = useState<BookingHistory | null>(null); // Initialize as null
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);

    const [bookingqueue2, setBookingqueue2] = useState<BookingQueue | null>(null); // Initialize as null

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);

    const [response1, setResponse1] = useState<Nanny | null>(null);
    const [complain, setComplain] = useState<Complains | null>(null);
    const [newScore, setNewScore] = useState<number | null>(null);

    const [startdate, setstartDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [enddate, setEndtDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })


    const getstartTimes = () => {
        if (!startdate.justDate) return

        const { justDate } = startdate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const starttimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            starttimes.push(i)
        }
        return starttimes
    }

    const getendTimes = () => {
        if (!enddate.justDate) return

        const { justDate } = enddate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const endtimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            endtimes.push(i)
        }
        return endtimes
    }

    const starttimes = getstartTimes()
    const endtimes = getendTimes()
    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-admin');
    };
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    if (!customer || !nanny) {
                        const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                        // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
                        const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                        // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);

                        setCustomer(response.data);
                        setNanny(response1.data);
                        setCustomertest(response.data);
                        setNannytest(response1.data);

                        if (response.data && response1.data) {
                            const response2 = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            // const response2 = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            setBookingqueue(response2.data);
                            console.log("All DATA Booking ", response2.data);
                            // Loop through bookingqueue and create BookingHistory for each booking

                            const response4 = await axios.get<BookingQueue>(`http://localhost:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            // const response4 = await axios.get<BookingQueue>(`http://35.213.139.253:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            setBookingqueue2(response4.data);

                            console.log("All DATA Booking TEST STATUS ", response4.data.id);
                            // for (const booking of response2.data) {

                            const response3 = await axios.get<BookingHistory>(`http://localhost:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            // const response3 = await axios.get<BookingHistory>(`http://35.213.139.253:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            setBookingqueuehistory(response3.data);
                            const response5 = await axios.get<Complains>(`http://localhost:9000/api/complains/getcomplainsbyBookingId/${response3.data.booking_id}`);
                            setComplain(response5.data);
                            //   console.log("Booking ID:", response3.data.booking_id);
                            console.log("All DATA Booking History", response3.data);
                            // }
                        }

                    }
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes
    }, [params.username, response1]);

    const handleUpdateStatus = async () => {
        try {
            if (bookingqueue2?.id) {
                // Create an array of promises for both PUT requests
                const promises = [
                    axios.put(`http://localhost:9000/api/bookinghistory/updatestatuscomplete/${bookingqueue2.id}`, {
                        // Replace with your actual API endpoint
                        status: 'Completed',
                    }),
                    axios.put(`http://localhost:9000/api/bookingqueue/updateStatusPaidSuccess/${bookingqueue2.id}`, {
                        // Replace with your actual API endpoint
                        status: 'Success',
                    }),
                ];

                // Wait for both PUT requests to complete
                await Promise.all(promises);

                // Redirect to the hiring page after successful updates
                router.push('/hiring');
            } else {
                console.error('Booking queue ID not found.');
            }
        } catch (err) {
            console.error('An error occurred while updating the status:', err);
        }
    };


    const formatDateToISO = (dateString: string | number | Date, includeTime: boolean = false) => {
        const date = new Date(dateString);
        const day = date.getDate();
        const month = date.toLocaleString('default', { month: 'long' });
        const year = date.getFullYear();
        const hour = date.getHours();
        const minute = date.getMinutes();

        const paddedHour = hour.toString().padStart(2, '0');
        const paddedMinute = minute.toString().padStart(2, '0');

        let formattedDate = `${day} ${month} ${year}`;
        if (includeTime) {
            formattedDate += ` ${paddedHour}:${paddedMinute}`;
        }

        return formattedDate;
    };



    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;

    if (!bookingqueuehistory) return <div>Bookingqueuehistory not found.</div>;

    return (

        <div style={{ fontFamily: 'Montserrat' }} className="mt-10 mx-auto max-w-3xl p-6 bg-white shadow-lg rounded-lg border border-purple-300">
            <div className="text-center text-4xl font-bold text-gray-900 mb-8">
                Report Conclusion
            </div>

            <div className='bg-pink-100 p-3 border border-pink-400 rounded-md mb-2'>
                <p className="text-gray-800 font-semibold">Booking ID: {bookingqueuehistory.booking_id}</p>
                <p className="text-gray-800 font-semibold">Time Session: {bookingqueuehistory.time_session}</p>
            </div>

            <div className="space-y-8">
                {bookingqueue.map((bookingqueues) => (
                    bookingqueues.status_payment === 'Bookings' && (
                        <div key={bookingqueues.id} className="bg-gray-100 p-4 rounded-lg">
                            <p className="text-gray-800 font-semibold">Transaction Number: {bookingqueues.id}</p>
                            <p className="text-gray-800 font-semibold">Customer Name: {customer?.first_name} {customer?.last_name}</p>
                            <p className="text-gray-800 font-semibold">Nanny Name: {nanny?.first_name} {nanny?.last_name}</p>
                            <p className="text-gray-800 font-semibold">Location Hiring: {bookingqueues.locationhiring}</p>
                            <p className="text-gray-800 font-semibold">Hours: {bookingqueues.hours}</p>
                            <p className="text-gray-800 font-semibold">Total Amount: {bookingqueues.total_amount.toLocaleString()}</p>
                            <p className="text-gray-800 font-semibold">Start Date: {formatDateToISO(new Date(bookingqueues.start_date), true).toString()}</p>
                            <p className="text-gray-800 font-semibold">End Date: {formatDateToISO(new Date(bookingqueues.end_date), true).toString()}</p>
                            {/* <button className="bg-blue-500 text-white px-4 py-2 rounded-lg hover:bg-blue-600" onClick={() => handleDownloadData(bookingqueues.start_date.toString(), bookingqueues.end_date.toString())}>Download Customer and Nanny Data</button> */}
                        </div>
                    )
                ))}
            </div>

            <div className="mt-8">
                <form className="mt-4" >
                    <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                        {/* <div>
                            <label htmlFor="name" className="block text-gray-800 font-semibold">Name:</label>
                            <input type="text" id="name" name="name" className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-blue-500" required />
                        </div>
                        <div>
                            <label htmlFor="email" className="block text-gray-800 font-semibold">Email:</label>
                            <input type="email" id="email" name="email" className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-blue-500" required />
                        </div> */}
                    </div>
                    {complain ? (
                        <div className="space-y-8">
                            <div className="bg-gray-100 p-4 rounded-lg">
                                <h2 className="text-2xl font-bold mb-4">Review Nanny</h2>
                                <div className="overflow-x-auto">
                                    <table className="min-w-full">
                                        <thead>
                                            <tr className="bg-gray-200">
                                                <th className="px-4 py-2 text-left">Has the nanny been arriving late to the service?</th>
                                                <th className="px-4 py-2 text-left">Does the nanny exhibit harmful behavior?</th>
                                                <th className="px-4 py-2 text-left">Has the nanny used rude language when speaking to your children?</th>
                                                <th className="px-4 py-2 text-left">Are there any concerns about the nanny professionalism or boundaries?</th>
                                                <th className="px-4 py-2 text-left">Does the nanny appear unkempt or dirty?</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody className="divide-y divide-gray-200">
                                            <tr>
                                                <td className="px-4 py-2">{complain?.complain_field_1 === 'T' ? 'Yes' : 'No'}</td>
                                                <td className="px-4 py-2">{complain?.complain_field_2 === 'T' ? 'Yes' : 'No'}</td>
                                                <td className="px-4 py-2">{complain?.complain_field_3 === 'T' ? 'Yes' : 'No'}</td>
                                                <td className="px-4 py-2">{complain?.complain_field_4 === 'T' ? 'Yes' : 'No'}</td>
                                                <td className="px-4 py-2">{complain?.complain_field_5 === 'T' ? 'Yes' : 'No'}</td>
                                                
                                            </tr>
                                        </tbody>
                                        
                                    </table>
                                    <h3 className="px-4 py-2 text-left">Complain Text:  {complain?.complain_text}</h3> 
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className='flex flex-col justify-center items-center'>
                            <p className="text-gray-500 mt-2">Currently, no complains available</p>
                        </div>
                    )}

                    <div className="flex justify-end mt-4">
                        {/* <button type="submit" className="bg-white text-black px-4 py-2 rounded-lg border border-black hover:bg-green-600">Submit</button> */}
                        <Link href={`/hiring`}>
                            <button className="ml-4 bg-green-400 text-white px-4 py-2 rounded-lg border border-green-500 hover:bg-red-600" onClick={handleUpdateStatus}>Finish</button>
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    );
}