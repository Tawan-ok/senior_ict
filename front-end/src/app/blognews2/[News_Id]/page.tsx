'use client';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import styles from '../../../../styles/blognews.module.css';
import news1 from '../../../../assets/rectangle-19.png'
import Image from 'next/image';



type Props = {};

type BlogNews = {
    News_Id: number;
    Topic: string;
    Article: string;
    admin_id: number;
    date: number;
}

type Admin = {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    age: number;
}



export default function Page({ params }: { params: { News_Id: number } }) {
    const [admin, setAdmin] = useState<Admin | null>(null);
    const [blognews, setBlogNews] = useState<BlogNews[] | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [blognewstest, setBlogNewstest] = useState<BlogNews | null>(null); // Initialize as null
    useEffect(() => {
        const fetchData = async () => {
            try {
                // const response = await axios.get(`http://localhost:9000/api/blognews2/${params.News_Id}`);
                const response = await axios.get(`http://localhost:9000/api/blognews2/${params.News_Id}`)
                // setBlogNews(response.data);
                setBlogNewstest(response.data);
                console.log('Response from API ', response.data);
            } catch (err) {
                if (err instanceof Error) {
                    setError(err.message);
                } else {
                    setError('An error occurred.');
                }
            }
            setLoading(false);
        };

        fetchData();
    }, [params.News_Id]);
    if (!blognewstest) return <div>Blog News not found.</div>;
    return (
        <div>
            {loading && <p>Loading...</p>}
            {error && <p>Error: {error}</p>}
            <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                <span style={{ fontFamily: 'Montserrat', }} className="text-white">News</span>
            </div>
            <div style={{ fontFamily: 'Montserrat', }} className={styles.rootContainer}>
                <div className="bg-white rounded-lg shadow-md p-6 mb-6 mt-2 m-2">
                    <h3 className="text-xl text-gray-800 font-bold mb-4">{blognewstest.Topic}</h3>
                    <div className="relative">
                        <Image
                            src={news1}
                            alt=""
                            layout="intrinsic"
                            className="rounded-lg"
                        />
                    </div>
                    <p className="text-gray-700 mt-4">{blognewstest.Article}</p>
                    <p className="text-gray-600 mt-2">Date: {blognewstest.date}</p>
                </div>
            </div>
        </div>
    );
}