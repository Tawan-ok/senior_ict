"use client";
import React, { useEffect, useState } from "react";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import axios from "axios";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import "react-big-calendar/lib/css/react-big-calendar.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { create } from "domain";
import { formatISO } from "date-fns";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/navigation";
import styles from "../../../../styles/appointment.module.css";
import { startOfDay } from 'date-fns';
import { isToday, setHours, setMinutes } from 'date-fns';
import Image from 'next/image';
import cancel from '../../../../assets/cancel.png'
import um from '../../../../assets/communication.png';
const locales = {
  "en-US": require("date-fns/locale/en-US"),
};

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

const events = [
  {
    title: "Big Meeting",
    customer_id: 1, // Example ID, adjust as necessary
    nanny_id: 1, // Example ID, adjust as necessary
    start_time: new Date(2024, 1, 6),
    end_time: new Date(2024, 1, 8),
    status: "Scheduled",
  },
  {
    title: "Vacation",
    allDay: true,
    customer_id: 1, // Example ID, adjust as necessary
    nanny_id: 1, // Example ID, adjust as necessary
    start: new Date(2024, 2, 9),
    end: new Date(2024, 2, 10),
    status: "Scheduled",
  },
  {
    title: "3-Hour Meeting",
    customer_id: 1, // Example ID, adjust as necessary
    nanny_id: 1, // Example ID, adjust as necessary
    start: new Date(2024, 1, 6, 10, 0), // March 6, 2024, 10:00 AM
    end: new Date(2024, 1, 6, 13, 0), // March 6, 2024, 1:00 PM
    status: "Scheduled",
  },
];

type Customer = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role: string;
  age: number;
  gender: string;
  locationall: string;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
};

interface AppointmentResponse {
  title: string;
  customer_id: number;
  nanny_id: number;
  start_time: string; // Assuming the API returns dates in ISO format
  end_time: string;
  status: string;
  interviewDetails?: string;
}

interface Slot {
  is_available: boolean;
  start_time: string;
  end_time: string;
}
export default function PageAppointmentInterview({
  params,
}: {
  params: { username: string };
}) {
  const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
  const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [response1, setResponse1] = useState<Nanny | null>(null);
  const [allEvent, setAllEvent] = useState<Event[]>([]);
  const [newEvent, setNewEvent] = useState<Event | null>(null); // Initialize as null
  const [showModal, setShowModal] = useState(false);
  interface Event {
    title: string;
    start: Date;
    end: Date;
    customer_id?: number;
    nanny_id: number;
    status?: string;
    isAvailable?: boolean;
    type?: string;
    interviewDetails?: string; // Add this line
  }

  const router = useRouter();

  const fetchData = async () => {
    try {
      const token = localStorage.getItem("jwt");
      if (token) {
        const decodedToken: any = jwt_decode(token);
        const userId: number = decodedToken.sub;

        if (!decodedToken.a.includes('USER')) {
          setError("User ID not found in token.");
          alert('Access denied. You do not have the required permissions.');
          setLoading(false);
          router.push('/home');
          return;
        }
  

        console.log("User ID:", userId);

        const response = await axios.get<Nanny>(
          // `http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`
          `http://localhost:9000/api/nannies/getbyusername/${params.username}`
        );
        const response1 = await axios.get<Customer>(
          // `http://35.213.139.253:9000/api/customers/${userId}`
          `http://localhost:9000/api/customers/${userId}`
        );
        setNanny(response.data);
        setCustomer(response1.data);
        setLoading(false);
      } else {
        alert("You need to be logged in first.");
        router.push("/login-user");
      }
    } catch (err) {
      if (err instanceof Error) {
        setError(err.message);
      } else {
        setError("An error occurred.");
      }
    }
  };

  useEffect(() => {
    const fetchAppointmentsAndSlots = async () => {
      if (customer && nanny) {
        try {
          // Fetch appointments
          const appointmentsResponse = await axios.get<AppointmentResponse[]>(
            `http://localhost:9000/api/appointments?customer_id=${customer.id}&nanny_id=${nanny.id}&status=confirmed`
            // `http://35.213.139.253:9000/api/appointments?customer_id=${customer.id}&nanny_id=${nanny.id}&status=confirmed`
          );
          const appointmentsConfirmed = appointmentsResponse.data.map((appointment) => ({
            title: appointment.title,
            start: new Date(appointment.start_time),
            end: new Date(appointment.end_time),
            customer_id: appointment.customer_id,
            nanny_id: appointment.nanny_id,
            status: appointment.status,
            interviewDetails: appointment.interviewDetails,
            isAvailable: true, // Assuming all appointments are marked as available
          }));

          const appointmentsResponsePending = await axios.get<AppointmentResponse[]>(
            `http://localhost:9000/api/appointments?customer_id=${customer.id}&nanny_id=${nanny.id}&status=pending`
            // `http://35.213.139.253:9000/api/appointments?customer_id=${customer.id}&nanny_id=${nanny.id}&status=pending`
          );
          const appointmentsPending = appointmentsResponsePending.data.map((appointment) => ({
            title: appointment.title,
            start: new Date(appointment.start_time),
            end: new Date(appointment.end_time),
            customer_id: appointment.customer_id,
            nanny_id: appointment.nanny_id,
            status: appointment.status,
            interviewDetails: appointment.interviewDetails,
            isAvailable: true, // Assuming all appointments are marked as available
          }));
          // Fetch availability slots
          // const slotsResponse = await axios.get<Slot[]>(`http://35.213.139.253:9000/api/availability/nanny/${nanny.id}`);
          const slotsResponse = await axios.get<Slot[]>(
            `http://localhost:9000/api/availability/nanny/${nanny.id}`
          );
          const slots = slotsResponse.data.map((slot) => ({
            title: slot.is_available ? "Nanny available time" : "Nanny is busy",
            start: new Date(slot.start_time),
            end: new Date(slot.end_time),
            nanny_id: nanny.id,
            isAvailable: slot.is_available,
            type: "availabilitySlot", // Add this property
          }));

          // Combine appointments and slots
          const combinedEvents: Event[] = [...appointmentsConfirmed,...appointmentsPending ,...slots];
          const currentDateTime = new Date(); // make it show only current date 
          // const startOfCurrentDay = startOfDay(currentDateTime);

          // const futureEvents = combinedEvents.filter(event => new Date(event.start) >= startOfCurrentDay);
          setAllEvent(combinedEvents);
          // setAllEvent(combinedEvents); show all date with all events.
        } catch (error) {
          console.error("Failed to fetch data:", error);
        }
      }
    };

    fetchAppointmentsAndSlots();
  }, [customer, nanny]);

  useEffect(() => {
    fetchData();
  }, [params.username]);

  useEffect(() => {
    if (customer && nanny) {
      setNewEvent({
        title: "",
        customer_id: customer.id,
        nanny_id: nanny.id,
        start: new Date(),
        end: new Date(),
        status: "pending",
        interviewDetails: "", // Initialize with an empty string
      });
    }
  }, [customer, nanny]);

  const convertedEvents = allEvent.map((event) => ({
    ...event,
    start: event.start ? new Date(event.start) : new Date(), // Default to current date if undefined or null
    end: event.end ? new Date(event.end) : new Date(), // Default to current date if undefined or null
  }));
  console.log("convertedEvents" + convertedEvents);

  const testEvents = [
    {
      title: "Test Event",
      start: new Date(),
      end: new Date(new Date().setDate(new Date().getDate() + 1)),
    },
  ];
  const createAppointment = async (appointment: Event) => {
    if (!appointment.start || !appointment.end) {
      console.error("Start and end times must be provided.");
      return; // Exit the function if start or end times are null
    }
    try {
      const formattedAppointment = {
        ...appointment,
        interviewDetails: appointment.interviewDetails,
        start_time: formatISO(appointment.start),
        end_time: formatISO(appointment.end),
      };

      const response = await axios.post(
        // "http://35.213.139.253:9000/api/appointments",
        "http://localhost:9000/api/appointments",
        formattedAppointment
      );
      console.log("Appointment created:", response.data);
      return response.data;
    } catch (error) {
      console.error("Failed to create appointment:", error);
      throw error;
    }
  };

  function handleAddEvent() {
    
    if (newEvent !== null) {
      if (!newEvent.title || newEvent.title.trim() === "") {
        alert("Please enter a title.");
        return; // Exit the function if the title is empty or only whitespace
      }
  
      // Convert newEvent start and end times to Date objects for comparison
      const newEventStart = new Date(newEvent.start);
      const newEventEnd = new Date(newEvent.end);
  
      if (newEvent.end <= newEvent.start) {
        alert('End time must be after the start time.');
        return; // Stop the function from proceeding
      }
      // Filter to get only the slots where the nanny is busy or unavailable
      const unavailableSlots = allEvent.filter(event => !event.isAvailable);
  
      // Check if the new event overlaps with any unavailable slot
      const isOverlappingWithUnavailableSlot = unavailableSlots.some(slot => {
        const slotStart = new Date(slot.start).getTime();
        const slotEnd = new Date(slot.end).getTime();
    
        // Converts dates to timestamps for comparison
        const newEventStartTimestamp = newEventStart.getTime();
        const newEventEndTimestamp = newEventEnd.getTime();
    
        // Check if new event overlaps with the slot
        // Condition 1: New event starts inside the slot
        // Condition 2: New event ends inside the slot
        // Condition 3: New event covers the slot entirely
        const isOverlap = (newEventStartTimestamp < slotEnd && newEventStartTimestamp >= slotStart) ||
                          (newEventEndTimestamp > slotStart && newEventEndTimestamp <= slotEnd) ||
                          (newEventStartTimestamp <= slotStart && newEventEndTimestamp >= slotEnd);
    
        return isOverlap;
    });
    
    if (isOverlappingWithUnavailableSlot) {
        alert("This time slot is unavailable. Please choose a different time.");
        return; // Prevent the creation of the event
    }
    
      // If the slot is available, proceed to create the appointment
      createAppointment(newEvent)
        .then(createdEvent => {
          const updatedEvent = {
            ...createdEvent,
            start: new Date(createdEvent.start_time),
            end: new Date(createdEvent.end_time),
          };
          setAllEvent(prevEvents => [...prevEvents, updatedEvent]);
          alert("Appointment created successfully!");
        })
        .catch(error => {
          console.error("Failed to create appointment:", error);
          alert("Failed to create appointment.");
        });
    } else {
      console.error("New event is null, cannot create appointment.");
    }
  }
  

  const eventStyleGetter = (event: Event) => {
    let backgroundColor = "#007bff"; // Default blue color for normal events

    // Apply color coding only for availability slots
    if (event.type === "availabilitySlot") {
      backgroundColor = event.isAvailable ? "lightgreen" : "lightcoral";
    }

    return { style: { backgroundColor } };
  };
  // Custom event component to display interviewDetails
  interface MyEvent {
    title: string;
    start: Date;
    end: Date;
    interviewDetails?: string; // Include other properties as needed
  }
  interface CustomEventProps {
    event: MyEvent; // Use the interface you defined
  }

  const CustomEvent: React.FC<CustomEventProps> = ({ event }) => {
    // Function to detect URLs within a string and return an array of parts (text and URLs)
    const parseTextForURLs = (text: string) => {
      const urlRegex = /(https?:\/\/[^\s]+)/g;
      const parts = text.split(urlRegex); // Split the text by URLs

      return parts.map((part, index) => {
        if (part.match(urlRegex)) {
          // If the part is a URL, return it as an anchor element
          return (
            <a
              key={index}
              href={part}
              target="_blank"
              rel="noopener noreferrer"
              style={{ marginLeft: "5px" }}
            >
              {part}
            </a>
          );
        } else {
          // If the part is not a URL, return it as a span element
          return <span key={index}>{part}</span>;
        }
      });
    };

    return (
      <div>
        <strong>{event.title}</strong>
        {event.interviewDetails && (
          <p>Details: {parseTextForURLs(event.interviewDetails)}</p>
        )}
      </div>
    );
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!customer) return <div>Customer not found.</div>;
  if (!nanny) return <div>Nanny not found.</div>;
  if (!newEvent) return <div>Loading event form...</div>; // Render loading state until newEvent is set

  return (
    <div className={styles.pageContainer}>
    <div className={styles.contentContainer}>
      <div className={styles.calendarHeader}>
        <h1>Calendar</h1>
        <button onClick={() => setShowModal(true)} className={styles.infoButton}>Advice of appointment</button>
      </div>
      {showModal && (
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-auto bg-gray-900 bg-opacity-50">
          <div className="relative bg-white rounded-lg mx-4 md:max-w-md p-6 w-full">
          <h3 style={{ fontWeight: 'bold' }}>Scheduling Appointments with Your Nanny</h3>
            <p>When planning appointments:</p>
            <ul>
              <li><span style={{ fontWeight: 'bold' }}>Preferred Dates:</span> {"It's best to choose dates when the nanny has already indicated availability."}</li>
              <li><span style={{ fontWeight: 'bold' }}>Open Slots:</span> {"You are also welcome to propose events on dates that currently show no specific availability status. These are considered open slots."}</li>
              <li><span style={{ fontWeight: 'bold' }}>Please Note:</span> {"While we encourage scheduling within open slots, we cannot guarantee the nanny’s availability for these times."}</li>
            </ul>
            <br/>
            <p>{"Thank you for understanding. We aim to make the scheduling process as smooth as possible for both users and nannies."}</p>
            <button onClick={() => setShowModal(false)} className="absolute top-0 right-0 m-2">
              <Image src={cancel} width={20} height={20} alt='..' layout='fixed' />
            </button>
          </div>
        </div>
      )}

        <h2>Add New Event</h2>
        <div>
          <input
            type="text"
            placeholder="Add Title"
            style={{ width: "20%", marginRight: "10px" }}
            value={newEvent.title}
            onChange={(e) =>
              setNewEvent({ ...newEvent, title: e.target.value })
            }
          />
          <textarea
            placeholder="Details"
            value={newEvent?.interviewDetails || ""}
            onChange={(e) =>
              setNewEvent({ ...newEvent, interviewDetails: e.target.value })
            }
            style={{ width: "100%", marginTop: "10px" }}
          />
          <div style={{ marginRight: "10px" }}>
            Start Date :
            <DatePicker
              placeholderText="Start Date"
              selected={newEvent.start}
              onChange={(date: Date) =>
                setNewEvent({ ...newEvent, start: date || new Date() })
              }
              showTimeSelect
              dateFormat="Pp"
              minDate={newEvent.start || new Date()}
              minTime={isToday(newEvent.start) ? setHours(setMinutes(new Date(), new Date().getMinutes()), new Date().getHours()) : setHours(setMinutes(new Date(), 0), 0)} // Set minimum time to current time if today, else start of day
              maxTime={setHours(setMinutes(new Date(), 59), 23)} 
            />
          </div>
          <div>
            End Date :
            <DatePicker
              placeholderText="End Date"
              selected={newEvent.end}
              onChange={(date: Date) =>
                setNewEvent({ ...newEvent, end: date || new Date() })
              }
              showTimeSelect
              dateFormat="Pp"
              minDate={newEvent.start || new Date()}
              minTime={isToday(newEvent.start) ? setHours(setMinutes(new Date(), new Date().getMinutes()), new Date().getHours()) : setHours(setMinutes(new Date(), 0), 0)} // Set minimum time to current time if today, else start of day
              maxTime={setHours(setMinutes(new Date(), 59), 23)} 
            />
          </div>
          <button style={{ marginTop: "10px" }} onClick={handleAddEvent}>
            Add Event
          </button>
        </div>
        <Calendar
          localizer={localizer}
          events={allEvent}
          startAccessor="start"
          endAccessor="end"
          style={{ height: 500 }}
          eventPropGetter={eventStyleGetter}
          components={{
            event: CustomEvent as any, // Cast to 'any' if necessary to match Big Calendar's expected types
          }}
        />
      </div>
    </div>
  );
}
