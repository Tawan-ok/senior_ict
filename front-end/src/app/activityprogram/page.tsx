'use client';

import React, { useEffect, useState, FormEvent } from 'react';
import axios from 'axios';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import Image from 'next/image';
import Link from 'next/link';
import { act } from 'react-dom/test-utils';
import jwt_decode from 'jwt-decode';
import { useRouter } from "next/navigation";
import styles from '../../../styles/activityprogram.module.css'
import { fetchActivityPrograms, updateActivityProgram, userResponse } from '../../../Component/apiService';
type Props = {};

interface ActivityProgramPageProps {
    customer_id: number;
}

type Customer = {
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
};

type ActivityProgram = {
    ProgramID: number;
    Normal_Period1: string;
    Normal_Period2: string;
    Normal_Period3: string;
    Normal_Period4: string;
    Normal_Period5: string;
    Overnight_Period1: string;
    Overnight_Period2: string;
    Overnight_Period3: string;
    Overnight_Period4: string;
    Overnight_Period5: string;
    customer_id: number;
}

export default function ActivityProgramPage({ }: Props) {
    const [customer, setCustomer] = useState<Customer | null>(null);
    const [activity, setActivity] = useState<ActivityProgram[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const router = useRouter();

    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError("User ID not found in token.");
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
              }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    // const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                    const response = await userResponse(userId);

                    // const response2 = await axios.get<ActivityProgram[]>(`http://localhost:9000/api/activityprogram/getbyusername/${userId}`);
                    const response2 = await fetchActivityPrograms(userId);
                    setCustomer(response.data);
                    setActivity(response2.data);
                    console.log('Response from API:', response2.data);
                    console.log('Customer id: ', response.data.username);
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
    }, []);





    const handleInputChange = async (index: number, field: string, value: string) => {
        const updatedActivityCopy = [...activity];
        updatedActivityCopy[index] = { ...updatedActivityCopy[index], [field]: value };
        setActivity(updatedActivityCopy);
    };



    const handleConfirmEdit = async () => {
        try {
            for (let i = 0; i < activity.length; i++) {
                // const response = await axios.put(`http://localhost:9000/api/activityprogram/${activity[i].customer_id}`, activity[i]);
                const response = await updateActivityProgram(activity[i].customer_id, activity[i]);
                 alert("Activity program updated!");
                console.log('Activity program updated:', response.data);
            }
        } catch (error) {
            console.error('Error updating activity program:', error);
            setError('Error updating activity program');
        }
    };



    if (!activity) return <div>Activity not found.</div>;

    return (
        <div style={{ fontFamily: 'Montserrat', }} className={styles.rootContainer}>
            <div className='contentContainer p-2'>
                <div className="mt-10 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-6xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-white">Activity Program</span>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-1 gap-8 mt-5 justify-center">
                    {activity.map((activityProgram, index) => (
                        <div key={index} className="bg-white rounded-lg p-6 flex flex-col items-center shadow-md">
                            <h3 className="text-lg font-bold mb-4">Morning Period</h3>
                            <div className="flex flex-col gap-4">
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`normalPeriod1-${index}`} className="text-sm font-semibold">Period 1: 06.00 - 08.00</label>
                                    <input
                                        id={`normalPeriod1-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Normal_Period1 : ''}
                                        onChange={(e) => handleInputChange(index, 'Normal_Period1', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`normalPeriod2-${index}`} className="text-sm font-semibold">Period 2: 09.00 - 11.00</label>
                                    <input
                                        id={`normalPeriod2-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Normal_Period2 : ''}
                                        onChange={(e) => handleInputChange(index, 'Normal_Period2', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`normalPeriod3-${index}`} className="text-sm font-semibold">Period 3: 12.00 - 13.00</label>
                                    <input
                                        id={`normalPeriod3-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Normal_Period3 : ''}
                                        onChange={(e) => handleInputChange(index, 'Normal_Period3', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`normalPeriod4-${index}`} className="text-sm font-semibold">Period 4: 14.00 - 16.00</label>
                                    <input
                                        id={`normalPeriod4-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Normal_Period4 : ''}
                                        onChange={(e) => handleInputChange(index, 'Normal_Period4', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`normalPeriod4-${index}`} className="text-sm font-semibold">Period 5: 17.00 - 19.00</label>
                                    <input
                                        id={`normalPeriod5-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Normal_Period5 : ''}
                                        onChange={(e) => handleInputChange(index, 'Normal_Period5', e.target.value)}
                                    />
                                </div>
                            </div>
                            <button className='rounded-lg bg-pink-500 py-2 px-4 text-white mt-4' onClick={handleConfirmEdit}>Confirm Edit</button>
                        </div>
                    ))}
                </div>
                <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-1 gap-8 mt-5 justify-center">
                    {activity.map((activityProgram, index) => (
                        <div key={index} className="bg-indigo-400 rounded-lg p-6 flex flex-col items-center shadow-md">
                            <h3 className="text-lg font-bold mb-4">Overnight Period</h3>
                            <div className="flex flex-col gap-4">
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`overnightPeriod1-${index}`} className="text-sm font-semibold">Period 6: 20.00 - 21.00</label>
                                    <input
                                        id={`overnightPeriod1-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Overnight_Period1 : ''}
                                        onChange={(e) => handleInputChange(index, 'Overnight_Period1', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`overnightPeriod1-${index}`} className="text-sm font-semibold">Period 7: 22.00 - 00.00</label>
                                    <input
                                        id={`overnightPeriod2-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Overnight_Period2 : ''}
                                        onChange={(e) => handleInputChange(index, 'Overnight_Period2', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`overnightPeriod3-${index}`} className="text-sm font-semibold">Period 8: 00.00 - 02.00</label>
                                    <input
                                        id={`overnightPeriod3-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Overnight_Period3 : ''}
                                        onChange={(e) => handleInputChange(index, 'Overnight_Period3', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`overnightPeriod4-${index}`} className="text-sm font-semibold">Period 9: 02.00 - 04.00</label>
                                    <input
                                        id={`overnightPeriod4-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Overnight_Period4 : ''}
                                        onChange={(e) => handleInputChange(index, 'Overnight_Period4', e.target.value)}
                                    />
                                </div>
                                <div className="flex flex-col gap-2">
                                    <label htmlFor={`overnightPeriod5-${index}`} className="text-sm font-semibold">Period 10: 04.00 - 06.00</label>
                                    <input
                                        id={`overnightPeriod5-${index}`}
                                        className='rounded-lg border px-3 py-2'
                                        type='text'
                                        value={activityProgram ? activityProgram.Overnight_Period5 : ''}
                                        onChange={(e) => handleInputChange(index, 'Overnight_Period5', e.target.value)}
                                    />
                                </div>
                            </div>
                            <button className='rounded-lg bg-pink-500 py-2 px-4 text-white mt-4' onClick={handleConfirmEdit}>Confirm Edit</button>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}      