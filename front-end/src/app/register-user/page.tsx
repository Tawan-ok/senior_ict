"use client";
import RootLayout from "../layout";
import logo from "../../../assets/Logo.png";
import Image from "next/image";
import React, { FormEvent, useState } from "react";
import styles from "../../../styles/Register.module.css";
import axios from "axios";
import { useRouter } from "next/navigation";
import Link from "next/link";
import { registerCustomer } from "../../../Component/apiService";
import profileData from "../../../Component/defaultprofile/page";
type Props = {};

export default function RegisterUserpage({}: Props) {
  const router = useRouter();
  const [profile, setProfile] = useState(profileData);
  const [formData, setFormData] = useState({
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    district: "",
    sub_district: "",
    province: "",
    zip_code: "",
    street_number: "",
    contact_number: "",
    role: "USER",
    age: "",
    gender: "",
    locationall: "",
    citizen_id: "",
    profile_image_url: profile,
  });

  // const handleChange = (e: any) => {
  //   setFormData({ ...formData, [e.target.name]: e.target.value })
  //   // const newLocation = `${formData.district} ${formData.sub_district} ${formData.province} ${formData.zip_code} ${formData.street_number}`;
  //   // setFormData({ ...formData, locationall: newLocation });
  // }
  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;

    if (name === "username" && value.includes(" ")) {
      alert("Spaces are not allowed in the username.");
      const newValue = value.replace(/\s+/g, "");
      setFormData({ ...formData, [name]: newValue });
      return;
    }

    if (name === "username" && value.includes(" ")) {
      alert("Spaces are not allowed in the username.");
      const newValue = value.replace(/\s+/g, "");
      setFormData({ ...formData, [name]: newValue });
      return;
    }
    if (name === "contact_number") {
      const regex = /^[0-9]*$/;
      if (!regex.test(value)) {
        alert("Only numeric values are allowed.");
        return;
      }
      if (value.length > 10) {
        alert("Contact number must not exceed 10 digits.");
        return;
      }
    }

    if (name === "contact_number" && value.length > 10) {
      alert("Contact number must not exceed 10 digits.");
      return; // Prevent update if validation fails
    }

    setFormData({ ...formData, [name]: value });

    // Update locationall based on other fields' values
    const newLocation = `${formData.district} ${formData.sub_district} ${formData.province} ${formData.zip_code} ${formData.street_number}`;
    setFormData((prevFormData) => ({
      ...prevFormData,
      locationall: newLocation,
    }));
  };
  // const handleChangeLocation = (e: any) => {

  //   const { district, sub_district, province, zip_code, street_number } = formData;
  //   const newLocation = `${district}, ${sub_district}, ${province}, ${zip_code}, ${street_number}`;
  //   setFormData({ ...formData, locationall: newLocation });
  // }
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    if (parseInt(formData.age) < 20) {
      alert("Age must be 20 years or older.");
      return; // Stop submission if the age is too low
    }
  
    if (formData.citizen_id.length !== 13) {
      alert("Citizen ID must be exactly 13 digits.");
      return; // Prevent submission if the citizen ID is not exactly 13 digits
    }
    try {
      const response = await registerCustomer(formData);
      // const response = await axios.post('http://localhost:9000/api/customers/register', formData);

      // const response = await axios.post('http://localhost:9000/api/customers/register', formData);
      const data = response.data;
      console.log("Registered:", data);
      router.push("/login-user");
    } catch (error: any) {
      if (error.response && error.response.status === 500) {
        alert("Email is already in use.");
      }
      console.error("Error registering:", error);
    }
  };
  return (
    <div className={styles.rootContainer}>
      <RootLayout showNavbar={false}>
        <div className="flex justify-center mx-auto">
          <div className="text-center mt-5">
            <Image
              src={logo}
              className="img-fluid"
              width="250"
              height="150"
              alt=""
            />
          </div>
        </div>

        <div className="text-center">
          <div className={`p-4 m-10 rounded-2xl ${styles.borderBox}`}>
            <span
              style={{ fontFamily: "Comfortaa" }}
              className="text-xl mt-4 sm:text-2xl md:text-3xl lg:text-4xl xl:text-5xl 2xl:text-6xl text-black font-bold"
            >
              Register User
            </span>

            <form
              onSubmit={handleSubmit}
              className="flex flex-col lg:grid lg:grid-cols-2 gap-2 text-center items-center"
            >
              <div className="mt-10 username">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Username <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  name="username"
                  value={formData.username}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 firstname">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  First Name <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="first_name"
                  value={formData.first_name}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 lastname">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Last Name <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="last_name"
                  value={formData.last_name}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 contactNum">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Citizen ID <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="number"
                  name="citizen_id"
                  value={formData.citizen_id}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 emails">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Email <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="email"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 passwords">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Password <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="password"
                  name="password"
                  value={formData.password}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 districts">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  District <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="district"
                  value={formData.district}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 subDistricts">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Sub District <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="sub_district"
                  value={formData.sub_district}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 streetnums">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Street Number <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="street_number"
                  value={formData.street_number}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 provice">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Province <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="province"
                  value={formData.province}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 zipcode">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Zip Code <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="text"
                  name="zip_code"
                  value={formData.zip_code}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 contactNum">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Contact Number <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="tel"
                  name="contact_number"
                  value={formData.contact_number}
                  onChange={handleChange}
                  required
                  pattern="[0-9]{10}"
                  title="Contact number must be 10 digits."
                  className="p-2 rounded-3xl"
                  placeholder="Enter 10-digit number"
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 age">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Age <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <input
                  type="number"
                  name="age"
                  value={formData.age}
                  onChange={handleChange}
                  required
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                  min="20"
                />
              </div>

              <div className="mt-10 gender">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Gender <span style={{ color: "red" }}>*</span>{" "}
                </label>
                <select
                  name="gender"
                  value={formData.gender}
                  onChange={handleChange}
                  required
                >
                  <option value="">Select Gender</option>
                  <option value="female">Female</option>
                  <option value="Male">Male</option>
                  <option value="PreferNotToSay">Prefer not to say</option>
                </select>
              </div>

              <div className="mt-10 locationAll">
                <label
                  style={{ fontFamily: "Comfortaa" }}
                  className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
                >
                  Location All
                </label>
                <input
                  type="text"
                  name="location_all"
                  value={formData.locationall}
                  onChange={handleChange}
                  required
                  readOnly
                  className={`p-2 rounded-3xl`}
                  style={{ fontFamily: "Comfortaa" }}
                />
              </div>

              <div className="mt-10 col-span-2 text-white bg-red-300 p-4 rounded-2xl">
                <button style={{ fontFamily: "Comfortaa" }} type="submit">
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </RootLayout>
    </div>
  );
}
