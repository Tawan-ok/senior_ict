'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"
import { useRouter } from 'next/navigation'
import jwt_decode from 'jwt-decode';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type BookingHistory = {
    booking_id: number,
    status: string,
    time_session: number
};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}



export default function Page({ params }: { params: { username: string } }) {
    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null

    const [bookingqueuehistory, setBookingqueuehistory] = useState<BookingHistory | null>(null); // Initialize as null
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);

    const [bookingqueue2, setBookingqueue2] = useState<BookingQueue | null>(null); // Initialize as null

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [value, onChange] = useState<Value>(new Date());
    const [response1, setResponse1] = useState<Nanny | null>(null);

    const [newScore, setNewScore] = useState<number | null>(null);

    const [startdate, setstartDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [enddate, setEndtDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [formData, setFormData] = useState({
        name: '',
        email: '',
    });

    const [selectedOptions, setSelectedOptions] = useState<string[]>([]);
    const [otherOption, setOtherOption] = useState<string>('');
    const [suggestions, setSuggestions] = useState<string>('');

    const handleSuggestionsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSuggestions(event.target.value);
    };

    const handleOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const optionValue = event.target.value;
        const isChecked = event.target.checked;

        if (optionValue === 'other') {
            setOtherOption('');
        }

        setSelectedOptions((prevSelectedOptions) => {
            if (isChecked) {
                return [...prevSelectedOptions, optionValue];
            } else {
                return prevSelectedOptions.filter((option) => option !== optionValue);
            }
        });
    };
    const [selectedOption2, setSelectedOption2] = useState<string>('');

    const handleOptionChange2 = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedOption2(event.target.value);
    };

    const getstartTimes = () => {
        if (!startdate.justDate) return

        const { justDate } = startdate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const starttimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            starttimes.push(i)
        }
        return starttimes
    }

    const getendTimes = () => {
        if (!enddate.justDate) return

        const { justDate } = enddate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const endtimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            endtimes.push(i)
        }
        return endtimes
    }

    const starttimes = getstartTimes()
    const endtimes = getendTimes()
    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-admin');
    };
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError("User ID not found in token.");
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }


            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    if (!customer || !nanny) {
                        const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                        // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
                        const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                        // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
                        setCustomer(response.data);
                        setNanny(response1.data);

                        if (response.data && response1.data) {
                            const response2 = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            // const response2 = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            setBookingqueue(response2.data);
                            console.log("All DATA Booking ", response2.data);
                            // Loop through bookingqueue and create BookingHistory for each booking

                            const response4 = await axios.get<BookingQueue>(`http://localhost:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            // const response4 = await axios.get<BookingQueue>(`http://35.213.139.253:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            setBookingqueue2(response4.data);
                            console.log("All DATA Booking TEST STATUS ", response4.data);
                            console.log("All DATA Booking TEST STATUS ", response4.data.id);
                            // for (const booking of response2.data) {

                            const response3 = await axios.get<BookingHistory>(`http://localhost:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            // const response3 = await axios.get<BookingHistory>(`http://35.213.139.253:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            setBookingqueuehistory(response3.data);
                            //   console.log("Booking ID:", response3.data.booking_id);
                            console.log("All DATA Booking History", response3.data);
                            // }
                        }

                    }
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes
    }, [params.username, response1]);

    // Confirm Hiring เป็นหน้ากดปุ่ม Confirm เพื่อ post ข้อมูล ลง Booking History เพื่อไปเรียกใช้ ในหน้า success fully hiring

    const handleUpdateStatus = async () => {
        try {

            if (!bookingqueue2) return <div>Bookingqueuehistory not found.</div>;
            console.log(bookingqueue2?.id)
            await axios.put(`http://localhost:9000/api/bookingqueue/updateStatusPaidSuccess/${bookingqueue2.id}`, {
                // await axios.put(`http://35.213.139.253:9000/api/bookingqueue/updateStatusPaidSuccess/${bookingqueue2.id}`, {
                status: 'Success',
            });

            await axios.put(`http://localhost:9000/api/bookinghistory/updatestatuscomplete/${bookingqueue2.id}`, {
                // await axios.put(`http://35.213.139.253:9000/api/bookinghistory/updatestatuscomplete/${bookingqueue2.id}`, {
                status: 'Completed',
            });


        } catch (err) {
            if (err instanceof Error) {
                console.error(err.message);
            } else {
                console.error('An error occurred while updating the status.');
            }
        }
    };

    const handleScoreChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newScoreValue = parseFloat(e.target.value);
        setNewScore(newScoreValue);
    };

    const handleUpdateScore = async () => {
        if (newScore === null) {
            console.error('New score is missing or invalid.');
            return;
        }
        if (!nanny) return <div>Nanny not found.</div>;
        try {
            // Update the booking queue's score
            const bookingQueueResponse = await axios.put(
                `http://localhost:9000/api/bookingqueue/updateScorebk/${bookingqueue2?.id}`,
                newScore,
                {
                    params: {
                        newScore: newScore,
                    },
                }
            );
            console.log('Booking queue score updated successfully:', bookingQueueResponse.data);

            // Update the nanny's score
            const nannyResponse = await axios.put(
                `http://localhost:9000/api/nannies/updateScore/${nanny.id}`,
                newScore,
                {
                    params: {
                        newScore: newScore,
                    },
                }
            );
            console.log('Nanny score updated successfully:', nannyResponse.data);

        } catch (error) {
            console.error('Error updating scores:', error);
        }

        // try {

        //     const response = await axios.put(`http://localhost:9000/api/bookingqueue/updateScorebk/${bookingqueue2?.id}`, newScore, {
        //         // const response = await axios.put(`http://35.213.139.253:9000/api/nannies/updateScore/${nanny.id}`, newScore, {
        //         params: {
        //             newScore: newScore,
        //         },
        //     });
        //     console.log('Nanny score updated successfully:', response.data);

        // } catch (error) {
        //     console.error('Error updating nanny score:', error);
        // }
    };


    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;

    if (!bookingqueuehistory) return <div>Bookingqueuehistory not found.</div>;

    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white">
                Assessment
            </div>

            {/* <div className="p-5 mt-4 mb-4  mx-6 bg-gray-100 rounded-lg shadow-md">
                <div>
                    <label htmlFor="purpose" className="block mb-2 font-semibold"><strong>What is the purpose of hiring a nanny? (Select multiple options)</strong></label>
                    <div className="mb-2">
                        <label>
                            <input
                                type="checkbox"
                                value="babysitting"
                                checked={selectedOptions.includes('babysitting')}
                                onChange={handleOptionChange}
                            />
                            Babysitting while working
                        </label>
                    </div>
                    <div className="mb-2">
                        <label>
                            <input
                                type="checkbox"
                                value="learning"
                                checked={selectedOptions.includes('learning')}
                                onChange={handleOptionChange}
                            />
                            Assisting with learning and skill development
                        </label>
                    </div>
                    <div className="mb-2">
                        <label>
                            <input
                                type="checkbox"
                                value="other"
                                checked={selectedOptions.includes('other')}
                                onChange={handleOptionChange}
                            />
                            Other (please specify):
                        </label>
                    </div>
                    {selectedOptions.includes('other') && (
                        <div>
                            <input
                                type="text"
                                placeholder="Insert text here"
                                disabled={!selectedOptions.includes('other')}
                                value={selectedOptions.includes('other') ? otherOption : ''}
                                onChange={(event) => setOtherOption(event.target.value)}
                                className="p-3 rounded-lg border border-gray-300 w-full focus:outline-none focus:border-stone-900"
                            />
                        </div>
                    )}
                </div>

                <div className="mt-4">
                    <label htmlFor="help" className="block mb-2 font-semibold"><strong>How do you think hiring a babysitter helps in taking care of and developing your child? (Choose one)</strong></label>
                    <div className="mb-2">
                        <label>
                            <input
                                type="radio"
                                name="childCareOption"
                                value="creativePlay"
                                checked={selectedOption2 === 'creativePlay'}
                                onChange={handleOptionChange2}
                            />
                            Provides time for the child to learn and play creatively
                        </label>
                    </div>
                    <div className="mb-2">
                        <label>
                            <input
                                type="radio"
                                name="childCareOption"
                                value="safeNurtured"
                                checked={selectedOption2 === 'safeNurtured'}
                                onChange={handleOptionChange2}
                            />
                            Makes the child feel safe and nurtured
                        </label>
                    </div>
                </div>

                <div className="mt-4">
                    <label htmlFor="suggestions" className="block mb-2 font-semibold"><strong>Do you have any suggestions or recommendations for improving the babysitting service?</strong></label>
                    <div>
                        <input
                            type="text"
                            value={suggestions}
                            onChange={handleSuggestionsChange}
                            placeholder="Your suggestions..."
                            className="p-3 rounded-lg border border-gray-300 w-full focus:outline-none focus:border-stone-900"
                        />
                    </div>
                </div>
            </div> */}

            <div className="mt-4 mx-6 bg-gray-100 rounded-lg shadow-md p-2 text-center">
                <label htmlFor="newScore" className='text-lg font-semibold'>Give A Score for Nanny</label>
                <div className="flex justify-between p-3">
                    {[1, 2, 3, 4, 5].map(score => (
                        <div key={score} className="flex items-center">
                            <input
                                type="radio"
                                id={`newScore${score}`}
                                name="newScore"
                                value={score}
                                checked={newScore === score}
                                onChange={handleScoreChange}
                                className="mr-2"
                            />
                            <label htmlFor={`newScore${score}`} className={`cursor-pointer ${newScore === score ? 'text-blue-500 font-semibold' : 'text-gray-500'}`}>{score}</label>
                        </div>
                    ))}
                </div>

                {/* <div className="mt-10 flex flex-col justify-center">
                    <div className='bg-white text-black border border-pink-400 rounded-lg'>
                        <Link href={`/reportconclusion/${nanny.username}`}>
                            <button onClick={handleUpdateScore} className="p-2">Submit</button>
                        </Link>
                    </div>
                    <div style={{ display: 'inline-block' }} className='bg-white text-black border border-pink-400 rounded-lg'>
                        <Link href={`/complain/${nanny.username}`}>
                            <button onClick={handleUpdateScore} className="p-2">Complain</button>
                        </Link>
                    </div>
                </div> */}
                <div className="mt-10 flex flex-col justify-center">
                    <div className='bg-white text-black border border-pink-400 rounded-lg'>
                        <Link href={`/reportconclusion/${nanny.username}`}>
                        <button onClick={handleUpdateScore} className={`p-2 ${newScore === null ? 'opacity-50 cursor-not-allowed' : ''}`} disabled={newScore === null}>
                                {newScore === null ? 'Select a score before submit' : 'Submit'}
                            </button>
                        </Link>
                    </div>
                    <div style={{ display: 'inline-block' }} className='bg-white text-black border border-pink-400 rounded-lg'>
                        <Link href={`/complain/${nanny.username}`}>
                            <button onClick={handleUpdateScore} className={`p-2 ${newScore === null ? 'opacity-50 cursor-not-allowed' : ''}`} disabled={newScore === null}>
                                {newScore === null ? 'Select a score before complaining' : 'Complain'}
                            </button>
                        </Link>
                    </div>
                </div>

            </div>
        </div>
    );

}