'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"
import jwt_decode from 'jwt-decode';
import { useRouter } from 'next/navigation';
import Image from 'next/image';
import payment from '../../../../assets/mobile-banking.png';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    profile_image_url: string;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}

export default function Page({ params }: { params: { username: string } }) {
    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [value, onChange] = useState<Value>(new Date());
    const [response1, setResponse1] = useState<Nanny | null>(null);
    const [startdate, setstartDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [enddate, setEndtDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })


    const getstartTimes = () => {
        if (!startdate.justDate) return

        const { justDate } = startdate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const starttimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            starttimes.push(i)
        }
        return starttimes
    }

    const getendTimes = () => {
        if (!enddate.justDate) return

        const { justDate } = enddate
        const beginning = add(justDate, { hours: 9 })
        const end = add(justDate, { hours: 17 })
        const interval = 30

        const endtimes = []
        for (let i = beginning; i <= end; i = add(i, { minutes: interval })) {
            endtimes.push(i)
        }
        return endtimes
    }

    const formatDateToISO = (dateString: string | number | Date, includeTime: boolean = false) => {
        const date = new Date(dateString);
        const day = date.getDate();
        const month = date.toLocaleString('default', { month: 'long' });
        const year = date.getFullYear();
        const hour = date.getHours();
        const minute = date.getMinutes();

        const paddedHour = hour.toString().padStart(2, '0');
        const paddedMinute = minute.toString().padStart(2, '0');

        let formattedDate = `${day} ${month} ${year}`;
        if (includeTime) {
            formattedDate += ` ${paddedHour}:${paddedMinute}`;
        }

        return formattedDate;
    };



    const starttimes = getstartTimes()
    const endtimes = getendTimes()
    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-user');
    };
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
              }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    if (!customer || !nanny) {
                        const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                        // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
                        const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                        // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
                        setCustomer(response.data);
                        setNanny(response1.data);

                        if (response.data && response1.data) {
                            // const response2 = await axios.get(`http://localhost:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            const response2 = await axios.get(`http://localhost:9000/api/bookingqueue/getbookingstatus/${response.data.id}/${response1.data.id}`);
                            // const response2 = await axios.get(`http://35.213.139.253:9000/api/bookingqueue/getbookingstatus/${response.data.id}/${response1.data.id}`);
                            setBookingqueue(response2.data);

                            console.log("All DATA", response1.data);
                            console.log("Nanny DATA", response1.data.email);

                            console.log("Booking QUEUE", response2.data);
                            console.log("Booking QUEUE DATE", response2.data.hours);
                        }
                    }
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes
    }, [params.username, response1]);


    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;
    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;
    if (!bookingqueue) return <div>Bookingqueue not found.</div>;

    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="block justify-center">
                <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-white">Payment</span>
                </div>
                {/* Other customer and nanny details here */}

                <div>
                    {bookingqueue.length === 0 ? (
                        <div className="mt-5 p-3 text-white text-xl sm:text-xl md:text-2xl lg:text-2xl xl:text-2xl 2xl:text-3xl font-bold bg-white border-2 border-solid border-pink-500 mr-4 ml-4 rounded-lg font-montserrat flex flex-col items-center justify-center">
                            <Image src={payment} alt="" width={200} height={150} layout="fixed" />
                            <p className="text-black mt-2" style={{ fontFamily: 'Montserrat' }}>No bookings yet, Please wait for the nanny to confirm.</p>
                        </div>
                    ) : (
                        <div className="mt-5 mb-5 bg-white bg-opacity-40 p-5 border border-pink-300 shadow-lg md:w-2/3 lg:w-1/2 xl:w-2/5 mx-auto">
                            <p className="mb-3 text-center bg-pink-500 shadow-lg p-4 font-bold text-white text-xl sm:text-xl md:text-2xl lg:text-2xl xl:text-2xl 2xl:text-3xl">Detail of Hiring</p>
                            <div className="flex flex-row">
                                <div className="bg-white mb-4 mx-auto p-4 text-center font-semibold font-montserrat">
                                    {/* Render booking details when there are bookings */}
                                    {bookingqueue.map((bookingqueues) => (
                                        <div key={bookingqueues.id} className="flex flex-col items-center justify-center">
                                            <div className="mb-4 sm:mb-0 sm:mr-4 flex items-center justify-center rounded-full overflow-hidden h-48 w-48 border-4 border-pink-300">
                                                <img src={"data:image/png;base64," + nanny.profile_image_url} alt="" className="object-cover w-full h-full" />
                                            </div>
                                            <p className="mt-2 font-bold text-lg sm:text-xl">{nanny?.first_name} {nanny?.last_name}</p>
                                            <div className='mt-2 p-2 bg-purple-100'>
                                                <p className="font-semibold">Location Hiring:</p>
                                                <p>{bookingqueues.locationhiring}</p>
                                            </div>
                                            <div className="mt-4 bg-pink-100 rounded-2xl border border-pink-200 text-start">
                                                <div className="bg-white shadow-md rounded-xl p-4 sm:p-6">
                                                    <div className="grid grid-cols-2 gap-2 sm:gap-4">
                                                        <div>
                                                            <p className="font-semibold">Start Date:</p>
                                                            <p>{formatDateToISO(new Date(bookingqueues.start_date), true)}</p>
                                                        </div>
                                                        <div>
                                                            <p className="font-semibold">End Date:</p>
                                                            <p>{formatDateToISO(new Date(bookingqueues.end_date), true)}</p>
                                                        </div>
                                                        <div>
                                                            <p className="font-semibold">Hours:</p>
                                                            <p>{bookingqueues.hours}</p>
                                                        </div>
                                                        <div>
                                                            <p className="font-semibold">Total Amount:</p>
                                                            <p>{bookingqueues.total_amount.toLocaleString()}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>

                            <div className='flex justify-center'>
                                <Link href={`/confirmhiring/${nanny.username}`}>
                                    <button className="bg-green-400 p-4 text-center text-white rounded-xl border border-green-500 font-medium font-montserrat">Confirm</button>
                                </Link>
                            </div>
                        </div>

                    )}
                </div>
            </div>
        </div >
    );

}