"use client";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Image from "next/image";
import styles from "../../../styles/rankingNanny.module.css";
import { fetchNannyRankings } from "../../../Component/apiService";

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  profile_image_url: string;
  weightedScore: number;
};

const RankingPage: React.FC = () => {
  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchNannies = async () => {
      try {
        // const response = await axios.get<
        //   { nanny: Nanny; weightedScore: number }[]
        // >("http://localhost:9000/api/nannies/ranking");
        // // >("http://35.213.139.253:9000/api/nannies/ranking");
        // // Handle the nested structure here
        // setNannies(response.data.map((item) => item.nanny));

        const rankedNannies = await fetchNannyRankings();
        setNannies(rankedNannies);
      } catch (error) {
        setError("An error occurred while fetching the nanny rankings.");
        console.error("Error fetching nanny rankings:", error);
      }
      setLoading(false);
    };

    fetchNannies();
  }, []);

  if (loading) return <div>Loading...</div>;
  if (error) return <div>{error}</div>;

  nannies.sort((a, b) => b.score - a.score);

  return (
    <div style={{ fontFamily: 'Montserrat', }} className="p-8 grid grid-cols-1 md:grid-cols-1 md:p-12 lg:grid-cols-1 lg:p-10 xl:grid-cols-1 xl:p-10 gap-6 rounded-lg shadow-md">
      <div className="text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white py-4 rounded-t-lg">
        Ranking Nanny
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-6">
        {!loading && nannies.map((nanny, index) => (
          <div key={nanny.id} className="bg-white rounded-lg shadow-md overflow-hidden">
            <div className="text-center p-2">
              <p className="text-black font-bold text-center">Ranking {index + 1}</p>
            </div>
            <img
              src={`data:image/jpeg;base64,${nanny.profile_image_url}`}
              alt={`${nanny.first_name} ${nanny.last_name}`}
              className="w-full h-48 object-cover"
            />
            <div className="p-4">
              <p className="text-xl font-semibold mb-2">Nanny Name: {nanny.first_name} {nanny.last_name}</p>
              <p className="text-gray-600">Score: {nanny.score}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default RankingPage;
