'use client';

import axios from 'axios';
import React, { ChangeEvent, useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, addMinutes, addMonths, format, getHours, getMinutes, isSameDay, isSameHour, set } from "date-fns"
import { addHours } from 'date-fns';
import { useRouter } from 'next/navigation';
import jwt_decode from 'jwt-decode';



type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
    locationall: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
    citizen_id: string;
    profile_image_url: string;
    skill_1: string;
    skill_2: string;
    skill_3: string;
    hours_cost: number;
    days_cost: number;
    months_cost: number;
};

type BookingQueue = {

    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number,
    hours_cost: number,
    days_cost: number,
    months_cost: number,
    child_age: number,
    basic_totalrates: number,
    type_hiring: string,
    bk_score : number;
};


type BookingQueuetest = {
    id: number;
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number
    hours_cost: number;
    days_cost: number;
    months_cost: number;
    bk_score : number;
};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}

interface Slot {
    is_available: boolean;
    start_time: string;
    end_time: string;
}

export default function Page({ params }: { params: { username: string } }) {
    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null
    const [checkbk, setCheckbk] = useState<BookingQueuetest | null>(null); // Initialize as null
    const [childAge, setChildAge] = useState(0.1);
    const [checkbktest, setCheckbktest] = useState<BookingQueuetest[]>([]);
    const [selectedMonth, setSelectedMonth] = useState<number>(0); // Default to 1

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [value, onChange] = useState<Value>(new Date());
    const [response1, setResponse1] = useState<Nanny | null>(null);
    const [selectedTime, setSelectedTime] = useState<Date | null>(null);
    const [selectedTime2, setSelectedTime2] = useState<Date | null>(null);

    const [selectedRate, setSelectedRate] = useState<string>('hour');

    const [selectedRatetest, setSelectedRateTest] = useState<Number>(1);


    const [startdate, setstartDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const [enddate, setEndtDate] = useState<DateType>({
        justDate: null,
        dateTime: null,
    })

    const router = useRouter();
    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-admin');
    };

    const [nannyUnavailableDates, setNannyUnavailableDates] = useState<{ start: Date, end: Date }[]>([]);

    const [selectedDate, setSelectedDate] = useState<Date | null>(null);
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
              }

            console.log("User ID:", userId);
            const fetchData = async () => {

                try {
                    const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                    // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
                    const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                    // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);

                    setCustomer(response.data);
                    setNanny(response1.data);

                    const response_slot = await axios.get<Slot[]>(`http://localhost:9000/api/availability/nanny/${response1.data.id}`);
                    const unavailableSlots = response_slot.data.filter((slot: Slot) => !slot.is_available);
                    const unavailableDates = unavailableSlots.map(slot => ({
                        start: new Date(slot.start_time),
                        end: new Date(slot.end_time)
                    }));
                    setNannyUnavailableDates(unavailableDates);

                    // const response2 = await axios.get<BookingQueuetest>(`http://localhost:9000/api/bookingqueue/getbookingstestdate/${response1.data.id}`);
                    // setCheckbk(response2.data);
                    // console.log("Booking DATA", response2.data);
                    const response3 = await axios.get(`http://localhost:9000/api/bookingqueue/getbookingstestdate/${response1.data.id}`)
                    // const response3 = await axios.get(`http://35.213.139.253:9000/api/bookingqueue/getbookingstestdate/${response1.data.id}`);
                    // const response3 = await axios.get(`http://localhost:9000/api/bookingqueue`);
                    setCheckbktest(response3.data);
                    console.log("Booking DATA TEST", response3.data);

                    console.log("All DATA", response1.data);
                    console.log("Nanny DATA", response1.data.email);
                    console.log("Cost DATA", response1.data.hours_cost);

                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes

    }, [params.username, response1]);
    const getstartTimes = () => {
        if (!startdate.justDate) return;
        const { justDate } = startdate;
        const beginning = add(justDate, { hours: 1 });
        const end = add(justDate, { hours: 24 });
        const interval = 1; // Change the interval to represent hours

        const starttimes = [];
        for (let i = beginning; i <= end; i = add(i, { hours: interval })) {
            starttimes.push(i);
        }
        return starttimes;
    };

    const getendTimes = () => {
        if (!enddate.justDate) return;

        const { justDate } = enddate;
        const beginning = add(justDate, { hours: 1 });
        const end = add(justDate, { hours: 24 });
        const interval = 1; // Change the interval to represent hours

        const endtimes = [];
        for (let i = beginning; i <= end; i = add(i, { hours: interval })) {
            endtimes.push(i);
        }
        return endtimes;
    };

    const starttimes = getstartTimes();
    const endtimes = getendTimes();

    const isDateOverlapping = (startDate: Date, endDate: Date, bookings: BookingQueuetest[]): boolean => {
        for (const booking of bookings) {
            const bookingStartDate = new Date(booking.start_date);
            const bookingEndDate = new Date(booking.end_date);

            if (
                (startDate.getTime() == bookingStartDate.getTime()) ||
                (endDate.getTime() == bookingEndDate.getTime())
            ) {
                return true; // Dates overlap with an existing booking
            }
        }
        return false; // No overlap found
    };

    const isDateInPast = (date: Date): boolean => {
        const currentDate = new Date(); // Get the current date and time
        return date.getTime() < currentDate.getTime();
    };

    const isSlotAvailable = (bookingStart: Date, bookingEnd: Date): boolean => {
        return !nannyUnavailableDates.some(({ start, end }) => {
            const startsDuringUnavailability = bookingStart >= start && bookingStart < end;
            const endsDuringUnavailability = bookingEnd > start && bookingEnd <= end;
            const encompassesUnavailability = bookingStart <= start && bookingEnd >= end;

            return startsDuringUnavailability || endsDuringUnavailability || encompassesUnavailability;
        });
    };

    const handleChildAgeChange = (event: ChangeEvent<HTMLInputElement>): void => {
        const newValue: number = parseFloat(event.target.value); // Parse input value to float
        if (!isNaN(newValue)) {
            setChildAge(newValue); // Update state only if parsing is successful
        }
    };

    const createBookingQueue = async (): Promise<void> => {

        try {

            if (startdate.dateTime !== null && enddate.dateTime !== null) {

                if (!isSlotAvailable(startdate.dateTime, enddate.dateTime)) {
                    alert("The selected time slot overlaps with nanny's unavailable time. Please choose another time slot.");
                    return;
                }
            } else {
                alert("Please ensure both start and end dates are selected.");
                return;
            }

            if (!startdate.dateTime || !enddate.dateTime) {
                console.error('Nanny data or selected date is missing or null.');
                return;
            }

            if (!nanny) {
                console.error('Nanny data is missing or null.');
                return;
            }
            if (!customer) {
                console.error('Customer data is missing or null.');
                return;
            }

            const startDate = addHours(startdate.dateTime, 0);
            const endDate = addHours(enddate.dateTime, 0);
            const durationInHours = (endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60);

            if (startDate.getTime() === endDate.getTime()) {
                alert('Start and end time cannot be the same.');
                return;
            }

            if (startDate > endDate) {
                alert('Start Date cannot more than End Date.');
                return;
            }

            if (startdate.dateTime < new Date()) {
                alert('Selected start date is in the past. Please choose a future start date.');
                return;
            }

            if (enddate.dateTime < new Date()) {
                alert('Selected end date is in the past. Please choose a future end date.');
                return;
            }

            // Check for date overlap
            // for (const booking of checkbktest) {
            //     const bookingStartDate = new Date(booking.start_date);
            //     const bookingEndDate = new Date(booking.end_date);

            //     if (
            //         startDate >= endDate ||                               // New booking starts after it ends
            //         endDate <= bookingStartDate ||                        // New booking ends before or at the same time as the existing booking starts
            //         startDate >= bookingEndDate                           // New booking starts after or at the same time as the existing booking ends
            //     ) {
            //         // Dates do not overlap, proceed with booking
            //     } else {
            //         // Dates overlap with an existing booking, prevent booking
            //         alert('Selected dates overlap with existing bookings. Please choose different dates.');
            //         return;
            //     }


            // }
            for (const booking of checkbktest) {
                const bookingStartDate = new Date(booking.start_date);
                const bookingEndDate = new Date(booking.end_date);

                // Check if the new booking overlaps with any existing booking
                if (
                    (startDate >= bookingStartDate && startDate < bookingEndDate) || // New booking starts during an existing booking
                    (endDate > bookingStartDate && endDate <= bookingEndDate) ||   // New booking ends during an existing booking
                    (startDate <= bookingStartDate && endDate >= bookingEndDate)   // New booking encompasses an existing booking
                ) {
                    // Dates overlap with an existing booking, prevent booking
                    alert('Selected dates overlap with existing bookings. Please choose different dates.');
                    return;
                }
            }

            let totalAmount = 0.0;

            switch (selectedRate) {
                case 'hour':
                    if (durationInHours >= 24) {
                        alert('Hourly rate can only be selected for durations up to 24 hours.');
                        return;
                    }
                    totalAmount = durationInHours * (nanny.hours_cost || 0);
                    break;

                case 'day':
                    if (durationInHours > 600 || durationInHours < 24) {
                        alert('Daily rate can only be selected for durations up to 25 days.');
                        return;
                    }
                    
                    const durationInDays = Math.floor(durationInHours / 24);
                    const remainingHours = durationInHours % 24;

                    const dayCost = nanny.days_cost || 0;
                    const hourCost = nanny.hours_cost || 0;

                    totalAmount = (durationInDays * dayCost) + (remainingHours * hourCost);
                    break;

                case 'month':
                    const daysInMonth = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0).getDate();
                    if (durationInHours < (28 * 24)) {
                        alert('Monthly rate can only be selected for durations of 26 days or more.');
                        return;
                    }
                    const durationInMonths = (endDate.getFullYear() - startDate.getFullYear()) * 12 + (endDate.getMonth() - startDate.getMonth());
                    totalAmount = durationInMonths * (nanny.months_cost || 0);
                    break;

                default:
                    console.error('Invalid rate selected.');
                    return;
            }

            console.log("Testt" + selectedRate);
            // If no overlap found, create the booking
            const bookingData: BookingQueue = {
                customer_id: customer.id,
                nanny_id: nanny.id,
                start_date: startDate,
                end_date: endDate,
                total_amount: 0,
                status_payment: 'Pending',
                locationhiring: "null",
                hours: durationInHours,
                hours_cost: nanny.hours_cost,
                days_cost: nanny.days_cost,
                months_cost: nanny.months_cost,
                child_age: childAge,
                basic_totalrates:totalAmount,
                type_hiring:selectedRate,
                bk_score : 0,
            };
            alert("Select Date successfully");
            const response = await axios.post<BookingQueue>('http://localhost:9000/api/bookingqueue', bookingData);
            // const response = await axios.post<BookingQueue>('http://35.213.139.253:9000/api/bookingqueue', bookingData);
            console.log('Booking created successfully:', response.data);

        } catch (error) {
            console.error('Error creating booking:', error);
            alert("Booking created unsuccessfully");
        }
        router.push(`/nannydetaillocation/${nanny?.username}`);
    };

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;
    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;
    if (!checkbktest) return <div>BKK Test not found 2.</div>;


    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="bg-white m-3 block justify-center p-3">
                <div className="mt-5 text-center text-5xl md:text-5xl lg:text-5xl xl:text-5xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-black">Choose the period</span>
                </div>

                <div className='bg-white mt-8 mx-auto'>
                    <div className="flex text-start text-white p-4 border-solid bg-pink-300 shadow-xl">
                        <p style={{ fontFamily: 'Comfortaa', }} className='mt-3 font-bold'>Choose the program of hiring</p>
                        <button
                            className={`mx-2 p-3 border-2 border-solid border-pink-500 bg-white text-black ${selectedRate === 'hour' ? 'opacity-100' : 'opacity-50'}`}
                            onClick={() => {
                                setSelectedRate('hour');
                                setSelectedRateTest(1)
                            }}
                            disabled={selectedRate === 'hour'}
                            style={{ fontFamily: 'Comfortaa', }}
                        >
                            Hour
                        </button>
                        <button
                            className={`mx-2 p-3 border-2 border-solid border-pink-500 bg-white text-black ${selectedRate === 'day' ? 'opacity-100' : 'opacity-50'}`}
                            onClick={() => {
                                setSelectedRate('day');
                                setSelectedRateTest(2)
                            }}
                            disabled={selectedRate === 'day'}
                            style={{ fontFamily: 'Comfortaa', }}
                        >
                            Day
                        </button>
                        <button
                            className={`mx-2 p-3 border-2 border-solid border-pink-500 bg-white text-black ${selectedRate === 'month' ? 'opacity-100' : 'opacity-50'}`}
                            onClick={() => {
                                setSelectedRate('month');
                                setSelectedRateTest(3)
                            }}
                            disabled={selectedRate === 'month'}
                            style={{ fontFamily: 'Comfortaa', }}
                        >
                            Month
                        </button>
                    </div>
                </div>

                <div className="mt-5 flex justify-center items-center">
                    <label htmlFor="child_age" className="text-black mr-2 font-semibold">
                        Child Age:
                    </label>
                    <div className="relative">
                        <input
                            type="number"
                            id="child_age"
                            value={childAge.toString()}
                            onChange={handleChildAgeChange}
                            min="0.1"
                            max="99"
                            step="0.1"
                            className="bg-gray-200 rounded-md p-2 text-gray-800 pl-10 pr-4 focus:outline-none focus:ring focus:border-blue-500"
                        />
                    </div>
                </div>

                {selectedRatetest === 1 && (
                    <div className="mt-5 flex flex-col items-center space-y-5">
                        <div style={{ fontFamily: 'Montserrat', }} className="flex flex-col items-center space-y-4">
                            <p className="font-semibold text-lg">Select Start Time</p>
                            {startdate.justDate ? (
                                <div className="grid grid-cols-3 gap-4">
                                    {starttimes?.map((time, i) => {
                                        const currentTime = new Date();
                                        const thirtyMinutesAfterCurrentTime = new Date(currentTime.getTime() + 30 * 60000); // Calculate 30 minutes after current time
                                        return (
                                            (time > thirtyMinutesAfterCurrentTime) && (
                                                <div
                                                    key={`start-time-${i}`}
                                                    className={`rounded-md bg-gray-200 p-3 cursor-pointer hover:bg-blue-500 hover:text-white text-center transition-colors duration-300 ${selectedTime && selectedTime.getTime() === time.getTime() ? 'bg-blue-500 text-white' : ''}`}
                                                    onClick={() => {
                                                        setstartDate((prev) => ({ ...prev, dateTime: time }));
                                                        setSelectedTime(time);
                                                    }}
                                                >
                                                    {format(time, 'kk:mm')}
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            ) : (
                                <DateTimePicker
                                    className="REACT-CALENDAR p-2 border border-gray-300 rounded-md"
                                    onChange={onChange}
                                    value={value}
                                    view="month"
                                    onClickDay={(date) => {
                                        setstartDate((prev) => ({ ...prev, justDate: date }));
                                        console.log('Selected Date:', date);
                                    }}
                                />
                            )}
                            {startdate.justDate && (
                                <button
                                    className="bg-gray-500 text-white p-2 rounded-md hover:bg-gray-600 transition-colors duration-300"
                                    onClick={() => {
                                        setstartDate((prev) => ({ ...prev, justDate: null }));
                                        setSelectedTime(null);
                                    }}
                                >
                                    Clear Selection
                                </button>
                            )}
                        </div>
                        <div className="flex flex-col items-center space-y-4">
                            <p className="font-semibold text-lg">Select End Time</p>
                            {enddate.justDate ? (
                                <div className="grid grid-cols-3 gap-4">
                                    {endtimes?.map((time, i) => {
                                        const currentTime = new Date();
                                        const thirtyMinutesAfterCurrentTime = new Date(currentTime.getTime() + 30 * 60000); // Calculate 30 minutes after current time
                                        return (
                                            (time > thirtyMinutesAfterCurrentTime) && (
                                                <div
                                                    key={`end-time-${i}`}
                                                    className={`rounded-md bg-gray-200 p-3 cursor-pointer hover:bg-blue-500 hover:text-white text-center transition-colors duration-300 ${selectedTime2 && selectedTime2.getTime() === time.getTime() ? 'bg-blue-500 text-white' : ''}`}
                                                    onClick={() => {
                                                        setEndtDate((prev) => ({ ...prev, dateTime: time }));
                                                        setSelectedTime2(time);
                                                    }}
                                                >
                                                    {format(time, 'kk:mm')}
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            ) : (
                                <DateTimePicker
                                    className="REACT-CALENDAR p-2 border border-gray-300 rounded-md"
                                    onChange={onChange}
                                    value={value}
                                    view="month"
                                    onClickDay={(date) => {
                                        setEndtDate((prev) => ({ ...prev, justDate: date }));
                                        console.log('Selected Date:', date);
                                    }}
                                />
                            )}
                            {enddate.justDate && (
                                <button
                                    className="bg-gray-500 text-white p-2 rounded-md hover:bg-gray-600 transition-colors duration-300"
                                    onClick={() => {
                                        setEndtDate((prev) => ({ ...prev, justDate: null }));
                                        setSelectedTime2(null);
                                    }}
                                >
                                    Clear Selection
                                </button>
                            )}
                        </div>
                    </div>
                )}


                {selectedRatetest === 2 && (
                    <div className="mt-5 flex flex-col items-center space-y-5">
                        <div style={{ fontFamily: 'Montserrat', }} className="flex flex-col items-center space-y-4">
                            <p className="font-semibold text-lg">Select Start Time</p>
                            {startdate.justDate ? (
                                <div className="grid grid-cols-3 gap-4">
                                    {starttimes?.map((time, i) => {
                                        const currentTime = new Date();
                                        const thirtyMinutesAfterCurrentTime = new Date(currentTime.getTime() + 30 * 60000); // Calculate 30 minutes after current time
                                        return (
                                            (time > thirtyMinutesAfterCurrentTime) && (
                                                <div
                                                    key={`time-${i}`}
                                                    className={`rounded-md bg-gray-200 p-3 cursor-pointer hover:bg-blue-500 hover:text-white text-center transition-colors duration-300 ${selectedTime && selectedTime.getTime() === time.getTime() ? 'bg-blue-500 text-white' : ''}`}
                                                    onClick={() => {
                                                        setstartDate((prev) => ({ ...prev, dateTime: time }));
                                                        setSelectedTime(time);
                                                    }}
                                                >
                                                    {format(time, 'kk:mm')}
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            ) : (
                                <DateTimePicker
                                    className="REACT-CALENDAR p-2 border border-gray-300 rounded-md"
                                    onChange={onChange}
                                    value={value}
                                    view="month"
                                    onClickDay={(date) => {
                                        setstartDate((prev) => ({ ...prev, justDate: date }));
                                        console.log('Selected Date:', date);
                                    }}
                                />
                            )}
                            {startdate.justDate && (
                                <button
                                    className="bg-gray-500 text-white p-2 rounded-md hover:bg-gray-600 transition-colors duration-300"
                                    onClick={() => {
                                        setstartDate((prev) => ({ ...prev, justDate: null }));
                                        setSelectedTime(null);
                                    }}
                                >
                                    Clear Selection
                                </button>
                            )}
                        </div>
                        <div className="flex flex-col items-center space-y-4">
                            <p className="font-semibold text-lg">Select End Time</p>
                            {enddate.justDate ? (
                                <div className="grid grid-cols-3 gap-4">
                                    {endtimes?.map((time, i) => {
                                        const currentTime = new Date();
                                        const thirtyMinutesAfterCurrentTime = new Date(currentTime.getTime() + 30 * 60000); // Calculate 30 minutes after current time
                                        return (
                                            (time > thirtyMinutesAfterCurrentTime) && (
                                                <div
                                                    key={`time-${i}`}
                                                    className={`rounded-md bg-gray-200 p-3 cursor-pointer hover:bg-blue-500 hover:text-white text-center transition-colors duration-300 ${selectedTime2 && selectedTime2.getTime() === time.getTime() ? 'bg-blue-500 text-white' : ''}`}
                                                    onClick={() => {
                                                        setEndtDate((prev) => ({ ...prev, dateTime: time }));
                                                        setSelectedTime2(time);
                                                    }}
                                                >
                                                    {format(time, 'kk:mm')}
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            ) : (
                                <DateTimePicker
                                    className="REACT-CALENDAR p-2 border border-gray-300 rounded-md"
                                    onChange={onChange}
                                    value={value}
                                    view="month"
                                    onClickDay={(date) => {
                                        setEndtDate((prev) => ({ ...prev, justDate: date }));
                                        console.log('Selected Date:', date);
                                    }}
                                />
                            )}
                            {enddate.justDate && (
                                <button
                                    className="bg-gray-500 text-white p-2 rounded-md hover:bg-gray-600 transition-colors duration-300"
                                    onClick={() => {
                                        setEndtDate((prev) => ({ ...prev, justDate: null }));
                                        setSelectedTime2(null);
                                    }}
                                >
                                    Clear Selection
                                </button>
                            )}
                        </div>
                    </div>
                )}

                {selectedRatetest === 3 && (
                    <div className="mt-5 flex flex-col items-center space-y-5">
                        <div style={{ fontFamily: 'Montserrat', }} className="flex flex-col items-center space-y-4">
                            <p className="font-semibold text-lg">Month</p>
                            {startdate.justDate ? (
                                <div className="grid grid-cols-3 gap-4">
                                    {starttimes?.map((time, i) => {
                                        const currentTime = new Date();
                                        const thirtyMinutesAfterCurrentTime = new Date(currentTime.getTime() + 30 * 60000); // Calculate 30 minutes after current time
                                        return (
                                            (time > thirtyMinutesAfterCurrentTime) && (
                                                <div
                                                    key={`time-${i}`}
                                                    className={`rounded-md bg-gray-200 p-3 cursor-pointer hover:bg-blue-500 hover:text-white text-center transition-colors duration-300 ${selectedTime && selectedTime.getTime() === time.getTime() ? 'bg-blue-500 text-white' : ''}`}
                                                    onClick={() => {
                                                        setstartDate((prev) => ({ ...prev, dateTime: time }));
                                                        setSelectedTime(time);
                                                    }}
                                                >
                                                    {format(time, 'kk:mm')}
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            ) : (
                                <DateTimePicker
                                    className="REACT-CALENDAR p-2 border border-gray-300 rounded-md"
                                    onChange={onChange}
                                    value={value}
                                    view="month"
                                    onClickDay={(date) => {
                                        setstartDate((prev) => ({ ...prev, justDate: date }));
                                        console.log('Selected Date:', date);
                                    }}
                                />
                            )}
                            {startdate.justDate && (
                                <button
                                    className="bg-gray-500 text-white p-2 rounded-md hover:bg-gray-600 transition-colors duration-300"
                                    onClick={() => {
                                        setstartDate((prev) => ({ ...prev, justDate: null }));
                                        setSelectedTime(null);
                                    }}
                                >
                                    Clear Selection
                                </button>
                            )}
                        </div>
                        <div style={{ fontFamily: 'Montserrat', }} className="mb-5 flex flex-col items-center space-y-4">
                            <div className="flex items-center space-x-4">
                                <label htmlFor="hireMonths" className="text-black">Hire for:</label>
                                <select
                                    id="hireMonths"
                                    value={selectedMonth}
                                    onChange={(e) => {
                                        const selectedMonth = parseInt(e.target.value);
                                        if (!isNaN(selectedMonth)) {
                                            const endDate = addMonths(startdate.justDate!, selectedMonth);
                                            const endTime = startdate.dateTime ? set(endDate, { hours: getHours(startdate.dateTime), minutes: getMinutes(startdate.dateTime) }) : endDate;
                                            setEndtDate({ justDate: endDate, dateTime: endTime });
                                            setSelectedTime2(endDate);
                                            setSelectedMonth(selectedMonth);
                                            console.log("TEST END DATE" + endDate);
                                            console.log("TEST END TIME" + endTime);
                                        }
                                    }}
                                    className="bg-gray-200 rounded-md p-2 text-gray-800"
                                >
                                    {[...Array(12)].map((_, index) => (
                                        <option key={index} value={index}>{index}</option>
                                    ))}
                                </select>
                                <span className="text-black"> months</span>
                            </div>
                            <p className="text-vlack">{enddate.dateTime ? enddate.dateTime.toString() : 'No end date time selected'}</p>
                            <button
                                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 transition-colors duration-300"
                                onClick={() => {
                                    // Handle button click
                                }}
                            >
                                Confirm Selection
                            </button>
                        </div>
                    </div>
                )}

            </div>
            <div style={{ fontFamily: 'Montserrat' }} className='flex justify-center p-4'>
                <button
                    style={{ width: '150px', fontFamily: 'Montserrat' }}
                    className="mt-2 bg-pink-400 text-white px-8 py-2 rounded-lg hover:bg-blue-700 shadow-md border-2 border-solid border-pink-200"
                    onClick={() => createBookingQueue()}
                >
                    Confirm
                </button>
            </div>
        </div>
    );
}