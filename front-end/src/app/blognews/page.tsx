'use client';
import React, { useEffect, useState, FormEvent } from 'react';
import axios from 'axios';
import styles from '../../../styles/blognews.module.css'
import Image from 'next/image';
import news1 from '../../../assets/rectangle-19.png';
import { useRouter } from 'next/navigation';
import jwt_decode from 'jwt-decode';
import Link from 'next/link';
import router from 'next/router';

type Props = {};

type BlogNews = {
    News_Id: number;
    Topic: string;
    Article: string;
    admin_id: number;
    date: number;
}

type Admin = {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    age: number;
}



export default function Page() {
    const [admin, setAdmin] = useState<Admin | null>(null);
    const [blognews, setBlogNews] = useState<BlogNews[] | null>(null); // Corrected the type
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                // const response = await axios.get('http://localhost:9000/api/blognews2');
                const response = await axios.get('http://localhost:9000/api/blognews2');
                setBlogNews(response.data);
                console.log('Response from API ', response.data);
            } catch (err) {
                if (err instanceof Error) {
                    setError(err.message);
                } else {
                    setError('An error occurred.');
                }
            }
            setLoading(false);
        };

        fetchData();
    }, []);

    return (
        <>
            {loading && <p>Loading...</p>}
            {error && <p>Error: {error}</p>}

            <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                <span style={{ fontFamily: 'Montserrat', }} className="text-white">News</span>
            </div>
            <div style={{ fontFamily: 'Montserrat', }} className="container mx-auto">
                {blognews && blognews.length > 0 && (
                    <div className="p-3 grid grid-cols-1 gap-6">
                        {blognews.map((news) => (
                            <Link key={news.News_Id} href={`/blognews2/${news.News_Id}`}>
                                <a className="block bg-white rounded-lg shadow-md overflow-hidden">
                                    <div className="relative flex items-center justify-center">
                                        <Image
                                            src={news1}
                                            alt=""
                                            width={180}
                                            height={150}
                                            className="object-cover"
                                        />
                                        <div className="absolute inset-0 flex items-center justify-center bg-black bg-opacity-50 transition-opacity opacity-0 hover:opacity-100">
                                            <span className="text-white text-lg font-bold">Read more</span>
                                        </div>
                                    </div>
                                    <div className="p-4">
                                        <h3 className="text-xl font-bold text-gray-800 mb-2 text-center">{news.Topic}</h3>
                                        <p className="text-gray-600">{news.Article}</p>
                                        <p className="text-gray-600 mb-3">Date: {news.date}</p>
                                    </div>
                                </a>
                            </Link>
                        ))}
                    </div>
                )}
            </div>


            {/* Display a message if there are no blog news */}
            {blognews && blognews.length === 0 && <p>No blog news available.</p>}
        </>
    );
}