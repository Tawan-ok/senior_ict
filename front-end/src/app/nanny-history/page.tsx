'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
type Props = {};

type Customer = {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  district: string;
  subDistrict: string;
  province: string;
  zipCode: string;
  streetNumber: string;
  contact_number: string;
  age: number;
  reviewScoreWeight: number;
  successfulJobsWeight: number;
  gender: string;
  role: string;
  locationall: string;
  profile_image_url: string | null;
};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
};

type BookingQueue = {
  id: number,
  customer_id: number,
  nanny_id: number,
  start_date: Date;
  end_date: Date;
  total_amount: number,
  status_payment: string,
  locationhiring: string,
  hours: number
  hours_cost: number,
  days_cost: number,
  months_cost: number,
  child_age: number,
  basic_totalrates: number,
  type_hiring: string,
  bk_score: number;
};

type BookingHistory = {
  id: number,
  booking_id: number,
  status: string,
  time_session: number
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};

// ของเก่า 
export default function CustomersPage({ }: Props) {

  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [customer, setCustomer] = useState<Customer[]>([]);
  const [bookinghistory, setbookinghistory] = useState<BookingHistory[]>([]);
  const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
  const [nanny, setnanny] = useState<Nanny | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const router = useRouter(); // Initialize the router

  const handleExit = () => {
    localStorage.removeItem('jwt'); // Remove the JWT token
    router.push('/login-nanny'); // Redirect to /login
  };


  useEffect(() => {
    const token = localStorage.getItem('jwt');
    // Decode the JWT to extract user ID
    if (token) {
      const decodedToken: any = jwt_decode(token);
      if (!decodedToken.a.includes('NANNY')) {
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }
      // Extract user ID from the "sub" key in JWT
      const userId = decodedToken.sub;

      if (!userId) {
        setError("User ID not found in token.");
        setLoading(false);
        return;
      }

      console.log("User ID:", userId);
      const fetchData = async () => {
        try {
          // const response = await axios.get(`http://localhost:9000/api/admins/${userId}`);


          const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${userId}`);
          // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getby/${userId}`);
          // const response2 = await axios.get<BookingHistory>(`http://localhost:9000/api/nannies/booking-dataBH/${userId}`);
          // const response3 = await axios.get<BookingQueue>(`http://localhost:9000/api/nannies/booking-dataBQ/${userId}`);
          const response2 = await axios.get(`http://localhost:9000/api/nannies/booking-dataBH/${userId}`);
          // const response2 = await axios.get(`http://35.213.139.253:9000/api/nannies/booking-dataBH/${userId}`);
          const response3 = await axios.get(`http://localhost:9000/api/nannies/booking-dataBQ/${userId}`);

          // const response3 = await axios.get(`http://35.213.139.253:9000/api/nannies/booking-dataBQ/${userId}`);
          // setAdmin(response.data);
          setnanny(response1.data);
          setbookinghistory(response2.data);
          setbookingqueue(response3.data);



          const uniqueCustomerIds = Array.from(new Set(response3.data.map((item: { customer_id: any; }) => item.customer_id)));
          const customerPromises = uniqueCustomerIds.map(customerId =>
            axios.get<Customer>(`http://localhost:9000/api/customers/${customerId}`)
          );

          // Resolve all promises
          const customerData = await Promise.all(customerPromises);

          // Set customer data
          setCustomer(customerData.map(customer => customer.data));

          console.log(response1.data);
          console.log(response2.data);
          console.log("Custoemr : ", customer);

          console.log(response3.data);

        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
        }
        setLoading(false);
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-nanny');
    }
  }, []);



  // useEffect(() => {
  //   const fetchCustomerData = async () => {
  //     try {
  //       // Fetch customer data for each booking's customer_id
  //       const customerPromises = bookingqueue.map(booking =>
  //         axios.get<Customer>(`http://localhost:9000/api/customers/${booking.customer_id}`)
  //       );

  //       // Resolve all promises
  //       const customerData = await Promise.all(customerPromises);

  //       // Set customer data
  //       setCustomer(customerData.map(customer => customer.data));
  //     } catch (err) {
  //       console.error('Error fetching customer data:', err);
  //       setError('Error fetching customer data');
  //     }
  //   };

  //   // Fetch customer data only if bookingqueue is not empty
  //   if (bookingqueue.length > 0) {
  //     fetchCustomerData();
  //   }
  // }, [bookingqueue]);

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!nanny) return <div>Nannies Not found {error}</div>;
  if (!bookinghistory) return <div>Bookinghistory Not found {error}</div>;
  if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;

  return (
    <div style={{ fontFamily: 'Montserrat' }} className="text-center">
      <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white">
        Nanny History
      </div>
      {bookinghistory.length > 0 && bookingqueue.length > 0 ? (
        bookinghistory.map((history) => {
          const queueItem = bookingqueue.find((queue) => queue.id === history.booking_id);
          const customerInfo = customer.find((customer) => customer.id === queueItem?.customer_id);
          return (
            <div key={history.id} className="border-2 border-pink-500 bg-pink-500 rounded-lg p-4 mb-4 mt-4 m-3">
              <div className="flex justify-between items-center mb-3"></div>
              {queueItem && customerInfo ? (
                <div key={history.id} className="border-2 border-pink-500 bg-white rounded-lg p-4 mb-4 mt-4 m-3">
                  <div className="flex flex-col sm:flex-row justify-between items-center mb-3">
                    <p className="text-gray-800 font-semibold text-sm sm:text-base">Booking ID: {history.booking_id}</p>
                    <p className={`text-lg sm:text-xl font-bold px-2 py-1 rounded ${history.status === 'Confirmed' ? 'text-green-800 bg-green-200' : 'text-pink-800 bg-pink-200'}`}>{history.status}</p>
                  </div>
                  {queueItem && customerInfo ? (
                    <div>
                      <section className="mb-8 px-4 py-6 bg-white">
                        <h2 className="text-gray-800 text-xl font-semibold mb-4">Customer Information</h2>
                        <div className="mt-4 flex justify-center">
                          <div className="w-40 h-40 sm:w-48 sm:h-48 flex justify-center">
                            <Image
                              width={48}
                              height={48}
                              layout="fixed"
                              src={"data:image/png;base64," + customerInfo.profile_image_url}
                              alt="Profile Image"
                              className="object-cover w-full h-full"
                            />
                          </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                          <div>
                            <p className="text-gray-700 font-medium mb-2">Name:</p>
                            <p className="text-gray-700">{customerInfo.first_name} {customerInfo.last_name}</p>
                          </div>
                          <div>
                            <p className="text-gray-700 font-medium mb-2">Contact Number:</p>
                            <p className="text-gray-700">{customerInfo.contact_number}</p>
                          </div>
                        </div>
                      </section>
                      <section className="mb-4">
                        <h2 className="text-gray-800 text-lg font-semibold mb-2">Booking Details</h2>
                        <p className="text-gray-700">Total Amount: {queueItem.total_amount.toLocaleString()} Baht</p>
                        <p className="text-gray-700">Location: {queueItem.locationhiring}</p>
                        <p className="text-gray-700">Hours: {queueItem.hours}</p>
                      </section>
                      <section>
                        <div className="flex flex-col sm:flex-row justify-between items-center bg-pink-200 text-black p-3 rounded-lg">
                          <p className="font-bold">Start Date: {new Date(queueItem.start_date).toLocaleDateString()}</p>
                          <p className="font-bold">End Date: {new Date(queueItem.end_date).toLocaleDateString()}</p>
                        </div>
                      </section>
                    </div>
                  ) : (
                    <p className="text-red-500">Booking Queue data not found for this ID</p>
                  )}
                </div>
              ) : (
                <p className="text-red-500">Booking Queue data not found for this ID</p>
              )}
            </div>
          );
        })
      ) : (
        <p>No Booking History data found</p>
      )}
    </div>
  );


}

