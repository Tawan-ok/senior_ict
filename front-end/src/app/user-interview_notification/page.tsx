'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import bell from '../../../assets/bell.png';
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
import { addHours } from 'date-fns';
type Props = {};



type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
    role: string;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    hours: number
};

type BookingHistory = {
    id: number,
    booking_id: number,
    status: string,
    time_session: number
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
    sub: string;
    exp: number;
    a: Role[];
};
type Appointment = {
    id: number;
    title: string;
    start_time: string;
    end_time: string;
    interview_details: string;
};

type Notification = {
    id: number;
    message: string;
    appointment_id: number;
    status: string;
};
// ของเก่า 
export default function CustomersPage({ }: Props) {

    const [nannies, setNannies] = useState<Nanny[]>([]);
    const [bookingqueue, setbookingqueue] = useState<BookingQueue[]>([]);
    const [nanny, setnanny] = useState<Nanny | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const router = useRouter(); // Initialize the router
    const [notifications, setNotifications] = useState<Notification[]>([]);
    const [appointmentDetails, setAppointmentDetails] = useState<Map<number, Appointment>>(new Map());
    const handleExit = () => {
        localStorage.removeItem('jwt'); // Remove the JWT token
        router.push('/login-nanny'); // Redirect to /login
    };
    const handleinterview = () => {
        router.push('/user-interview_notification');
    };
    const handlebooking = () => {
        router.push('/user-booking_notification');
    };
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        // Decode the JWT to extract user ID
        if (token) {
            const decodedToken: any = jwt_decode(token);
            if (!decodedToken.a.includes('USER')) {
                setError('Access denied. You do not have the required permissions.');
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
            }
            // Extract user ID from the "sub" key in JWT
            const userId: number = decodedToken.sub;

            if (!userId) {
                setError("User ID not found in token.");
                setLoading(false);
                return;
            }

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    const response = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getfindBookingsPaidAndCancleCustomer/${userId}`);
                    // const response = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/bookingqueue/getbookingsbycustomer/${userId}`);
                    setbookingqueue(response.data);
                    console.log("118" + response.data);
                    const notificationsResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}`);
                    const notificationsConfirmedResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}?status=confirmed`);
                    const notificationsDeniedResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}?status=denied`);
                    const notificationsPaddingResponse = await axios.get<Notification[]>(`http://localhost:9000/api/notifications/for-user/${userId}?status=pending`);
                    // const notificationsResponse = await axios.get<Notification[]>(`http://35.213.139.253:9000/api/notifications/for-user/${userId}`);
                    const mergedNotifications = [...notificationsConfirmedResponse.data, ...notificationsDeniedResponse.data, ...notificationsPaddingResponse.data];
                    setNotifications(mergedNotifications);
                    const appointmentIds = notificationsResponse.data.map((notification) => notification.appointment_id);
                    const appointments = await Promise.all(
                        appointmentIds.map((id) => axios.get<Appointment>(`http://localhost:9000/api/appointments/${id}`))
                        // appointmentIds.map((id) => axios.get<Appointment>(`http://35.213.139.253:9000/api/appointments/${id}`))
                    );
                    const newAppointmentDetails = new Map();
                    appointments.forEach((response) => {
                        const appointment = response.data;
                        newAppointmentDetails.set(appointment.id, appointment);
                    });
                    setAppointmentDetails(newAppointmentDetails);


                    setLoading(false);
                    // const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${userId}`);
                    // setnanny(response1.data);
                    const nannyIds = response.data.map((queue) => queue.nanny_id);
                    const nanniesResponse = await Promise.all(
                        nannyIds.map((nannyId) =>
                            axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${nannyId}`)
                            // axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getby/${nannyId}`)
                        )
                    );
                    const nanniesData = nanniesResponse.map((res) => res.data);
                    setNannies(nanniesData);
                    console.log(nanniesData);
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                }
                setLoading(false);
            };

            fetchData();
        } else {
            alert('You need to be logged in first.');
            router.push('/login-nanny');
        }
    }, []);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!nannies) return <div>Nanny Not found {error}</div>;
    if (!bookingqueue) return <div>Bookingqueue Not found {error}</div>;


    const handleCancelBooking = async (queueId: number) => {
        try {
            // Send a request to the API to cancel the booking using the queue ID
            await axios.delete(`http://localhost:9000/api/bookingqueue/delete/${queueId}`);
            // await axios.delete(`http://35.213.139.253:9000/api/bookingqueue/delete/${queueId}`);

            // Reload the page after cancellation
            window.location.reload();
        } catch (err) {
            if (err instanceof Error) {
                console.error(err.message);
            } else {
                console.error('An error occurred while canceling the booking.');
            }
        }
    };
    const formatDate = (dateString: string): string => {
        const date = new Date(dateString);
        const options: Intl.DateTimeFormatOptions = {
            year: 'numeric', month: '2-digit', day: '2-digit',
            hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false,
        };
        return new Intl.DateTimeFormat('default', options).format(date).replace(',', '');
    };

    const handleCloseNotification = async (notificationId: number) => {
        try {
            await axios.put(`http://localhost:9000/api/notifications/${notificationId}/close`);
            // await axios.put(`http://35.213.139.253:9000/api/notifications/${notificationId}/close`);
            // Update the notifications list by filtering out the closed notification
            setNotifications(notifications.filter(notification => notification.id !== notificationId));
        } catch (error: any) {
            console.error('Failed to close notification:', error.message);
        }
    };
    return (
        <div style={{ fontFamily: 'Montserrat', }}>
            <div className="mt-8 px-4">
                <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat', }} className="text-pink-600">Interview Notification</span>
                </div>
                <div className="flex justify-center mt-8">
                    <div className="mr-4">
                        <button onClick={() => handleinterview()} className="bg-white border border-pink-500 hover:bg-pink-600 text-black font-semibold px-4 py-2 rounded-md transition duration-300 ease-in-out">
                            Interview
                        </button>
                    </div>
                    <div>
                        <button onClick={() => handlebooking()} className="bg-pink-400 border border-pink-200 hover:bg-pink-600 text-white font-semibold px-4 py-2 rounded-md transition duration-300 ease-in-out">
                            Booking
                        </button>
                    </div>
                </div>
                <div className="mt-3 mb-3 text-center text-xl sm:text-xl md:text-2xl lg:text-2xl xl:text-2xl 2xl:text-3xl font-semibold">
                    <div className="bg-pink-100 rounded-lg shadow-md mx-5">
                        {notifications.length > 0 ? (
                            notifications.map((notification) => (
                                <div key={notification.id} className="bg-white rounded-lg shadow-md my-4 p-4">
                                    <p className="bg-purple-100 p-2 text-xl font-semibold text-pink-800 mb-2">
                                        {notification.status === 'confirmed' ? 'Nanny has confirmed your appointment.' : 'Nanny has canceled your appointment.'}
                                    </p>
                                    {appointmentDetails.has(notification.appointment_id) && (
                                        <div className="bg-pink-100 rounded-lg shadow-md p-4 mt-4">
                                            <div className="flex justify-between items-center">
                                                <p className="text-lg font-semibold text-pink-800">Appointment Details</p>
                                                <button onClick={() => handleCloseNotification(notification.id)} className="text-pink-600 hover:text-pink-800">
                                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div className="border-t text-2xl border-pink-200 mt-4 pt-4 grid grid-cols-1 md:grid-cols-2 gap-4">
                                                <div>
                                                    <p className="text-pink-700 mb-1">Appointment Topic:</p>
                                                    <p className="text-pink-900 font-semibold">{appointmentDetails.get(notification.appointment_id)?.title || 'N/A'}</p>
                                                </div>
                                                <div>
                                                    <p className="text-pink-700 mb-1">Details:</p>
                                                    <p className="text-pink-900 font-semibold">{appointmentDetails.get(notification.appointment_id)?.interview_details || 'N/A'}</p>
                                                </div>
                                                <div>
                                                    <p className="text-pink-700 mb-1">Start Time:</p>
                                                    <p className="text-pink-900 font-semibold">{appointmentDetails.get(notification.appointment_id)?.start_time ? formatDate(appointmentDetails.get(notification.appointment_id)!.start_time) : 'N/A'}</p>
                                                </div>
                                                <div>
                                                    <p className="text-pink-700 mb-1">End Time:</p>
                                                    <p className="text-pink-900 font-semibold">{appointmentDetails.get(notification.appointment_id)?.end_time ? formatDate(appointmentDetails.get(notification.appointment_id)!.end_time) : 'N/A'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </div>
                            ))
                        ) : (
                            <div className="bg-white rounded-lg shadow-md my-4 p-6">
                                <div className="flex flex-col items-center justify-center">
                                    <Image src={bell} width={100} height={100} alt='Notification Bell' layout='fixed' />
                                    <p className='mt-4 text-pink-700 text-lg font-semibold'>No notifications</p>
                                </div>
                            </div>
                        )}
                    </div>
                </div>




            </div>
        </div>


    );
}

