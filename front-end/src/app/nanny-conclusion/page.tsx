'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState, useRef } from 'react';
// import Chart from 'chart.js/auto';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';

type Props = {};

type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
};

type BookingHistory = {
  id: number,
  booking_id: number,
  status: string,
  time_session: number
};

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};

export default function CustomersPage({ }: Props) {

  const [nanny, setNanny] = useState<Nanny | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [userids, setUserids] = useState<number | null>(null);
  const [selectedMonth, setSelectedMonth] = useState<number>(new Date().getMonth() + 1); // Default to current month
  const [selectedYear, setSelectedYear] = useState<number>(new Date().getFullYear()); // Default to current year
  const [bookinghistory, setBookingHistory] = useState<BookingHistory[]>([]);
  const router = useRouter(); // Initialize the router
  const [totalAmounts, setTotalAmount] = useState<number>(0);
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
  ];
  const handleExit = () => {
    localStorage.removeItem('jwt'); // Remove the JWT token
    router.push('/login-nanny'); // Redirect to /login
  };



  useEffect(() => {
    const token = localStorage.getItem('jwt');
    // Decode the JWT to extract user ID
    if (token) {
      const decodedToken: any = jwt_decode(token);
      if (!decodedToken.a.includes('NANNY')) {
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }
      // Extract user ID from the "sub" key in JWT
      const userId = decodedToken.sub;
      setUserids(userId);
      if (!userId) {
        setError("User ID not found in token.");
        setLoading(false);
        return;
      }

      const fetchData = async () => {
        try {
          const response = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${userId}`);
          const response2 = await axios.get<number>(`http://localhost:9000/api/nannies/totalAmount/${userId}/${selectedMonth}/${selectedYear}`);
          setTotalAmount(response2.data); // Update total amount state
          setNanny(response.data);

        } catch (err) {
          if (err instanceof Error) {
            setError(err.message);
          } else {
            setError('An error occurred.');
          }
        }
        setLoading(false);
      };

      fetchData();
      // handleFetchTotalAmount(); // Fetch total amount when component mounts
    } else {
      alert('You need to be logged in first.');
      router.push('/login-nanny');
    }
  }, [selectedMonth, selectedYear]);

  // const handleFetchTotalAmount = async () => {
  //   setLoading(true);
  //   try {
  //     // const response = await axios.get<number>(`http://localhost:9000/api/nannies/totalAmount/${nanny?.id}/${selectedMonth}/${selectedYear}`);
  //     const response = await axios.get<number>(`http://localhost:9000/api/nannies/totalAmount/${userids}/4/2024`);
  //     setTotalAmount(response.data); // Update total amount state
  //   } catch (err) {
  //     if (err instanceof Error) {
  //       setError(err.message);
  //     } else {
  //       setError('An error occurred.');
  //     }
  //   }
  //   setLoading(false);
  // };


  const handleMonthChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedMonth(parseInt(e.target.value));
  };

  const handleYearChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedYear(parseInt(e.target.value));
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!nanny) return <div>Nannies Not found {error}</div>;
  if (!bookinghistory) return <div>Bookinghistory Not found {error}</div>;

  return (
    <div style={{ fontFamily: 'Montserrat' }} className="flex flex-col items-center justify-center space-y-6 text-white py-8 px-4">
      <h1 className="text-4xl sm:text-3xl md:text-4xl lg:text-5xl xl:text-6xl font-bold text-center">
        Nanny Conclusion
      </h1>
      <div className="bg-gray-900 p-6 rounded-lg shadow-md w-full max-w-md">
        <div className="flex justify-center items-center space-x-4">
          <div>
            <label htmlFor="monthSelect" className="block text-white">Select Month:</label>
            <select
              id="monthSelect"
              value={selectedMonth}
              onChange={handleMonthChange}
              className="mt-1 block w-full px-4 py-2 bg-white text-gray-900 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:border-transparent"
            >
              {monthNames.map((month, index) => (
                <option key={index} value={index + 1}>{month}</option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="yearSelect" className="block text-white">Select Year:</label>
            <select
              id="yearSelect"
              value={selectedYear}
              onChange={handleYearChange}
              className="mt-1 block w-full px-4 py-2 bg-white text-gray-900 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:border-transparent"
            >
              {Array.from({ length: 10 }, (_, i) => 2024 + i).map((year) => (
                <option key={year} value={year}>{year}</option>
              ))}
            </select>
          </div>
        </div>
        {totalAmounts !== null && totalAmounts !== 0 ? (
          <p className="mt-4 text-center">Total Amount: <span className="text-green-400">{totalAmounts.toLocaleString()} Baht</span></p>
        ) : (
          <p className="mt-4 text-center">You didn&apos;t receive any money this month.</p>
        )}
      </div>
    </div>
  );
}