'use client'
import React, { useEffect, useState } from 'react';
import RootLayout from "../layout";
import styles from '../../../styles/Home.module.css';
import hearticon from '../../../assets/bold-like-hearts.png';
import msgicon from '../../../assets/bold-messages-conversation-dialog.png';
import mac2 from '../../../assets/mac2.png';
import homephone from '../../../assets/homephone.png';
import mapicon from '../../../assets/bold-map-location-streets-map-point.png';
import bg1 from '../../../assets/rectangle-19.png';
import hiring from '../../../assets/hiring.png'
import activity from '../../../assets/lifestyle.png'
import history from '../../../assets/history.png'
import interview from '../../../assets/interview.png'
import schedule from '../../../assets/calendar.png'
import pending from '../../../assets/circular-clock.png'
import admin from '../../../assets/qa.png'
import hanni from '../../../assets/hanni.png'
import phone2 from '../../../assets/homephone.png';
import mac from '../../../assets/mac.png'
import exlogo from '../../../assets/mask-group-p3B.png';
import phone from '../../../assets/main-bg-BwF.png';
import bgphone from '../../../assets/space-black-iphone-14-pro-mockup-label.png';
import pc from '../../../assets/main-bg.png';
import bgpc from '../../../assets/new-macbook-pro-mockup-front-view-label.png';
import Image from 'next/image';
// import Navbar from '../../../Component/navigationbar/page';
import { useRouter } from 'next/router';
import Link from 'next/link';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt_decode from 'jwt-decode';
type Props = {}

type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};


export default function Hoempage() {
  const [role, setRole] = useState<Role | null>(null);


  useEffect(() => {


    const token = localStorage.getItem('jwt');

    if (token) {
      const decodedToken: DecodedToken = jwt_decode(token);

      if (decodedToken.a.includes('USER')) {
        setRole('USER');
      } else if (decodedToken.a.includes('NANNY')) {
        setRole('NANNY');
      } else if (decodedToken.a.includes('ADMIN')) {
        setRole('ADMIN');
      }

    }
  }, []);

  // Conditional content based on the role
  const renderContentBasedOnRole = () => {
    switch (role) {
      case 'USER':
        return (
          <div className="flex justify-center items-center flex-grow-1">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 p-2">
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={hiring}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-sm lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Hiring</span>
                <a href="/hiring" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={activity}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-l lg:text-xl xl:text-2xl 2xl:text-2xl font-medium whitespace-nowrap'>Activity Program</span>
                <a href="/pre-activityprogram" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={history}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-l lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>User History</span>
                <a href="/user-history" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={interview}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-l lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Interview </span>
                <a href="/user-interview-history" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
            </div>
          </div>
        );
      case 'NANNY':
        return (
          <div className="flex justify-center items-center flex-grow-1">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 p-2">
              <div className={`text-white text-center ${styles.editServiceHome} col-span-1 md:col-span-2 lg:col-span-3`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={schedule}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-sm lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Schedule</span>
                <a href="/nanny-schedule" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome} col-span-1 md:col-span-2 lg:col-span-3`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={history}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-sm lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Nanny History</span>
                <a href="/nanny-history" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome} col-span-1 md:col-span-2 lg:col-span-3`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={pending}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-sm lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Booking Pending</span>
                <a href="/nanny-history" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
            </div>
          </div>
        );
      case 'ADMIN':
        return (
          <div className="flex justify-center items-center flex-grow-1">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 p-2">
              <div className={`text-white text-center ${styles.editServiceHome} col-span-1 md:col-span-2 lg:col-span-3`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={admin}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-sm lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Admin control</span>
                <a href="/admin-control" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
            </div>
          </div>
        );
      default:
        return (
          <div className="flex justify-center items-center flex-grow-1">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 p-2">
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={hiring}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-sm lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Customers</span>
                <a href="/login-user" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={hiring}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-l lg:text-xl xl:text-2xl 2xl:text-2xl font-medium whitespace-nowrap'>Nannies</span>
                <a href="/login-nanny" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
              <div className={`text-white text-center ${styles.editServiceHome}`}>
                <div className="rounded-circle overflow-hidden flex justify-center items-center mx-auto mb-3" style={{ width: "120px", height: "120px" }}>
                  <Image
                    src={hiring}
                    alt='..'
                    width={120}
                    height={80}
                    className="img-fluid rounded-circle border-2 border-pink-200"
                  />
                </div>
                <span className='text-xl md:text-l lg:text-xl xl:text-2xl 2xl:text-2xl font-medium'>Admin</span>
                <a href="/login-admin" className={`mt-3 p-2 text-base md:text-sm lg:text-base xl:text-xl 2xl:text-xl font-semibold text-black ${styles.tryButton}`}>Try it now</a>
              </div>
            </div>
          </div>
        );
    }
  };
  return (
    <div className={styles.rootContainer}>
      <RootLayout showNavbar={true} showFooter={false}>
        <div className="container px-2 py-2 h-screen flex items-center overflow-hidden bg-pink-100">
          <div className="lg:flex lg:justify-center w-full">
            <div className="lg:w-1/2">
              <div style={{ fontFamily: 'Montserrat' }} className="lg:text-left border-2 border-pink-600 rounded-2xl p-10">
                <div className="text-4xl md:text-3xl lg:text-4xl xl:text-5xl 2xl:text-6xl font-semibold text-pink-500 animate-fadeInDown">
                  <span className="font-montserrat">Feel</span> <span className="text-pink-500">Free</span>{" "}
                  <span className="text-pink-500">And</span> <span className="text-pink-500">Safe</span>
                </div>
                <div className="text-4xl md:text-3xl lg:text-4xl xl:text-5xl 2xl:text-6xl font-bold mt-4 text-pink-500 animate-fadeInUp">
                  <span>With</span> <span className="text-pink-500">Find Us Nanny</span>
                </div>
                <div className="mt-4 text-lg md:text-xl lg:text-2xl xl:text-3xl 2xl:text-4xl font-semibold text-pink-500 animate-fadeIn">
                  Your Child&apos;s Happiness is Our Priority
                </div>
              </div>
            </div>
            {/* <div className="lg:w-1/2 mt-5 lg:mt-0">
      <div className="p-3">
        <Link href="/blognews">
          <Image
            className="rounded-lg overflow-hidden"
            src={bg1}
            alt={'bg1'}
          />
        </Link>
      </div>
    </div> */}
          </div>
        </div>

        <div className={`container p-5 mx-auto ${styles.bgContainer}`} style={{ marginTop: '10rem' }}>
          <div className={`row align-items-center ${styles.flexColumn}`}>
            <div className={`col text-center d-flex align-items-center mb-2 ${styles.textPinkBG}`}>
              <Image
                className="rounded-lg overflow-hidden"
                src={msgicon}
                alt={'bg1'}
                width={50}
                height={20}
              />
              <span className="ms-2">Interview Nanny Before Hiring</span>
            </div>
            <div className={`col text-center d-flex align-items-center mb-2 ${styles.textPinkBG}`}>
              <Image
                className="rounded-lg overflow-hidden"
                src={hearticon}
                alt={'bg1'}
                width={50}
                height={20}
              />
              <span className="ms-2">Can Add Favorites Nanny</span>
            </div>
            <div className={`col text-center d-flex align-items-center mb-2 ${styles.textPinkBG}`}>
              <Image
                className="rounded-lg overflow-hidden"
                src={mapicon}
                alt={'bg1'}
                width={50}
                height={20}
              />
              <span className="ms-2">Hiring Nanny From Anywhere</span>
            </div>
          </div>
        </div>

        <div className={`ml-5 mt-10 p-5 text-4xl md:text-3xl lg:text-4xl xl:text-5xl 2xl:text-6xl font-semibold ${styles.normalText}`}>
          <span className={styles.whiteText}>Why</span> <span className={styles.orangeText}>Find Us Nanny</span>{" "} <span className={styles.whiteText}>?</span>
        </div>

        <div className="container overflow-hidden">
          <div className="row gx-0 gx-md-5">
            <div className="col-12 col-md-6 d-flex justify-content-center align-items-center">
              <div className="p-3">
                <Image
                  className="rounded-lg overflow-hidden"
                  src={homephone}
                  alt={'bg1'}
                  width={300}
                  height={250}
                />
              </div>
            </div>
            <div className="col-12 col-md-6">
              <div className="row gx-0 gx-md-5">
                <div className="col-12">
                  <div className={`p-2 ${styles.textPromote}`}>
                    Find Us Nanny: Bridging the Gap Across Platforms – Seamlessly Caring for Your Children Anytime, Anywhere!
                  </div>
                </div>
                <div className="col-12">
                  <div className={`p-2 mt-3 mt-md-5 ${styles.textPromote}`}>
                    <Image
                      className="rounded-lg overflow-hidden"
                      src={mac2}
                      alt={'bg1'}
                      width={700}
                      height={500}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style={{ fontFamily: 'Montserrat' }} className="container mx-auto py-10 bg-pink-200 p-4 rounded-lg">
          <div className="text-4xl font-semibold text-center mb-8">Role Level</div>
          <div className="text-center text-gray-700 mb-8">Find Us Nanny is the center that provide nanny that passed the certificated from TPQI</div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-8">
            <div className="bg-white shadow-lg rounded-lg p-6">
              <div className="text-xl font-semibold mb-4">Role Level 1</div>
              <p className="text-gray-700 leading-relaxed">
                Individuals at this level possess comprehensive knowledge and skills required to perform basic routine tasks in childcare. They demonstrate proficiency in solving fundamental problems within their defined scope of work and exhibit competence in recognizing and addressing details related to children&rsquo;s nutrition, daily routines, safety, and environmental care.
                <br />
                Moreover, they have the capacity to promote holistic development, encompassing physical, mental-emotional, social, and intellectual aspects, while upholding professional ethics in their role as childcare providers, thereby delivering high-quality service.
              </p>
            </div>
            <div className="bg-white shadow-lg rounded-lg p-6">
              <div className="text-xl font-semibold mb-4">Role Level 2</div>
              <p className="text-gray-700 leading-relaxed">They are individuals with practical skills in performing tasks as prescribed, capable of solving routine problems by applying theories, tools, and basic information. They are competent in implementing preventive and corrective measures in daily childcare routines, controlling activities that promote physical, mental-emotional, social, and intellectual development, as well as preparing tools and equipment for children.</p>
            </div>
            <div className="bg-white shadow-lg rounded-lg p-6">
              <div className="text-xl font-semibold mb-4">Role Level 3</div>
              <p className="text-gray-700 leading-relaxed">They are individuals with specialized knowledge and advanced technical skills in their field of work, with diverse thinking and practices. They can solve technical problems while utilizing manuals, and they possess the ability to observe work processes in order to provide guidance for improvement. They are proficient in monitoring and reporting on the outcomes of daily childcare, child development, physical, mental-emotional, social, and intellectual aspects, as well as in preparing tools, equipment, and environmental management.</p>
            </div>
            <div className="bg-white shadow-lg rounded-lg p-6">
              <div className="text-xl font-semibold mb-4">Role Level 4</div>
              <p className="text-gray-700 leading-relaxed">They are individuals with specialized and technical skills in their field of work, possessing diverse thinking and practices. They can solve technical problems while utilizing manuals and are capable of controlling work processes to draw conclusions. They excel in analyzing, planning, and designing childcare routines, creating suitable environments for children, and fostering child development in physical, mental-emotional, social, and intellectual aspects. Additionally, they focus on self-development and collaboration with colleagues.</p>
            </div>
          </div>
          <div className="text-center mt-8">
            <a href="https://tpqi-net.tpqi.go.th/qualifications/2265" target="_blank" rel="noopener noreferrer" className="bg-gray-800 text-white px-6 py-3 rounded-full inline-block transition duration-300 ease-in-out hover:bg-gray-900">Read More</a>
          </div>
        </div>


        <div className="mt-10 p-5 text-4xl md:text-3xl lg:text-4xl xl:text-5xl 2xl:text-6xl font-semibold">
          <div className={styles.serviceText}>Service</div>
          {renderContentBasedOnRole()}
        </div>

      </RootLayout>
    </div>
  );
}
