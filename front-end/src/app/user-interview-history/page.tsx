'use client'
import jwt_decode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from "next/navigation";
import styles from '../../../styles/Hiring.module.css';
import like from '../../../assets/Heart.png';
import logo from '../../../assets/Logo.png'
import nanny1 from '../../../assets/hanni.png'
import searchicon from '../../../assets/Magnifer.png'
import Image from 'next/image';
import Link from 'next/link';
import { set } from 'date-fns';
type Props = {};



type Nanny = {
  id: number;
  username: string;
  email: string;
  pass_word: string;
  district: string;
  sub_district: string;
  province: string;
  zip_code: string;
  street_number: string;
  contact_number: string;
  first_name: string;
  last_name: string;
  role_level: string;
  cost: number | null;
  type_work: string;
  status: string;
  age: number;
  gender: string;
  score: number;
  role: string;
  citizen_id: string;
  profile_image_url: string;
  skill_1: string;
  skill_2: string;
  skill_3: string;
  hours_cost: number;
  days_cost: number;
  months_cost: number;
};


type Role = 'USER' | 'ADMIN' | 'NANNY';

type DecodedToken = {
  sub: string;
  exp: number;
  a: Role[];
};

interface AppointmentResponse {
  title: string;
  customer_id: number;
  nanny_id: number;
  start_time: string; // Assuming the API returns dates in ISO format
  end_time: string;
  status: string;
  interviewDetails?: string;
}


export default function CustomersInterviewHistoryPage({ }: Props) {

  const [nannies, setNannies] = useState<Nanny[]>([]);
  const [nanny, setnanny] = useState<Nanny | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [appointments, setAppointments] = useState<AppointmentResponse[]>([]);
  const router = useRouter(); // Initialize the router
  const handleExit = () => {
    localStorage.removeItem('jwt'); // Remove the JWT token
    router.push('/login-user'); // Redirect to /login
  };



  useEffect(() => {
    const token = localStorage.getItem('jwt');

    if (token) {
      const decodedToken: any = jwt_decode(token);


      if (!decodedToken.a.includes('USER')) {
        setError('Access denied. You do not have the required permissions.');
        alert('Access denied. You do not have the required permissions.');
        setLoading(false);
        router.push('/home');
        return;
      }

      const userId = decodedToken.sub;

      if (!userId) {
        setError("User ID not found in token.");
        setLoading(false);
        return;
      }

      console.log("User ID:", userId);
      const fetchData = async () => {
        try {
          const appointmentResponse = await axios.get<AppointmentResponse[]>(`http://localhost:9000/api/appointments/customers?customerId=${userId}&status=confirmed`);
          setAppointments(appointmentResponse.data);

          const uniqueNannyIdsSet = new Set(appointmentResponse.data.map(appointment => appointment.nanny_id));


          const uniqueNannyIdsArray = Array.from(uniqueNannyIdsSet);

          const nannyPromises = uniqueNannyIdsArray.map(nannyId =>
            axios.get<Nanny>(`http://localhost:9000/api/nannies/getby/${nannyId}`)
          );

          const nannyResponses = await Promise.all(nannyPromises);


          setNannies(nannyResponses.map(response => response.data));
        } catch (err) {
          if (axios.isAxiosError(err)) {
            setError(err.response?.data.message || 'An error occurred.');
          } else {
            setError('An error occurred.');
          }
        } finally {
          setLoading(false);
        }
      };

      fetchData();
    } else {
      alert('You need to be logged in first.');
      router.push('/login-user');
    }
  }, []);

  const sortAppointments = () => {
    const sortedAppointments = [...appointments].sort((a, b) => {
      return new Date(b.start_time).getTime() - new Date(a.start_time).getTime();
    });
    setAppointments(sortedAppointments);
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div className="mt-3 text-center text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-white">No appointments interview found</div>;
  if (appointments.length === 0) return <p>No appointments interview found</p>;
  return (
    <div style={{ fontFamily: 'Montserrat', }} className="min-h-screen py-8">
      <div className="container mx-auto">
        <h1 className="text-4xl sm:text-2xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold text-center text-white mb-8">Interview History</h1>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4 mx-auto block"
          onClick={sortAppointments}
        >
          Sort by Date
        </button>
        {appointments.length > 0 ? (
          appointments.map((appointment) => {
            const nannyForAppointment = nannies.find((nanny) => nanny.id === appointment.nanny_id);

            return (
              <div key={appointment.customer_id} className="bg-white border border-pink-300 rounded-lg p-4 mb-4 mt-4 mx-auto max-w-xl shadow-lg">
                <div className="flex flex-col sm:flex-row justify-between items-center mb-4">
                  <h2 className="text-lg font-semibold text-gray-800 mb-2 sm:mb-0">Title: {appointment.title}</h2>
                  <div className="flex items-center space-x-4 mt-2 sm:mt-0">
                    {nannyForAppointment ? (
                      <>
                        <Link href={`/nannydetail/${nannyForAppointment.username}`} passHref>
                          <a>
                            <img src={"data:image/png;base64," + nannyForAppointment.profile_image_url} alt={`Image of ${nannyForAppointment.first_name} ${nannyForAppointment.last_name}`} className="w-12 h-12 object-cover rounded-full" />
                          </a>
                        </Link>
                        <div className="flex flex-col">
                          <p className="text-sm font-semibold text-gray-700">Nanny Name:</p>
                          <p className="text-sm text-gray-800">{nannyForAppointment.first_name} {nannyForAppointment.last_name}</p>
                        </div>
                        <Link href={`/nannydetail/${nannyForAppointment.username}`} passHref>
                          <a className="text-sm font-semibold text-white bg-pink-500 px-3 py-1 rounded-lg hover:bg-pink-600 transition duration-300 ease-in-out">Hire Nanny</a>
                        </Link>
                      </>
                    ) : (
                      <p className="text-sm text-gray-700">Nanny details not found</p>
                    )}
                  </div>
                </div>
                <div className="text-sm text-gray-700 mb-4">
                  <p className="mb-2">Start Date: {new Date(appointment.start_time).toLocaleString()}</p>
                  <p>End Date: {new Date(appointment.end_time).toLocaleString()}</p>
                </div>
                {appointment.interviewDetails ? (
                  <div className="text-sm text-gray-700">
                    <p className="font-semibold mb-2">Detail:</p>
                    <p>{appointment.interviewDetails}</p>
                  </div>
                ) : (
                  <><p className="font-semibold mb-2">Detail:</p><p className="text-sm text-gray-700">Sorry, no detail</p></>
                )}
              </div>
            );
          })
        ) : (
          <p className="text-lg text-gray-800 text-center">No interview appointments found</p>
        )}
      </div>
    </div>

  );
}


