'use client';

import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { FC } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import DateTimePicker from 'react-datetime-picker';
import { add, format, parseISO, addHours } from "date-fns"
import { useRouter } from 'next/navigation'
import jwt_decode from 'jwt-decode';

type Props = {};

type Customer = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role: string;
    age: number;
    gender: string;
};

type Nanny = {
    id: number;
    username: string;
    email: string;
    pass_word: string;
    district: string;
    sub_district: string;
    province: string;
    zip_code: string;
    street_number: string;
    contact_number: string;
    first_name: string;
    last_name: string;
    role_level: string;
    cost: number | null;
    type_work: string;
    status: string;
    age: number;
    gender: string;
    score: number;
};

type BookingQueue = {
    id: number,
    customer_id: number,
    nanny_id: number,
    start_date: Date;
    end_date: Date;
    total_amount: number,
    status_payment: string,
    locationhiring: string,
    hours: number

};

type BookingHistory = {
    booking_id: number,
    status: string,
    time_session: number
};

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

interface DateType {
    justDate: Date | null,
    dateTime: Date | null,
}

type ComplainFields = {
    field1: string;
    field2: string;
    field3: string;
    field4: string;
    field5: string;
};

type Complains = {
    booking_id: number;
    customer_id: number;
    nanny_id: number;
    complain_field_1: string,
    complain_field_2: string,
    complain_field_3: string,
    complain_field_4: string,
    complain_field_5: string,
    complain_count: number;
};

const initialComplainFields: ComplainFields = {
    field1: 'F',
    field2: 'F',
    field3: 'F',
    field4: 'F',
    field5: 'F',
};

export default function Page({ params }: { params: { username: string } }) {
    const [customer, setCustomer] = useState<Customer | null>(null); // Initialize as null
    const [nanny, setNanny] = useState<Nanny | null>(null); // Initialize as null

    const [bookingqueuehistory, setBookingqueuehistory] = useState<BookingHistory | null>(null); // Initialize as null
    const [bookingqueue, setBookingqueue] = useState<BookingQueue[]>([]);

    const [bookingqueue2, setBookingqueue2] = useState<BookingQueue | null>(null); // Initialize as null

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<string | null>(null);
    const [value, onChange] = useState<Value>(new Date());
    const [response1, setResponse1] = useState<Nanny | null>(null);

    const [newScore, setNewScore] = useState<number | null>(null);

    const [description, setDescription] = useState<string>('');


    const [formData, setFormData] = useState({
        name: '',
        email: '',
    });

    const [selectedOptions, setSelectedOptions] = useState<string[]>([]);
    const [otherOption, setOtherOption] = useState<string>('');

    const [complainCount, setComplainCount] = useState<number>(0);
    const handleDescriptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDescription(event.target.value);
    };
    const [complainFields, setComplainFields] = useState<ComplainFields>(initialComplainFields);
    const [suggestions, setSuggestions] = useState<string>('');

    const handleCheckboxChange = (fieldName: keyof ComplainFields) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { checked } = event.target;
        const value = checked ? 'T' : 'F';
        setComplainFields(prevState => ({
            ...prevState,
            [fieldName]: value,
        }));
    };

    const [selectedOption2, setSelectedOption2] = useState<string>('');

    const handleOptionChange2 = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedOption2(event.target.value);
    };

    const router = useRouter();

    const handleExit = () => {
        localStorage.removeItem('jwt');
        router.push('/login-admin');
    };
    useEffect(() => {
        const token = localStorage.getItem('jwt');
        if (token) {
            const decodedToken: any = jwt_decode(token);

            const userId: number = decodedToken.sub;

            if (!decodedToken.a.includes('USER')) {
                setError("User ID not found in token.");
                alert('Access denied. You do not have the required permissions.');
                setLoading(false);
                router.push('/home');
                return;
              }
        

            console.log("User ID:", userId);
            const fetchData = async () => {
                try {
                    if (!customer || !nanny) {
                        const response = await axios.get<Customer>(`http://localhost:9000/api/customers/${userId}`);
                        // const response = await axios.get<Customer>(`http://35.213.139.253:9000/api/customers/${userId}`);
                        const response1 = await axios.get<Nanny>(`http://localhost:9000/api/nannies/getbyusername/${params.username}`);
                        // const response1 = await axios.get<Nanny>(`http://35.213.139.253:9000/api/nannies/getbyusername/${params.username}`);
                        setCustomer(response.data);
                        setNanny(response1.data);

                        if (response.data && response1.data) {
                            const response2 = await axios.get<BookingQueue[]>(`http://localhost:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            // const response2 = await axios.get<BookingQueue[]>(`http://35.213.139.253:9000/api/bookingqueue/getbookings/${response.data.id}/${response1.data.id}`);
                            setBookingqueue(response2.data);
                            console.log("All DATA Booking ", response2.data);
                            // Loop through bookingqueue and create BookingHistory for each booking

                            const response4 = await axios.get<BookingQueue>(`http://localhost:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            // const response4 = await axios.get<BookingQueue>(`http://35.213.139.253:9000/api/bookingqueue/getbookingsteststatus/${response.data.id}/${response1.data.id}`);
                            setBookingqueue2(response4.data);
                            console.log("All DATA Booking TEST STATUS ", response4.data);
                            console.log("All DATA Booking TEST STATUS ", response4.data.id);
                            // for (const booking of response2.data) {

                            const response3 = await axios.get<BookingHistory>(`http://localhost:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            // const response3 = await axios.get<BookingHistory>(`http://35.213.139.253:9000/api/bookinghistory/getbybookingid/${response4.data.id}`);
                            setBookingqueuehistory(response3.data);
                            //   console.log("Booking ID:", response3.data.booking_id);
                            console.log("All DATA Booking History", response3.data);
                            // }
                        }

                    }
                } catch (err) {
                    if (err instanceof Error) {
                        setError(err.message);
                    } else {
                        setError('An error occurred.');
                    }
                } finally {
                    setLoading(false);
                }
            };

            fetchData(); // Fetch data when the component mounts
        } else {
            alert('You need to be logged in first.');
            router.push('/login-user');
        }
        // Use an effect to update the nanny state when response1 changes
    }, [params.username, response1]);

    const handleSuggestionsChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setSuggestions(event.target.value);
    };

    // const handleSubmitComplaint = async () => {
    //     try {
    //         const complaintData: Complains = {
    //             booking_id: bookingqueuehistory?.booking_id || 0,
    //             customer_id: customer?.id || 0,
    //             nanny_id: nanny?.id || 0,
    //             complain_fields: complainFields,
    //             complain_text: suggestions,
    //             complain_count: Object.values(complainFields).filter(value => value === 'T').length,
    //         };

    //         const response = await axios.post('http://localhost:9000/api/complains', complaintData);
    //         console.log('Complaint submitted successfully:', response.data);
    //     } catch (error) {
    //         console.error('Error submitting complaint:', error);
    //     }
    // };
    const handleSubmitComplaint = async () => {
        try {
            // Prepare complain data to send to the database
            const complaintData = {
                booking_id: bookingqueuehistory?.booking_id || 0,
                customer_id: customer?.id || 0,
                nanny_id: nanny?.id || 0,
                complain_field_1: complainFields.field1,
                complain_field_2: complainFields.field2,
                complain_field_3: complainFields.field3,
                complain_field_4: complainFields.field4,
                complain_field_5: complainFields.field5,
                complain_text: suggestions,
                complain_count: Object.values(complainFields).filter(value => value === 'T').length,
            };

            // Send complaint data to the database
            const response = await axios.post('http://localhost:9000/api/complains', complaintData);
            console.log('Complaint submitted successfully:', response.data);
            // alert("Booking created successfully");
        } catch (error) {
            alert("Complains created Error");
            console.error('Error submitting complaint:', error);
        }
        router.push(`/reportconclusion/${nanny?.username}`);
    };

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error: {error}</div>;

    if (!customer) return <div>Customer not found.</div>;
    if (!nanny) return <div>Nanny not found.</div>;

    if (!bookingqueuehistory) return <div>Bookingqueuehistory not found.</div>;

    return (
        <>
            <div>
                <div className="mt-3 text-center text-4xl sm:text-xl md:text-4xl lg:text-4xl xl:text-4xl 2xl:text-5xl font-bold">
                    <span style={{ fontFamily: 'Montserrat' }} className="text-white">Complain</span>
                </div>

                <div style={{ fontFamily: 'Montserrat' }} className='p-5 mt-4 bg-slate-50 border border-pink-400 text-center mx-6 rounded-xl shadow-md'>
                    <div className="text-left mb-6">
                        <div className="mb-4">
                            <label className="inline-flex items-center">
                                <input
                                    type="checkbox"
                                    name="field1"
                                    checked={complainFields.field1 === 'T'}
                                    onChange={handleCheckboxChange('field1')}
                                    className="mr-2 form-checkbox h-4 w-4 text-pink-600"
                                />
                                Has the nanny been arriving late to the service?
                            </label>
                        </div>
                        <div className="mb-4">
                            <label className="inline-flex items-center">
                                <input
                                    type="checkbox"
                                    name="field2"
                                    checked={complainFields.field2 === 'T'}
                                    onChange={handleCheckboxChange('field2')}
                                    className="mr-2 form-checkbox h-4 w-4 text-pink-600"
                                />
                                Does the nanny exhibit harmful behavior?
                            </label>
                        </div>
                        <div className="mb-4">
                            <label className="inline-flex items-center">
                                <input
                                    type="checkbox"
                                    name="field3"
                                    checked={complainFields.field3 === 'T'}
                                    onChange={handleCheckboxChange('field3')}
                                    className="mr-2 form-checkbox h-4 w-4 text-pink-600"
                                />
                                Has the nanny used rude language when speaking to your children?
                            </label>
                        </div>
                        <div className="mb-4">
                            <label className="inline-flex items-center">
                                <input
                                    type="checkbox"
                                    name="field4"
                                    checked={complainFields.field4 === 'T'}
                                    onChange={handleCheckboxChange('field4')}
                                    className="mr-2 form-checkbox h-4 w-4 text-pink-600"
                                />
                                Are there any concerns about the nanny professionalism or boundaries?
                            </label>
                        </div>
                        <div className="mb-4">
                            <label className="inline-flex items-center">
                                <input
                                    type="checkbox"
                                    name="field5"
                                    checked={complainFields.field5 === 'T'}
                                    onChange={handleCheckboxChange('field5')}
                                    className="mr-2 form-checkbox h-4 w-4 text-pink-600"
                                />
                                Does the nanny appear unkempt or dirty?
                            </label>
                        </div>
                    </div>
                    <div className="text-left">
                        <label htmlFor="complainText" className="mb-2 block">Complain Text (max 250 words):</label>
                        <textarea
                            id="complainText"
                            name="complainText"
                            value={suggestions}
                            onChange={handleSuggestionsChange}
                            placeholder="Enter complaint text here..."
                            className='mt-2 p-2 rounded-xl border-stone-900 w-full'
                            maxLength={250}
                            rows={5}
                        />
                    </div>
                    <div className="mt-6">
                        <div className="bg-gray-100 rounded-lg p-4 mb-4">
                            <p className="font-semibold">Booking Details:</p>
                            <p className="mb-2">Booking ID: {bookingqueuehistory.booking_id}</p>
                            <p className="mb-2">Status: {bookingqueuehistory.status}</p>
                            <p className="mb-2">Time Session: {bookingqueuehistory.time_session}</p>
                        </div>
                        <button onClick={handleSubmitComplaint} className="block w-full bg-pink-600 text-white py-3 rounded-lg hover:bg-pink-700 focus:outline-none focus:bg-pink-700 font-bold">
                            Submit Complaint
                        </button>
                    </div>

                </div>

            </div>
        </>
    );
}