"use client";
import React, { FormEvent, useState } from "react";
import RootLayout from "../layout";
import Image from "next/image";
import logo from "../../../assets/Logo.png";
import styles from "../../../styles/Register.module.css";
import axios from "axios";
import { useRouter } from "next/navigation";
import Modal from "react-modal";
import { registerNanny } from "../../../Component/apiService";
import profileData from "../../../Component/defaultprofile/page";
type Props = {};

export default function RegisterNannypage({ }: Props) {
  const router = useRouter();
  const [profile, setProfile] = useState(profileData);
  const [formData, setFormData] = useState({
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    pass_word: "",
    district: "",
    sub_district: "",
    province: "",
    zip_code: "",
    street_number: "",
    contact_number: "",
    role: "NANNY",
    role_level: "",
    type_work: "",
    cost: 0.0,
    gender: "",
    age: "",
    status: "Inactive",

    citizen_id: "",
    skill_1: "",
    skill_2: "",
    skill_3: "",
    profile_image_url: profile,
    score: 0.0,
    descriptions: "",
  });

  // const handleChange = (e: any) => {
  //   setFormData({ ...formData, [e.target.name]: e.target.value })
  // }
  const [usernameError, setUsernameError] = useState<string | null>(null);
  const [consentGiven, setConsentGiven] = useState(false);
  const [showTooltip, setShowTooltip] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen);
  };

  const handleConsent = () => {
    setConsentGiven(true);
    setModalIsOpen(false);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    if (name === "username" && value.includes(" ")) {
      alert("Spaces are not allowed in the username.");
      const newValue = value.replace(/\s+/g, "");
      setFormData({ ...formData, [name]: newValue });
      return;
    }
    if (name === "contact_number") {
      const regex = /^[0-9]*$/;
      if (!regex.test(value)) {
        alert("Only numeric values are allowed.");
        return;
      }
      if (value.length > 10) {
        alert("Contact number must not exceed 10 digits.");
        return;
      }
    }
    setFormData({ ...formData, [name]: value });

    // Check if the input contains a space
    if (name === "username" && value.includes(" ")) {
      setUsernameError("Username cannot contain spaces");
    } else {
      setUsernameError(null);
    }
  };

  const handleChange2 = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };
  // const handleSubmit = async (e: any) => {
  //   e.preventDefault();
  //   try {
  //     const response = await axios.post('http://localhost:9000/api/nannies/register', formData);
  //     const data = response.data;
  //     console.log('Registered:', data);
  //     router.push("/login-nanny");
  //   } catch (error: any) {
  //     if (error.response && error.response.status === 500) {
  //       alert("Email is already in use.")
  //     }
  //     console.error('Error registering:', error);
  //   }
  // };
  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Check if there's an error with the username
    if (!consentGiven) {
      alert("Please agree to the terms to register.");
      return;
    }
    if (usernameError) {
      alert(usernameError);
      return;
    }

    if (parseInt(formData.age) < 20) {
      alert("Age must be 20 years or older.");
      return; // Stop submission if the age is too low
    }

    if (formData.citizen_id.length !== 13) {
      alert("Citizen ID must be exactly 13 digits.");
      return; // Prevent submission if the citizen ID is not exactly 13 digits
    }
    try {
      // const response = await axios.post('http://localhost:9000/api/nannies/register', formData);
      const response = await registerNanny(formData);
      const data = response.data;
      console.log("Registered:", data);
      router.push("/login-nanny");
    } catch (error) {
      if (
        axios.isAxiosError(error) &&
        error.response &&
        error.response.status === 500
      ) {
        alert("Email is already in use.");
      }
      console.error("Error registering:", error);
    }
  };
  return (
    <div className={styles.rootContainer}>
      <RootLayout showNavbar={false}>
        <div className="flex justify-center">
          <div className="text-center mt-5">
            <Image
              src={logo}
              className="img-fluid"
              width="250"
              height="150"
              alt=""
            />
          </div>
        </div>
        <div
          className={` text-center p-4 m-10 rounded-2xl mt-5 ${styles.borderBox}`}
        >
          <span
            style={{ fontFamily: "Comfortaa" }}
            className="text-xl mt-4 sm:text-2xl md:text-3xl lg:text-4xl xl:text-5xl 2xl:text-6xl text-black font-bold"
          >
            Register Nanny
          </span>

          {/* <form onSubmit={handleSubmit} className="flex flex-col lg:grid lg:grid-cols-2 gap-2 text-center items-center">
            <div className="mt-10 username">
              <label style={{ fontFamily: 'Comfortaa' }} className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold">Username</label>
              <input
                type="text"
                name="username"
                value={formData.username}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: 'Comfortaa' }}
              />
            </div> */}
          <form
            onSubmit={handleSubmit}
            className="flex flex-col lg:grid lg:grid-cols-2 gap-2 text-center items-center"
          >
            <div className="mt-10 username">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Username
              </label>
              <input
                type="text"
                name="username"
                value={formData.username}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
              {usernameError && (
                <div className="text-red-500">{usernameError}</div>
              )}
            </div>

            <div className="mt-10 firstname">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                First Name
              </label>
              <input
                type="text"
                name="first_name"
                value={formData.first_name}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 lastname">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Last Name
              </label>
              <input
                type="text"
                name="last_name"
                value={formData.last_name}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 emails">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Email
              </label>
              <input
                type="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 passwords">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Password
              </label>
              <input
                type="password"
                name="pass_word"
                value={formData.pass_word}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 districts">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                District
              </label>
              <input
                type="text"
                name="district"
                value={formData.district}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 subDistricts">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Sub District
              </label>
              <input
                type="text"
                name="sub_district"
                value={formData.sub_district}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 streetnums">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Street Number
              </label>
              <input
                type="text"
                name="street_number"
                value={formData.street_number}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 provice">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Province
              </label>
              <input
                type="text"
                name="province"
                value={formData.province}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 zipcode">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Zip Code
              </label>
              <input
                type="text"
                name="zip_code"
                value={formData.zip_code}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 contactNum">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Contact Number
              </label>
              <input
                type="tel"
                name="contact_number"
                value={formData.contact_number}
                onChange={handleChange}
                required
                pattern="[0-9]{10}"
                title="Contact number must be 10 digits."
                className="p-2 rounded-3xl"
                placeholder="Enter 10-digit number"
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 age">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Age
              </label>
              <input
                type="number"
                name="age"
                value={formData.age}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
                min="20"
              />
            </div>

            <div className="mt-10 gender">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Gender
              </label>
              <select
                name="gender"
                value={formData.gender}
                onChange={handleSelectChange}
                required
              >
                <option value="">Select Gender</option>
                <option value="female">Female</option>
                <option value="Male">Male</option>
                <option value="PreferNotToSay">Prefer not to say</option>
              </select>
            </div>

            <div className="mt-10 roleLevel">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Role Level
              </label>
              <select
                name="role_level"
                value={formData.role_level}
                onChange={handleSelectChange}
                required
              >
                <option value="">Select role level</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
              </select>
            </div>

            <div className="mt-10 typeWork">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Type Work
              </label>
              <select
                name="type_work"
                value={formData.type_work}
                onChange={handleSelectChange}
                required
              >
                <option value="">Select type of work</option>
                <option value="F">Full Time</option>
                <option value="P">Part Time</option>
                <option value="A">ALL</option>
              </select>
            </div>

            <div className="mt-10 citizen_id">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Citizen ID
              </label>
              <input
                type="number"
                name="citizen_id"
                value={formData.citizen_id}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 skill_1">
              <label className="block text-start text-black mb-2 text-sm font-semibold">
                Cooking Skill
              </label>
              <select
                name="skill_1"
                value={formData.skill_1}
                onChange={handleChange2}
                required
                className={`p-2 rounded-3xl`}
              >
                <option value="">Select Cooking Skill</option>
                <option value="Thai cuisine cooking">Thai cuisine cooking</option>
                <option value="Western cuisine cooking">Western cuisine cooking</option>
                <option value="Japanese cuisine cooking">Japanese cuisine cooking</option>
                <option value="Baby/toddler food preparation">Baby/toddler food preparation</option>
              </select>
            </div>

            {/* Skill 2: Speaking Skill */}
            <div className="mt-10 skill_2">
              <label className="block text-start text-black mb-2 text-sm font-semibold">
                Speaking Skill
              </label>
              <select
                name="skill_2"
                value={formData.skill_2}
                onChange={handleChange2}
                required
                className={`p-2 rounded-3xl`}
              >
                <option value="">Select Speaking Skill</option>
                <option value="Native Thai speaker">Native Thai speaker</option>
                <option value="Fluent English speaker">Fluent English speaker</option>
                <option value="Mandarin/Cantonese Chinese speaker">Mandarin/Cantonese Chinese speaker</option>
                <option value="Fluent Japan speaker">Fluent Japan speaker</option>
                <option value="Fluent French Speaker">Fluent French Speaker</option>
              </select>
            </div>

            {/* Skill 3: General Skill */}
            <div className="mt-10 skill_3">
              <label className="block text-start text-black mb-2 text-sm font-semibold">
                General Skill
              </label>
              <select
                name="skill_3"
                value={formData.skill_3}
                onChange={handleChange2}
                required
                className={`p-2 rounded-3xl`}
              >
                <option value="">Select General Skill</option>
                <option value="Drawing Skill">Drawing Skill</option>
                <option value="Singing Skill">Singing Skill</option>
                <option value="Entertain Skill">Entertain Skill</option>
              </select>
            </div>

            {/* <div className="mt-10 skill_1">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Skill 1
              </label>
              <input
                type="text"
                name="skill_1"
                value={formData.skill_1}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 skill_2">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Skill 2
              </label>
              <input
                type="text"
                name="skill_2"
                value={formData.skill_2}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 skill_3">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Skill 3
              </label>
              <input
                type="text"
                name="skill_3"
                value={formData.skill_3}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div> */}

            <div className="mt-10 descriptions">
              <label
                style={{ fontFamily: "Comfortaa" }}
                className="block text-start text-black mb-2 text-sm sm:text-sm md:text-sm lg:text-sm xl:text-sm 2xl:text-sm font-semibold"
              >
                Bio
              </label>
              <input
                type="text"
                name="descriptions"
                value={formData.descriptions}
                onChange={handleChange}
                required
                className={`p-2 rounded-3xl`}
                style={{ fontFamily: "Comfortaa" }}
              />
            </div>

            <div className="mt-10 col-span-2 text-white bg-red-300 p-4 rounded-2xl">
              <button style={{ fontFamily: "Comfortaa" }} type="submit">
                Register
              </button>
            </div>

            <div className="text-center">
              <button style={{ fontFamily: "Comfortaa" }} onClick={toggleModal}>
                Open Consent Form
              </button>
              <Modal
                isOpen={modalIsOpen}
                onRequestClose={toggleModal}
                contentLabel="Consent Form"
              >
                <div className="p-6">
                  <h2 className="text-lg font-semibold mb-4">Consent Form</h2>
                  <p className="text-gray-700 mb-4">
                    You consent to the collection and processing of your
                    personal information in accordance with our Privacy Policy.
                    You understand that the information provided during
                    registration may be used for account management,
                    communication, and service improvement purposes. You
                    acknowledge that you have read and understood our Terms of
                    Service, and agree to abide by them. You affirm that you are
                    at least [insert age requirement] years old, or that you
                    have obtained parental/legal guardian consent if under the
                    specified age. Your registration signifies your consent to
                    these terms. If you do not agree with any of these terms,
                    please refrain from registering on our website.
                  </p>
                  <div className="mt-4 flex items-center justify-start">
                    <label className="flex items-center mr-2">
                      <input
                        type="checkbox"
                        checked={consentGiven}
                        onChange={(e) => setConsentGiven(e.target.checked)}
                        className="p-2 rounded-3xl"
                      />
                      <span className="ml-2 text-gray-800">
                        I agree to the terms and conditions
                      </span>
                    </label>
                  </div>
                  <div className="mt-6 flex justify-end">
                    <button
                      onClick={toggleModal}
                      className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-semibold py-2 px-4 mr-2 rounded"
                    >
                      Close
                    </button>
                  </div>
                </div>
              </Modal>
            </div>
          </form>
        </div>
      </RootLayout>
    </div>
  );
}
