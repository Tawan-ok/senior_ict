
USE `nannyrightnow`;
CREATE TABLE customers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(100) NOT NULL UNIQUE,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
 pass_word VARCHAR(60) NOT NULL,
    district VARCHAR(255) NOT NULL,
 sub_district VARCHAR(255) NOT NULL,
 province VARCHAR(255) NOT NULL,
 zip_code VARCHAR(10) NOT NULL,
    street_number VARCHAR(255) NOT NULL,
    contact_number VARCHAR(15) NOT NULL,
    `role` VARCHAR(15) NOT NULL,
    age INT NOT NULL,
    gender VARCHAR(20),
    locationall varchar(255),
    profile_image_url blob
);


CREATE TABLE nannies (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(100) NOT NULL UNIQUE,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
 pass_word VARCHAR(60) NOT NULL,
    district VARCHAR(255) NOT NULL,
 sub_district VARCHAR(255) NOT NULL,
 province VARCHAR(255) NOT NULL,
 zip_code VARCHAR(10) NOT NULL,
    street_number VARCHAR(255) NOT NULL,
    contact_number VARCHAR(15) NOT NULL,
    role_level VARCHAR(20) NOT NULL,
    cost DECIMAL(8, 2) NOT NULL,
    type_work VARCHAR(20) NOT NULL,
    status VARCHAR(10) NOT NULL CHECK (status IN ('Active', 'Inactive')),
    age INT NOT NULL,
    score DOUBLE DEFAULT 0,
    gender VARCHAR(20),
    `role` VARCHAR(15) NOT NULL
);

CREATE TABLE admins (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(100) NOT NULL,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
 pass_word VARCHAR(60) NOT NULL,
    district VARCHAR(255) NOT NULL,
 sub_district VARCHAR(255) NOT NULL,
 province VARCHAR(255) NOT NULL,
 zip_code VARCHAR(10) NOT NULL,
    street_number VARCHAR(255) NOT NULL,
    contact_number VARCHAR(15) NOT NULL,
    `role` VARCHAR(15) NOT NULL,
    age INT NOT NULL,
    gender VARCHAR(20)
);

CREATE TABLE favourite_nannies (
    id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    nanny_id INT NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    FOREIGN KEY (nanny_id) REFERENCES nannies(id)
);

CREATE TABLE bookings (
    id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    nanny_id INT NOT NULL,
    start_date DATETIME NOT NULL,
    end_date DATETIME NOT NULL,
    total_amount DECIMAL(10, 2) NOT NULL,
    status_payment VARCHAR(20) NOT NULL,
    hours INT NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    FOREIGN KEY (nanny_id) REFERENCES nannies(id)
);

CREATE TABLE booking_history (
    id INT PRIMARY KEY AUTO_INCREMENT,
   booking_id INT NOT NULL,
    status VARCHAR(10) NOT NULL CHECK (status IN ('Completed', 'Canceled', 'Process')),
    time_session INT NOT NULL,
	FOREIGN KEY (booking_id) REFERENCES bookings(id)
);

CREATE TABLE activity_program (
    ProgramID INT AUTO_INCREMENT PRIMARY KEY,
    Normal_Period1 VARCHAR(255),
    Normal_Period2 VARCHAR(255),
    Normal_Period3 VARCHAR(255),
    Normal_Period4 VARCHAR(255),
    Normal_Period5 VARCHAR(255),
    Overnight_Period1 VARCHAR(255),
    Overnight_Period2 VARCHAR(255),
    Overnight_Period3 VARCHAR(255),
    Overnight_Period4 VARCHAR(255),
    Overnight_Period5 VARCHAR(255),
    customer_id INT,
    CONSTRAINT fk_activityprogram_customer FOREIGN KEY (customer_id) REFERENCES customers(id)
);
INSERT INTO customers 
(username, first_name, last_name, email, pass_word, district, sub_district, province, zip_code, street_number, contact_number, `role`, age, gender) 
VALUES 
('DoeJohn123', 'Doe', 'John', 'doejohn@example.com', 'hashedpasswordhere', 'East District', 'Sub District 2', 'Khornkan', '54321', '95th Street', '+1234567890', 'USER', 35, 'male');


INSERT INTO nannies 
(username, first_name, last_name, email, pass_word, district, sub_district, province, zip_code, street_number, contact_number, role_level, cost, type_work, status, age, gender, `role`) 
VALUES 
('nannyLinda99', 'Linda', 'Smith', 'linda.smith@example.com', 'somehashedpassword', 'Central District', 'Sub District 1', 'Nanny Province', '12345', '123 Maple Street', '+1234567890', 'Senior', 30.00, 'F', 'Active', 28, 'female', 'NANNY');


