package com.example.demo.Service;

import com.example.demo.Model.*;

import com.example.demo.Repository.BookingHistoryRepository;
import com.example.demo.Repository.NannyRepository;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NannyService {
    @Autowired
    private NannyRepository nannyRepository;
    @Autowired
    private CommonService commonService;
    @Autowired
    private BookingHistoryRepository bookingHistoryRepository;

    public List<Nanny> getAllNannies() {
        return nannyRepository.findAll();
    }

    public Nanny getNanniesById(Long id) {

        return nannyRepository.findById(id).orElse(null);
    }

    public List<Nanny> findNannyRankingByScore() {

        return nannyRepository.findNannyRankingByScore();
    }
//    public Nanny saveNanny(Nanny nanny) {
//        return nannyRepository.save(nanny);
//    }

    public Nanny saveNanny(Nanny nanny) {
        if (commonService.isEmailUnique(nanny.getEmail())) {
            return nannyRepository.save(nanny);
        } else {
            throw new IllegalArgumentException("Email is already in use.");
        }
    }

    public Nanny saveNannyByAdmin(Nanny nanny) {
        return nannyRepository.save(nanny);
    }

    public void updateNannyStatusToInactive(String username) {
        nannyRepository.updateStatusByUsername(username);
    }

    public  Nanny findNannyByUsername(String username) {
        return  nannyRepository.findNannyByUsername(username);
    }



    public void deleteNanny(Long id) {
        nannyRepository.deleteById(id);
    }

    @Transactional
    public void updateScoreByAdding(Long nannyId, double newScore) {
        Nanny nanny = nannyRepository.findById(nannyId).orElse(null);
        if (nanny != null) {
            double currentScore = nanny.getScore();
            double updatedScore = currentScore + newScore;
            nanny.setScore(updatedScore);
            nannyRepository.save(nanny);
        }
    }

    public List<Nanny> findByFullNameContaining(String keyword) {
        return nannyRepository.findByFullNameContaining("%" + keyword + "%");
    }

    public List<Nanny> findByFullNameContainingAndAllskill(String keyword, List<String> allskill) {
        return nannyRepository.findByFullNameContainingAndAllskill("%" + keyword + "%",allskill);
    }

    public Nanny findByEmail(String email) {

        return nannyRepository.findByEmail(email).orElse(null);
    }

    public List<Nanny> findByFullNameContainingAndRoleLevelAndType(String keyword,  List<String> roleLevel, String typeWork) {
        return nannyRepository.findByFullNameContainingAndRoleLevelAndType("%" + keyword + "%", roleLevel, typeWork);
    }

    public List<Nanny> findByRoleLevel( List<String> roleLevel) {
        return nannyRepository.findByRoleLevel(roleLevel);
    }

    public List<Nanny> findByRoleLevelAndAllskill( List<String> roleLevel , List<String> allskill) {
        return nannyRepository.findByRoleLevelAndAllskill(roleLevel , allskill);
    }


    public List<Nanny> findByFullNameContainingAndRoleLevel(String keyword,  List<String> roleLevel) {
        return nannyRepository.findByFullNameContainingAndRoleLevel("%" + keyword + "%", roleLevel);
    }

    public List<Nanny> findByFullNameContainingAndRoleLevelAndAllskill(String keyword,  List<String> roleLevel,List<String> allskill) {
        return nannyRepository.findByFullNameContainingAndRoleLevelAndAllskill("%" + keyword + "%", roleLevel,allskill);
    }

    public List<Nanny> findByFullNameContainingAndTypeOfWork(String keyword, String typeWork) {
        return nannyRepository.findByFullNameContainingAndTypeOfWork("%" + keyword + "%", typeWork);
    }

    public List<Nanny> findByFullNameContainingAndTypeOfWorkAndAllskill(String keyword, String typeWork, List<String> allskill) {
        return nannyRepository.findByFullNameContainingAndTypeOfWorkAndAllskill("%" + keyword + "%", typeWork,allskill);
    }

    public List<Nanny> findByRoleLevelAndTypeOfWork( List<String> roleLevel, String typeWork) {
        return nannyRepository.findByRoleLevelAndTypeOfWork(roleLevel, typeWork);
    }

    public List<Nanny> findByRoleLevelAndTypeOfWorkAndAllskill( List<String> roleLevel, String typeWork,List<String> allskill) {
        return nannyRepository.findByRoleLevelAndTypeOfWorkAndAllskill(roleLevel, typeWork , allskill);
    }


    public List<Nanny> findNanniesByTypeOfWork(String type_work) {
        return nannyRepository.findByTypeOfWork(type_work);
    }

    public List<Nanny> findByTypeOfWorkAndAllskill(String type_work,List<String> allskill) {
        return nannyRepository.findByTypeOfWorkAndAllskill(type_work,allskill);
    }
    // Skill
    public List<Nanny> findNanniesBySkill1(String skill_1) {
        return nannyRepository.findBySkill1(skill_1);
    }
    public List<Nanny> findNanniesBySkill2(String skill_2) {
        return nannyRepository.findBySkill2(skill_2);
    }
    public List<Nanny> findNanniesBySkill3(String skill_3) {
        return nannyRepository.findBySkill3(skill_3);
    }

//    public List<Nanny> findNanniesByAllSkill(String allskill) {
//        return nannyRepository.findByAllSkill(allskill);
//    }
public List<Nanny> findNanniesByAllSkill(List<String> skills) {
    return nannyRepository.findBySkills(skills);
}


    //
    // New
    public List<Nanny> findByGender(String gender) {
        return nannyRepository.findByGender(gender);
    }

    public List<Nanny> findByGenderAndAllskill(String gender , List<String> allskill) {
        return nannyRepository.findByGenderAndAllskill(gender,allskill);
    }

    public List<Nanny> findByGenderAndTypeOfWork(String type_work, String gender) {
        return nannyRepository.findByTypeOfWorkAndGender(type_work, gender);
    }

    public List<Nanny> findByTypeOfWorkAndGenderAllskill(String type_work, String gender,  List<String> allskill) {
        return nannyRepository.findByTypeOfWorkAndGenderAllskill(type_work, gender, allskill);
    }

    public List<Nanny> findByGenderAndRoleLevel( List<String> roleLevel, String gender) {
        return nannyRepository.findByRoleLevelAndGender(roleLevel, gender);
    }

    public List<Nanny> findByRoleLevelAndGenderAndAllskill( List<String> roleLevel, String gender, List<String> allskill) {
        return nannyRepository.findByRoleLevelAndGenderAndAllskill(roleLevel, gender , allskill);
    }

    public List<Nanny> findByGenderAndFullNameContaining(String keyword, String gender) {
        return nannyRepository.findByFullNameContainingAndGender("%" + keyword + "%",gender);
    }
    public List<Nanny> findByFullNameContainingAndGenderAllskill(String keyword, String gender, List<String> allskill) {
        return nannyRepository.findByFullNameContainingAndGenderAllskill("%" + keyword + "%",gender,allskill);
    }


    public List<Nanny> findByGenderAndTypeOfWorkAndRoleLevel(String roleLevel,String typeWork, String gender) {
        return nannyRepository.findByTypeOfWorkAndRoleLevelAndGender( roleLevel,typeWork, gender);
    }

    public List<Nanny> findsByGenderAndTypeOfWorkAndFullNameContaining(String keyword,String typeWork,String gender) {
        return nannyRepository.findByTypeOfWorkAndFullNameContainingAndGender( "%" + keyword + "%",typeWork,gender);
    }

    public List<Nanny> findByTypeOfWorkAndFullNameContainingAndGenderAndAllskill(String keyword,String typeWork,String gender,List<String> allskill) {
        return nannyRepository.findByTypeOfWorkAndFullNameContainingAndGenderAndAllskill( "%" + keyword + "%",typeWork,gender,allskill);
    }

    public List<Nanny> findByGenderAndRoleLevelAndFullNameContaining(String keyword,  List<String> roleLevel, String gender) {
        return nannyRepository.findByRoleLevelAndFullNameContainingAndGender("%" + keyword + "%",roleLevel,gender);
    }

    public List<Nanny> findByRoleLevelAndFullNameContainingAndGenderAndAllskill(String keyword,  List<String> roleLevel, String gender ,List<String> allskill) {
        return nannyRepository.findByRoleLevelAndFullNameContainingAndGenderAndAllskill("%" + keyword + "%",roleLevel,gender,allskill);
    }

    public List<Nanny> findByGenderAndTypeOfWorkAndRoleLevelAndFullNameContaining(String keyword, List<String> roleLevel,String typeWork,String gender) {
        return nannyRepository.findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGender( "%" + keyword + "%",roleLevel,typeWork,gender);
    }

    public List<Nanny> findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndAllskill(String keyword, List<String> roleLevel,String typeWork,String gender , List<String> allskill) {
        return nannyRepository.findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndAllskill( "%" + keyword + "%",roleLevel,typeWork,gender,allskill);
    }
    //

    // for District
    public List<Nanny> findNanniesByDistrict(String district) {
        return nannyRepository.findByDistrict(district);
    }

    public List<Nanny> findByDistrictAndAllskill(String district, List<String> allskill) {
        return nannyRepository.findByDistrictAndAllskill(district,allskill);
    }

    public List<Nanny> findByGenderAndTypeOfWorkAndRoleLevelAndFullNameContainingandDistrict(String keyword,String roleLevel,String typeWork,String gender , String district) {
        return nannyRepository.findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndDistrict( "%" + keyword + "%",roleLevel,typeWork,gender ,district );
    }
    public List<Nanny> findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndDistrictAndAllskill(String keyword, List<String> roleLevel,String typeWork,String gender , String district , List<String> allskill) {
        return nannyRepository.findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndDistrictAndAllskill( "%" + keyword + "%",roleLevel,typeWork,gender ,district , allskill );
    }

    public List<Nanny> findByRoleLevelAndTypeOfWorkAndFullNameContainingAndDistrict(String keyword,  List<String> roleLevel, String typeWork, String district) {
        return nannyRepository.findByRoleLevelAndTypeOfWorkAndFullNameContainingAndDistrict(keyword, roleLevel, typeWork, district);
    }

    public List<Nanny> findByRoleLevelAndTypeOfWorkAndFullNameContainingAndDistrictAndAllskill(String keyword,  List<String> roleLevel, String typeWork, String district , List<String> allskill ) {
        return nannyRepository.findByRoleLevelAndTypeOfWorkAndFullNameContainingAndDistrictAndAllskill(keyword, roleLevel, typeWork, district , allskill );
    }

    public List<Nanny> findByGenderAndRoleLevelAndFullNameContainingAndDistrict(String keyword,  List<String> roleLevel, String gender, String district) {
        return nannyRepository.findByGenderAndRoleLevelAndFullNameContainingAndDistrict(keyword, roleLevel, gender, district);
    }

    public List<Nanny> findByGenderAndRoleLevelAndFullNameContainingAndDistrictAndAllskill(String keyword,  List<String> roleLevel, String gender, String district, List<String> allskill) {
        return nannyRepository.findByGenderAndRoleLevelAndFullNameContainingAndDistrictAndAllskill(keyword, roleLevel, gender, district,allskill);
    }

    public List<Nanny> findByGenderAndTypeOfWorkAndFullNameContainingAndDistrict(String keyword, String typeWork, String gender, String district) {
        return nannyRepository.findByGenderAndTypeOfWorkAndFullNameContainingAndDistrict(keyword, typeWork, gender, district);
    }

    public List<Nanny> findByGenderAndTypeOfWorkAndFullNameContainingAndDistrictAndAllskill(String keyword, String typeWork, String gender, String district , List<String> allskill) {
        return nannyRepository.findByGenderAndTypeOfWorkAndFullNameContainingAndDistrictAndAllskill(keyword, typeWork, gender, district , allskill);
    }

    public List<Nanny> findByFullNameContainingAndDistrict(String keyword, String district) {
        return nannyRepository.findByFullNameContainingAndDistrict(keyword, district);
    }

    public List<Nanny> findByFullNameContainingAndDistrictAndAllskill(String keyword, String district, List<String> allskill) {
        return nannyRepository.findByFullNameContainingAndDistrictAndAllskill(keyword, district,allskill);
    }

    public List<Nanny> findByRoleLevelAndDistrict( List<String> roleLevel, String district) {
        return nannyRepository.findByRoleLevelAndDistrict(roleLevel, district);
    }

    public List<Nanny> findByRoleLevelAndDistrictAllskill( List<String> roleLevel, String district , List<String> allskill) {
        return nannyRepository.findByRoleLevelAndDistrictAllskill(roleLevel, district , allskill);
    }

    public List<Nanny> findByTypeOfWorkAndDistrict(String typeWork, String district) {
        return nannyRepository.findByTypeOfWorkAndDistrict(typeWork, district);
    }

    public List<Nanny> findByTypeOfWorkAndDistrictAndAllskill(String typeWork, String district , List<String> allskill) {
        return nannyRepository.findByTypeOfWorkAndDistrictAndAllskill(typeWork, district , allskill);
    }

    public List<Nanny> findByGenderAndDistrict(String gender, String district) {
        return nannyRepository.findByGenderAndDistrict(gender, district);
    }

    public List<Nanny> findByGenderAndDistrictAndAllskill(String gender, String district ,List<String> allskill) {
        return nannyRepository.findByGenderAndDistrictAndAllskill(gender, district,allskill);
    }
    //

//    public List<Object[]> getBookingHistoryByNannyId(Long nannyId) {
//        return bookingHistoryRepository.findByNannyId(nannyId);
//    }

    public List<BookingHistory> getBookingDataByNannyIdBH(Long nannyId) {
        return nannyRepository.findByNannyIdBH(nannyId);
    }

    public List<BookingQueue> getBookingDataByNannyIdBQ(Long nannyId) {
        return nannyRepository.findByNannyIdBQ(nannyId);
    }

    public List<BookingHistory> getBookingDataByNannyIdBHCompleted(Long nannyId) {
        return nannyRepository.findByNannyIdBHCompleted(nannyId);
    }

    public List<BookingQueue> getBookingDataByNannyIdBQSuccess(Long nannyId) {
        return nannyRepository.findByNannyIdBQSuccess(nannyId);
    }

    public List<BookingQueue> findPendingBQByNannyID(Long nannyId) {
        return nannyRepository.findBQByNannyIDAndNannyChoose(nannyId);
    }

    public List<BookingQueue> findBQByNannyIDStatusBookings(Long nannyId) {
        return nannyRepository.findBQByNannyIDStatusBookings(nannyId);
    }

    private String saveProfileImageToFileSystem(MultipartFile profile_image_url) throws IOException {

        // Implement the logic to save the image to a directory and return the URL.
        // You might want to generate a unique filename, handle errors, etc.
        // Example: Save the file to a directory and return the URL.
        String directory = "path/to/profile/images";
        String filename = "profile-image-" + UUID.randomUUID().toString() + ".jpg";

        byte[] image = Base64.encodeBase64(profile_image_url.getBytes(),true);
        String result = new String(image);
        return result;
//        try {
//            Path filePath = Paths.get(directory, filename);
//            Files.copy(profileImage.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
////            return "/profile/images/" + filename; // Return the URL
//
//        } catch (IOException e) {
//            throw new RuntimeException("Failed to save profile image", e);
//        }
    }

    public Nanny uploadProfileImage(Long nannyId, MultipartFile profile_image_url) throws IOException {
        System.out.println(profile_image_url);
        Nanny nanny = nannyRepository.findById(nannyId)
                .orElseThrow(() -> new EntityNotFoundException("Nanny not found with ID: " + nannyId));

        String imageUrl = saveProfileImageToFileSystem(profile_image_url);
        nanny.setProfileImageUrl(imageUrl);
//        customer.setProfileImageUrl("TESESESE");
        System.out.println("IMAGE:"+imageUrl);
        nannyRepository.save(nanny);
        return nanny;
    }

    public List<Optional> findNanniesWithAvailabilitySlots() {
        return nannyRepository.findNanniesWithAvailabilitySlots();
    }


}