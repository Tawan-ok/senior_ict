package com.example.demo.Service;

import com.example.demo.Model.AvailabilitySlot;
import com.example.demo.Repository.AvailabilitySlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AvailabilitySlotService {

    @Autowired
    private AvailabilitySlotRepository repository;

    public AvailabilitySlot createSlot(AvailabilitySlot slot) {
        // Additional validation and business logic here
        return repository.save(slot);
    }

    public List<AvailabilitySlot> getSlotsByNannyId(Long nannyId) {
        return repository.findByNannyId(nannyId);
    }
    public List<AvailabilitySlot> getAllSlots() {
        return repository.findAll();
    }

//    public List<AvailabilitySlot> findAvailableSlots(Date startTime, Date endTime) {
//        return repository.findAll().stream()
//                //ICT timezone
//                // Temporarily comment out other conditions to test basic filtering
//                .filter(AvailabilitySlot::isAvailable)
//                .collect(Collectors.toList());
//    }

    public List<AvailabilitySlot> findAvailableSlots(Date startTime, Date endTime) {
        LocalDate startDate = startTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = endTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        return repository.findAll().stream()
                .filter(AvailabilitySlot::isAvailable)
                .filter(slot -> {
                    LocalDate slotStartDate = slot.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    LocalDate slotEndDate = slot.getEndTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    // Ensure the slot fully covers the requested range
                    return !slotStartDate.isAfter(startDate) && !slotEndDate.isBefore(endDate);
                })
                .collect(Collectors.toList());
    }


    // Additional service methods as needed
    //ICT timezone
}
