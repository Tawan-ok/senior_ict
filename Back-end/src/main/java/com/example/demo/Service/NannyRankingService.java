package com.example.demo.Service;


import com.example.demo.Model.BookingQueue;
import com.example.demo.Model.Customer;
import com.example.demo.Model.Nanny;
import com.example.demo.Model.NannyRankingDTO;
import com.example.demo.Repository.BookingHistoryRepository;
import com.example.demo.Repository.BookingQueueRepository;
import com.example.demo.Repository.CustomerRepository;
import com.example.demo.Repository.NannyRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class NannyRankingService {

    @Autowired
    private NannyRepository nannyRepository;

    @Autowired
    private BookingHistoryRepository bookingHistoryRepository;

    @Autowired
    private BookingQueueRepository bookingQueueRepository;

    @Autowired
    private CustomerRepository customerRepository;

    public List<NannyRankingDTO> getRankedNannies(Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found"));
        double reviewScoreWeight = customer.getReviewScoreWeight();
        double successfulJobsWeight = customer.getSuccessfulJobsWeight();

        List<Nanny> nannies = nannyRepository.findAll();
        return nannies.stream().map(nanny -> {
                    double reviewScore = nanny.getScore();
                    long successfulJobs = bookingHistoryRepository.countByNannyIdAndStatus(nanny.getNannyId(), "Completed");
                    double avgreview =  reviewScore / successfulJobs;

                    double weightedScore = (reviewScoreWeight * avgreview) + (successfulJobsWeight * successfulJobs);
                    return new NannyRankingDTO(nanny, weightedScore);

                })
                .sorted((n1, n2) -> Double.compare(n2.getWeightedScore(), n1.getWeightedScore()))
                .collect(Collectors.toList());
    }
    public List<NannyRankingDTO> getRankedNanniesHiringType(Long customerId, String typeHiring) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found"));
        double reviewScoreWeight = customer.getReviewScoreWeight();
        double successfulJobsWeight = customer.getSuccessfulJobsWeight();

        List<Nanny> nannies = nannyRepository.findAll();
        Map<Long, List<BookingQueue>> bookingsByNanny = bookingQueueRepository.findBookingsByTypeHiring(typeHiring).stream()
                .collect(Collectors.groupingBy(BookingQueue::getNanny_id));

        List<NannyRankingDTO> rankings = new ArrayList<>();

        for (Nanny nanny : nannies) {
            double reviewScore = nanny.getScore();
            long successfulJobs = bookingHistoryRepository.countByNannyIdAndStatus(nanny.getNannyId(), "Completed");
            double averageReview = reviewScore / successfulJobs;

            double weightedScore = (reviewScoreWeight * averageReview) + (successfulJobsWeight * successfulJobs);
            NannyRankingDTO nannyRankingDTO = new NannyRankingDTO(nanny, weightedScore);

            List<BookingQueue> bookings = bookingsByNanny.get(nanny.getNannyId());
            if (bookings != null && !bookings.isEmpty()) {
                rankings.add(nannyRankingDTO);
            }
        }

        return rankings.stream()
                .sorted(Comparator.comparingDouble(NannyRankingDTO::getWeightedScore).reversed())
                .collect(Collectors.toList());
    }

    public List<NannyRankingDTO> getRankedNanniesHour(Long customerId) {
        return getRankedNanniesHiringType(customerId, "hour");
    }

    public List<NannyRankingDTO> getRankedNanniesDay(Long customerId) {
        return getRankedNanniesHiringType(customerId, "day");
    }

    public List<NannyRankingDTO> getRankedNanniesMonth(Long customerId) {
        return getRankedNanniesHiringType(customerId, "month");
    }
    public Customer updatePreferences(Long customerId, double reviewScoreWeight, double successfulJobsWeight) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (customerOptional.isEmpty()) {
            throw new EntityNotFoundException("Customer not found with ID: " + customerId);
        }
        Customer customer = customerOptional.get();
        customer.setReviewScoreWeight(reviewScoreWeight);
        customer.setSuccessfulJobsWeight(successfulJobsWeight);
        return customerRepository.save(customer);
    }
}