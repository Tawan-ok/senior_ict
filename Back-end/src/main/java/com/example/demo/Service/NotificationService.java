package com.example.demo.Service;

import com.example.demo.Model.Notification;
import com.example.demo.Repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    public Notification createNotification(Notification notification) {
        return notificationRepository.save(notification);
    }

    public List<Notification> getNotificationsForRecipient(Long recipientId, String recipient_type) {
        return notificationRepository.findByRecipientId(recipientId);
    }
    public Notification updateNotificationStatus(Long id, String status) {
        Notification notification = notificationRepository.findById(id).orElse(null);
        if (notification != null) {
            notification.setStatus(status);
            return notificationRepository.save(notification);
        }
        return null; // Or handle this case as you see fit (e.g., throw an exception)
    }

    public void deleteNotification(Long id) {
        notificationRepository.deleteById(id);
    }

    public List<Notification> getNotificationsForNanny(Long nannyId) {
        // Assuming you have a method in your repository to find notifications by nannyId
        return notificationRepository.findByRecipientIdAndRecipientType(nannyId, "NANNY");
    }


    public Notification createNannyNotification(Long nannyId, String message) {
        Notification notification = new Notification();
        notification.setRecipientId(nannyId);
        notification.setRecipientType("NANNY");
        notification.setMessage(message);
        notification.setStatus("UNREAD"); // Assuming you have an "UNREAD" status by default
        // Additional setup for notification as needed
        return notificationRepository.save(notification);
    }


    public Notification createCustomerNotification(Long customerId, String message) {
        Notification notification = new Notification();
        notification.setRecipientId(customerId);
        notification.setRecipientType("CUSTOMER");
        notification.setMessage(message);
        notification.setStatus("UNREAD");
        // Additional setup for notification as needed
        return notificationRepository.save(notification);
    }

    public Notification handleNotificationAction(Long id, String action) {
        Notification notification = notificationRepository.findById(id).orElse(null);

        if (notification != null) {
            // Assuming you have a method to map actions to notification statuses
            String status = mapActionToStatus(action);
            notification.setStatus(status);
            return notificationRepository.save(notification);
        }
        return null;
    }

    private String mapActionToStatus(String action) {
        // Implement logic to convert actions like 'confirm', 'deny' to statuses
        // For example:
        switch (action) {
            case "confirm":
                return "confirmed";
            case "deny":
                return "denied";
            default:
                return "pending"; // Default or unknown action
        }
    }

    public List<Notification> getNotificationsForNannyAndStatus(Long nannyId, String status) {
        return notificationRepository.findByRecipientIdAndRecipientTypeAndStatus(nannyId, "NANNY", status);
    }

    public List<Notification> getNotificationsForUser(Long userId) {
        // Assuming you have a method in NotificationRepository to find notifications by recipientId
        return notificationRepository.findByRecipientId(userId);
    }

    public List<Notification> getNotificationsBySenderId(Long senderId) {
        return notificationRepository.findBySenderId(senderId);
    }
    public Notification closeNotification(Long id) {
        Optional<Notification> notificationOptional = notificationRepository.findById(id);
        if (notificationOptional.isPresent()) {
            Notification notification = notificationOptional.get();
            notification.setStatus("closed"); // Or any status indicating the notification is acknowledged/closed
            return notificationRepository.save(notification);
        }
        return null;
    }

    public List<Notification> getNotificationsByUserIdAndStatus(Long userId, String status) {
        return notificationRepository.findBySenderIdAndStatus(userId, status);
    }

    public List<Notification> getNotificationsForUserExcludingClosed(Long userId) {
        return notificationRepository.findByRecipientIdAndStatusNot(userId, "closed");
    }

}
