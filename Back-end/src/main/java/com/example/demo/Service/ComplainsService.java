package com.example.demo.Service;

import com.example.demo.Model.BookingQueue;
import com.example.demo.Model.Complains;
import com.example.demo.Repository.ComplainsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComplainsService {

    @Autowired
    private ComplainsRepository complainsrepository;

    public List<Complains> getAllComplains() {
        return complainsrepository.findAll();
    }

    public Complains getComplainsById(Long id) {

        return complainsrepository.findById(id).orElse(null);
    }

    public Complains saveComplains(Complains complains) {

        return complainsrepository.save(complains);
    }

    public void deleteComplains(Long id) {
        complainsrepository.deleteById(id);
    }

    public List<Complains> getComplainByNannyId( Long nanny_id) {
        return complainsrepository.findbyComplainNannyId( nanny_id);
    }

    public Complains getComplainByBookingId( Long booking_id) {
        return complainsrepository.findbyComplainBookingId( booking_id);
    }
}
