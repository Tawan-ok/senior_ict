package com.example.demo.Service;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
public class WebAutomationService {

    public void performWebAutomation(String searchValue) {
//        WebDriverManager.edgedriver().setup();
        WebDriver driver = new EdgeDriver();
        driver.manage().window().maximize();

        try {
            // Open the website
            driver.get("https://tpqi-net.tpqi.go.th/certified-person");

            // Wait until the accept button is clickable and click it
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            WebElement acceptButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
            acceptButton.click();
            WebElement inputElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='input_search']")));
            inputElement.sendKeys(searchValue);
            WebElement searchButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btn_search']")));
            searchButton.click();
            Thread.sleep(3000);

            JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
            jsExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight / 2);");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}
