package com.example.demo.Service;
import com.example.demo.Repository.BlogNewsRepository;
import com.example.demo.Model.BlogNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogNewsService {

    @Autowired
    private BlogNewsRepository blogNewsRepository;

    public List<BlogNews> getAllBlogNews() {
        return blogNewsRepository.findAll();
    }

    public BlogNews saveBlogNews(BlogNews blogNews) {
        return blogNewsRepository.save(blogNews);
    }

    public void deleteBlogNews(Long News_Id) {
        blogNewsRepository.deleteById(News_Id);
    }

    public BlogNews findBlogNewsByNewsId(Long News_Id) {
        return blogNewsRepository.findBlogNewsByNewsId(News_Id);
    }
}