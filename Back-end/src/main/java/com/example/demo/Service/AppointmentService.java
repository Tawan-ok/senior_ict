package com.example.demo.Service;

import com.example.demo.Model.Appointments;
import com.example.demo.Repository.AppointmentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AppointmentService {

    @Autowired
    private AppointmentsRepository appointmentsRepository;

    public List<Appointments> getAllAppointments() {
        return appointmentsRepository.findAll();
    }

    public Appointments saveAppointment(Appointments appointment) {
        return appointmentsRepository.save(appointment);
    }

    public void deleteAppointments(Long id){
         appointmentsRepository.deleteById(id);
    }

    public  Appointments getAppointmentById(long id){
        return  appointmentsRepository.findById(id).orElseThrow(() -> new NoSuchElementException("No appointments found with ID:"+ id));
    }

    public List<Appointments> getAppointmentsByCustomerID(Long customerID) {
        return appointmentsRepository.findByCustomerID(customerID);
    }

    // Method to get all appointments filtered by nannyID
    public List<Appointments> getAppointmentsByNannyID(Long nannyID) {
        return appointmentsRepository.findByNannyID(nannyID);
    }

    // If you need to filter by both customerID and nannyID, you might need to add a method for that as well
    // Assuming you've added a method in your repository called findByCustomerIDAndNannyID
    public List<Appointments> getAppointmentsByCustomerIDAndNannyID(Long customerID, Long nannyID) {
        return appointmentsRepository.findByCustomerIDAndNannyID(customerID, nannyID);
    }


    public List<Appointments> getAppointmentsByCustomerIDAndNannyIDAndStatus(Long customerID, Long nannyID, String status) {
        return appointmentsRepository.findByCustomerIDAndNannyIDAndStatus(customerID, nannyID, status);
    }

    public List<Appointments> getAppointmentsByCustomerIDAndStatus(Long customerID, String status) {
        return appointmentsRepository.findByCustomerIDAndStatus(customerID, status);
    }

    public List<Appointments> getAppointmentsByNannyIDAndStatus(Long nannyID, String status) {
        return appointmentsRepository.findByNannyIDAndStatus(nannyID, status);
    }

    public List<Appointments> getAppointmentsByStatus(String status) {
        return appointmentsRepository.findByStatus(status);
    }






}
