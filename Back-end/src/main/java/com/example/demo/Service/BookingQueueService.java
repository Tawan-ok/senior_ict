package com.example.demo.Service;
import com.example.demo.Model.BookingQueue;

import com.example.demo.Model.Nanny;
import com.example.demo.Repository.BookingQueueRepository;

import com.example.demo.Repository.NannyRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookingQueueService {

    @Autowired
    private BookingQueueRepository bookingqueuerepository;

    public List<BookingQueue> getAllBookingQueue() {
        return bookingqueuerepository.findAll();
    }

    public BookingQueue getBookingQueueById(Long id) {

        return bookingqueuerepository.findById(id).orElse(null);
    }
//    public BookingQueue getfindBookingsfromIdCustomerandNannyid(Long customer_id , Long nanny_id ){
//        return bookingqueuerepository.findBookingsfromIdCustomerandNannyid( customer_id,nanny_id );
//    }

//    public List<BookingQueue> getBookingsfromIdCustomer(Long customer_id , Long nanny_id  ){
//        return bookingqueuerepository.findBookingsfromIdCustomer( customer_id,nanny_id);
//    }
///////////////////////////////////////////////////////////
    public List<BookingQueue> getBookingsByCustomerIdAndNannyId(Long customer_id, Long nanny_id) {
        return bookingqueuerepository.findBookingsByCustomerIdAndNannyId(customer_id, nanny_id);
    }

    public List<BookingQueue> getBookingsByCustomerIdAndNannyIdStatusPaid(Long customer_id, Long nanny_id) {
        return bookingqueuerepository.findBookingsByCustomerIdAndNannyIdStatusPaid(customer_id, nanny_id);
    }

    public BookingQueue getBookingsByCustomerIdAndNannyIdStatusPending(Long customer_id, Long nanny_id) {
        return bookingqueuerepository.findBookingsByCustomerIdAndNannyIdStatusPending(customer_id, nanny_id);
    }

    public List<BookingQueue> getBookingsByCustomerIdStatusPending(Long customer_id) {
        return bookingqueuerepository.findBookingsByCustomerIdStatusPending(customer_id);
    }
    /////////////////////////////////////////////////////////
    public BookingQueue getBookingsByCustomerIdAndNannyId1(Long customer_id, Long nanny_id) {
        return bookingqueuerepository.findBookingsByCustomerIdAndNannyIdtest(customer_id, nanny_id);
    }


    public BookingQueue getBookingsByCustomerIdAndNannyId1Status(Long customer_id, Long nanny_id) {
        return bookingqueuerepository.findBookingsByCustomerIdAndNannyIdtestStatus(customer_id, nanny_id);
    }

    public List<BookingQueue>  getfindBookingsCheckdate( Long nanny_id) {
        return bookingqueuerepository.findBookingsCheckdate(nanny_id);
    }

    public List<BookingQueue>  getfindBookingsPaidCustomer( Long customer_id) {
        return bookingqueuerepository.findBookingsPaidCustomer(customer_id);
    }

    public List<BookingQueue>  getfindBookingsPaidAndCancleCustomer( Long customer_id) {
        return bookingqueuerepository.findBookingsPaidAndCancleCustomer(customer_id);
    }

    public List<BookingQueue>  getfindBookingsByCustomerID( Long customer_id) {
        return bookingqueuerepository.findBookingsByCustomerID(customer_id);
    }

    public List<BookingQueue>  getfindBookingsByCustomerIDStatusBookings( Long customer_id) {
        return bookingqueuerepository.findBookingsByCustomerIDStatusBookings(customer_id);
    }

    public BookingQueue saveBookingQueue(BookingQueue bookingqueue) {

        return bookingqueuerepository.save(bookingqueue);
    }


    public void deleteBookingQueue(Long id) {
        bookingqueuerepository.deleteById(id);
    }

    public void updateStatusByPaid(Long bookingID) {
        bookingqueuerepository.updateStatusByPaid(bookingID);
    }

    public void updateStatusByCancle(Long bookingID) {
        bookingqueuerepository.updateStatusByCancle(bookingID);
    }

    public void updateStatusByBookings(Long bookingID) {
        bookingqueuerepository.updateStatusByBooking(bookingID);
    }

    public void updateStatusByPaidSuccess(Long bookingID) {
        bookingqueuerepository.updateStatusByPaidSuccess(bookingID);
    }
    public List<String> findPaidNannyIdsByCustomerId(Long customerId) {
        return bookingqueuerepository.findPaidNannyIdsByCustomerId(customerId);
    }


    public void updateLocationForHiring(String locationhiring, Long bookingID) {
        bookingqueuerepository.updateLocationHiringByUserInput(locationhiring, bookingID);
    }

    @Transactional
    public void updateScoreByAddingBk(Long bookingID, int newScore) {
       BookingQueue bookingqueue = bookingqueuerepository.findById(bookingID).orElse(null);
        if (bookingqueue != null) {
            int currentScore = bookingqueue.getBk_score();
            int updatedScore = currentScore + newScore;
            bookingqueue.setBk_score(updatedScore);
            bookingqueuerepository.save(bookingqueue);
        }
    }

    @Transactional
    public void updateTotalAmountByAddingBk(Long bookingID, double newTotalAmount) {
        BookingQueue bookingqueue = bookingqueuerepository.findById(bookingID).orElse(null);
        if (bookingqueue != null) {

            bookingqueue.setTotal_amount(newTotalAmount);
            bookingqueuerepository.save(bookingqueue);
        }
    }

    @Transactional
    public Double getTotalAmountByMonthAndYearForNanny(Long nannyId, Integer month, Integer year) {
        return bookingqueuerepository.getTotalAmountByMonthAndYearForNanny(nannyId, month, year);
    }

}
