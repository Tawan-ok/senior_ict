package com.example.demo.Model;

public class NannyRankingDTO {
    private Nanny nanny;
    private double weightedScore;

    public NannyRankingDTO(Nanny nanny, double weightedScore) {
        this.nanny = nanny;
        this.weightedScore = weightedScore;
    }

    // Getters and Setters
    public Nanny getNanny() {
        return nanny;
    }

    public void setNanny(Nanny nanny) {
        this.nanny = nanny;
    }

    public double getWeightedScore() {
        return weightedScore;
    }

    public void setWeightedScore(double weightedScore) {
        this.weightedScore = weightedScore;
    }
}
