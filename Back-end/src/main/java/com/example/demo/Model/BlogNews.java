package com.example.demo.Model;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "blog_news")
public class BlogNews {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("News_Id")
    @Column(name = "News_Id")
    private Long News_Id;

    @JsonProperty("Topic")
    @Column(name = "Topic")
    private String Topic;

    @JsonProperty("Article")
    @Column(name = "Article")
    private String Article;

    @JsonProperty("date")
    @Column(name = "date")
    private LocalDateTime date;

    @JsonProperty("admin_id")
    @Column(name = "admin_id")
    private String admin_id;

}
