package com.example.demo.Model;

public class PreferencesDTO {
    private double reviewScoreWeight;
    private double successfulJobsWeight;

    // Getters and setters
    public double getReviewScoreWeight() {
        return reviewScoreWeight;
    }

    public void setReviewScoreWeight(double reviewScoreWeight) {
        this.reviewScoreWeight = reviewScoreWeight;
    }

    public double getSuccessfulJobsWeight() {
        return successfulJobsWeight;
    }

    public void setSuccessfulJobsWeight(double successfulJobsWeight) {
        this.successfulJobsWeight = successfulJobsWeight;
    }
}