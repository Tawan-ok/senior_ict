package com.example.demo.Model;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "complains")
public class Complains {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("complain_id")
    @Column(name = "complain_id")
    private Long complainsID;

    @JsonProperty("booking_id")
    @Column(name = "booking_id")
    private Long booking_id;

    @JsonProperty("customer_id")
    @Column(name = "customer_id")
    private Long customer_id;

    @JsonProperty("nanny_id")
    @Column(name = "nanny_id")
    private Long nanny_id;

    @JsonProperty("complain_field_1")
    @Column(name = "complain_field_1")
    private String complain_field_1;

    @JsonProperty("complain_field_2")
    @Column(name = "complain_field_2")
    private String complain_field_2;

    @JsonProperty("complain_field_3")
    @Column(name = "complain_field_3")
    private String complain_field_3;

    @JsonProperty("complain_field_4")
    @Column(name = "complain_field_4")
    private String complain_field_4;

    @JsonProperty("complain_field_5")
    @Column(name = "complain_field_5")
    private String complain_field_5;

    @JsonProperty("complain_text")
    @Column(name = "complain_text")
    private String complain_text;

    @JsonProperty("complain_count")
    @Column(name = "complain_count")
    private Integer complain_count;

    public Long getComplainsID() {
        return complainsID;
    }

    public void setComplainsID(Long complainsID) {
        this.complainsID = complainsID;
    }

    public Long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Long booking_id) {
        this.booking_id = booking_id;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public Long getNanny_id() {
        return nanny_id;
    }

    public void setNanny_id(Long nanny_id) {
        this.nanny_id = nanny_id;
    }

    public String isComplain_field_1() {
        return complain_field_1;
    }

    public void setComplain_field_1(String complain_field_1) {
        this.complain_field_1 = complain_field_1;
    }

    public String isComplain_field_2() {
        return complain_field_2;
    }

    public void setComplain_field_2(String complain_field_2) {
        this.complain_field_2 = complain_field_2;
    }

    public String isComplain_field_3() {
        return complain_field_3;
    }

    public void setComplain_field_3(String complain_field_3) {
        this.complain_field_3 = complain_field_3;
    }

    public String isComplain_field_4() {
        return complain_field_4;
    }

    public void setComplain_field_4(String complain_field_4) {
        this.complain_field_4 = complain_field_4;
    }

    public String isComplain_field_5() {
        return complain_field_5;
    }

    public void setComplain_field_5(String complain_field_5) {
        this.complain_field_5 = complain_field_5;
    }

    public String getComplain_text() {
        return complain_text;
    }

    public void setComplain_text(String complain_text) {
        this.complain_text = complain_text;
    }

    public Integer getComplain_count() {
        return complain_count;
    }

    public void setComplain_count(Integer complain_count) {
        this.complain_count = complain_count;
    }
}
