package com.example.demo.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Column;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.sql.Time;

@Entity
@Table(name = "appointments")
public class Appointments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonProperty("id")
    private Long appointmentsID;


    @Column(name = "title")
    @JsonProperty("title")
    private String title;
    @Column(name = "customer_id")
    @JsonProperty("customer_id")
    private Long customerID;

    @Column(name = "nanny_id")
    @JsonProperty("nanny_id")
    private Long nannyID;

    @Column(name = "start_time")
    @JsonProperty("start_time")
    private Date start_time;

    @Column(name = "end_time")
    @JsonProperty("end_time")
    private Date end_time;

    @Column(name = "interview_details")
    @JsonProperty("interview_details")
    private String interview_details;

    @Column(name = "status")
    @JsonProperty("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInterview_details() {
        return interview_details;
    }

    public void setInterview_details(String interview_details) {
        this.interview_details = interview_details;
    }

    public String getInterviewDetails() {
        return interview_details;
    }

    public void setInterviewDetails(String interviewDetails) {
        this.interview_details = interviewDetails;
    }

    // Getters and setters

    public Long getAppointmentsID() {
        return appointmentsID;
    }

    public void setAppointmentsID(Long appointmentsID) {
        this.appointmentsID = appointmentsID;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getNannyID() {
        return nannyID;
    }

    public void setNannyID(Long nannyID) {
        this.nannyID = nannyID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }
}
