package com.example.demo.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
@Entity
@Table(name = "notifications")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "recipient_id")
    @JsonProperty("recipient_id")
    private Long recipientId;
    @Column(name = "sender_id")
    @JsonProperty("sender_id")
    private Long senderId;
    @Column(name = "recipient_type")
    @JsonProperty("recipient_type")
    private String recipientType;  // Type of the recipient (e.g., "NANNY", "CUSTOMER")

    @Column(name = "message")
    @JsonProperty("message")
    private String message;

    @Column(name = "status")
    @JsonProperty("status")
    private String status;

    @Column(name = "appointment_id")
    @JsonProperty("appointment_id")
    private Long appointmentId;

    // Getters and setters


    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }
}
//    CREATE TABLE notifications (
//        id BIGINT AUTO_INCREMENT PRIMARY KEY,
//        recipient_id BIGINT NOT NULL,
//        recipient_type VARCHAR(255) NOT NULL,
//    appointment_id BIGINT,
//    message TEXT NOT NULL,
//    status VARCHAR(255) NOT NULL,
//    FOREIGN KEY (appointment_id) REFERENCES appointments(id)
//        );

