package com.example.demo.Repository;

import com.example.demo.Model.Appointments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppointmentsRepository extends JpaRepository<Appointments,Long> {
    List<Appointments> findByCustomerID(Long customerID);
    List<Appointments> findByNannyID(Long nannyID);
    List<Appointments> findByCustomerIDAndNannyID(Long customerID, Long nannyID);

    List<Appointments> findByCustomerIDAndNannyIDAndStatus(Long customerID, Long nannyID, String status);

    List<Appointments> findByCustomerIDAndStatus(Long customerID, String status);

    List<Appointments> findByNannyIDAndStatus(Long nannyID, String status);

    List<Appointments> findByStatus(String status);

}
