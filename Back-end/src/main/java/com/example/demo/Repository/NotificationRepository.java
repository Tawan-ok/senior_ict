package com.example.demo.Repository;

import com.example.demo.Model.Nanny;
import com.example.demo.Model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
    List<Notification> findByRecipientId(Long recipientId);

    List<Notification> findByRecipientIdAndRecipientType(Long recipientId, String recipientType);
    List<Notification> findByRecipientIdAndRecipientTypeAndStatus(Long recipientId, String recipientType, String status);

    List<Notification> findBySenderId(Long senderId);
    List<Notification> findBySenderIdAndStatus(Long senderId, String status);

    List<Notification> findByRecipientIdAndStatusNot(Long recipientId, String status);

}
