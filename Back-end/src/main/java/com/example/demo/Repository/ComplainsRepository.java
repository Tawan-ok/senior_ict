package com.example.demo.Repository;


import com.example.demo.Model.BookingQueue;
import com.example.demo.Model.Complains;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ComplainsRepository extends JpaRepository<Complains, Long> {
    @Query("SELECT n FROM Complains n WHERE n.nanny_id = :nanny_id  ")
    List<Complains> findbyComplainNannyId(@Param("nanny_id") Long nanny_id);

    @Query("SELECT n FROM Complains n WHERE n.booking_id = :booking_id  ")
    Complains findbyComplainBookingId(@Param("booking_id") Long booking_id);
}
