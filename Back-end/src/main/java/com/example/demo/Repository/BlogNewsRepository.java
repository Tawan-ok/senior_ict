package com.example.demo.Repository;
import com.example.demo.Model.BlogNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BlogNewsRepository extends JpaRepository<BlogNews, Long>{
    @Query("SELECT b FROM BlogNews b WHERE b.News_Id = ?1")
    BlogNews findBlogNewsByNewsId(Long News_Id);
}