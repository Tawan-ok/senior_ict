package com.example.demo.Controller;

import com.example.demo.Model.BookingQueue;
import com.example.demo.Model.Complains;
import com.example.demo.Model.Nanny;
import com.example.demo.Service.ComplainsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/complains")
//@CrossOrigin("http://35.213.136.159:3000")
 @CrossOrigin(origins = "http://localhost:3000") // Allow requests from localhost:3000
public class ComplainsController {

    @Autowired
    ComplainsService complainsService;

    @GetMapping
    public List<Complains> getAllComplains() {
        return complainsService.getAllComplains();
    }


    @GetMapping("/getby/{id}")
    public Complains getComplainsById(@PathVariable Long id) {
        return complainsService.getComplainsById(id);
    }

    @PostMapping
    public Complains createComplains(@RequestBody Complains complains) {
        return complainsService.saveComplains(complains);
    }

    @PutMapping("/{id}")
    public Complains updateComplains(@PathVariable Long id, @RequestBody Complains complains) {
        // You should add logic to handle the update.
        // For simplicity, this example directly saves the received object.
        return complainsService.saveComplains(complains);
    }

    @DeleteMapping("/{id}")
    public void deleteComplains(@PathVariable Long id) {
        complainsService.deleteComplains(id);
    }

    @GetMapping("/getcomplainsbyNannyId/{nanny_id}")
    public List<Complains> getComplainByNannyId(
            @PathVariable("nanny_id") Long nanny_id) {
        return complainsService.getComplainByNannyId( nanny_id);
    }

    @GetMapping("/getcomplainsbyBookingId/{booking_id}")
    public Complains getComplainByBookingId(
            @PathVariable("booking_id") Long booking_id) {
        return complainsService.getComplainByBookingId( booking_id);
    }
}
