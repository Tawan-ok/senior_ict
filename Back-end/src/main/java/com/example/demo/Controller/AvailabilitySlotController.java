package com.example.demo.Controller;

import com.example.demo.Model.AvailabilitySlot;
import com.example.demo.Service.AvailabilitySlotService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
//@CrossOrigin("http://35.213.136.159:3000")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/availability")
public class AvailabilitySlotController {

    @Autowired
    private AvailabilitySlotService service;

    @PostMapping
    public AvailabilitySlot createSlot(@RequestBody AvailabilitySlot slot) {
        return service.createSlot(slot);
    }

    @GetMapping("/nanny/{nannyId}")
    public List<AvailabilitySlot> getSlotsByNanny(@PathVariable Long nannyId) {
        return service.getSlotsByNannyId(nannyId);
    }

    @GetMapping
    public List<AvailabilitySlot> getAllSlots() {
        return service.getAllSlots();
    }

    @GetMapping("/search")
    public ResponseEntity<List<AvailabilitySlot>> searchAvailableNannies(
            @RequestParam("start_time") String startTimeStr,
            @RequestParam("end_time") String endTimeStr) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
            Date startTime = dateFormat.parse(startTimeStr);
            Date endTime = dateFormat.parse(endTimeStr);
            List<AvailabilitySlot> availableSlots = service.findAvailableSlots(startTime, endTime);
            return ResponseEntity.ok(availableSlots);
        } catch (ParseException e) {
            return ResponseEntity.badRequest().build(); // Or handle more gracefully
        }
    }

}