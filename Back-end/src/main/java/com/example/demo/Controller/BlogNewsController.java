package com.example.demo.Controller;
import com.example.demo.Model.BlogNews;
import com.example.demo.Service.BlogNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/blognews2")
//@CrossOrigin("http://35.213.136.159:3000")
@CrossOrigin(origins = "http://localhost:3000")
public class BlogNewsController {

    @Autowired
    BlogNewsService blogNewsService;

    @GetMapping
    public List<BlogNews> getAllBlogNews() {
        return blogNewsService.getAllBlogNews();
    }

    @GetMapping("/{News_Id}")
    public BlogNews findBlogNewsByNewsId(@PathVariable Long News_Id) {
        return blogNewsService.findBlogNewsByNewsId(News_Id);
    }

    @DeleteMapping("/{News_Id}")
    public void deleteBlogNews(@PathVariable Long News_Id) {
        blogNewsService.deleteBlogNews(News_Id);
    }

    @PutMapping("/{News_Id}")
    public BlogNews updateBlogNews(@PathVariable Long News_Id, @RequestBody BlogNews blogNews){
        return blogNewsService.saveBlogNews(blogNews);
    }

    @PostMapping
    public BlogNews createBlogNews(@RequestBody BlogNews blogNews) {
        return blogNewsService.saveBlogNews(blogNews);
    }



}