package com.example.demo.Controller;
import com.example.demo.Model.*;

import com.example.demo.Repository.BookingHistoryRepository;
import com.example.demo.Repository.NannyRepository;
import com.example.demo.Service.BookingQueueService;
import com.example.demo.Service.NannyRankingService;
import com.example.demo.Service.NannyService;
import com.example.demo.Service.WebAutomationService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/nannies")
//@CrossOrigin("http://35.213.136.159:3000")
@CrossOrigin(origins = "http://localhost:3000") // Allow requests from localhost:3000

public class NannyController {

    @Autowired
    NannyService nannyService;

    @Autowired
    WebAutomationService webAutomationService;

    @Autowired
    private NannyRankingService nannyRankingService;

    @Autowired
    private BookingQueueService bookingqueueService;

    @Autowired
    private NannyRepository nannyRepository;

    @Autowired
    private BookingHistoryRepository bookingHistoryRepository;

    @GetMapping
    public List<Nanny> getAllNannies() {
        return nannyService.getAllNannies();
    }


    @GetMapping("/getby/{id}")
    public Nanny getNannyById(@PathVariable Long id) {
        return nannyService.getNanniesById(id);
    }

    @GetMapping("/getbyranking")
    public List<Nanny> getNannyRankingByScore() {
        return nannyService.findNannyRankingByScore();
    }

    @PostMapping("/register")
    public Nanny createNanny(@RequestBody Nanny nanny) {
        return nannyService.saveNanny(nanny);
    }

    @PutMapping("/{id}")
    public Nanny updateNanny(@PathVariable Long id, @RequestBody Nanny nanny) {
        // You should add logic to handle the update.
        // For simplicity, this example directly saves the received object.
        return nannyService.saveNanny(nanny);
    }

    @DeleteMapping("/{id}")
    public void deleteNanny(@PathVariable Long id) {
        nannyService.deleteNanny(id);
    }

    // Add a new endpoint to update the status to "Inactive"
    @PutMapping("/updateStatus/{username}")
    public ResponseEntity<String> updateNannyStatusToInactive(@PathVariable String username) {
        try {
            nannyService.updateNannyStatusToInactive(username);
            return ResponseEntity.ok("Nanny status updated successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating nanny status");
        }
    }

    @GetMapping("/getbyusername/{username}")
    public Nanny findNannyByUsername(@PathVariable String username) {
        return nannyService.findNannyByUsername(username);
    }

    @PutMapping("/updateScore/{nannyId}")
    public ResponseEntity<String> updateNannyScore(@PathVariable Long nannyId, @RequestParam double newScore) {
        try {
            nannyService.updateScoreByAdding(nannyId, newScore);
            return ResponseEntity.ok("Nanny score updated successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating nanny score");
        }
    }

    @GetMapping("/search")
    public List<Nanny> searchNanniesByFullName(@RequestParam String keyword) {
        return nannyService.findByFullNameContaining(keyword);
    }

    @GetMapping("/searchtest")
    public List<Nanny> searchNanniesByCriteria(
            @RequestParam(required = false) String keyword,
            @RequestParam(required = false)  List<String> role_level,
            @RequestParam(required = false) String type_work) {
        if (keyword != null && role_level != null) {
            return nannyService.findByFullNameContainingAndRoleLevel(keyword, role_level);
        } else if (keyword != null) {
            return nannyService.findByFullNameContaining(keyword);
        } else if (role_level != null) {
            return nannyService.findByRoleLevel(role_level);
        } else if (type_work != null) {
            return nannyService.findNanniesByTypeOfWork(type_work);
        } else {
            return Collections.emptyList(); // Return an empty list when no criteria are provided.
        }
    }

    @GetMapping("/searchtestall")
    public List<Nanny> searchNanniesByCriteriaall(
            @RequestParam(required = false) String keyword,
            @RequestParam(required = false)  List<String> role_level,
            @RequestParam(required = false) String type_work,
            @RequestParam(required = false) String gender,
            @RequestParam(required = false) String district,
            @RequestParam(required = false) List<String> allskill) {

        if (keyword != null && role_level != null && type_work != null && gender != null && district != null && allskill != null) {
            return nannyService.findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndDistrictAndAllskill(keyword, role_level, type_work, gender, district, allskill);
        } else if (keyword != null && role_level != null && type_work != null && gender != null && district != null) {
            return nannyService.findByGenderAndTypeOfWorkAndRoleLevelAndFullNameContaining(keyword, role_level, type_work, gender);
        } else if (keyword != null && role_level != null && type_work != null && district != null && allskill != null) {
            return nannyService.findByRoleLevelAndTypeOfWorkAndFullNameContainingAndDistrictAndAllskill(keyword, role_level, type_work, district, allskill);
        } else if (keyword != null && role_level != null && type_work != null && district != null) {
            return nannyService.findByRoleLevelAndTypeOfWorkAndFullNameContainingAndDistrict(keyword, role_level, type_work, district);
        } else if (keyword != null && role_level != null && gender != null && district != null && allskill != null) {
            return nannyService.findByGenderAndRoleLevelAndFullNameContainingAndDistrictAndAllskill(keyword, role_level, gender, district, allskill);
        } else if (keyword != null && role_level != null && gender != null && district != null) {
            return nannyService.findByGenderAndRoleLevelAndFullNameContainingAndDistrict(keyword, role_level, gender, district);
        } else if (keyword != null && type_work != null && gender != null && district != null && allskill != null) {
            return nannyService.findByGenderAndTypeOfWorkAndFullNameContainingAndDistrictAndAllskill(keyword, type_work, gender, district, allskill);
        } else if (keyword != null && type_work != null && gender != null && district != null) {
            return nannyService.findByGenderAndTypeOfWorkAndFullNameContainingAndDistrict(keyword, type_work, gender, district);
        } else if (keyword != null && role_level != null && type_work != null ) {
            return nannyService.findByFullNameContainingAndRoleLevelAndType(keyword,role_level, type_work);
        } else if (keyword != null && district != null && allskill != null) {
            return nannyService.findByFullNameContainingAndDistrictAndAllskill(keyword, district , allskill);
        } else if (keyword != null && district != null) {
            return nannyService.findByFullNameContainingAndDistrict(keyword, district);
        } else if (role_level != null && district != null && allskill != null) {
            return nannyService.findByRoleLevelAndDistrictAllskill(role_level, district, allskill);
        } else if (role_level != null && district != null) {
            return nannyService.findByRoleLevelAndDistrict(role_level, district);
        } else if (type_work != null && district != null&& allskill != null) {
            return nannyService.findByTypeOfWorkAndDistrictAndAllskill(type_work, district,allskill);
        } else if (type_work != null && district != null) {
            return nannyService.findByTypeOfWorkAndDistrict(type_work, district);
        } else if (gender != null && district != null && allskill != null) {
            return nannyService.findByGenderAndDistrictAndAllskill(gender, district,allskill);
        } else if (gender != null && district != null) {
            return nannyService.findByGenderAndDistrict(gender, district);
        } else if (keyword != null && role_level != null && type_work != null && gender != null && allskill != null ) {
            return nannyService.findByTypeOfWorkAndRoleLevelAndFullNameContainingAndGenderAndAllskill(keyword, role_level, type_work, gender,allskill);
        } else if (keyword != null && role_level != null && type_work != null) {
            return nannyService.findByGenderAndTypeOfWorkAndRoleLevelAndFullNameContaining(keyword, role_level, type_work, gender);
        } else if (keyword != null && role_level != null && gender != null && allskill != null) {
            return nannyService.findByRoleLevelAndFullNameContainingAndGenderAndAllskill(keyword, role_level, gender,allskill);
        } else if (keyword != null && role_level != null && gender != null) {
            return nannyService.findByGenderAndRoleLevelAndFullNameContaining(keyword, role_level, gender);
        } else if (keyword != null && type_work != null && gender != null && allskill != null) {
            return nannyService.findByTypeOfWorkAndFullNameContainingAndGenderAndAllskill(keyword, type_work, gender,allskill);
        } else if (keyword != null && type_work != null && gender != null) {
            return nannyService.findsByGenderAndTypeOfWorkAndFullNameContaining(keyword, type_work, gender);
        } else if (keyword != null && role_level != null && allskill != null) {
            return nannyService.findByFullNameContainingAndRoleLevelAndAllskill(keyword, role_level,allskill);
        } else if (keyword != null && role_level != null) {
            return nannyService.findByFullNameContainingAndRoleLevel(keyword, role_level);
        } else if (keyword != null && type_work != null && allskill != null) {
            return nannyService.findByFullNameContainingAndTypeOfWorkAndAllskill(keyword, type_work,allskill);
        } else if (keyword != null && type_work != null) {
            return nannyService.findByFullNameContainingAndTypeOfWork(keyword, type_work);
        } else if (keyword != null && gender != null && allskill != null) {
            return nannyService.findByFullNameContainingAndGenderAllskill(keyword, gender,allskill);
        } else if (keyword != null && gender != null) {
            return nannyService.findByGenderAndFullNameContaining(keyword, gender);
        } else if (role_level != null && type_work != null && allskill != null) {
            return nannyService.findByRoleLevelAndTypeOfWorkAndAllskill(role_level, type_work,allskill);
        } else if (role_level != null && type_work != null) {
            return nannyService.findByRoleLevelAndTypeOfWork(role_level, type_work);
        } else if (role_level != null && gender != null && allskill != null) {
            return nannyService.findByRoleLevelAndGenderAndAllskill(role_level, gender,allskill);
        } else if (role_level != null && gender != null) {
            return nannyService.findByGenderAndRoleLevel(role_level, gender);
        } else if (type_work != null && gender != null && allskill != null) {
            return nannyService.findByTypeOfWorkAndGenderAllskill(type_work, gender,allskill);
        } else if (type_work != null && gender != null) {
            return nannyService.findByGenderAndTypeOfWork(type_work, gender);
        } else if (keyword != null && allskill != null) {
            return nannyService.findByFullNameContainingAndAllskill(keyword,allskill);
        } else if (role_level != null && allskill != null) {
            return nannyService.findByRoleLevelAndAllskill(role_level,allskill);
        } else if (type_work != null && allskill != null) {
            return nannyService.findByTypeOfWorkAndAllskill(type_work,allskill);
        } else if (gender != null && allskill != null) {
            return nannyService.findByGenderAndAllskill(gender,allskill);
        } else if (district != null && allskill != null) {
            return nannyService.findByDistrictAndAllskill(district,allskill);
        } else if (keyword != null ) {
            return nannyService.findByFullNameContaining(keyword);
        } else if (role_level != null) {
            return nannyService.findByRoleLevel(role_level);
        } else if (type_work != null) {
            return nannyService.findNanniesByTypeOfWork(type_work);
        } else if (gender != null) {
            return nannyService.findByGender(gender);
        } else if (district != null) {
            return nannyService.findNanniesByDistrict(district);
        } else if (allskill != null) {
            return nannyService.findNanniesByAllSkill(allskill);
        } else {
            return Collections.emptyList(); // Return an empty list when no criteria are provided.
        }
    }


    @GetMapping("/searchallskill")
    public List<Nanny> searchNanniesByAllskill(@RequestParam List<String> allskill) {
        return nannyService.findNanniesByAllSkill(allskill);
    }


    @GetMapping("/searchgender/{gender}")
    public List<Nanny> searchNanniesByGender(@PathVariable String gender) {
        return nannyService.findByGender(gender);
    }

    @GetMapping("/searchgenderss/{gender}/{allskill}")
    public List<Nanny> searchNanniesByGenderasd(@PathVariable String gender , @PathVariable List<String> allskill) {
        return  nannyService.findByGenderAndAllskill(gender,allskill);

    }

    @PutMapping("/updatebyAdmin/{nannyId}")
    public Nanny updateActivityProgram(@PathVariable Long nannyId, @RequestBody Nanny nanny) {
        return nannyService.saveNannyByAdmin(nanny);
    }

    //    @GetMapping("/booking-history")
//    public List[] getBookingHistoryForNanny(@RequestParam Long nannyId) {
//        return nannyService.getBookingHistoryByNannyId(nannyId);
//    }
//
//    @GetMapping("/bookingtest/{nannyId}")
//    public List[] getBookingHistoryForNannytest(@PathVariable Long nannyId) {
//        return nannyService.getBookingHistoryByNannyId(nannyId);
//    }
    @GetMapping("/booking-dataBH/{nannyId}")
    public List<BookingHistory> getBookingDataByNannyIdBH(@PathVariable Long nannyId) {
        return nannyService.getBookingDataByNannyIdBH(nannyId);
    }

    @GetMapping("/booking-dataBQ/{nannyId}")
    public List<BookingQueue> getBookingDataByNannyIdBQ(@PathVariable Long nannyId) {
        return nannyService.getBookingDataByNannyIdBQ(nannyId);
    }

    @GetMapping("/booking-dataBHComplete/{nannyId}")
    public List<BookingHistory> getBookingDataByNannyIdBHComplete(@PathVariable Long nannyId) {
        return nannyService.getBookingDataByNannyIdBHCompleted(nannyId);
    }

    @GetMapping("/booking-dataBQSuccess/{nannyId}")
    public List<BookingQueue> getBookingDataByNannyIdBQSuccess(@PathVariable Long nannyId) {
        return nannyService.getBookingDataByNannyIdBQSuccess(nannyId);
    }

    @GetMapping("/booking-queues/pending/{nannyId}")
    public List<BookingQueue> getPendingBookingQueuesByNannyId(@PathVariable Long nannyId) {
        return nannyService.findPendingBQByNannyID(nannyId);
    }

    @GetMapping("/bookings/byNannyId/{nannyId}")
    public List<BookingQueue> findBQByNannyIDStatusBookings(@PathVariable Long nannyId) {
        return nannyService.findBQByNannyIDStatusBookings(nannyId);
    }

    @GetMapping("/perform-web-automation")
    public ResponseEntity<String> performWebAutomation(@RequestParam String searchValue) {
        try {
            webAutomationService.performWebAutomation(searchValue);
            return ResponseEntity.ok("Web automation completed successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error during web automation");
        }
    }

    @PutMapping("/{nannyId}/uploadProfileImage")
    public ResponseEntity<?> uploadProfileImage(
            @PathVariable Long nannyId,
            @RequestParam("profileImage") MultipartFile file) {

        try {
            Nanny img_profile = nannyService.uploadProfileImage(nannyId, file);

            return ResponseEntity.ok(img_profile);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(404).body("Customer not found with ID: " + nannyId);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Error uploading profile image: " + e.getMessage());
        }
    }

    @GetMapping("/ranking/{customerId}")
    public ResponseEntity<List<NannyRankingDTO>> getNannyRanking(@PathVariable Long customerId) {
        List<NannyRankingDTO> rankedNannies = nannyRankingService.getRankedNannies(customerId);

        for (NannyRankingDTO nannyRankingDTO : rankedNannies) {
            double weightscore = nannyRankingDTO.getWeightedScore();
            Long nannyId = nannyRankingDTO.getNanny().getNannyId();
            System.out.println("Id: " + nannyId + " " + "Weight Score: " + weightscore);
        }
        System.out.println("Test list: " + rankedNannies);
        return ResponseEntity.ok(rankedNannies);
    }

    @GetMapping("/ranked/hour/{customerId}")
    public List<NannyRankingDTO> getRankedNanniesHour(@PathVariable Long customerId) {
        return nannyRankingService.getRankedNanniesHour(customerId);
    }

    @GetMapping("/ranked/day/{customerId}")
    public List<NannyRankingDTO> getRankedNanniesDay(@PathVariable Long customerId) {
        return nannyRankingService.getRankedNanniesDay(customerId);
    }

    @GetMapping("/ranked/month/{customerId}")
    public List<NannyRankingDTO> getRankedNanniesMonth(@PathVariable Long customerId) {
        return nannyRankingService.getRankedNanniesMonth(customerId);
    }

    @GetMapping("/with-availability-slots")
    public List<Optional> getNanniesWithAvailabilitySlots() {
        return nannyService.findNanniesWithAvailabilitySlots();
    }



    @GetMapping("/totalAmount/{nannyId}/{month}/{year}")
    public ResponseEntity<Double> getTotalAmountByMonthAndYearForNanny(
            @PathVariable Long nannyId,
            @PathVariable Integer month,
            @PathVariable Integer year) {
        try {
            Double totalAmount = bookingqueueService.getTotalAmountByMonthAndYearForNanny(nannyId, month, year);
            return ResponseEntity.ok(totalAmount);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}