package com.example.demo.Controller;

import com.example.demo.Model.Appointments;
import com.example.demo.Model.Notification;
import com.example.demo.Service.AppointmentService;
import com.example.demo.Service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/notifications")
//@CrossOrigin("http://35.213.136.159:3000")
@CrossOrigin("http://localhost:3000")
public class NotificationController {


    @Autowired
    private NotificationService notificationService;

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping
    public ResponseEntity<Notification> createNotification(@RequestBody Notification notification) {
        Notification createdNotification = notificationService.createNotification(notification);
        return new ResponseEntity<>(createdNotification, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Notification>> getNotificationsForRecipient(@RequestParam Long recipient_id, @RequestParam String recipient_type) {
        List<Notification> notifications = notificationService.getNotificationsForRecipient(recipient_id, recipient_type);
        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Notification> updateNotificationStatus(@PathVariable Long id, @RequestBody Notification notification) {
        Notification updatedNotification = notificationService.updateNotificationStatus(id, notification.getStatus());
        return new ResponseEntity<>(updatedNotification, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNotification(@PathVariable Long id) {
        notificationService.deleteNotification(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/for-nanny/{nannyId}")
    public ResponseEntity<List<Notification>> getNotificationsForNanny(
            @PathVariable Long nannyId,
            @RequestParam(required = false) String status
    ) {
        List<Notification> notifications;
        if (status != null) {
            notifications = notificationService.getNotificationsForNannyAndStatus(nannyId, status);
        } else {
            notifications = notificationService.getNotificationsForNanny(nannyId);
        }
        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<Notification> updateNotificationAction(@PathVariable Long id, @RequestBody Map<String, String> updateBody) {
        String action = updateBody.get("action"); // Assuming the body contains an 'action' key
        Notification updatedNotification = notificationService.handleNotificationAction(id, action);

        if (updatedNotification != null) {
            // Retrieve the appointment linked to this notification
            Appointments appointment = appointmentService.getAppointmentById(updatedNotification.getAppointmentId());

            if (appointment != null) {
                // Update the appointment status based on the action taken on the notification
                if ("confirm".equals(action)) {
                    appointment.setStatus("confirmed");
                } else if ("deny".equals(action)) {
                    appointment.setStatus("denied");
                }
                // Save the updated appointment
                appointmentService.saveAppointment(appointment);

                return new ResponseEntity<>(updatedNotification, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    // Endpoint to mark notification as read/unread

    @GetMapping("/for-user/{userId}")
    public ResponseEntity<List<Notification>> getNotificationsForUser(
            @PathVariable Long userId,
            @RequestParam(required = false) String status) {

        List<Notification> notifications;

        if (status != null) {
            // Assuming you have a method in your service to filter by userId and status
            notifications = notificationService.getNotificationsByUserIdAndStatus(userId, status);
        } else {
            // Your existing method to get all notifications for a user
            notifications = notificationService.getNotificationsBySenderId(userId);
        }

        // Always return OK with the list (which may be empty)
        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }

    @PutMapping("/{id}/close")
    public ResponseEntity<Notification> closeNotification(@PathVariable Long id) {
        Notification closedNotification = notificationService.closeNotification(id);
        if (closedNotification != null) {
            return new ResponseEntity<>(closedNotification, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}