package com.example.demo.Controller;

import com.example.demo.Model.Appointments;
import com.example.demo.Model.Notification;
import com.example.demo.Service.AppointmentService;
import com.example.demo.Service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin("http://35.213.136.159:3000")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/appointments")
public class AppointmentController {

    @Autowired
    AppointmentService appointmentService;

    @Autowired
    NotificationService notificationService;

    @GetMapping
    public ResponseEntity<List<Appointments>> getAllAppointment(
            @RequestParam(required = false) Long customer_id,
            @RequestParam(required = false) Long nanny_id,
            @RequestParam(required = false) String status) {

        List<Appointments> appointments;

        if (customer_id != null && nanny_id != null) {
            appointments = appointmentService.getAppointmentsByCustomerIDAndNannyIDAndStatus(customer_id, nanny_id, status);
        } else if (customer_id != null) {
            appointments = appointmentService.getAppointmentsByCustomerIDAndStatus(customer_id, status);
        } else if (nanny_id != null) {
            appointments = appointmentService.getAppointmentsByNannyIDAndStatus(nanny_id, status);
        } else {
            appointments = appointmentService.getAppointmentsByStatus(status);
        }

        return new ResponseEntity<>(appointments, HttpStatus.OK);
//        http://localhost:9000/api/appointments?customer_id=31&nanny_id=15&status=confirmed   test api

    }



//    @PostMapping
//    public Appointments createAppointment(@RequestBody Appointments appointment) {
//        return appointmentService.saveAppointment(appointment);
//    }

    @PostMapping
    public ResponseEntity<Appointments> createAppointment(@RequestBody Appointments appointment) {
//        appointment.setStatus("pending");
        Appointments savedAppointment = appointmentService.saveAppointment(appointment);

        // Create notification for the nanny
        Notification notification = new Notification();
        notification.setRecipientId(appointment.getNannyID());
        notification.setSenderId(appointment.getCustomerID());
        notification.setMessage("You have a new appointment request from customer " + appointment.getCustomerID());
        notification.setStatus("pending");
        notification.setRecipientType("NANNY");
        notification.setAppointmentId(savedAppointment.getAppointmentsID());
        notificationService.createNotification(notification);

        return new ResponseEntity<>(savedAppointment, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<Appointments> updateAppointmentStatus(@PathVariable Long id, @RequestBody String status) {
        Appointments appointment = appointmentService.getAppointmentById(id);

        if (appointment != null) {
            appointment.setStatus(status);
            Appointments updatedAppointment = appointmentService.saveAppointment(appointment);

            // Create notification for the customer based on the nanny's response
            String message = status.equals("confirmed") ?
                    "Your appointment with nanny " + appointment.getNannyID() + " has been confirmed." :
                    "Your appointment with nanny " + appointment.getNannyID() + " has been denied.";

            Notification notification = new Notification();
            notification.setRecipientId(appointment.getCustomerID());
            notification.setMessage(message);
            notification.setStatus("unread");
            notification.setAppointmentId(appointment.getAppointmentsID());
            notificationService.createNotification(notification);

            return new ResponseEntity<>(updatedAppointment, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<Appointments> getAppointmentById(@PathVariable Long id) {
        Appointments appointment = appointmentService.getAppointmentById(id);
        if (appointment != null) {
            return new ResponseEntity<>(appointment, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public  void  deleteAppointment(@PathVariable Long id){
        appointmentService.deleteAppointments(id);
    }


    @GetMapping("/customers")
    public ResponseEntity<List<Appointments>> getAppointmentsByCustomerAndStatus(
            @RequestParam Long customerId,
            @RequestParam String status) {
        List<Appointments> appointments = appointmentService.getAppointmentsByCustomerIDAndStatus(customerId, status);
        if (appointments.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(appointments);
        }
    }



}
