package com.example.demo;

import com.example.demo.Model.Customer;
import com.example.demo.Model.Nanny;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import java.time.Duration;
@SpringBootTest
class DemoApplicationTests {
    static Nanny nanny;
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\USER\\Downloads\\New folder\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        // Open the website
        driver.get("https://tpqi-net.tpqi.go.th/certified-person");

        // Find and click the button
        int seconds = 5;
        Duration duration = Duration.ofSeconds(seconds);

        // Now 'duration' represents a duration of 5 seconds

        WebDriverWait wait = new WebDriverWait(driver, duration);
        WebElement acceptButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
        acceptButton.click();

        WebElement inputElement = driver.findElement(By.xpath("//*[@id='input_search']"));

        // Input text into the found element
//        String key = nanny.getRole()+"123456789132";
//        System.out.println(key);
        inputElement.sendKeys("1234567891327");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id='btn_search']"));
        searchButton.click();

        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight/2);");
//        driver.quit();
    }

}
